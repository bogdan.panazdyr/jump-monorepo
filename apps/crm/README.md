# Личный кабинет Jump Taxi v2

- Сайт [https://my.jump.taxi](https://my.jump.taxi)
- Develop [http://my.taxi.test](https://my.jump.taxi)

## Доступные скрипты

В проектной директории, вы можете запустить:

### `yarn start`

Запустит приложение в режиме разработки.<br />
Откройте [http://localhost:3000](http://localhost:3000) для просмотра в браузере.

### `yarn test`

Запускает тесты в интерактивном режиме.<br />
[Подробнее о запуске тестов](https://facebook.github.io/create-react-app/docs/running-tests).

### `yarn build`

Собирает приложение для production в `build` директории.<br />
