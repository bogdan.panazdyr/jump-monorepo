# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.3.1](https://gitlab.com/bogdan.panazdyr/jump-monorepo/compare/crm@0.3.0...crm@0.3.1) (2021-09-19)

**Note:** Version bump only for package crm





# [0.3.0](https://gitlab.com/bogdan.panazdyr/jump-monorepo/compare/crm@0.1.3...crm@0.3.0) (2021-09-19)


### Features

* resolve DEV-2682 ci ([dc6fe3c](https://gitlab.com/bogdan.panazdyr/jump-monorepo/commit/dc6fe3ca9b60a82302d30f31d79ad33e7ddca143))
* resolve DEV-2682 ci ([6aea7cd](https://gitlab.com/bogdan.panazdyr/jump-monorepo/commit/6aea7cd59d72d024543bb27c43cb431d5f4b38f6))





# [0.2.0](https://gitlab.com/bogdan.panazdyr/jump-monorepo/compare/crm@0.1.3...crm@0.2.0) (2021-09-19)


### Features

* resolve DEV-2682 ci ([6aea7cd](https://gitlab.com/bogdan.panazdyr/jump-monorepo/commit/6aea7cd59d72d024543bb27c43cb431d5f4b38f6))





## [0.1.4](https://gitlab.com/bogdan.panazdyr/jump-monorepo/compare/crm@0.1.3...crm@0.1.4) (2021-09-19)

**Note:** Version bump only for package crm
