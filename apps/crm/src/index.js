import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import * as Sentry from '@sentry/react';
import * as ReactQuery from 'react-query';
import { ReactQueryDevtools } from 'react-query/devtools'

import { Config } from '@justlook/core';

import { history, Provider, store } from 'app/store';
import { logout } from 'app/auth/store/actions';
import { MenuProvider } from 'components/menu';
import { JustDevTools } from 'components';
import Application from 'app';
import 'components/index.scss';

Sentry.init({
    dsn: Config.sentry.dsn,
    release: window.taxi_release_id || null,
});

const queryClient = new ReactQuery.QueryClient({
    defaultOptions: {
        queries: {
            refetchOnWindowFocus: Config.refetchOnWindowFocus,
        },
    },
})

const render = () => {
    ReactDOM.render(
        <BrowserRouter>
            <Provider store={store}>
                <ReactQuery.QueryClientProvider client={queryClient}>
                    <MenuProvider logoutAction={logout} brandName={Config.brandName}>
                        <Application history={history}/>
                        <ReactQueryDevtools position={'bottom-right'} />
                        <JustDevTools />
                    </MenuProvider>
                </ReactQuery.QueryClientProvider>
            </Provider>
        </BrowserRouter>,
        document.getElementById('root'),
    );
};

render();

if (module.hot) {
    module.hot.accept();
}
