import { useQuery, useInfiniteQuery } from 'react-query';
import { useMemo } from 'react';

import { joinInfinityPages } from 'utils/join-infinity-pages';
import Api from 'api/client';
import { CACHE_REPORT_TRIPS_DRIVERS, CACHE_REPORT_TRIPS_INTEGRATIONS } from 'domains/common/cache-keys';

export const useDriver = (filter) => {
    const { data, isLoading, isFetching, fetchNextPage, hasNextPage, isFetchingNextPage } = useInfiniteQuery(
        [CACHE_REPORT_TRIPS_DRIVERS, filter],
        ({ pageParam = 1 }) => Api.reports.fetch(`trips/${filter.integration_id}`, {...filter, per_page: 50, page: pageParam}),
        {
            staleTime: 300000,
            getNextPageParam: (lastPage, _) => {
                if (lastPage.meta.current_page < lastPage.meta.last_page) {
                    return lastPage.meta.current_page + 1
                }

                return false;
            }
        }
    );

    return {
        drivers: joinInfinityPages(data?.pages),
        fetchNextPage,
        hasNextPage,
        isLoading: isLoading || isFetchingNextPage,
        isFetching,
    }
}

export const useIntegrations = (filter) => {
    const { data, isLoading, isFetching } = useQuery(
        [CACHE_REPORT_TRIPS_INTEGRATIONS, filter],
        () => Api.reports.fetch('trips', filter),
        {
            staleTime: 300000
        }
    );

    const result = useMemo(() => {
        return {
            integrations: data?.items || [],
            attachments: data?.meta?.attachments || [],
            meta: data?.meta?.meta || {},
        }
    }, [data]);

    return {
        ...result,
        isLoading,
        isFetching,
    }
}
