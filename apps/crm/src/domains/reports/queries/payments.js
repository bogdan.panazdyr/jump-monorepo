import { useInfiniteQuery, useQuery } from 'react-query';

import { joinInfinityPages } from 'utils/join-infinity-pages';
import Api from 'api/client';
import {
    CACHE_REPORT_PAYMENTS_INTEGRATIONS,
    CACHE_REPORT_PAYMENTS_DRIVERS,
    CACHE_REPORT_PAYMENTS_BANKS
} from 'domains/common/cache-keys';

export const useIntegrations = (filter) => {
    const { data, isLoading, isFetching } = useQuery(
        [CACHE_REPORT_PAYMENTS_INTEGRATIONS, filter],
        () => Api.reports.fetch('payments', filter),
        {
            staleTime: 300000,
        }
    )

    return {
        integrations: data?.items ?? [],
        attachments: data?.meta?.attachments || [],
        isLoading,
        isFetching,
    }
}

export const useBanks = (filter) => {
    const { data, isLoading, isFetching } = useQuery(
        [CACHE_REPORT_PAYMENTS_BANKS, filter],
        () =>  Api.reports.fetch(`payments/${filter.integration_id}`, filter),
        {
            staleTime: 300000,
        }
    )

    return {
        banks: data?.items ?? [],
        isLoading,
        isFetching,
    }
}

export const useDrivers = (filter) => {
    const { data, isLoading, isFetching, fetchNextPage, hasNextPage, isFetchingNextPage } = useInfiniteQuery(
        [CACHE_REPORT_PAYMENTS_DRIVERS, filter],
        ({ pageParam = 1 }) => Api.reports.fetch(
            `payments/${filter.integration_id}/${filter.bank_id}`,
            {...filter, per_page: 50, page: pageParam}
        ),
        {
            staleTime: 300000,
            getNextPageParam: (lastPage, _) => {
                if (lastPage.meta.current_page < lastPage.meta.last_page) {
                    return lastPage.meta.current_page + 1
                }

                return false;
            }
        }
    );

    return {
        drivers: joinInfinityPages(data?.pages),
        fetchNextPage,
        hasNextPage,
        isLoading: isLoading || isFetchingNextPage,
        isFetching,
    }
}
