import { useQuery } from 'react-query';

import Api from 'api/client';

export const useChartReport = (reportType, chartType, filter) => {
    const cacheKey = reportType + '_' + chartType + '_root';

    const { data, isLoading, isFetching, isError, error } = useQuery(
        [cacheKey, filter],
        () => Api.reports.fetchReportsChart(reportType, chartType, filter),
        {
            refetchOnWindowFocus: false,
            staleTime: 300000,
        }
    );

    return {
        data,
        isLoading,
        isFetching,
        isError,
        error,
    }
}
