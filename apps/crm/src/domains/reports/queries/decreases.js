import { useInfiniteQuery, useQuery } from 'react-query';

import { joinInfinityPages } from 'utils/join-infinity-pages';
import Api from 'api/client';
import { CACHE_REPORT_DECREASES_DEBITS, CACHE_REPORT_DECREASES_DRIVERS } from 'domains/common/cache-keys';

export const useDebits = (filter) => {
    const { data, isLoading, isFetching } = useQuery(
        [CACHE_REPORT_DECREASES_DEBITS, filter],
        () => Api.reports.fetch('decreases', filter),
        {
            staleTime: 300000,
        }
    )

    return {
        debits: data?.items ?? [],
        attachments: data?.meta?.attachments || [],
        isLoading,
        isFetching,
    }
}

export const useDrivers = (filter) => {
    const { data, isLoading, isFetching, fetchNextPage, hasNextPage, isFetchingNextPage } = useInfiniteQuery(
        [CACHE_REPORT_DECREASES_DRIVERS, filter],
        ({ pageParam = 1 }) => Api.reports.fetch(`decreases/${filter.debit_id}`, {...filter, per_page: 50, page: pageParam}),
        {
            staleTime: 300000,
            getNextPageParam: (lastPage, _) => {
                if (lastPage.meta.current_page < lastPage.meta.last_page) {
                    return lastPage.meta.current_page + 1
                }

                return false;
            }
        }
    );

    return {
        drivers: joinInfinityPages(data?.pages),
        fetchNextPage,
        hasNextPage,
        isLoading: isLoading || isFetchingNextPage,
        isFetching,
    }
}
