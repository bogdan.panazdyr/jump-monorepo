export const REPORT_TRIPS = 'trips';
export const REPORT_DECREASES = 'decreases';
export const REPORT_PAYMENTS = 'payments';

const names = {
    [REPORT_TRIPS]: 'Поездки',
    [REPORT_DECREASES]: 'Списания',
    [REPORT_PAYMENTS]: 'Выплаты',
}

const oldLinks = {
    [REPORT_TRIPS]: 'https://drivers.jump.taxi/reports/orders',
    [REPORT_PAYMENTS]: 'https://drivers.jump.taxi/reports/payments',
}

export const reportName = (report) => {
    return names[report] ?? 'Отчет';
}

export const isValid = (report) => {
    return [
        REPORT_TRIPS,
        REPORT_DECREASES,
        REPORT_PAYMENTS,
    ].includes(report);
}

export const oldLinkReports = (report) => {
    return oldLinks[report] ?? null;
}
