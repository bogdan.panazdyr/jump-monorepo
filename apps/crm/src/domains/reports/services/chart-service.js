import { REPORT_DECREASES, REPORT_PAYMENTS, REPORT_TRIPS } from './report-service';

const charts = {
    [REPORT_TRIPS]: ['count', 'commissions'],
    [REPORT_PAYMENTS]: ['commissions', 'count'],
    [REPORT_DECREASES]: ['count', 'sum'],
}

export const chartsByReport = (report) => {
    return charts[report] ?? [];
}
