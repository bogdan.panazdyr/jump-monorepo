import { useMemo, useState } from 'react';
import { useLocation } from 'react-router-dom';
import { parse } from 'query-string';

import { getPeriodOfPath } from 'utils/get-period-of-path';

const parseFilter = (hash) => {
    const path = parse(hash);

    return {...path, ...getPeriodOfPath(path?.period || path)};
}

export const useDashboardFilter = () => {
    const { hash } = useLocation();
    const [ filter, updateFilter ] = useState(parseFilter(hash));

    useMemo(() => {
        updateFilter(prevState => ({
            ...prevState,
            ...parseFilter(hash)
        }));
    }, [ hash ]);

    return {
        filter,
        updateFilter,
    };
}
