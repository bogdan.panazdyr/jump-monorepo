import { useMutation } from 'react-query';

import Api from 'api/client';

export const useBalanceSyncJob = () => {
    const { mutate, data, isLoading } = useMutation(() => Api.dashboard.syncBalances())

    return {
        jobId: data?.item?.id || null,
        startSyncJob: mutate,
        isStartingSyncJob: isLoading,
    }
}
