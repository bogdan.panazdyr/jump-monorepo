import { useCallback, useEffect, useState } from 'react';
import { useQuery, useQueryClient } from 'react-query';
import { CancelToken } from 'axios';

import { Forms, Utils } from '@justlook/core';

import { useMoney } from 'hooks/use-money';
import { snackBar } from 'components';
import Api from 'api/client';
import {
    CACHE_DASHBOARD_BALANCE,
    CACHE_BALANCE_SYNC_STATUS,
    CACHE_DASHBOARD_CONTROL_ROOM,
    CACHE_DASHBOARD_SUMMARY,
} from 'domains/common/cache-keys';

import { useBalanceSyncJob } from './dashboard-mutators';

export const useBalance = () => {
    const { format, currency } = useMoney();
    const queryClient = useQueryClient();

    const { data, isLoading } = useQuery([CACHE_DASHBOARD_BALANCE], () => Api.dashboard.fetchBalance());

    const refreshBalance = useCallback(() => {
        return queryClient.refetchQueries([CACHE_DASHBOARD_BALANCE])
    }, [queryClient]);

    const item = data?.item ?? {};
    const balance = item?.balance || 0;

    return {
        balance,
        balanceLabel: `${format(balance, 0, 0)} ${currency}`,
        syncTime: item?.synced_at ? Utils.Dates.getTime(item?.synced_at) : null,
        isFetchingBalance: isLoading,
        refreshBalance,
    }
}

export const useRefreshBalance = (onSuccessRefresh) => {
    const { jobId, isStartingSyncJob, startSyncJob } = useBalanceSyncJob();
    const { isFinishedSyncJob, cancelSyncJob } = useBalanceSyncJobStatus(jobId, onSuccessRefresh);

    useEffect(() => {
        startSyncJob();

        return cancelSyncJob;
    }, []); // eslint-disable-line

    return {
        startSyncJob,
        isFetchingBalance: !isFinishedSyncJob || isStartingSyncJob,
    }
}

const useBalanceSyncJobStatus = (syncId, onSuccessSync) => {
    const REFETCH_INTERVAL_SECONDS = 2000;
    const queryClient = useQueryClient();
    const [ isFinishedSyncJob, setIsFinished ] = useState(false);
    const [ refetchInterval, setRefetchInterval ] = useState(false);

    useQuery([CACHE_BALANCE_SYNC_STATUS, syncId], () => {
        const source = CancelToken.source()

        const promise = Api.dashboard.syncBalancesStatus(syncId, source.token);

        promise.cancel = () => source.cancel();

        return promise;
    }, {
        onSuccess: (status) => {
            if(status === 202) {
                setIsFinished(false);
                setRefetchInterval(REFETCH_INTERVAL_SECONDS);
                return
            }

            if(status === 302) {
                setIsFinished(true);
                setRefetchInterval(false);
                onSuccessSync()
                return
            }

            setIsFinished(true);
            setRefetchInterval(false);

            snackBar('Ошибка синхронизации баланса', { hideAfter: 0 })
        },
        enabled: Boolean(syncId),
        refetchOnWindowFocus: false,
        refetchInterval,
    })

    return {
        isFinishedSyncJob,
        cancelSyncJob: () => queryClient.cancelQueries([CACHE_BALANCE_SYNC_STATUS, syncId])
    }
}

export const useControlRoom = () => {
    const { data, isLoading } = useQuery(
        [CACHE_DASHBOARD_CONTROL_ROOM],
        () => Api.dashboard.fetchIntegrations(), {
            refetchOnWindowFocus: false
        }
    )

    const getOptions = () => {
        const options = data?.items ?? [];

        const preOptions = options.map(item => Forms.itemToOption(item, { value: 'value', label: 'title' }))

        return [ { value: '', label: 'Все диспетчерские' }, ...preOptions ]
    };

    return {
        options: getOptions(),
        isLoading,
    }
}

export const useSummary = () => {
    const { data, isLoading } = useQuery(
        [CACHE_DASHBOARD_SUMMARY],
        () => Api.dashboard.fetchSummary(), {
            refetchOnWindowFocus: false
        }
    )

    return {
        summary: data?.items ?? [],
        isLoading,
    }
}
