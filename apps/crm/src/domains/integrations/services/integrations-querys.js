import { useQuery } from 'react-query';

import Api from 'api/client';
import { CACHE_INTEGRATIONS_LIST, CACHE_INTEGRATION_ITEM } from 'domains/common/cache-keys';

import { useFilter } from './integrations-service';

export const useIntegrationsList = () => {
    const { filter, updateFilter } = useFilter();

    const { data, isLoading, isFetching, isError, error } = useQuery(
        [CACHE_INTEGRATIONS_LIST, filter],
        () => Api.integrations.fetchList(filter),
    )

    return {
        items: data?.items || [],
        meta: data?.meta || {},
        links: data?.links || {},
        updateFilter,
        isLoading,
        isFetching,
        isError,
        error,
    }
}

export const useIntegrationItem = (id, initialData = undefined) => {
    const { data, isLoading, isFetching, isError, error } = useQuery(
        [CACHE_INTEGRATION_ITEM, id],
        () => Api.integrations.fetchItem(id).then(response => response?.item), {
            enabled: Boolean(id),
            refetchOnWindowFocus: false,
            initialData,
        }
    )

    return {
        integration: data,
        isLoading,
        isFetching,
        isError,
        error,
    }
}
