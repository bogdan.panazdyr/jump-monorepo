import { useMemo } from 'react';
import { useQueryClient } from 'react-query';

import { Forms } from '@justlook/core';

import { useUpdateQueryFilter } from 'hooks/use-update-search-filter';
import { useAgentsMy } from 'domains/agents';
import { useGroups } from 'domains/groups';
import { CACHE_INTEGRATIONS_LIST } from 'domains/common/cache-keys';

export const servicesIdNeedConfirmation = [ 24 ];

export const DEFAULT_FILTER = {
    order: '-id',
    page: 1,
    per_page: 10,
};

export const useFilter = () => {
    const query = useUpdateQueryFilter(DEFAULT_FILTER);

    return useMemo(() => ({
        filter: query.filterFull,
        updateFilter: query.updateFilter,
    }), [query.updateFilter, query.filterFull]);
}

export const useDictionaries = () => {
    const { agents, isLoading: isLoadingAgents } = useAgentsMy();
    const { groups, isLoading: isLoadingGroups } = useGroups();

    const defaultAgent = agents.length === 1 ? Forms.itemToOption(agents[0]) : null;
    const defaultGroup = groups.length === 1 ? Forms.itemToOption(groups[0]) : null;

    return {
        isLoading: isLoadingAgents || isLoadingGroups,
        agents,
        groups,
        defaultAgent,
        defaultGroup,
    };
}

export const useGetIntegrationById = () => {
    const queryClient = useQueryClient();

    const { filter } = useFilter();

    return (id) => {
        if (!id) return null;

        const data = queryClient.getQueryData([ CACHE_INTEGRATIONS_LIST, filter ]) || {};

        return data?.items?.find(item => item.id === id) || null;
    }
}
