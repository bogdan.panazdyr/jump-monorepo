import { useMutation, useQueryClient } from 'react-query';

import Api from 'api/client';
import { errorToString } from 'utils/add-server-errors';
import { CACHE_CONTRACTOR_PERMISSIONS } from 'domains/common/cache-keys';

export const useUpdateContractorPermission = (contractorId, snackBar) => {
    const queryClient = useQueryClient();

    const { mutate, isLoading, isError, error } = useMutation(
        ({ permissionId, value }) => Api.contractorPermissions.update(contractorId, permissionId, value), {
            onSuccess: ({ item }) => {
                queryClient.setQueryData([CACHE_CONTRACTOR_PERMISSIONS, contractorId], item);
            },

            onError: (error) => {
                return snackBar(errorToString(error), { hideAfter: 3 })
            }
        }
    )

    return {
        onUpdatePermission: mutate,
        isUpdatingPermission: isLoading,
        isError,
        error,
    }
}
