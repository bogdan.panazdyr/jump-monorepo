import React from 'react';

import { Icons16 } from '@justlook/uikit';

export const getStatusIcon = (status) => {
    const icons = {
        formed: Icons16.IconWatchFill,
        warning: Icons16.IconWatchFill,
        info: Icons16.IconBanCircleFill,
        success: Icons16.IconCheckCircleFill,
    }


    const Icon = icons[status] || icons['info'];

    return <Icon/>
}
