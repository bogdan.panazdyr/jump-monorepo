import { useMemo } from 'react';
import { useForm } from 'react-hook-form';
import { useQueryClient } from 'react-query';
import { useUpdateQueryFilter } from 'hooks/use-update-search-filter';

import { errorToString } from 'utils/add-server-errors';

import { useCreate, useDelete, useUpdate } from './agents-mutators';
import { useAgent } from './agents-query';

import { CACHE_AGENTS_LIST } from 'domains/common/cache-keys';

export const DEFAULT_FILTER = {
    order: '-id',
    page: 1,
    per_page: 10,
};

export const useFilter = () => {
    const { filterFull, filterQuery, updateFilter, pathname } = useUpdateQueryFilter(DEFAULT_FILTER);

    return useMemo(() => ({
        filter: filterFull,
        updateFilter,
        filterQuery,
        pathname,
    }), [updateFilter, filterFull, filterQuery, pathname]);
}

export const useGetAgentById = () => {
    const queryClient = useQueryClient();

    const { filter } = useFilter();

    return (id) => {
        if (!id) return null;

        const data = queryClient.getQueryData([ CACHE_AGENTS_LIST, filter ]) || {};

        return data?.items?.find(item => item.id === id) || null;
    }
}

export const useInitialForm = (id, onDismiss) => {

    const getAgentById = useGetAgentById()

    const { data: agent, isLoading } = useAgent(id, getAgentById(id));

    const { handleSubmit, errors, control, setError, reset } = useForm({
        defaultValues: {
            name: agent && agent?.name,
            inn: agent && agent?.inn,
            ogrn: agent && agent?.ogrn,
            address: agent && agent?.address,
        }
    });

    const handleOnSelectAgent = (agent) => {
        reset({
            name: agent?.data.name?.short_with_opf || '',
            inn: agent?.data?.inn || '',
            ogrn: agent?.data?.ogrn || '',
            address: agent?.data?.address?.value || '',
        })
    }

    const header = agent ? 'Редактирование юрлица' : 'Новое юрлицо';
    const submitLabel = agent ? 'Сохранить' : 'Добавить';

    const { onCreate, isCreating } = useCreate(onDismiss, setError);
    const { onUpdate, isUpdating } = useUpdate(id, onDismiss, setError);
    const { onDelete, isDeleting, error: errorDeletion } = useDelete(id, onDismiss);

    const getError = field => errors?.[field]?.message;

    const onSubmit = handleSubmit((values) => agent ? onUpdate(values) : onCreate(values));

    return {
        agent,
        header,
        submitLabel,
        isLoading,
        handleOnSelectAgent,
        errorDeletion: errorToString(errorDeletion),
        form: {
            isMutating: isCreating || isUpdating || isDeleting,
            control,
            onSubmit,
            onDelete,
            isCreating,
            isUpdating,
            isDeleting,
            getError,
        }
    }
}
