import { useState } from 'react';
import { useQuery } from 'react-query';

import Api  from 'api/client';

import { CACHE_AGENTS_MY, CACHE_AGENT_BY_INN, CACHE_AGENT, CACHE_AGENTS_LIST } from 'domains/common/cache-keys';

import { useFilter } from './agents-service';

export const useAgentsMy = () => {
    const [ defaultAgent, setDefaultAgent ] = useState({});

    const onFetch = async () => {
        const resp = await Api.agents.my();
        const items = resp?.items || [];

        if(items.length > 0) { setDefaultAgent(items[0]) }

        return items;
    }

    const { data, isLoading, isFetching } =  useQuery([CACHE_AGENTS_MY], onFetch, { refetchOnWindowFocus: false });

    return {
        agents: data || [],
        defaultAgent,
        isLoading,
        isFetching,
    }
}

export const useAgentsList = () => {
    const { filter, updateFilter } = useFilter();

    const onFetch = async () => await Api.agents.fetchAgentsList(filter);

    const { data, isLoading, isFetching, isError, error } = useQuery([CACHE_AGENTS_LIST, filter], onFetch)

    return {
        agents: data?.items || [],
        meta: data?.meta || {},
        updateFilter,
        isLoading,
        isFetching,
        isError,
        error,
    }
}

export const useAgent = (id, initialData = undefined) => {
    const onFetch = async () => {
        const response = await Api.agents.fetchAgent(id);

        return response?.item || {}
    }

    const { data, isLoading, isFetching, isError, error } = useQuery([CACHE_AGENT, id], onFetch, {
        enabled: Boolean(id),
        refetchOnWindowFocus: false,
        initialData,
    })

    return {
        data,
        isLoading,
        isFetching,
        isError,
        error,
    }
}

export const useAgentByInn = () => {
    const [ value, setValue ] = useState(null);

    const onFetch = async () => {
        const response = await Api.agents.fetchAgentByInn(value);

        return response?.suggestions || [];
    }

    const { data, isLoading } = useQuery([CACHE_AGENT_BY_INN, value], onFetch, {
        enabled: Boolean(value),
        refetchOnWindowFocus: false,
    });

    const onClearState = () => {
        setValue(null)
    }

    return {
        result: data || [],
        onFetch: setValue,
        isLoading,
        onClearState,
    }
}
