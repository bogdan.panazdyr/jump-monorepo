import React, {createContext, useContext} from 'react';
import PropTypes from 'prop-types';
import { Hooks } from '@justlook/core';

const CreateEntityContext = createContext(null);

export const useCreateEntityContext = () => {
    return useContext(CreateEntityContext);
}

const CreateEntityContextProvider = ({ children }) => {
    const {isShow, onOpenForm, onCloseForm} = Hooks.useModalState();

    return (
        <CreateEntityContext.Provider value={{
            isShowEntityForm: isShow,
            onCloseEntityForm: onCloseForm,
            onOpenEntityForm: onOpenForm,
        }}>
            { children }
        </CreateEntityContext.Provider>
    )
}

CreateEntityContextProvider.propTypes = {
    children: PropTypes.any
}

export default CreateEntityContextProvider
