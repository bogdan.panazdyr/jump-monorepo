import { useCallback, useMemo } from 'react';
import { useQuery, useQueryClient } from 'react-query';

import Api from 'api/client';
import { CACHE_PAYMENT_STATUSES } from 'domains/common/cache-keys';

import { PaymentStatusList } from '../helpers';

export const usePaymentStatusBadges = () => {
    const client = useQueryClient();

    const { data, isLoading, isError, error, isFetching } = useQuery([CACHE_PAYMENT_STATUSES], () => Api.payments.counts());

    const refreshBadges = useCallback(() => client.refetchQueries([CACHE_PAYMENT_STATUSES]), [client]);

    const badges = useMemo(() => {
        return new PaymentStatusList(data?.items);
    }, [data?.items])

    return {
        badgesData: badges,
        badgesHasError: isError,
        badgesError: error,
        isBadgesLoading: isLoading,
        isBadgesFetching: isFetching,
        refreshBadges,
    };
}
