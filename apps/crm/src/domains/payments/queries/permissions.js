import { useQuery } from 'react-query';

import Api from 'api/client';
import { CACHE_PAYMENT_ONLINE_PERMISSION } from 'domains/common/cache-keys';

export const useOnlinePermissions = () => {
    const { data, isLoading, isFetching, isError, error } = useQuery([CACHE_PAYMENT_ONLINE_PERMISSION], async () => {
        const resp = await Api.payments.checkPermissions();

        return resp?.data || {};
    }, { refetchOnWindowFocus: false })

    return {
        actionLabel: data?.action_label || '...',
        isAllow: data?.is_allow || false,
        message: data?.message || '',
        permissionsHasError: isError,
        permissionsError: error,
        isPermissionsLoading: isLoading || isFetching,
    }
}
