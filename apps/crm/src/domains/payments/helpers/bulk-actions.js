import { useHasPermission } from 'app/profile/store/selectors';

export const useBulkActionsForPayments = (availableActions = []) => {
    const canPay = useHasPermission('write-payment');

    const actions = {
        cancel: false,
        confirm: false,
        refund: false,
        repeat: false,
    }

    availableActions.map(action => actions[action] = action && canPay);

    return actions;
}
