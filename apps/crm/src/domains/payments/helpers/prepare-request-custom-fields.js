import { formatISO, isDate } from 'date-fns';

import { getCustomFieldsKey } from './get-custom-fields-key';

export const prepareRequestCustomFields = (fields, values) => {
    const prepareValues = {};

    if (! fields) {
        return prepareValues;
    }

    for(let fieldKey of getCustomFieldsKey(fields)) {
        let fieldData = values[fieldKey];

        if (! fieldData) {
            continue;
        }

        if (isDate(fieldData)) {
            fieldData = formatISO(fieldData)
        } else if (fieldData instanceof Object) {
            fieldData = fieldData.value
        }

        prepareValues[fieldKey] = fieldData;
    }

    return prepareValues;
}
