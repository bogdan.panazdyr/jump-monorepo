import React from 'react';

import { Icons16 } from '@justlook/uikit';

export const PAYMENT_STATUS = {
    ALL: null,
    PAYED: 1,
    DECLINED: 2,
    IN_PROCESS: 3,
    WAITING_CONFIRMATION: 4,
    DECLINED_WITH_ACTIONS: 5,
}

export const isActiveStatus = (statusId, state) => {
    if (statusId === PAYMENT_STATUS.ALL && !state?.status_id) {
        return true;
    }

    return parseInt(state?.status_id) === parseInt(statusId);
}

export const isDeclinedWithActions = (statusId) => {
    return parseInt(statusId) === PAYMENT_STATUS.DECLINED_WITH_ACTIONS;
}

export const isWaitingConfirm = (statusId) => {
    return parseInt(statusId) === PAYMENT_STATUS.WAITING_CONFIRMATION;


}

export class PaymentStatusList {
    counts = {}

    constructor (data) {
        (data || []).map(info => this.counts[info.status_id] = parseInt(info.count));
    }

    getCount (statusId) {
        if (statusId === PAYMENT_STATUS.ALL) {
            return this.counts?.all || 0;
        }

        return this.counts?.[statusId] || 0;
    }
}

const FILTER_OPTIONS = [
    {label: 'Все', value: PAYMENT_STATUS.ALL},
    {label: 'Нужно подтверждение', value: PAYMENT_STATUS.WAITING_CONFIRMATION},
    {label: 'Ошибка выплаты', value: PAYMENT_STATUS.DECLINED_WITH_ACTIONS},
    {label: 'Оплаченные', value: PAYMENT_STATUS.PAYED},
    {label: 'Отменённые', value: PAYMENT_STATUS.DECLINED},
]

export const getFilterOptions = (badges) => {
    if(!badges) return FILTER_OPTIONS;

    return FILTER_OPTIONS.map(option => ({ ...option, badge: badges.getCount(option.value) }));
}

export const getDefaultFilter = (state) => {
    if(!state?.status_id) {
        return FILTER_OPTIONS[0]
    }

    return FILTER_OPTIONS.filter(option => parseInt(option.value) === parseInt(state?.status_id))[0]
}

export const getStatusIcon = (status) => {
    const icons = {
        success: Icons16.IconCheckCircleFill,
        info: Icons16.IconBanCircleFill,
        warning: Icons16.IconWatchFill,
        danger: Icons16.IconCrossCircleFill,
    }


    const Icon = icons[status] || icons['info'];

    return <Icon/>
}
