import { useState } from 'react';

import Api from 'api/client';
import { useMoney } from 'hooks/use-money';
import { setApiErrorsToForm } from 'utils/add-server-errors';
import { paymentInfo } from 'utils/helpers';

import { WITHDRAW_BALANCE_CONTRACTOR, WITHDRAW_BALANCE_COMPANY } from '../constants';

export const useValidateAmount = (setError, clearErrors) => {
    const { currency } = useMoney();

    const [ isValidating, setIsValidating ] = useState(false);
    const [ validateInfo, setValidateInfo ] = useState(null);

    const withdrawPreview = async (data) => await Api.payments.withdrawPreview(data)
    const withdrawSmartPreview = async (data) => await Api.payments.withdrawSmartPreview(data)

    const getTheMethod = (type, data) => {
        if (type === WITHDRAW_BALANCE_CONTRACTOR) {
            return withdrawPreview(data) }
        if (type === WITHDRAW_BALANCE_COMPANY) {
            return withdrawSmartPreview(data)
        }
    }

    const fetchValidating = (values) => {
        setIsValidating(true)
        setValidateInfo(null);
        clearErrors();

        const paymentFrom = values?.payment_from?.value;

        getTheMethod(paymentFrom, values)
            .then(({ data }) => {
                clearErrors();

                setValidateInfo(paymentInfo({
                    ...data,
                    currency,
                }))
            })
            .catch(error => {
                setValidateInfo(null);

                setApiErrorsToForm(error, setError);
            })
            .finally(() => setIsValidating(false))
    }

    return { fetchValidating, validateInfo, isValidating }
}
