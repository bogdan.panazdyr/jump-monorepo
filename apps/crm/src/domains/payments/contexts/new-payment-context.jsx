import React, {createContext, useContext} from 'react';
import PropTypes from 'prop-types';

import { Hooks } from '@justlook/core';

const NewPaymentContext = createContext(null);

export const useNewPaymentContext = () => {
    return useContext(NewPaymentContext);
}

const NewPaymentContextProvider = ({ children }) => {
    const {isShow, onOpenForm, onCloseForm} = Hooks.useModalState();

    return (
        <NewPaymentContext.Provider value={{
            isShowNewPaymentForm: isShow,
            onCloseNewPaymentForm: onCloseForm,
            onOpenNewPaymentForm: onOpenForm,
        }}>
            { children }
        </NewPaymentContext.Provider>
    )
}

NewPaymentContextProvider.propTypes = {
    children: PropTypes.any
}

export default NewPaymentContextProvider
