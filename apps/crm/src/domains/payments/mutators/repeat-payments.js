import { useMutation } from 'react-query';

import Api from 'api/client';

export const useRepeatPayments = (onSuccess) => {
    const { mutate, isLoading, isError, error } = useMutation(
        (data) => Api.payments.repeatPayments(data), {
            onSuccess
        }
    )

    return {
        onRepeat: mutate,
        mutateRepeatHasError: isError,
        isRepeatMutating: isLoading,
        mutateRepeatError: error
    }
}
