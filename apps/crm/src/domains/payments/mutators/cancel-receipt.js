import { useMutation, useQueryClient } from 'react-query';

import Api from 'api/client';
import { CACHE_PAYMENT_ITEM, CACHE_PAYMENT_LIST } from 'domains/common/cache-keys';

import { useFilter } from '../helpers/filter';

export const useCancelReceipt = (setError, onSuccess) => {
    const queryClient = useQueryClient();
    const { filter } = useFilter();

    const { mutate, isLoading, isError, error } = useMutation(
        ({ id, reason }) => Api.payments.cancelReceipt(id, reason), {
            onSuccess: ({ item: updatedPayment }) => {
                const mutationPayments = (old) => ({
                    ...old,
                    items: old.items.map(item => item.id === updatedPayment.id ? updatedPayment : item)
                });

                queryClient.setQueryData([CACHE_PAYMENT_LIST, filter], mutationPayments);
                queryClient.setQueryData([CACHE_PAYMENT_ITEM, updatedPayment.id], updatedPayment);

                onSuccess(updatedPayment);
            },
            onError: setError
        })

    return {
        onCancelReceipt: mutate,
        isCancelReceiptMutating: isLoading,
        mutateCancelReceiptHasError: isError,
        mutateCancelReceiptError: error,
    }
}
