export const WITHDRAW_BALANCE_CONTRACTOR = 'contractor';
export const WITHDRAW_BALANCE_COMPANY = 'company';

export const ACTION_CANCEL = 'cancel';
export const ACTION_REPEAT = 'repeat';
export const ACTION_REFUND = 'refund';

export const FILTER = {
    order: '-updated_at',
    include: 'agent, creator, requisite, history',
    page: 1,
    per_page: 30,
};
