import { Utils } from '@justlook/core';

import { formattingDate } from 'utils/formatting-date';
import { round } from 'utils/helpers';

export function Payment (data = {}) {
    this.original = data;

    this.id = data?.id ?? '';
    this.status = data?.status ?? '';
    this.createdAt = data?.created_at ?? '';
    this.updatedAt = data?.updated_at ?? data?.created_at ?? '';
    this.fullName = data?.contractor?.full_name ?? '';
    this.shortName = data?.contractor?.short_name ?? '';
    this.amount = data?.amount ?? 0;
    this.commission = data?.commission ?? 0;
    this.creator = data?.creator?.full_name ?? '';
    this.agent = data?.agent?.full_name ?? '';
    this.taxAmount = data?.tax_amount ?? 0;
    this.amountPaid = data?.amount_paid ?? 0;
    this.phone = data?.contractor?.phone ?? '';
    this.commissionBank = data?.commission_bank ?? 0;
    this.requisite = data?.requisite ?? null;
    this.paymentPurpose = data?.payment_purpose ?? '–';
    this.history = data?.history ?? null;
    this.receipt = data?.receipt ?? null;
    this.canRepeatFlag = data?.abilities?.can_repeat ?? false;
    this.canRefundFlag = data?.abilities?.can_refund ?? false;
    this.canCancelFlag = data?.abilities?.can_cancel ?? false;
}

Payment.prototype.getOriginal = function() {
    return this.original;
};

Payment.prototype.getId = function() {
    return this.id;
};

Payment.prototype.getStatus = function() {
    return this.status.title;
};

Payment.prototype.getStatusTheme = function() {
    return this.status.theme;
};

Payment.prototype.getCreateAt = function() {
    return formattingDate(this.createdAt);
};

Payment.prototype.getCreateTime = function() {
    return Utils.Dates.getTime(this.createdAt);
};

Payment.prototype.getFullCreateAt = function() {
    const items = [
        this.getCreateAt(),
        this.getCreateTime(),
    ];

    return items.filter(Boolean).join(', ');
};

Payment.prototype.getUpdatedAt = function() {
    return formattingDate(this.updatedAt);
};

Payment.prototype.getUpdatedTime = function() {
    return Utils.Dates.getTime(this.updatedAt);
};

Payment.prototype.getFullUpdatedAt = function() {
    const items = [
        this.getUpdatedAt(),
        this.getUpdatedTime(),
    ];

    return items.filter(Boolean).join(', ');
};

Payment.prototype.getContractorFullName = function() {
    return this.fullName;
};

Payment.prototype.getContractorShortName = function() {
    return this.shortName;
};

Payment.prototype.getContractorPhone = function() {
    return this.phone;
};

Payment.prototype.getAmount = function(isShort = false) {
    return round(this.amount, isShort);
};

Payment.prototype.getAmountPaid = function (isShort = false) {
    return round(this.amountPaid, isShort);
};

Payment.prototype.getTaxAmount = function (isShort = false) {
    return round(this.taxAmount, isShort);
};

Payment.prototype.getCommissionBank = function (isShort = false) {
    return round(this.commissionBank, isShort);
}

Payment.prototype.getCommission = function (isShort = false) {
    return round(this.commission, isShort);
}

Payment.prototype.getCreator = function() {
    return this.creator;
};

Payment.prototype.getAgent = function() {
    return this.agent;
};

Payment.prototype.getMobileDescription = function(prefix = '') {
    const description = [
        this.getCreateAt(),
        this.getCreateTime(),
    ];

    return description.filter(Boolean).join(prefix);
};

Payment.prototype.isRequisite = function() {
    return this.requisite !== null;
};

Payment.prototype.getRequisiteTitle = function() {
    return this.requisite.title;
};

Payment.prototype.getRequisiteDescription = function() {
    return this.requisite.description;
};

Payment.prototype.getPaymentPurpose = function() {
    return this.paymentPurpose;
};

Payment.prototype.isHistory = function() {
    return this.history !== null && this.history.length > 0;
};

Payment.prototype.getHistory = function() {
    return this.history;
};

Payment.prototype.canRepeat = function() {
    return this.canRepeatFlag === true;
};

Payment.prototype.canRefund = function() {
    return this.canRefundFlag === true;
};

Payment.prototype.canBeCancel = function () {
    return this.canCancelFlag === true;
};

Payment.prototype.hasReceipt = function() {
    return this.receipt !== null;
}
