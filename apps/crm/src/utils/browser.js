import { useIfMediaScreen } from '@justlook/uikit/lib/hooks';

export const VIEW_MODE_FULLSCREEN = 'fullscreen';
export const VIEW_MODE_DETAILS = 'details';
export const VIEW_MODE_LIST = 'list';

export function useContentView (details) {
    const isMobile = useIfMediaScreen();

    if (!isMobile) {
        return VIEW_MODE_FULLSCREEN;
    }

    return details ? VIEW_MODE_DETAILS : VIEW_MODE_LIST;
}


/**
 * Проверка на мобильный экран
 * @returns {boolean}
 */
export function isMobile () {
    return window.matchMedia('(max-width: 768px)').matches;
}

/**
 * Проверка на планшетный экран
 * @returns {boolean}
 */
export function isTablet () {
    return window.matchMedia(
        '(min-width: 768px) and (max-width: 1199px)').matches;
}

/**
 * Проверка на десктопный экран
 * @returns {boolean}
 */
export function isDesktop () {
    return window.matchMedia('(min-width: 1199px)').matches;
}

/**
 * Возвращает координаты элемента относительно страницы
 * @param elem
 * @returns {{top: number, left: number}}
 */
export function getCoords (elem) {
    const box = elem.getBoundingClientRect();
    const body = document.body;
    const docEl = document.documentElement;

    const scrollTop = window.pageYOffset || docEl.scrollTop || body.scrollTop;
    const scrollLeft = window.pageXOffset || docEl.scrollLeft ||
        body.scrollLeft;

    const clientTop = docEl.clientTop || body.clientTop || 0;
    const clientLeft = docEl.clientLeft || body.clientLeft || 0;

    const top = box.top + scrollTop - clientTop;
    const left = box.left + scrollLeft - clientLeft;

    return {
        top: top,
        left: left,
    };
}
