import React from 'react';

import { Icons16 } from '@justlook/uikit';

export const chipsProps = (payment) => {
    const props = {
        avatar: <Icons16.IconWatchRhombus/>,
        label: payment.status.title
    }

    const properties = [
        [null, Icons16.IconCheck],
        ['alert', Icons16.IconClose],
        [null, Icons16.IconWatch],
        [null, Icons16.IconWatchRhombus]
    ];

    const id = payment?.status?.id || 0;
    if (id && properties[id-1]) {
        const Icon = properties[id-1][1];
        props.color=properties[id-1][0];
        props.avatar = <Icon/>;
    }

    return props;
}
