import { errorToJs } from '@justlook/core/src/errors';
import { trimString } from 'utils/helpers';

export const setApiErrorsToForm = (errorApi, setError) => {
    if (!(errorApi instanceof Error)) {
        return;
    }

    (errorToJs(errorApi)?.fields || []).forEach(field => {
        const message = (field.messages || [])
            .map(trimString)
            .join('. ')
            .trim();

        if (message) {
            setError(field.field, { message });
        }
    });
}

/**
 * description
 * @param {array} errors
 * @param {function} setError
 * @returns {*[]}
 */
export const addServerErrors = (errors, setError) => {
    return errors.map(item => (
        setError(item.field, {
            type: 'server',
            message: item.messages[0]
        })
    ))
}

export const errorToString = (errorRaw, defaultText) => {
    if (errorRaw instanceof Error) {
        const error = errorToJs(errorRaw);

        const concat = (error.fields ?? [])
            .reduce((acc, field) => {
                const message = (field.messages || [])
                    .map(trimString)
                    .join('. ');

                return [ ...acc, message ];
            }, [])
            .map(trimString)
            .join('. ');

        return trimString(concat || error.detail || defaultText) + '.';
    }

    return defaultText;
}
