import {
    format, subWeeks, subMonths, subYears, subQuarters, parseISO, isValid, subDays, differenceInCalendarMonths
} from 'date-fns';

const parseDate = (date) => {
    const dt = parseISO(date);
    return isValid(dt) ? dt : new Date();
};

export const getPeriodOfPath = (period = {}) => {
    const result = {
        date_from: subWeeks(new Date(), 1),
        date_to: subDays(new Date(), 1),
        period_type: 'day',
    };

    switch (period) {
        case 'month':
            result.date_from = subMonths(new Date(), 1);
            result.period_type = 'week';
            break;
        case 'quarter':
            result.date_from = subQuarters(new Date(), 1);
            result.period_type = 'month';
            break;
        case 'year':
            result.date_from = subYears(new Date(), 1);
            result.period_type = 'month';
            break;
        default:
            if (period.from && period.to) {
                result.date_from = parseDate(period.from);
                result.date_to = parseDate(period.to);

                const difference = Math.abs(differenceInCalendarMonths(result.date_from, result.date_to));

                result.period_type = difference >= 5 ? 'month' : 'day';
            }
    }

    result.date_from = format(result.date_from, 'yyyy-MM-dd');
    result.date_to = format(result.date_to, 'yyyy-MM-dd');

    return result;
};
