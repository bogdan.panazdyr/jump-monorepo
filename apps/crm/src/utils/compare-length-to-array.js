/**
 *
 * @param {array} array
 * @param {number} length
 * @returns {boolean}
 */
export const compareLengthToArray = (array, length) => {
    return array.length === length
}
