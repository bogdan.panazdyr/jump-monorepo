import React from 'react';
import PropTypes from 'prop-types';
import { Button, ParagraphV2 as Paragraph } from '@justlook/uikit';

import cx from 'components/empty-content/empty-content.module.scss';

export const EmptyContent = ({ message, actionLabel, action, children, isMute = false }) => {
    return (
        <div className={cx.base}>
            <div className={cx.wrapper}>
                <Paragraph.LH24 className={isMute ? cx.mute : ''}>{message}</Paragraph.LH24>
                {actionLabel && <Button styling='primary' onClick={action}>{actionLabel}</Button>}
                {
                    children &&
                    <div className={cx.children}>
                        {children}
                    </div>
                }
            </div>
        </div>
    )
};

EmptyContent.propTypes = {
    isMute: PropTypes.bool,
    action: PropTypes.func,
    actionLabel: PropTypes.string,
    message: PropTypes.string,
    children: PropTypes.node,
};
