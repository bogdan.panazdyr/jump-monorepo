import React, { useState, useRef } from 'react';
import PropTypes from 'prop-types';
import { Input } from '@justlook/uikit';

import Api from 'api/client';

import { useClickOutside } from '../../hooks/use-click-outside';

import cx from 'components/dadata-field/dadata-field.module.scss';

export const DaDataField = ({name, label, onSelect, onChange, error, value}) => {
    const [data, setData] = useState([])
    const optionRef = useRef(null);

    const fetchData = (value) => {
        Api.address.fetchAddress(value)
            .then(response => setData(response.data.suggestions))
            .catch(error => alert(error))
    }

    const onSelectHint = (item) => {
        onSelect(item);
        setData([]);
    }

    const outsideClicked = () => {
        onSelect(data[0].data);
        setData([]);
    }

    const onChangeField = (value) => {
        fetchData(value);
        onChange(value);
    }

    useClickOutside(optionRef, outsideClicked);

    return (
        <div className={cx.base}>
            <Input
                label={label}
                name={name}
                value={value || ''}
                autoComplete='nope'
                className={cx.grid}
                placeholder='Город, Улица, Дом'
                onChange={onChangeField}
                error={error}
            />

            {
                data.length > 0 &&
                <div className={cx.resultData} ref={optionRef}>
                    {data.map((item, index) => (
                        <div
                            key={index}
                            className={cx.option}
                            onClick={() => onSelectHint(item.data)}
                        >
                            {item.value}
                        </div>
                    ))}
                </div>
            }
        </div>
    )
}

DaDataField.propTypes = {
    name: PropTypes.string,
    label: PropTypes.string,
    granular: PropTypes.string,
    locations: PropTypes.object,
    onSelect: PropTypes.func,
    onChange: PropTypes.func,
    value: PropTypes.string,
    error: PropTypes.any,
}
