import React from 'react';
import PropTypes from 'prop-types';
import { Icons24, ParagraphV2 as Paragraph } from '@justlook/uikit';

import cx from './snackbar-message.module.scss';

const SnackbarMessage = ({isLoading, isLoadingMessage, icon, children}) => {
    const Icon = isLoading ? Icons24.IconLoader : (icon ? Icons24[icon] : null);

    return (
       <div className={cx.container}>
           <div className={cx.content} >
               { Icon && <Icon/> }

               <Paragraph.LH24>
                   {isLoading ? isLoadingMessage : children}
               </Paragraph.LH24>
           </div>
       </div>
    )
}

SnackbarMessage.defaultProps = {
    isLoading: false,
    isLoadingMessage: 'загрузка ...'
}

SnackbarMessage.propTypes = {
    icon: PropTypes.string,
    children: PropTypes.any,
    isLoading: PropTypes.bool,
    isLoadingMessage: PropTypes.string,
}

export default SnackbarMessage
