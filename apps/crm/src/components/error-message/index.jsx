import React from 'react';
import PropTypes from 'prop-types';
import { Icons24, InfoMessage } from '@justlook/uikit';

import cx from './error-message.module.scss';

export const ErrorMessage = ({ message }) => {
    return (
        <div className={cx['error-message']}>
            <InfoMessage icon={<Icons24.IconInfo/>} type='error'>{message}</InfoMessage>
        </div>
    )
}

ErrorMessage.propTypes = {
    message: PropTypes.string,
}
