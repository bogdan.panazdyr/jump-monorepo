import React, {Children} from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import { ListItem } from './list-item';

import cx from './list.module.scss';
import cxItem from './list-item.module.scss'

export const List = ({ children = [], className, fluid = false }) => {
    const cls = classnames(cx.list, {[cx.fluid]: fluid}, className)

    return <ul className={cls}>
        {Children.map(children, (child, index) => {
            if(child?.type?.defaultProps?.displayName !== 'ListItem'){
                const itemCls = classnames(child.props.className, cxItem.item)
                return <ListItem key={index} className={itemCls}>{child.props.children}</ListItem>
            } else {
                return child;
            }
        })}
    </ul>
}

List.propTypes = {
    children: PropTypes.any.isRequired,
    className: PropTypes.string,
    fluid: PropTypes.bool,
}
