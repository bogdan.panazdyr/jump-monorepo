import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import { Checkbox } from '@justlook/uikit/lib/checkbox';

import cx from './list-item.module.scss';

export const ListItem = ({
        children = [],
        className,
        onClick,
        onSelect,
        selectable,
        hoverable,
        id = '',
        checked,
        disabled,
        icon = null,
    }) => {

    const cls = classnames(cx.item, className, {
        [cx.selectable]: selectable,
        [cx.hoverable]: hoverable
    });

    return (
        <li className={cls}>
            {
                selectable &&
                <div className={cx.checkboxContainer} onClick={() => onSelect(!checked)}>
                    <Checkbox
                        className={cx.checkbox}
                        id={`checkbox-list-${id}`}
                        disabled={disabled}
                        checked={checked}
                        onClick={e => e.stopPropagation()}
                    />
                </div>
            }
            <div className={cx.container} onClick={onClick}>
                <div className={cx.content}>{children}</div>
                {Boolean(icon) && <div className={cx.icon}>{icon}</div>}
            </div>
        </li>
    )
}

ListItem.defaultProps = {
    displayName: 'ListItem',
    id: '',
    disabled: false,
    selectable: false,
}

ListItem.propTypes = {
    id: PropTypes.string,
    checked: PropTypes.bool,
    icon: PropTypes.any,
    className: PropTypes.string,
    children: PropTypes.any.isRequired,
    disabled: PropTypes.bool,
    onClick: PropTypes.func,
    onSelect: PropTypes.func,
    selectable: PropTypes.bool,
    hoverable: PropTypes.bool,
}
