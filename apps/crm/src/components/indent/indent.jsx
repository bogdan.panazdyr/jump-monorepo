import React, { Children, cloneElement, Fragment } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import cx from './indent.module.scss';

const Indent = ({
        children,
        as: Component,
        outside,
        outsideTop,
        outsideRight,
        outsideBottom,
        outsideLeft,
        inside,
        insideTop,
        insideRight,
        insideBottom,
        insideLeft,
        pl = 0,
        pr = 0,
        pb = 0,
        pt = 0,
        ml = 0,
        mr = 0,
        mb = 0,
        mt = 0
    }) => {

    const compClasses = classnames(cx.indent, {
        [cx[`all_margin_${outside}`]]: Boolean(outside),
        [cx[`top_margin_${outsideTop}`]]: Boolean(outsideTop),
        [cx[`right_margin_${outsideRight}`]]: Boolean(outsideRight),
        [cx[`bottom_margin_${outsideBottom}`]]: Boolean(outsideBottom),
        [cx[`left_margin_${outsideLeft}`]]: Boolean(outsideLeft),
        [cx[`all_padding_${inside}`]]: Boolean(inside),
        [cx[`top_padding_${insideTop}`]]: Boolean(insideTop),
        [cx[`right_padding_${insideRight}`]]: Boolean(insideRight),
        [cx[`bottom_padding_${insideBottom}`]]: Boolean(insideBottom),
        [cx[`left_padding_${insideLeft}`]]: Boolean(insideLeft),
    });

    const style = {
        paddingTop: pt ? `${pt}px` : null,
        paddingRight: pr ? `${pr}px` : null,
        paddingBottom: pb ? ` ${pb}px` : null,
        paddingLeft: pl ? `${pl}px` : null,
        marginTop: mt ? `${mt}px` : null,
        marginRight: mr ? `${mr}px` : null,
        marginBottom: mb ? `${mb}px` : null,
        marginLeft: ml ? `${ml}px` : null,
    };

    if (!Component && Children.count(children) <= 1 && children.type !== Fragment && children?.props) {
        return cloneElement(children, {
            ...children.props,
            style,
            className: classnames(children.props?.className, compClasses)
        });
    }
    return Component
        ? <Component className={compClasses} style={style}>{children}</Component>
        : <section className={compClasses} style={style}>{children}</section>
}

Indent.propTypes = {
    as: PropTypes.string,
    children: PropTypes.any,
    outside: PropTypes.oneOfType([ PropTypes.string, PropTypes.array, PropTypes.object ]),
    inside: PropTypes.oneOfType([ PropTypes.string, PropTypes.array ]),
    outsideTop: PropTypes.string,
    outsideRight: PropTypes.string,
    outsideBottom: PropTypes.string,
    outsideLeft: PropTypes.string,
    insideTop: PropTypes.string,
    insideRight: PropTypes.string,
    insideBottom: PropTypes.string,
    insideLeft: PropTypes.string,
    pl: PropTypes.number,
    pr: PropTypes.number,
    pb: PropTypes.number,
    pt: PropTypes.number,
    ml: PropTypes.number,
    mr: PropTypes.number,
    mb: PropTypes.number,
    mt: PropTypes.number
}

export default Indent;
