import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Button } from '@justlook/uikit';

import cx from './tabs-navigation.module.scss';

export const Navigation = ({ navLabel, className, onChangeTab, disabled, notification, isActive, value }) => {

    const classes = classNames(
        cx.base,
        { [cx.active]: isActive },
        className
    );

    return (
        <div className={cx.wrapper}>
            <Button
                styling={'hollow'}
                className={classes}
                onClick={() => { onChangeTab(value) }}
                disabled={disabled}
            >
                {navLabel}
                {notification}
            </Button>
            <div className={cx.focus}/>
        </div>

    )
};

Navigation.defaultProps = {
    navLabel: 'Tab',
    className: '',
    onChangeTab: () => {}
};

Navigation.propTypes = {
    navLabel: PropTypes.string,
    className: PropTypes.string,
    onChangeTab: PropTypes.func,
    disabled: PropTypes.bool,
    notification: PropTypes.any,
    isActive: PropTypes.bool,
    value: PropTypes.any,
};

