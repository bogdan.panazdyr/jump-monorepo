import React, { useState } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { ParagraphV2 as Paragraph } from '@justlook/uikit';

import cx from 'components/hidden-content/hidden-content.module.scss';

export const HiddenContent = ({ label, children, position }) => {
    const [ isVisible, setIsVisible ] = useState(false);

    const cls = classnames(cx.base, { [cx.standard]: position === 'standard' });

    const onVisibleField = () => setIsVisible(true);

    return (
        <div className={cls}>
            {
                !isVisible &&
                <Paragraph.LH24 className={cx.button} onClick={onVisibleField}>
                    { label }
                </Paragraph.LH24>
            }

            { isVisible && <div className={cx.children}>{ children }</div> }
        </div>
    )
}

HiddenContent.defaultValue = {
    position: 'grid'
}

HiddenContent.propTypes = {
    label: PropTypes.string,
    children: PropTypes.any,
    position: PropTypes.oneOf(['standard', 'grid']),
}
