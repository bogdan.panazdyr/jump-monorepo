import React, { Fragment, useState } from 'react';
import PropTypes from 'prop-types';
import { Input, Button, Icons24, ParagraphV2 as Paragraph } from '@justlook/uikit';

import { idFromString } from 'utils/helpers';

import cx from 'components/editable-field/editable-field.module.scss';

export const EditableField = ({ id, label, placeholder, defaultValue, onUpdateAction, loading, error }) => {
    const key = id || idFromString(label || placeholder);

    const [disabled, setDisabled] = useState(true);
    const [value, setValue] = useState(defaultValue);

    const styling = disabled ? 'hollow-border' : 'primary';
    const icon = disabled ? <Icons24.IconPencil/> : <Icons24.IconCheck/>;

    const handleOnAction = () => {
        if(disabled) {
            setDisabled(false)
        } else {
            onUpdateAction(value);
            setDisabled(true);
        }
    }

    return (
        <Fragment>
            {label && <label htmlFor={key}> <Paragraph.LH24>{ label }</Paragraph.LH24> </label> }
            <div className={cx.base}>
                <Input
                    id={key}
                    className={cx.input}
                    disabled={error ? false : disabled}
                    value={value}
                    onChange={setValue}
                    placeholder={placeholder}
                    error={error}
                />

                <Button
                    styling={styling}
                    icon={icon}
                    loading={loading}
                    onClick={handleOnAction}
                />
            </div>
        </Fragment>
    )
}

EditableField.propTypes = {
    id: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ]),
    label: PropTypes.string,
    placeholder: PropTypes.string,
    defaultValue: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ]),
    onUpdateAction: PropTypes.func,
    loading: PropTypes.bool,
    error: PropTypes.any,
}
