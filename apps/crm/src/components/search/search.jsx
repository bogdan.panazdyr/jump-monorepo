import React, { useState, useCallback } from 'react';
import PropTypes from 'prop-types';
import { Input } from '@justlook/uikit';
import { debounce } from 'debounce';

import cx from 'components/search/search.module.scss';

export const Search = ({ onSearch, value, placeholder }) => {
    const [ search, setSearch ] = useState(value);

    // eslint-disable-next-line
    const handleOnSearch = useCallback(debounce((search) => {
        onSearch({ search, page: 1 })
    }, 500), [onSearch]);

    const handleOnChange = (search) => {
        handleOnSearch.clear();

        setSearch(search);
        handleOnSearch(search);
    };

    return (
        <div className={cx.base}>
            <Input
                placeholder={placeholder}
                value={search}
                onChange={handleOnChange}
                autoFocus={true}
            />
        </div>
    )
};

Search.propTypes = {
    onSearch: PropTypes.func,
    value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ]),
    placeholder: PropTypes.string,
};
