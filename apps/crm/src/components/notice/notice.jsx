import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import { Icons24, ParagraphV2 as Paragraph } from '@justlook/uikit';
import { useIfMediaScreen } from '@justlook/uikit/lib/hooks';

import cx from 'components/notice/notice.module.scss';

const Notice = ({ onClose, icon, label, className, children }) => {
    const isMobile = useIfMediaScreen();
    const Icon = Icons24[icon];

    const cxs = classnames(cx.base, className)

    return (
        <div className={cxs}>
            <div className={cx.content}>
                {!isMobile && <Icon className={cx['icon-stick']}/>}

                <Paragraph.LH24>
                    { label || children }
                </Paragraph.LH24>
            </div>

            {onClose &&
                <Icons24.IconCross
                    className={cx['icon-cross']}
                    onClick={onClose}
                />
            }
        </div>
    )
}

Notice.defaultProps = {
    icon: 'IconStick'
}

Notice.propTypes = {
    onClose: PropTypes.func,
    icon: PropTypes.string,
    label: PropTypes.any,
    children: PropTypes.any,
    className: PropTypes.string,
}

export default Notice;
