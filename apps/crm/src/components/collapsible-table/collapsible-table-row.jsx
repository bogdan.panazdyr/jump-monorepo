import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Icons24 } from '@justlook/uikit';

import { Cell } from './collapsible-table-cell';

import cx from './collapsible-table-row.module.scss';

export const Row = ({ children, cells, onClick, active, nested = false, ...rest  }) => {
    const Icon = Icons24[active ? 'IconArrowTop' : 'IconArrowDown'];

    const inlineStyle = {
        gridTemplateColumns: `${nested ? 307 : 347}px repeat(${cells.length - 1}, 1fr)`,
        cursor: onClick ? 'pointer' : 'default',
    }

    const handleOnClick = (e) => {
        if (onClick) {
            onClick(e);
        }
    }

    return (
        <Fragment>
            <div className={cx.base} onClick={handleOnClick} style={inlineStyle} {...rest}>
                {cells.map((item, index) => (
                    <Cell key={index + item} className={index === 0 && cx.firstCell}>
                        {
                            index === 0 && onClick &&
                            <span className={cx.arrow}>
                                <Icon/>
                            </span>
                        }

                        { item }
                    </Cell>
                ))}
            </div>

            { children }
        </Fragment>
    )
}

Row.propTypes = {
    children: PropTypes.any,
    cells: PropTypes.array,
    onClick: PropTypes.func,
    active: PropTypes.bool,
    nested: PropTypes.bool,
}
