import React from 'react';
import PropTypes from 'prop-types';
import classes from 'classnames';
import { ParagraphV2 as Paragraph } from '@justlook/uikit';

import cx from './collapsible-table-cell.module.scss';

export const Cell = ({ children, className, ...rest }) => {

    const cls = classes(cx.base, className);

    return (
        <div className={cls} {...rest}>
            <Paragraph.LH24>
                { children }
            </Paragraph.LH24>
        </div>
    )
}

Cell.propTypes = {
    children: PropTypes.any.isRequired,
    className: PropTypes.any,
}
