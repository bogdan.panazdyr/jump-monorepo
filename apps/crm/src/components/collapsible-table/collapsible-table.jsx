import React from 'react';
import PropTypes from 'prop-types';
import { Description, Icons16 } from '@justlook/uikit';

import { Row } from './collapsible-table-row';
import { Cell } from './collapsible-table-cell';
import { Collapse } from './collapsible-table-collapse';
import { Loader } from './collapsible-table-loader';
import { Load } from './collapsible-table-load';

import cx from './collapsible-table.module.scss';

export const CollapsibleTable = ({ headerCell, children, ...rest }) => {
    return(
        <div className={cx.base} {...rest}>
            <div className={cx.head} style={{ gridTemplateColumns: `347px repeat(${headerCell.length - 1}, 1fr)` }}>
                {headerCell.map(item =>
                    <Description.LH24 key={item.label} className={cx.cell}>
                        {item.label}
                        {item.sort && <Icons16.IconSortDown/>}
                    </Description.LH24>
                )}
            </div>

            { children }
        </div>
    )
}

CollapsibleTable.Row = Row;
CollapsibleTable.Cell = Cell;
CollapsibleTable.Collapse = Collapse;
CollapsibleTable.Loader = Loader;
CollapsibleTable.Load = Load;

CollapsibleTable.propTypes = {
    headerCell: PropTypes.array.isRequired,
    children: PropTypes.any,
}
