import React from 'react';
import classname from 'classnames';
import PropTypes from 'prop-types';

import cx from './collapsible-table-collapse.module.scss';

export const Collapse = ({ children, className, style, nested, ...rest }) => {
    const cls = classname(cx.base, className);

    const inlineStyle = {
        gridTemplateColumns: `${nested ? 267 : 307}px repeat(${children.length - 1}, 1fr)`,
        ...style,
    }

    return (
        <div className={cls} style={inlineStyle} {...rest}>
            { children }
        </div>
    )
}

Collapse.propTypes = {
    children: PropTypes.any,
    className: PropTypes.string,
    style: PropTypes.object,
    nested: PropTypes.bool,
}
