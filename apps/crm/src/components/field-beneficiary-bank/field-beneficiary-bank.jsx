import React from 'react';
import PropTypes from 'prop-types';
import { debounce } from 'debounce';
import { SelectAsync, ParagraphV2 as Paragraph } from '@justlook/uikit';

import Api from 'api/client';

import cx from 'components/field-beneficiary-bank/field-beneficiary-bank.module.scss';

const loadFetchBanks = debounce((search, callback) => {
    Api.banks
        .fetchBanksList({ search })
        .then(options => {
            const prepared = options.items.map(option => ({
                value: option?.bik,
                label: option?.name,
            }));

            return callback(prepared);
        })
}, 1000);

export const BeneficiaryBank = ({ name, label, onChange, value, placeholder, error }) => {
    return (
        <div className={cx.grid}>
            <label><Paragraph.LH24>{label}</Paragraph.LH24></label>
            <SelectAsync
                name={name}
                loadOptions={loadFetchBanks}
                onChange={onChange}
                defaultValue={value ? value : null}
                noOptionsMessage={() => <div>{placeholder}</div>}
                error={error}
            />
        </div>
    )
}

BeneficiaryBank.propTypes = {
    name: PropTypes.string,
    label: PropTypes.string,
    onChange: PropTypes.func,
    value: PropTypes.object,
    placeholder: PropTypes.string,
    error: PropTypes.any,
}
