import React from 'react';
import PropTypes from 'prop-types';
import { ParagraphV2 as Paragraph, Description, Button, Icons24 } from '@justlook/uikit';
import { useIfMediaScreen } from '@justlook/uikit/lib/hooks';

import { TableCell } from './table-cell';
import { TableMobileRow } from './table-mobile-row'
import { TablePopup } from './table-popup';

import cx from './table-row.module.scss';

export const TableRow = ({ renderCheckBox, customCellByAccessor, renderControlCell, data, accessor, ...rest }) => {
    const isMobile = useIfMediaScreen();

    const renderCells = (object) => {
        const cells = [];

        if (customCellByAccessor) {
            for (let i = 0; i < customCellByAccessor.length; i++) {
                cells.push(
                    <TableCell key={i}>
                        {customCellByAccessor[i].content
                            ? customCellByAccessor[i].content
                            : (
                                <Paragraph.LH24>
                                    {object[customCellByAccessor[i].accessor]}
                                </Paragraph.LH24>
                            )
                        }
                    </TableCell>
                )
            }
            return cells;
        }

        for (let i = 0; i < accessor.length; i++) {
            cells.push(
                <TableCell key={i}>
                    <Paragraph.LH24>
                        {object[accessor[i]]}
                    </Paragraph.LH24>
                </TableCell>
            )
        }
        return cells;
    }

    if (isMobile) {
        return (
            <TableMobileRow>
                <Paragraph.LH24 className={cx.paragraph}>{data[customCellByAccessor[0].accessor]}</Paragraph.LH24>
                <Description.LH24 className={cx.description}>{data[customCellByAccessor[1].accessor]}</Description.LH24>

                <TablePopup
                    renderProps={(open) => (
                        <Button
                            className={cx.button}
                            styling='hollow'
                            icon={ <Icons24.IconInfo className={cx.icon} /> }
                            onClick={open}
                        />
                    )}
                    data={data}
                    accessor={accessor}
                    customCellByAccessor={customCellByAccessor}
                    renderControlCell={renderControlCell}
                />
            </TableMobileRow>
        )
    }

    return (
        <div className={cx.base} {...rest}>
            {renderCheckBox && (
                <TableCell className={cx['children-cell']}>
                    {renderCheckBox}
                </TableCell>
            )}

            {renderCells(data)}

            {renderControlCell &&
                <TableCell className={cx['custom-cell']}>
                    {renderControlCell}
                </TableCell>
            }
        </div>
    )
}

TableRow.propTypes = {
    renderCheckBox: PropTypes.any,
    data: PropTypes.object.isRequired,
    accessor: PropTypes.array,
    renderControlCell: PropTypes.any,
    customCellByAccessor: PropTypes.arrayOf(
        PropTypes.shape({
            accessor: PropTypes.string,
            content: PropTypes.any,
        })
    )
}
