import React                    from 'react';
import PropTypes                from 'prop-types';
import { Description, Icons16 } from '@justlook/uikit';
import { useIfMediaScreen } from '@justlook/uikit/lib/hooks';

import { TableCell } from './table-cell';

import cx from './table-head.module.scss';

export const TableHead = ({ renderCheckBox, cells, onSort, sortField }) => {
    const isMobile = useIfMediaScreen();

    const sign = sortField.slice(0, 1);
    const direction = sign.search(/[+-]/) === 0 ? sign : '+';
    const field = sign.search(/[+-]/) === 0 ? sortField.slice(1) : sortField;

    const onClickSort = (selectedField) => {
        if (!selectedField) return

        const nextDirection = selectedField === field ? (direction === '+' ? '-' : '+') : '+';
        onSort(`${nextDirection}${selectedField}`)
    }

    if(isMobile) return null;

    return (
        <div className={cx.base}>
            {renderCheckBox && (
                <TableCell className={cx['children-cell']}>
                    {renderCheckBox}
                </TableCell>
            )}
            {cells.map(item => (
                <TableCell
                    key={item.label}
                    className={`${item?.sort && cx.hover} ${item?.sort === field && cx.active}`}
                    onClick={() => onClickSort(item?.sort)}
                >
                    <Description.LH24 element='span'>{item.label}</Description.LH24>
                    {item?.sort === field
                        ? direction === '+'
                            ? <Icons16.IconSortDown/>
                            : <Icons16.IconSortUp/>
                        : null
                    }
                </TableCell>
            ))}
        </div>
    )
}

TableHead.defaultProps = {
    sortField: ''
}

TableHead.propTypes = {
    renderCheckBox: PropTypes.any,
    cells: PropTypes.array.isRequired,
    onSort: PropTypes.any,
    sortField: PropTypes.string,
}
