import { Table } from './table';
import { TableHead } from './table-head';
import { TableRow } from './table-row';

export {
    Table,
    TableHead,
    TableRow,
}
