import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Button, Icons24, Header, ParagraphV2 as Paragraph } from '@justlook/uikit';
import { Hooks } from '@justlook/core';

import cx from './table-popup.module.scss';

export const TablePopup = ({ data, customCellByAccessor, renderControlCell, renderProps }) => {

    const { isShow, onOpenForm, onCloseForm } = Hooks.useModalState();

    return (
        <Fragment>
            {renderProps(onOpenForm)}
            {isShow &&
            <div className={cx.base}>
                <div className={cx.shadow}/>
                <div className={cx.content}>
                    <div className={cx.header}>
                        <Header.H3>Lorem ipsum</Header.H3>
                        <Button styling='hollow' icon={<Icons24.IconClose/>} onClick={() => onCloseForm()}/>
                    </div>

                    {customCellByAccessor.map((item, index) => {
                        if(item.content) {
                            return (
                                <Paragraph.LH24 key={index} className={cx.paragraph}>
                                    {item.title}: {item.content}
                                </Paragraph.LH24>
                            )
                        }
                        return (
                            <Paragraph.LH24 key={index} className={cx.paragraph}>
                                {item.title}: {data[item.accessor]}
                            </Paragraph.LH24>
                        )
                    })}

                    <div className={cx.buttons}>
                        {renderControlCell}
                    </div>
                </div>
            </div>
            }
        </Fragment>
    )
}

TablePopup.propTypes = {
    data: PropTypes.object,
    customCellByAccessor: PropTypes.array,
    renderControlCell: PropTypes.any,
    renderProps: PropTypes.func,

}
