import React, { memo } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import cx from './label.module.scss';

export const Label = memo(({text, color, className}) => {
    const classes = classnames(cx.label, className, {
        [cx.success]: color === 'success',
        [cx.info]: color === 'info',
        [cx.warning]: color === 'warning',
        [cx.danger]: color === 'danger',
    })

    if(!text) return;
    return (
        <span className={classes}>
            {text}
        </span>
    )
});


Label.propTypes = {
    text: PropTypes.string,
    color: PropTypes.string,
    className: PropTypes.string,
}

