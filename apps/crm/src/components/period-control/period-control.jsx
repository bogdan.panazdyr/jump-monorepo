import React, { useState, useEffect, useMemo } from 'react';
import PropTypes from 'prop-types';
import { parse } from 'query-string';
import {
    differenceInCalendarDays, differenceInCalendarMonths,
    isSameMonth, isLastDayOfMonth, isFirstDayOfMonth, getDate
} from 'date-fns';
import { useNavigate, useLocation } from 'react-router-dom';

import { useIfMediaScreen } from '@justlook/uikit/src/hooks';
import { Utils } from '@justlook/core';
import { CalendarPeriod } from '@justlook/uikit';

import { getPeriodOfPath } from 'utils/get-period-of-path';
import { Box, TabsNavigation as Tab } from 'components';
import cx from 'components/period-control/period-control.module.scss';

const INNER_PATH = {
    WEEK: 'week',
    MONTH: 'month',
    QUARTER: 'quarter',
    YEAR: 'year',
}

export const PeriodControl = ({ isRequest }) => {
    const { pathname, search, hash } = useLocation();
    const navigate = useNavigate();
    const path = useMemo(() => parse(hash), [hash]);
    const { date_from, date_to } = useMemo(() => getPeriodOfPath(path?.period || path), [path]);

    const [ sortDate, setSortDate ] = useState({ from: undefined, to: undefined });
    const [ period, setPeriod ] = useState(path.period || 'week');
    const isMobile = useIfMediaScreen();

    useEffect(() => {
        setSortDate({
            from: new Date(Utils.Dates.toISO8601(date_from)) || undefined,
            to: new Date(Utils.Dates.toISO8601(date_to)) || undefined
        })
    }, [ path.period, date_from, date_to ])

    const countsThePeriod = (value = {}) => {
        const sameMonth = isSameMonth(value.from, value.to);
        const differenceMonth = differenceInCalendarMonths(value.to, value.from);
        const differenceDay = differenceInCalendarDays(value.to, value.from);

        const fromDate = Utils.Dates.formatDate(value.from, 'yyyy-MM-dd');
        const toDate = Utils.Dates.formatDate(value.to, 'yyyy-MM-dd');
        const calendarPath = `#from=${fromDate}&to=${toDate}`;

        if(sameMonth) {
            if(differenceDay === 6 || differenceDay === 7) {
                return navigate(pathname + `${search}${calendarPath}&period-calendar=${INNER_PATH.WEEK}`)
            }

            if(isFirstDayOfMonth(value.from) && isLastDayOfMonth(value.to)) {
                return navigate(pathname + `${search}${calendarPath}&period-calendar=${INNER_PATH.MONTH}`)
            }

            return navigate(pathname + search + calendarPath);
        }

        if(differenceMonth === 1 && getDate(value.from) === getDate(value.to)) {
            return navigate(pathname + `${search}${calendarPath}&period-calendar=${INNER_PATH.MONTH}`)
        }

        if(differenceMonth === 6) {
            return navigate(pathname + `${search}${calendarPath}&period-calendar=${INNER_PATH.QUARTER}`)
        }

        if(differenceMonth === 12) {
            return navigate(pathname + `${search}${calendarPath}&period-calendar=${INNER_PATH.YEAR}`)
        }

        return navigate(pathname + search + calendarPath);
    }

    const onChangePeriod = (value) => {
        if(value instanceof Object) {
            if(value.from && value.to) countsThePeriod(value);

            if(value.from === undefined && value.to === undefined) {
                navigate(pathname + `${search}#period=${period}`);
            }

            return;
        } else {
            setSortDate({ from: undefined, to: undefined })
            navigate(pathname + `${search}#period=${value}`);
        }

        setPeriod(value);
    }

    const handleOnChangeCalendarPeriod = (value) => {
        setSortDate({from: value.from, to: value.to})
    }

    const emptyHash = !path['period-calendar'] && !path.period && !path.from && !path.to;
    const hashCalendarPeriod = path['period-calendar'] === INNER_PATH.WEEK;
    const hashPeriod = path.period === INNER_PATH.WEEK;

    return (
        <Box className={cx.filter}>
            <div className={cx.navigation}>
                <Tab navLabel='Неделя' value='week'
                     disabled={isRequest} onChangeTab={onChangePeriod}
                     isActive={emptyHash || hashCalendarPeriod || hashPeriod}
                />
                <Tab navLabel='Месяц' value='month'
                     disabled={isRequest} onChangeTab={onChangePeriod}
                     isActive={path.period === INNER_PATH.MONTH || path['period-calendar'] === INNER_PATH.MONTH}
                />
                <Tab navLabel='Квартал' value='quarter'
                     disabled={isRequest} onChangeTab={onChangePeriod}
                     isActive={path.period === INNER_PATH.QUARTER || path['period-calendar'] === INNER_PATH.QUARTER}
                />
                <Tab navLabel='Год' value='year'
                     disabled={isRequest} onChangeTab={onChangePeriod}
                     isActive={path.period === INNER_PATH.YEAR || path['period-calendar'] === INNER_PATH.YEAR}
                />
            </div>


            <CalendarPeriod
                disabled={isRequest}
                placeholder='Дата'
                value={sortDate}
                small={isMobile}
                onChange={handleOnChangeCalendarPeriod}
                onApply={onChangePeriod}
            />
        </Box>
    )
}

PeriodControl.propTypes = {
    isRequest: PropTypes.bool,
}
