import React from 'react';
import { Ellipsis } from '@justlook/uikit';

import cx from './splash-screen.module.scss';

export const SplashScreen = () => {
    return (
        <div className={cx.container}>
            <Ellipsis/>
        </div>
    );
}
