import React, { useState } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { Icons24 } from '@justlook/uikit';

import cx from './input.module.scss';

export const Password = React.forwardRef((
    {
        id,
        name,
        label,
        error,
        className,
        onChange,
        value,
        disabled,
        labelPosition,
        ...rest
    }, ref) => {

    const [ controlType, changeControlType ] = useState('password');

    const clsName = classnames(cx.base, className, {
        [cx['label-left']]: labelPosition === 'left',
        [cx['label-top']]: labelPosition === 'top',
        [ cx.error ]: error
    });
    const isPassword = controlType === 'password';

    const onInvert = () => changeControlType(isPassword ? 'text' : 'password');
    const handlerOnChange = (e) => onChange(e.target.value || '');

    return (
        <div className={clsName}>
            {label && <label htmlFor={id} className={cx.label}>{label}</label>}

            <div className={cx['input-wrapper']}>
                <input
                    id={id}
                    name={name}
                    ref={ref}
                    className={cx.input}
                    autoComplete='on'
                    type={controlType}
                    value={value}
                    onChange={handlerOnChange}
                    {...rest}
                />

                {
                    !disabled && (
                        <div className={cx.control} onClick={onInvert}>
                            { isPassword ? <Icons24.IconEyeClose/> : <Icons24.IconEyeOpen/> }
                        </div>
                    )
                }
            </div>

            {error && <span className={cx[ 'error-message' ]}>{error}</span>}
        </div>
    );
})

Password.defaultProps = {
    label: '',
    className: '',
    onChange: () => {},
    labelPosition: 'left',
};

Password.propTypes = {
    id: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]),
    name: PropTypes.string,
    label: PropTypes.string,
    className: PropTypes.string,
    value: PropTypes.any,
    onChange: PropTypes.func,
    error: PropTypes.any,
    disabled: PropTypes.bool,
    labelPosition: PropTypes.oneOf([ 'top', 'left' ]),
};

export const Input = React.forwardRef((
    {
        id,
        type,
        label,
        className,
        error,
        labelPosition,
        disabled,
        value,
        onChange,
        isClearable,
        readOnly,
        ...rest
    }, ref) => {

    const cls = classnames(cx.base, className, {
        [cx['label-left']]: labelPosition === 'left',
        [cx['label-top']]: labelPosition === 'top',
        [cx.error]: error,
    })

    const onClear = () => { onChange('') };
    const handlerOnChange = (e) => onChange(e.target.value || '');

    return (
        <div className={cls}>
            {label && <label htmlFor={id} className={cx.label}>{label}</label>}

            <div className={cx['input-wrapper']}>
                <input
                    id={id}
                    type={type}
                    value={value}
                    onChange={handlerOnChange}
                    className={cx.input}
                    readOnly={readOnly}
                    disabled={disabled}
                    ref={ref}
                    {...rest}
                />
                {value && !disabled && isClearable &&
                    <div className={cx.control} onClick={onClear}>
                        <Icons24.IconClose/>
                    </div>
                }
            </div>
            {error && <span className={cx['error-message']}>{error}</span>}
        </div>
    )
})

Input.defaultProps = {
    labelPosition: 'left',
    label: '',
    value: '',
    className: '',
    type: 'text',
    onChange: () => {},
    isClearable: true,
    readOnly: false,
}

Input.propTypes = {
    id: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]),
    type: PropTypes.string,
    label: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]),
    error: PropTypes.any,
    labelPosition: PropTypes.oneOf([ 'top', 'left' ]),
    className: PropTypes.string,
    isClearable: PropTypes.bool,
    value: PropTypes.any,
    disabled: PropTypes.bool,
    readOnly: PropTypes.bool,
    onChange: PropTypes.func,
}
