import React from 'react';
import PropTypes from 'prop-types';
import classes from 'classnames';

import { Config } from '@justlook/core';
import { Icons24, Icons16 } from '@justlook/uikit';
import { useIfMediaScreen } from '@justlook/uikit/src/hooks';

import { IconBar } from 'components/icon-bar';
import { Table } from 'components/new-table';

import cx from './receipt-cell.module.scss';

function ReceiptCell ({ payment, className }) {
    const isMobile = useIfMediaScreen();

    const { id, receipt } = payment;
    const { key: receiptNumber, status, can_cancel, cancel_reason } = receipt || {};

    const isCanceled = status === 'canceled' || !can_cancel;
    const iconClass = isCanceled ? cx.cancel : '';
    const iconLabel = isCanceled ? cancel_reason : 'Чек ' + receiptNumber;

    const Icon = isMobile ? Icons16.IconReceipt : Icons24.IconReceipt;

    const cls = classes(cx.base, className, {
        [cx.pointer]: receipt,
    })

    return (
        <Table.Cell
            className={cls}
            data-number={receiptNumber}
            data-id={receipt ? id : ''}
            onClick={receipt && openReceipt}
        >
            {
                receipt && (
                    <IconBar
                        className={cx.center}
                        data={{
                            icon: <Icon className={iconClass}/>,
                            label: iconLabel
                        }}
                        positionBar={isMobile ? null : 'top-right'}
                    />
                )
            }
        </Table.Cell>
    )
}

ReceiptCell.propTypes = {
    payment: PropTypes.any,
    className: PropTypes.string,
}

function openReceipt(e) {
    e.preventDefault();
    e.stopPropagation();

    const target = e.currentTarget;
    const {id, number } = target.dataset;

    if (!id) {
        return false;
    }

    window.open(
        Config.api_url.v2 + `payments/${id}/receipt`,
        `Чек №${number}`,
        'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=330,height=700'
    );
}

export default ReceiptCell;
