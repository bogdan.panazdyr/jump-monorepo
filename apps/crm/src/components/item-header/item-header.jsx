import React from 'react';
import PropTypes from 'prop-types';

import { Header } from '@justlook/uikit';

import cx from 'components/item-header/item-header.module.scss';

export const ItemHeader = ({ header, description, children }) => {
    return (
        <div className={cx.base}>
            <div>
                {header && <Header.H1>{header}</Header.H1>}
                {description && <div className={cx.description}>{description}</div>}
            </div>

            {children}
        </div>
    )
}

ItemHeader.propTypes = {
    header: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]),
    description: PropTypes.any,
    children: PropTypes.any,
}
