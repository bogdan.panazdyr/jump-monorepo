import React, { useState } from 'react';
import PropTypes from 'prop-types';

import { Button, Icons24 } from '@justlook/uikit';

import { snackBar } from 'components/snackbar';
import { download } from 'utils/download';

export const DownloadButton = ({ link, file }) => {
    const [ loading, setLoading ] = useState(false);

    const actionDownload = () => {
        setLoading(true);
        download({ url: link, attacheFile: file })
            .then(() => setLoading(false))
            .catch((e) => {
                setLoading(false)
                snackBar(e.message)
            })
    }

    return (
        <Button
            styling='hollow-border'
            icon={<Icons24.IconDownload/>}
            loading={loading}
            onClick={actionDownload}
        >
            Скачать
        </Button>
    )
}

DownloadButton.propTypes = {
    link: PropTypes.string.isRequired,
    file: PropTypes.string.isRequired,
}
