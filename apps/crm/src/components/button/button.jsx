import { Description, Button as BaseButton } from '@justlook/uikit';
import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';
import cx from './button.module.scss';

const Button = ({active, className, onClick, disabled, badge, children, ...rest}) => {
    const classes = classNames(cx.base, { [cx.active]: active }, className);

    return (
        <div className={cx.wrapper}>
            <BaseButton
                styling={'hollow'}
                className={classes}
                onClick={onClick}
                disabled={disabled}
                {...rest}
            >
                { children }

                {
                    !!badge && (
                        <Description.LH16 element='span' className={cx.badge}>
                            { badge }
                        </Description.LH16>
                    )
                }
            </BaseButton>
            <div className={cx.focus}/>
        </div>

    )
}

Button.defaultProps = {
    onClick: () => {}
};

Button.propTypes = {
    children: PropTypes.any,
    badge: PropTypes.any,
    disabled: PropTypes.bool,
    active: PropTypes.bool,
    className: PropTypes.string,
    onClick: PropTypes.func,
};

export default Button;
