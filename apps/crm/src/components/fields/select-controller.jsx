import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Controller } from 'react-hook-form';
import { Select, Form } from '@justlook/uikit';

export const SelectController = ({ name, isRequired, options, control, error, placeholder, className, defaultValue, onSelect }) => {
    const [ selectedValue, setSelectedValue ] = useState(defaultValue || null);

    return (
        <Form.Field>
            <Controller
                name={name}
                control={control}
                defaultValue={defaultValue?.value}
                rules={isRequired && { required: 'Обязательно для ввода' }}
                render={({ onChange, ...rest }) => (
                    <Select
                        {...rest}
                        options={options}
                        value={selectedValue}
                        onChange={(option) => {
                            setSelectedValue(option)
                            onChange(option?.value)
                            onSelect(option?.value)
                        }}
                        className={className}
                        placeholder={placeholder}
                        error={error}
                    />
                )}
            />
        </Form.Field>
    )
}

SelectController.defaultProps = {
    onSelect: () => {},
}

SelectController.propTypes = {
    name: PropTypes.string,
    options: PropTypes.array,
    control: PropTypes.object,
    error: PropTypes.string,
    placeholder: PropTypes.string,
    onSelect: PropTypes.func,
    isRequired: PropTypes.bool,
    className: PropTypes.string,
    defaultValue: PropTypes.object,
}
