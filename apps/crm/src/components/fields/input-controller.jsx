import React from 'react';
import PropTypes from 'prop-types';
import { Controller } from 'react-hook-form';
import { Form, Input } from '@justlook/uikit';

export const InputController = ({ label, name, type, control, placeholder, defaultValue, className, error, isRequired, onChange, ...props }) => {
    return (
        <Form.Field>
            <Controller
                control={control}
                name={name}
                defaultValue={defaultValue}
                rules={isRequired && { required: 'Обязательно для ввода' }}
                render={({ onChange: onChangeControl, ...rest }) => (
                    <Input
                        type={type}
                        label={label}
                        className={className}
                        placeholder={placeholder}
                        error={error}
                        onChange={(value) => {
                            onChangeControl(value)
                            onChange(value)
                        }}
                        {...rest}
                        {...props}
                    />
                )}
            />
        </Form.Field>
    )
}

InputController.defaultProps = {
    onChange: () => {},
}

InputController.propTypes = {
    label: PropTypes.string,
    name: PropTypes.string,
    control: PropTypes.object,
    placeholder: PropTypes.string,
    defaultValue: PropTypes.any,
    className: PropTypes.string,
    error: PropTypes.string,
    onChange: PropTypes.func,
    isRequired: PropTypes.bool,
    type: PropTypes.string,
}
