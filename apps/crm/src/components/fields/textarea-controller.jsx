import React from 'react';
import PropTypes from 'prop-types';
import { Controller } from 'react-hook-form';
import { Form, ParagraphV2 as Paragraph, Textarea } from '@justlook/uikit';

export const TextareaController = ({ label, name, control, placeholder, className, error, isRequired }) => {
    return (
        <Form.Field>
            <div className={className}>
                { label && <label><Paragraph.LH24>{ label }</Paragraph.LH24></label> }

                <Controller
                    as={Textarea}
                    name={name}
                    control={control}
                    placeholder={placeholder}
                    rules={isRequired && { required: 'Обязательно для ввода' }}
                    error={error}
                />
            </div>
        </Form.Field>
    )
}

TextareaController.propTypes = {
    label: PropTypes.string,
    name: PropTypes.string,
    control: PropTypes.object,
    placeholder: PropTypes.string,
    className: PropTypes.string,
    error: PropTypes.string,
    isRequired: PropTypes.bool,
}
