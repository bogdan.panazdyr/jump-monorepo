import React, { useCallback, useEffect } from 'react';
import PropTypes from 'prop-types';
import { debounce } from 'debounce';

import { Input, ParagraphV2 as Paragraph, Description, Icons24 } from '@justlook/uikit';

import { usePhoneField } from 'hooks/use-phone-field';
import { useSearchContractor } from 'domains/contractors';

import cx from './search-contractor.module.scss';

export const SearchContractor = React.forwardRef((
    {
        onSelect = () => {},
        onManualChange,
        changeNeedCreate,
        value,
        onChange,
        name,
        placeholder,
        label,
        changeIsSearching,
        ...rest
    }, ref
) => {
    const { onSearch, result, isPhone, needCreate, isLoading, onClearState } = useSearchContractor();

    const { isPhoneEntered, onChangeField, valueField, maskPhone, onReset } = usePhoneField(value);

    useEffect(() => { changeIsSearching(isLoading) }, [changeIsSearching, isLoading])

    useEffect(() => {
        if(isPhone && needCreate) changeNeedCreate(needCreate);
    }, [  isPhone, needCreate, changeNeedCreate ])

    // eslint-disable-next-line
    const fetchContractor = useCallback(debounce((value) => {
        const canSearchPermission = value.length >= 2 && !value.includes('_');

        if(canSearchPermission) {
            onSearch(value);
        }
    }, 500), [ onSearch ]);

    const onSelectHint = async (item) => {
        await onReset();
        await onSelect(item);
        await onClearState();
    }

    const handleOnChangeField = (value) => {
        onChangeField(value);

        fetchContractor.clear();
        fetchContractor(value);

        onManualChange(value);
        onClearState()
        onChange(value)
    }

    const renderEmptySearch = () => {
        return (
            <div className={cx.options}>
                <div className={cx.emptyOption}>
                    <Paragraph.LH24>
                        Получатель не найден в списке ваших исполнителей. Для перевода новому исполнителю, введите его номер телефона
                    </Paragraph.LH24>
                </div>
            </div>
        )
    }

    const renderResultSearch = () => {
        return (
            <div className={cx.options}>
                {result.map(item => {
                    const fullNameContractor = [
                        item.last_name,
                        item.first_name,
                        item.middle_name,
                    ];

                    return (
                        <div key={item.id} className={cx.option} onClick={() => onSelectHint(item)}>
                            <Paragraph.LH24>
                                {fullNameContractor.filter(Boolean).join(' ')}
                            </Paragraph.LH24>
                            <Description.LH16 className={cx.phone}>
                                {item.phone}
                            </Description.LH16>
                        </div>
                    )
                })}
            </div>
        )
    }

    return (
        <div ref={ref} className={cx.base}>
            <div className={cx.searchField}>
                <Input
                    type={isPhoneEntered ? 'phone' : 'text'}
                    name={name}
                    label={label}
                    value={valueField}
                    mask={maskPhone}
                    onChange={handleOnChangeField}
                    placeholder={placeholder}
                    className={cx.input}
                    {...rest}
                />
                {isLoading && <Icons24.IconLoader/>}
            </div>

            {!isPhone && needCreate && renderEmptySearch()}
            {result.length > 0 && renderResultSearch()}
        </div>
    )
});

SearchContractor.propTypes = {
    onSelect: PropTypes.func.isRequired,
    name: PropTypes.string.isRequired,
    value: PropTypes.any,
    onChange: PropTypes.func,
    onManualChange: PropTypes.func,
    placeholder: PropTypes.string,
    label: PropTypes.string,
    changeIsSearching: PropTypes.func,
    changeNeedCreate: PropTypes.func,
};
