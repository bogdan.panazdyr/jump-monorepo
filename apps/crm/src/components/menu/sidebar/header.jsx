import React, { useState, useRef } from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import { NavLink as Link } from 'react-router-dom';
import classNames from 'classnames';

import { Header as Heading, Description, Item, Icons24, Button } from '@justlook/uikit';

import { getIsRequest } from 'app/auth/store/selectors';
import { useTestMode } from 'hooks/use-testing';

import { useClickOutside } from '../hooks';
import { useLogout, useBrandName } from '../index';

import cx from './sidebar.module.scss';

export const Header = ({ user, menu }) => {
    const [ dropMenuShow, setDropMenuShow ] = useState(false);
    const dispatch = useDispatch();
    const dropdownRef = useRef();
    const brandName = useBrandName();
    const onLogoutAction = useLogout();
    const isLogouting = useSelector(getIsRequest);
    const { isTestMode } = useTestMode();

    const onLogout = () => dispatch(onLogoutAction());

    useClickOutside(dropdownRef, dropMenuShow, setDropMenuShow);

    const avatarCls = classNames(cx.avatar, {
        [cx.test_mode]: isTestMode
    })

    return (
        <div className={cx.menu_header}>
            <div className={cx.menu_header_info}
                 onClick={() => setDropMenuShow(true)}>
                <div className={cx.menu_avatar_container}>
                    <div className={avatarCls}>{user.getAvatarLetter()}</div>
                </div>
                <div className={cx.menu_header_title}>
                    <div className={cx.text_primary_wrapper}>
                        <Heading.H3 className={cx.text_primary}>
                            {user.getCompany()}
                        </Heading.H3>
                        <Icons24.IconArrowDown className={cx.dropdown}/>
                    </div>

                    <Description.LH16>{brandName}</Description.LH16>
                </div>
            </div>

            {dropMenuShow && menu &&
            <div className={cx.dropdown_menu} ref={dropdownRef}>
                <div className={cx.dropdown_menu_header}>
                    <div className={cx.dropdown_menu_avatar}>
                        <Icons24.IconUser/>
                    </div>
                    <Heading.H3 className={cx.name}>
                        {user.getFullName()}
                    </Heading.H3>
                    <Description.LH16 className={cx.description}>
                        {user.getEmail()}
                    </Description.LH16>
                </div>

                {menu.map((item, index) => (
                    <Link to={item.to} className={cx.dropdown_menu_link}
                          key={index}>
                        <Item>
                            <Item.Description>
                                {item.label}
                            </Item.Description>
                        </Item>
                    </Link>
                ))}

                <Button
                    styling='hollow'
                    className={cx.logout}
                    disabled={isLogouting}
                    loading={isLogouting}
                    onClick={onLogout}
                >
                    Выход
                </Button>
            </div>
            }
        </div>
    );
};

Header.propTypes = {
    user: PropTypes.object,
    menu: PropTypes.array,
};
