import React, { Fragment, useState } from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import classes from 'classnames';
import { Icons24 } from '@justlook/uikit';

import { useMatchRootLocation, useMatchLocation } from '../hooks';
import { useMenu } from '../index';

import { isDesktop, getCoords, isTablet, isMobile } from 'utils/browser';

import cx from './sidebar.module.scss';

function MenuLink ({ to, children, ...rest }) {
    const isExternal = to.startsWith('http');

    return isExternal
        ? <a href={to} {...rest}>{children}</a>
        : <NavLink to={to} {...rest}>{children}</NavLink>;
}

MenuLink.propTypes = {
    to: PropTypes.string.isRequired,
    children: PropTypes.any,
};

export function Menu ({ items }) {
    const isMatch = useMatchRootLocation();
    const { isCollapsedDesktop, isExpandedMobile } = useMenu();

    return (
        <ul className={cx.sidebar_top_level_items}>
            {
                items.map(item => {
                    return (
                        <MenuItem
                            key={item.to}
                            item={item}
                            isActive={isMatch(item)}
                            isCollapsedDesktop={isCollapsedDesktop}
                            isExpandedMobile={isExpandedMobile}
                        />
                    );
                })
            }
        </ul>
    );
}

Menu.propTypes = {
    items: PropTypes.array,
};

MenuItem.propTypes = {
    item: PropTypes.object,
    isActive: PropTypes.bool,
    isCollapsedDesktop: PropTypes.bool,
    isExpandedMobile: PropTypes.bool,
};

function MenuItem ({ item, isActive, isCollapsedDesktop, isExpandedMobile }) {
    const [ hover, setHover ] = useState(false);
    const [ style, setStyle ] = useState(null);
    const { to, items = [] } = item;

    const onHover = (e) => {
        const top = getCoords(e.currentTarget).top;

        const width = isTablet()
            ? (isExpandedMobile ? 251 : 71)
            : (isCollapsedDesktop ? 71 : 251);

        setStyle({
            display: 'block',
            transform: `translate3d(${width}px, ${top}px, 0px)`,
        });

        if (isTablet() && !isExpandedMobile) {
            return setHover(true);
        }

        if (isTablet() && isExpandedMobile && !isActive && items.length) {
            return setHover(true);
        }

        if (isDesktop() && isCollapsedDesktop) {
            return setHover(true);
        }

        if (isDesktop() && !isCollapsedDesktop && !isActive && items.length) {
            return setHover(true);
        }
    };

    const onBlur = () => {
        setHover(false);
        setStyle(null);
    };

    const display = hover && (                              // ховер если
        !isActive                                           // пункт не активен
        || (isDesktop() && isActive && isCollapsedDesktop)  // активен и свернут на десктопе
        || (isTablet() && isActive && !isExpandedMobile)    // активен и не развернут на планшете
    );

    const cls = classes({
        [cx.active]: isActive,
        [cx.is_showing_fly_out]: display,
    });

    return (
        <li key={to}
            className={cls}
            onMouseEnter={onHover}
            onMouseLeave={onBlur}
        >
            <MenuGroup
                item={item}
                style={display ? style : null}
                onHideExpandedMobile={onBlur}
            />
        </li>
    );
}

MenuGroup.propTypes = {
    item: PropTypes.object,
    style: PropTypes.object,
    onHideExpandedMobile: PropTypes.func,
};

function MenuGroup ({ item, style, onHideExpandedMobile }) {
    const isMatch = useMatchLocation();
    const { to, label, items = [] } = item;
    const { onReset, onToggleSideBar, isExpandedMobile } = useMenu();

    const Icon = Icons24[item.icon];

    const clsSubLevel = classes(cx.fly_out_top_item, {
        [cx.active]: isMatch(to),
    });

    const hideSidebar = () => {
        if(isMobile()) {
            onReset();
        }

        if(isTablet() && isExpandedMobile) {
            onToggleSideBar();
            onHideExpandedMobile();
        }

        if(isDesktop()) {
            onHideExpandedMobile();
        }
    };

    return (
        <Fragment>
            <MenuLink to={to} onClick={hideSidebar}>
                <div className={cx.nav_icon_container}>
                    <Icon/>
                </div>
                <span className={cx.nav_item_name}>{label}</span>
            </MenuLink>

            <ul className={cx.sidebar_sub_level_items} style={style}>
                <li className={clsSubLevel}>
                    <MenuLink to={to}>
                        <strong className={cx.fly_out_top_item_name}>{label}</strong>
                    </MenuLink>
                </li>

                {items.length > 0 &&
                <li className={`${cx.divider} ${cx.fly_out_top_item}`}/>}

                {
                    items.map((item) => {
                        const cls = isMatch(item.to, item.to === to)
                            ? cx.active
                            : '';

                        return (
                            <li key={item.to} className={cls}>
                                <MenuLink to={item.to} onClick={hideSidebar}>
                                    <span>{item.label}</span>
                                </MenuLink>
                            </li>
                        );
                    })
                }
            </ul>
        </Fragment>
    );
}
