import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import classes from 'classnames';
import { Icons24 } from '@justlook/uikit';
import { useIfMediaScreen } from '@justlook/uikit/src/hooks';

import { Menu } from './menu';
import { Header } from './header';
import { useMenu } from '../index';

import cx from './sidebar.module.scss';

const Sidebar = ({ menu, userMenu, user }) => {
    const { onReset, onToggleSideBar, isCollapsedDesktop, isExpandedMobile } = useMenu();

    const isMobile = useIfMediaScreen();

    const cls = classes(cx.nav_sidebar, {
        [cx.sidebar_expanded_mobile]: isExpandedMobile,
        [cx.sidebar_collapsed_desktop]: isCollapsedDesktop,
    });

    return (
        <Fragment>
            <div id='nav-sidebar-root' className={cls}>
                <div className={cx.nav_sidebar_inner_scroll}>
                    <Header menu={userMenu} user={user}/>

                    <Menu items={menu}/>

                    <div className={cx.sidebar_buttons}>
                        {
                            isMobile &&
                            <div id='intercom-launcher' className={cx.toggle_intercom}>
                                <Icons24.IconIntercom/>
                                <strong className={cx.collapse_text}>Техподдержка</strong>
                            </div>
                        }

                        <div onClick={onToggleSideBar} className={cx.toggle_sidebar_button}>
                            <Icons24.IconPanel/>
                            <strong className={cx.collapse_text}>Свернуть меню</strong>
                        </div>

                        <div onClick={onReset} className={cx.close_nav_button}>
                            <Icons24.IconClose/>
                            <strong className={cx.collapse_text}>Закрыть меню</strong>
                        </div>
                    </div>
                </div>
            </div>

            <MobileOverlayContent open={isExpandedMobile} onClick={onReset}/>
        </Fragment>
    );
};

Sidebar.propTypes = {
    menu: PropTypes.any,
    userMenu: PropTypes.any,
    user: PropTypes.object,
};

export default Sidebar;

const MobileOverlayContent = ({ open, onClick }) => {
    let cls = classes(cx.mobile_overlay, {
        [cx.mobile_nav_open]: open,
    });

    return <div onClick={onClick} className={cls}/>;
};

MobileOverlayContent.propTypes = {
    open: PropTypes.bool.isRequired,
    onClick: PropTypes.func.isRequired,
};
