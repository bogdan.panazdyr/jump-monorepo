import React     from 'react';
import PropTypes from 'prop-types';

import cx from './one-column-layout.module.scss';

export const OneColumnsLayout = ({ isProgress = false, header, children }) => {

    return (
        <div className={cx['page-wrapper']}>
            <div className={isProgress ? cx.is_loading : ''}>
                {header}
            </div>

            <div className={cx['scroll-wrapper']}>
                {children}
            </div>
        </div>
    );
};

OneColumnsLayout.propTypes = {
    isProgress: PropTypes.bool,
    header: PropTypes.any.isRequired,
    children: PropTypes.any,
};
