import React     from 'react';
import PropTypes from 'prop-types';

import cx from './page.module.scss';

const render = ({ children }) => {
    return (
        <div className={cx.layout}>
            <div className={cx.content_wrapper}>
                <div className={cx.content}>
                    {children}
                </div>
            </div>
        </div>
    );
};

render.displayName = 'BlankPageLayout';

render.propTypes = {
    children: PropTypes.any,
};

export default render;
