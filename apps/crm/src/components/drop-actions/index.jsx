import React, { useState, useRef } from 'react';
import classes from 'classnames';
import PropTypes from 'prop-types';

import { Icons24, ParagraphV2 as Paragraph } from '@justlook/uikit';

import { useClickOutside } from 'hooks/use-click-outside';

import cx from './drop-actions.module.scss';

export const DropActions = ({ icon, actions, loading, disabled, positionDropDown, className, ...rest }) => {
    const [ isOpen, setIsOpen ] = useState(false);
    const ref = useRef(null);

    const clsBase = classes(cx.base, className);

    const clsControl = classes(cx.control, {
        [cx.open]: isOpen,
        [cx.disabled]: disabled || loading,
    })

    const clsActions = classes(cx.actions, {
        [cx.bottomLeft]: positionDropDown === 'bottom-left',
        [cx.bottomRight]: positionDropDown === 'bottom-right',
    })

    const clsIcon = classes({
        [cx.load]: loading
    })

    const Icon = loading ? Icons24.IconLoader : (icon || Icons24.IconKebab);

    const handleClickAction = async (callback) => {
        await callback();
        await setIsOpen(false);
    }

    useClickOutside(ref, () => setIsOpen(false));

    return (
        <div className={clsBase} {...rest}>
            <div className={clsControl} onClick={() => setIsOpen(true)}>
                <Icon className={clsIcon}/>
            </div>
            {
                isOpen &&
                <div className={clsActions} ref={ref}>
                    {
                        actions.map(item => {
                            if(! item.action) {
                                return null
                            }

                            return(
                                <Paragraph.LH24
                                    key={item}
                                    className={cx.action}
                                    onClick={() => handleClickAction(item.action)}
                                >
                                    { item.label }
                                </Paragraph.LH24>
                            )
                        })
                    }
                </div>
            }
        </div>
    )
}

DropActions.propTypes = {
    icon: PropTypes.any,
    actions: PropTypes.arrayOf(PropTypes.shape({
        label: PropTypes.string,
        action: PropTypes.any,
    })).isRequired,
    loading: PropTypes.bool,
    disabled: PropTypes.bool,
    positionDropDown: PropTypes.oneOf(['bottom-left', 'bottom-right']),
    className: PropTypes.string,
}
