import React, { Fragment } from 'react';
import { Helmet } from 'react-helmet';
import { useSelector } from 'react-redux';
import { ErrorScreen } from '@justlook/uikit';
import { Config } from '@justlook/core';

import { getPermissions } from 'app/profile/store/selectors';

const PageDeny = () => {
    return (
        <Fragment>
            <Helmet>
                <title>Доступ запрещен - {Config.brandName}</title>
            </Helmet>

            <ErrorScreen error={{
                title: 'Доступ запрещен',
                message: 'У вас не хватает прав, обратитесь к администратору',
            }}/>
        </Fragment>
    );
};

const withPermissions = (Component, needPermissions = [], allRequired = false) => {
    return (props) => {
        const userPermissions = useSelector(getPermissions);
        const emptyPermissions = userPermissions.length === 0;
        const denyCrmAccess = !userPermissions.includes(`access-${Config.crm_type}-crm`);

        if (emptyPermissions || denyCrmAccess) {
            return <PageDeny/>;
        }

        // если требуется только один пермишен, то проверяем без флага allRequired
        if (needPermissions.length === 1) {
            return userPermissions.includes(needPermissions[0])
                ? <Component {...props} />
                : <PageDeny/>;
        }

        for (let i = 0; i < needPermissions.length; i += 1) {
            if (userPermissions.includes(needPermissions[i]) && !allRequired) {
                return <Component {...props} />;
            }

            if (!userPermissions.includes(needPermissions[i]) && allRequired) {
                return <PageDeny/>;
            }
        }

        return <Component {...props} />;
    };
}

export default withPermissions;
