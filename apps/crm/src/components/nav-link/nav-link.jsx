import React from 'react';
import PropTypes from 'prop-types';
import classname from 'classnames';
import { NavLink as RouteLink } from 'react-router-dom';

import {ParagraphV2 as Paragraph} from '@justlook/uikit';

import cx from './nav-link.module.scss';

const NavLink = ({ className, children, ...rest }) => {
    const cls = classname(className, cx.base);

    return (
        <RouteLink className={cls} {...rest}>
            <Paragraph.LH24>{children}</Paragraph.LH24>
        </RouteLink>
    )
}

NavLink.propTypes = {
    className: PropTypes.string,
    children: PropTypes.any
}


export default NavLink;
