import React from 'react';
import PropTypes from 'prop-types';
import classes from 'classnames';

import { Description, Header, Icons24 } from '@justlook/uikit';

import { Box } from 'components/box';

import cx from './summary-box.module.scss';

export const SummaryBox = ({ url, title, description, className, ...rest }) => {
    const cls = classes(cx.base, className, { [cx.link]: url });

    const Component = url ? 'a' : 'div';
    const props = url ? { target: '_blank', href: url } : {};

    return (
        <Component className={cls} {...rest} {...props}>
            <Box>
                <Header.H1>{title}</Header.H1>
                <Description.LH16 className={cx.desc}>{description}</Description.LH16>
            </Box>
            { url && <Icons24.IconArrowRight/> }
        </Component>
    )
}

SummaryBox.propTypes = {
    url: PropTypes.string,
    title: PropTypes.string,
    description: PropTypes.string,
    className: PropTypes.string,
}
