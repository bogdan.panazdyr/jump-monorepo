import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { ParagraphV2 as Paragraph } from '@justlook/uikit';

import cx from './countdown.module.scss';

export const Countdown = ({ seconds = 60, template = '$', onTimeout }) => {
    const [count, setCount] = useState(seconds);

    useEffect(() => {
        let timeoutId = null;

        if (count > 0) {
            timeoutId = setTimeout(() => setCount(count - 1), 1000);
        } else {
            clearTimeout(timeoutId);
            onTimeout();
        }

        return () => clearTimeout(timeoutId);
    }, [count, onTimeout])

    return count > 0
        ? <Paragraph.LH24 className={cx.base}>{template.replace('$', count)}</Paragraph.LH24>
        : null;
};

Countdown.propTypes = {
    seconds: PropTypes.number,
    template: PropTypes.string,
    onTimeout: PropTypes.any,
}