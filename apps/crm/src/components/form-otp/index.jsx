import React, { useState } from 'react';
import PropTypes from 'prop-types';

import { ParagraphV2 as Paragraph, Input, Icons24 } from '@justlook/uikit';

import { Countdown } from 'components/countdown';

import cx from './form-otp.module.scss';

const ButtonRetry = ({ onClick }) => {
    return (
        <Paragraph.LH24
            onClick={onClick}
            className={cx.retry}
        >
            Получить новый код
        </Paragraph.LH24>
    )
}

export const FormOtp = ({ seconds, code, setCode, onRetryOtp, error, isRetrying }) => {
    const [ isCountDown, setIsCountDown ] = useState(true);

    const showCountDown = () => setIsCountDown(true);
    const hideCountDown = () => setIsCountDown(false);

    const onRetry = () => {
        showCountDown()

        onRetryOtp()
    }

    return (
        <div className={cx.base}>
            <div className={cx.field}>
                <Input
                    placeholder='СМС-код'
                    value={code}
                    onChange={setCode}
                    className={cx.input}
                    error={error}
                />
                {isRetrying && <Icons24.IconLoader style={{ marginLeft: '12px' }}/>}
            </div>
            {isCountDown && <Countdown seconds={seconds} onTimeout={hideCountDown} template='Получить новый код 00:$'/>}
            {!isCountDown && <ButtonRetry onClick={onRetry}/>}
        </div>
    )
}

FormOtp.propTypes = {
    seconds: PropTypes.number,
    code: PropTypes.string,
    setCode: PropTypes.func,
    onRetryOtp: PropTypes.func,
    error: PropTypes.string,
    isRetrying: PropTypes.bool,
}

ButtonRetry.propTypes = {
    onClick: PropTypes.func,
}
