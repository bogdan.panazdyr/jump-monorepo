import React, { Fragment, useState } from 'react';
import PropTypes from 'prop-types';
import classes from 'classnames';

import { Description } from '@justlook/uikit';

import cx from './icon-bar.module.scss';

export const IconBar = ({ data, positionBar, className = '', ...rest }) => {
    const [ isHover, setIsHover ] = useState(false);

    const cls = classes(cx.label, {
        [cx['top-left']]: positionBar === 'top-left',
        [cx['top-right']]: positionBar === 'top-right',
        [cx['bottom-left']]: positionBar === 'bottom-left',
        [cx['bottom-right']]: positionBar === 'bottom-right',
        [cx['top-center']]: positionBar === 'top-center',
        [cx['bottom-center']]: positionBar === 'bottom-center',
        [cx['left-center']]: positionBar === 'left-center',
        [cx['right-center']]: positionBar === 'right-center',
    })

    return (
        <Fragment>
            {data.icon &&
            <div
                className={`${cx.base} ${className}`}
                onMouseEnter={() => setIsHover(true)}
                onMouseLeave={() => setIsHover(false)}
                {...rest}
            >
                {data.icon}
                {
                    isHover && data.label && positionBar &&
                    <div className={cls}>
                        <Description.LH16>
                            {data.label}
                        </Description.LH16>
                    </div>
                }
            </div>
            }
        </Fragment>
    )
}

IconBar.defaultProps = {
    positionBar: 'top-right'
}

IconBar.propTypes = {
    data: PropTypes.shape({
        label: PropTypes.any,
        icon: PropTypes.any
    }).isRequired,
    className: PropTypes.any,
    positionBar: PropTypes.oneOf([
        'top-left', 'top-right', 'bottom-left',
        'bottom-right', 'top-center', 'bottom-center',
        'right-center', 'left-center'
    ])
}
