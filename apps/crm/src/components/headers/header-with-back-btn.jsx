import React, { memo } from 'react';
import PropTypes from 'prop-types';

import { Icons24 } from '@justlook/uikit';

import cx from './header-with-back-btn.module.scss';

export const HeaderWithBackBtn = memo(({ title, description, onClickBack }) => {
    return (
        <header className={cx.header}>
            <div
                className={cx.back}
                onClick={ () => onClickBack ? onClickBack() : window.history.back() }
            >
                <Icons24.IconArrowLeftLine/>
            </div>

            <div className={cx.info}>
                {
                    title && (
                        <div className={cx.title}>
                            {title}
                        </div>
                    )
                }
                <div className={cx.description}>
                    {description}
                </div>
            </div>
        </header>
    )
});


HeaderWithBackBtn.propTypes = {
    title: PropTypes.string,
    description: PropTypes.any,
    onClickBack: PropTypes.func,
}
