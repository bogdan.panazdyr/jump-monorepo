import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';

import { Hooks } from '@justlook/core';
import { Checkbox, Popup } from '@justlook/uikit';

import { useTestMode } from 'hooks/use-testing';
import { getProfile } from 'app/init/store/selectors';

import cx from './just-devtools.module.scss';

export const JustDevTools = () => {
    const profile = useSelector(getProfile);
    const { onClose, onOpen, isOpen } = Hooks.useModal();
    const { isTestMode, setTestMode } = useTestMode();

    useEffect(function() {
        window.JustDevTools = {
            open() {
                onOpen('just-dev-tools')
            },
            setTestMode(val) {
                setTestMode(val)
            }
        }

        document.onkeyup = function(e) {

            if (e.shiftKey && e.altKey && parseInt(e.which) === 90) { // shift + alt + z
                onOpen('just-dev-tools')
            }
        };
    });

    return isOpen('just-dev-tools') && (
        <Popup onDismiss={() => onClose('just-dev-tools')}>
            <Popup.Header>
                Just DevTools
            </Popup.Header>
            <Popup.Content>
                <Checkbox label={'Тестовый режим'} checked={isTestMode} onChange={setTestMode} />

                <div className={cx.profile}>
                    <strong>Id:</strong> <span>{ profile?.id || '-' }</span>
                    <strong>RootId:</strong> <span>{ profile?.parent?.id || '-' }</span>
                    <strong>CompanyName:</strong> <span>{ profile?.company?.name || '-' }</span>
                    <strong>CompanyId:</strong> <span>{ profile?.company?.id || '-' }</span>
                    <strong>Login:</strong> <span>{ profile?.login || '-' }</span>
                </div>
            </Popup.Content>
        </Popup>
    );
}
