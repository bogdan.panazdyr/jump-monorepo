import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { CartesianGrid, Line, LineChart as Chart, Tooltip, XAxis, YAxis, ResponsiveContainer } from 'recharts';
import { useIfMediaScreen } from '@justlook/uikit/lib/hooks';
import { Description, Icons24, ParagraphV2 as Paragraph } from '@justlook/uikit';

import { formatMoney } from 'utils/helpers';

import { getCurrency } from 'app/init/store/selectors';

import { CustomTooltip } from './custom-tooltip';
import { CustomAxisTick } from './custom-axis-tick';

import cx from './line-chart.module.scss';

const COLORS = [ '#26D962', '#FFCC66', '#2691FF', '#EC393D', '#850AC2', '#CCCFDB', '#E7EAEE' ];

export const LineChart = ({ isRequest, data, XDataKey }) => {
    const isMedia = useIfMediaScreen();
    const currency = useSelector(getCurrency);

    if(isRequest) {
        return (
            <div className={cx.empty}>
                <Icons24.IconLoader/>
            </div>
        )
    }

    if(data.items.length === 0) {
        return(
            <div className={cx.empty}>
                <Paragraph.LH24>Нет данных</Paragraph.LH24>
            </div>
        )
    }

    const Container = isMedia ? ResponsiveContainer : 'div';
    const containerProps = isMedia ? {width: '100%', height: 150} : {style: {width:'434px', height:'150px'}};
    const charProps = isMedia ? {width: '100%', height: 150} : {width: 434, height: 150};

    return (
        <Fragment>
            <Container {...containerProps}>
                <Chart data={data.items} {...charProps}>

                    <CartesianGrid vertical={false} strokeWidth={1} stroke='#E7EAEE'/>

                    <XAxis
                        dataKey={XDataKey}
                        axisLine={false}
                        tickLine={false}
                        ticks={[data.items[0][XDataKey], data.items[data.items.length - 1][XDataKey]]}
                        tick={<CustomAxisTick/>}
                        interval={0}
                    />
                    <YAxis
                        axisLine={false}
                        tickLine={false}
                        tick={<CustomAxisTick/>}
                        interval={0}
                        mirror={true}
                    />

                    <Tooltip content={<CustomTooltip meta={data.meta} accessor='lines' />}/>

                    {data.meta.lines.map((item, index) => (
                        <Line
                            key={item.key}
                            type='linear'
                            dataKey={item.key}
                            stroke={COLORS[index % COLORS.length]}
                            strokeWidth={2}
                            dot={false}
                        />
                    ))}
                </Chart>
            </Container>
            {data.meta.lines.length > 1 &&
                <div className={cx.legends}>
                    {data.meta.lines.map((item, index) => (
                        <div key={item.legend} className={cx['legends-line']}>
                            <Description.LH16 className={cx['legends-name']}>
                                <span className={cx['legends-dot']} style={{background: COLORS[index % COLORS.length]}}/>
                                {item.legend}
                            </Description.LH16>
                            <Description.LH16>
                                {formatMoney(item.total)}&nbsp;
                                { data.meta.currency && currency }
                            </Description.LH16>
                        </div>
                    ))}
                </div>
            }
        </Fragment>
    )
}

LineChart.propTypes = {
    isRequest: PropTypes.bool,
    data: PropTypes.object,
    XDataKey: PropTypes.string,
}
