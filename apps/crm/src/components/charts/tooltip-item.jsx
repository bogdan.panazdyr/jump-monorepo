import React from 'react';
import PropTypes from 'prop-types';
import classes from 'classnames';
import { Description } from '@justlook/uikit';

import cx from './tooltip-item.module.scss';

export const TooltipItem = ({ title, value, color, isHeader }) => {
    const cls = classes(cx.base, {
        [cx.isHeader]: isHeader,
        [cx.emptyTitle]: !title.length
    });

    return (
        <div className={cls}>
            {color && <span className={cx.dot} style={{ background: color }}/>}
            {title && <Description.LH16 className={cx.name}>{title}</Description.LH16>}
            {value && <Description.LH16 className={cx.value}>{value}</Description.LH16>}
        </div>
    )
}

TooltipItem.propTypes = {
    title: PropTypes.string,
    value: PropTypes.string,
    color: PropTypes.string,
    isHeader: PropTypes.bool,
}
