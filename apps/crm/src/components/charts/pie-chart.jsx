import React from 'react';
import PropTypes from 'prop-types';
import { PieChart as Chart, Pie, Cell, Tooltip } from 'recharts';

import { Description, Icons24, ParagraphV2 as Paragraph } from '@justlook/uikit';

import { useMoney } from 'hooks/use-money';
import { IconBarStorage } from 'components/icon-bar-storage';

import { CustomTooltip } from './custom-tooltip';

import cx from './pie-chart.module.scss';


const COLORS = [ '#2691FF', '#26D962', '#FFCC66', '#EC393D', '#850AC2', '#CCCFDB', '#E7EAEE' ];

const getIncorrectText = (name) => (
    `У некоторых пользователей данные по\u00A0агрегатору “${name}” могут отличаться от\u00A0фактических. Мы\u00A0работаем над\u00A0корректностью отображаемых данных.`
)

export const PieChart = ({ isRequest, data, isStorageInfo }) => {
    const { currency, format: formatMoney } = useMoney()

    if(isRequest) {
        return (
            <div className={cx.empty}>
                <Icons24.IconLoader/>
            </div>
        )
    }

    if(data.items.length === 0) {
        return(
            <div className={cx.empty}>
                <Paragraph.LH24>Нет данных</Paragraph.LH24>
            </div>
        )
    }

    return (
        <div className={cx.chart}>
            <Chart width={160} height={162}>
                <Pie data={data.items} dataKey='sum' cx={74} cy={74} outerRadius={74} innerRadius={28} paddingAngle={0}>
                    {data.items.map((entry, index) =>
                        <Cell key={index} fill={COLORS[index % COLORS.length]}/>
                    )}
                </Pie>
                <Tooltip content={<CustomTooltip meta={data.meta}/>}/>
            </Chart>
            <div className={cx.legends}>
                {data.items.map((item, index) => (
                    <div key={index} className={cx['legends-line']}>
                        <span className={cx['legends-dot']} style={{background: COLORS[index % COLORS.length]}}/>
                        <Description.LH16 element='span' className={cx['legends-name']}>
                            { item.name }
                            { isStorageInfo && item.slug === 'yandex_taxometer' && <IconBarStorage
                                text={getIncorrectText(item.name)}
                                storageKey={item.slug}
                            />}
                        </Description.LH16>
                        <Description.LH16 element='span'>
                            { formatMoney(item.sum) }&nbsp;
                            { data.meta.currency && currency }
                        </Description.LH16>
                    </div>
                ))}
            </div>
        </div>
    )
}

CustomTooltip.propTypes = {
    payload: PropTypes.array,
    active: PropTypes.bool,
    meta: PropTypes.object,
}

PieChart.propTypes = {
    isRequest: PropTypes.bool,
    data: PropTypes.object,
    isStorageInfo: PropTypes.bool,
}
