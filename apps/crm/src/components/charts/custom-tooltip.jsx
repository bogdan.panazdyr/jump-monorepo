import React from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';

import { TooltipHeader } from './tooltip-header';
import { TooltipItem } from './tooltip-item';

import { numberWithPercentLabel } from 'utils/helpers';
import { getCurrency } from 'app/init/store/selectors';

import cx from './custom-tooltip.module.scss';

export const CustomTooltip = ({ payload, active, meta, accessor }) => {
    const currency = useSelector(getCurrency);

    if (active && payload?.length) {
        return (
            <div className={cx.base}>
                {accessor && <TooltipHeader payload={payload} meta={meta}/>}

                {payload.map((item, index) => {
                    const title = accessor ? meta[accessor][index].legend : item?.name;

                    const {value, payload: {percent = null}} = item;
                    const unit = meta?.currency ? currency : null;
                    const label = numberWithPercentLabel(value, percent, unit);

                    return (
                        <TooltipItem
                            key={index}
                            title={title}
                            value={label}
                            color={item?.color || item?.payload?.fill}
                        />
                    )
                })}
            </div>
        );
    }

    return null;
}

CustomTooltip.propTypes = {
    payload: PropTypes.array,
    active: PropTypes.bool,
    meta: PropTypes.object,
    accessor: PropTypes.string,
}
