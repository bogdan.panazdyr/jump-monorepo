import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { ReferenceLine, ComposedChart as Chart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, ResponsiveContainer } from 'recharts';
import { useIfMediaScreen } from '@justlook/uikit/lib/hooks';
import { Description, Icons24, ParagraphV2 as Paragraph } from '@justlook/uikit';

import { useMoney } from 'hooks/use-money';

import { CustomTooltip } from './custom-tooltip';
import { CustomAxisTick } from './custom-axis-tick';

import { BAR, hasNegativeValues } from './services/charts-service';

import cx from './bar-chart.module.scss';

const COLORS = [ '#2691FF', '#26D962', '#FFCC66', '#E7EAEE', '#EC393D', '#850AC2', '#CCCFDB' ];

export const BarChart = ({ isRequest, data, XDataKey }) => {
    const isMedia = useIfMediaScreen();
    const { format: formatMoney } = useMoney();

    if(isRequest) {
        return (
            <div className={cx.empty}>
                <Icons24.IconLoader/>
            </div>
        )
    }

    if(data.items.length === 0) {
        return(
            <div className={cx.empty}>
                <Paragraph.LH24>Нет данных соответсвующих условиям.</Paragraph.LH24>
            </div>
        )
    }

    const Container = isMedia ? ResponsiveContainer : 'div';
    const containerProps = isMedia ? {width: '100%', height: 150} : {style: {width:'434px', height:'150px'}};
    const charProps = isMedia ? {width: '100%', height: 150} : {width: 434, height: 150};

    return (
        <Fragment>
            <Container{...containerProps} >
                <Chart data={data.items} {...charProps}>
                    <CartesianGrid vertical={false} strokeWidth={1} stroke='#E7EAEE'/>

                    <XAxis
                        dataKey={XDataKey}
                        axisLine={false}
                        tickLine={false}
                        tick={<CustomAxisTick chartType={BAR}/>}
                        interval={0}
                        padding={{ left: 40 }}
                    />
                    <YAxis
                        axisLine={false}
                        tickLine={false}
                        tick={<CustomAxisTick/>}
                        interval={0}
                        mirror={true}
                    />

                    <Tooltip content={<CustomTooltip meta={data.meta}  accessor='bars'/>}/>
                    {hasNegativeValues(data.items) && <ReferenceLine y={0} stroke='#333333' />}

                    {data.meta.bars.map((item, index) => (
                        <Bar
                            key={index}
                            dataKey={item.key}
                            stackId='a'
                            fill={COLORS[index % COLORS.length]}
                        />
                    ))}
                </Chart>
            </Container>

            {data.meta.bars.length > 1 &&
            <div className={cx.legends}>
                {data.meta.bars.map((item, index) => (
                    <div key={item.key} className={cx['legends-line']}>
                        <Description.LH16 className={cx['legends-name']}>
                            <span className={cx['legends-dot']} style={{background: COLORS[index % COLORS.length]}}/>
                            {item.legend}
                        </Description.LH16>
                        <Description.LH16 className={cx['legends-total']}>
                            {formatMoney(item.total)}
                        </Description.LH16>
                    </div>
                ))}
            </div>
            }
        </Fragment>
    );
}

BarChart.propTypes = {
    isRequest: PropTypes.bool,
    data: PropTypes.object,
    XDataKey: PropTypes.string,
}
