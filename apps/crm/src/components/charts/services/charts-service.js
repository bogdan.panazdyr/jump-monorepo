import { Utils } from '@justlook/core';
import { fromUnixTime } from 'date-fns';

export const LINE = 'line';
export const BAR = 'bar';
export const PIE = 'pie';

export const tickFormatter = (tick, format = 'dd MMM yyyy') => (Utils.Dates.formatDate(fromUnixTime(tick), format));

export const hasNegativeValues = (data = []) => {
    const negativeValues = [];

    const checkForNegativeValues = (item) => {
        for(let key in item) {
            if (Object.prototype.hasOwnProperty.call(item, key)) {
                if(Math.sign(item[key]) === -1) {
                    negativeValues.push(item[key]);
                    break;
                }
            }
        }
    }

    data.map(item => checkForNegativeValues(item));

    return negativeValues.includes(-1)
}
