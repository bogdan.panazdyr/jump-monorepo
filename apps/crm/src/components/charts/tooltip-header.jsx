import React from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { Utils } from '@justlook/core';
import { fromUnixTime } from 'date-fns';

import { getCurrency } from 'app/init/store/selectors';
import { formatMoney } from 'utils/helpers';

import { TooltipItem } from './tooltip-item';

const getTotalValue = (values = [], accessor = '', unit = '') => {
    if(values.length <= 1) return null;

    const sum = values.reduce((ac, item) => ac + item[accessor], 0);

    return formatMoney(sum) + ' ' + unit;
}

const getTooltipDate = (tooltip) => {
    if(tooltip instanceof Object) {
        let period = [];

        for (let key in tooltip) {
            if (Object.prototype.hasOwnProperty.call(tooltip, key)) {
                period.push(Utils.Dates.formatDate(fromUnixTime(tooltip[key]), 'dd MMM yyyy'))
            }
        }

        return period.join(' - ');
    }

    return Utils.Dates.formatDate(fromUnixTime(tooltip), 'dd MMM yyyy');
}

export const TooltipHeader = ({ payload, meta }) => {
    const currency = useSelector(getCurrency);

    return (
        <TooltipItem
            title={getTooltipDate(payload[0].payload.tooltip)}
            value={
                getTotalValue(
                    payload,
                    'value',
                    meta?.currency ? currency : ''
                )
            }
            isHeader
        />
    )
}

TooltipHeader.propTypes = {
    payload: PropTypes.array,
    meta: PropTypes.object,
}
