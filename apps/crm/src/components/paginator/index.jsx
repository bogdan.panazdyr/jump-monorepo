import React      from 'react';
import PropTypes  from 'prop-types';
import { Button, Icons24, ParagraphV2 as Paragraph } from '@justlook/uikit';

import cx from './paginator.module.scss';

function Paginator ({
    total = 0, page = 1, per_page = 0, last_page = 0,
    loading = false, onChangePage
}) {
    if (!total) {
        return null;
    }

    const current_page = parseInt(page);
    const from = (current_page - 1) * per_page + 1;
    const to = Math.min((current_page) * per_page, total);

    const enablePrev = current_page > 1 && !loading;
    const enableNext = current_page < last_page && !loading;

    return (
        <div className={cx.base}>
            <Button
                disabled={!enablePrev}
                element="button"
                icon={<Icons24.IconArrowLeft/>}
                onClick={() => onChangePage({ page: current_page - 1 })}
                styling="hollow-border"
                type="button"
            />

            <Paragraph.LH24>
                {from}-{to} из {total}
            </Paragraph.LH24>

            <Button
                disabled={!enableNext}
                element="button"
                icon={<Icons24.IconArrowRight/>}
                onClick={() => onChangePage({ page: current_page + 1 })}
                styling="hollow-border"
                type="button"
            />
        </div>
    );
}

Paginator.propTypes = {
    total: PropTypes.number,
    page: PropTypes.oneOfType(
        [ PropTypes.number, PropTypes.string ]).isRequired,
    per_page: PropTypes.oneOfType(
        [ PropTypes.number, PropTypes.string ]).isRequired,
    last_page: PropTypes.number,
    onChangePage: PropTypes.func.isRequired,
    loading: PropTypes.bool,
};

export { Paginator };
