import React, { useState } from 'react';
import PropTypes from 'prop-types';
import classes from 'classnames';
import { Icons16, Description } from '@justlook/uikit';

import { usePersist } from 'hooks/use-persist';

import cx from './icon-bar-storage.module.scss';

export const IconBarStorage = ({ icon, text, storageKey, isStorage }) => {
    const [ isShowTooltip, setIsShowTooltip ] = usePersist(storageKey, isStorage);

    const [ isHover, setIsHover ] = useState(false);

    const onHideInfo = () => setIsShowTooltip(false);

    const isVisibleInfo = isShowTooltip || isHover;

    const clsView = classes(cx.view, { [cx.forced]: isShowTooltip });

    const Icon = Icons16[icon];

    return (
        <div className={cx.base}>
            <Icon
                className={cx.icon}
                onMouseEnter={() => setIsHover(true)}
                onMouseLeave={() => setIsHover(false)}
            />

            {isVisibleInfo &&
                <div className={clsView}>
                    <Description.LH16 element='span'>{ text }</Description.LH16>

                    {isShowTooltip &&
                        <Icons16.IconClose
                            className={cx.closeView}
                            onClick={onHideInfo}
                        />
                    }
                </div>
            }
        </div>
    )
}

IconBarStorage.defaultProps = {
    icon: 'IconInfo',
    isStorage: true
}

IconBarStorage.propTypes = {
    icon: PropTypes.string,
    text: PropTypes.string.isRequired,
    storageKey: PropTypes.string.isRequired,
    isStorage: PropTypes.bool,
}
