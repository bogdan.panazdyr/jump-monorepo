import React, { useRef, useState } from 'react';
import PropTypes from 'prop-types';
import { ParagraphV2 as Paragraph, Password } from '@justlook/uikit';

import { useClickOutside } from 'hooks/use-click-outside';

import cx from './security-input.module.scss';

const StaticSecurityInput = ({ setOnFocus, label, className }) => {
    return (
        <div tabIndex={0} onFocus={() => setOnFocus(true)} className={className}>
            <label>
                <Paragraph.LH24>{ label }</Paragraph.LH24>
            </label>
            <div className={cx.field}>●●●●●●●●●●●●</div>
        </div>
    )
}

export const SecurityInput = ({ value, ...rest }) => {
    const [ onFocus, setOnFocus ] = useState(false);
    const field = useRef(null);

    const outsideClicked = () => {
        if(value) return setOnFocus(true)

        return setOnFocus(false)
    };
    useClickOutside(field, outsideClicked);

    if(!onFocus) { return <StaticSecurityInput setOnFocus={setOnFocus} label={rest.label} className={rest.className}/> }

    return (
        <div ref={field}>
            <Password value={value} autoFocus {...rest}/>
        </div>
    )
}

SecurityInput.propTypes = {
    value: PropTypes.any,
}

StaticSecurityInput.propTypes = {
    setOnFocus: PropTypes.func,
    label: PropTypes.string,
    className: PropTypes.string,
}
