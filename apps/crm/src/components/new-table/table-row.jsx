import React, { Fragment, useState } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { Description, Icons24, ParagraphV2 as Paragraph } from '@justlook/uikit';
import { useIfMediaScreen } from '@justlook/uikit/lib/hooks';

import { TablePopup } from './table-popup';
import { TableCell } from './table-cell';

import cx from './table-row.module.scss';

const MobileRow = ({
                       children,
                       checkboxCell,
                       customCell,
                       title,
                       description,
                       disablePopup,
                       vAlign,
                       icon: Icon,
                       onClick
                   }) => {
    const [ isShowTablePopup, setIsShowTablePopup ] = useState(false);

    const cls = classnames(cx.mobileRowBase, { [cx.mobileRowClicked]: Boolean(onClick) }, {
        [cx.alignCenter]: vAlign === 'center',
        [cx.alignStart]: vAlign === 'start',
        [cx.alignEnd]: vAlign === 'end',
    })

    return (
        <Fragment>
            <div className={cls}>
                {checkboxCell && <div className={cx.checkboxContainer}>
                    {checkboxCell}
                </div>}
                <div className={cx.mobileRow} onClick={onClick}>
                    <Paragraph.LH24 className={cx.paragraph}>{title}</Paragraph.LH24>
                    <Description.LH24 className={cx.description}>{description}</Description.LH24>
                </div>
                {!checkboxCell && !disablePopup && !Icon &&
                <Icons24.IconInfo className={cx.icon} onClick={() => setIsShowTablePopup(true)}/>}
                {Icon && <Icon className={cx.icon}/>}
            </div>
            {
                isShowTablePopup && <TablePopup
                    content={children}
                    controls={customCell}
                    onClose={() => setIsShowTablePopup(false)}
                />
            }
        </Fragment>
    )
}

export const TableRow = ({
                             children,
                             className,
                             onClick,
                             renderCustomCell,
                             renderCheckboxCell,
                             mobileTitle,
                             mobileDescription,
                             disableMobilePopup,
                             mobileIcon,
                             mobileVAlign,
                             mobileRow,
                             ...rest
                         }) => {
    const isMobile = useIfMediaScreen();

    const cls = classnames(cx.row, className, {
        [cx['click-row']]: onClick,
    });

    if(isMobile && mobileRow) { return mobileRow }

    if(isMobile && !mobileRow) {
        return (
            <MobileRow
                title={mobileTitle}
                description={mobileDescription}
                customCell={renderCustomCell}
                checkboxCell={renderCheckboxCell}
                disablePopup={disableMobilePopup}
                icon={mobileIcon}
                onClick={onClick}
                vAlign={mobileVAlign}
            >
                {children}
            </MobileRow>
        )
    }

    return (
        <div className={cx.base}>
            {renderCheckboxCell &&
            <TableCell className={cx.checkbox}>
                {renderCheckboxCell}
            </TableCell>
            }
            <div className={cls} onClick={onClick} {...rest}>
                {children}
                {renderCustomCell &&
                <TableCell className={cx.custom}>
                    {renderCustomCell}
                </TableCell>
                }
            </div>
        </div>
    )
}

MobileRow.propTypes = {
    children: PropTypes.any,
    checkboxCell: PropTypes.any,
    customCell: PropTypes.any,
    title: PropTypes.any,
    description: PropTypes.any,
    icon: PropTypes.any,
    onClick: PropTypes.func,
    disablePopup: PropTypes.bool,
    vAlign: PropTypes.string,
}

TableRow.propTypes = {
    children: PropTypes.any,
    className: PropTypes.string,
    onClick: PropTypes.func,
    renderCustomCell: PropTypes.any,
    renderCheckboxCell: PropTypes.any,
    mobileTitle: PropTypes.any,
    mobileDescription: PropTypes.any,
    disableMobilePopup: PropTypes.bool,
    mobileIcon: PropTypes.any,
    mobileVAlign: PropTypes.string,
    mobileRow: PropTypes.any,
}
