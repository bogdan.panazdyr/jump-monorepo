import React from 'react';
import PropTypes from 'prop-types';
import { Button, Icons24, ParagraphV2 as Paragraph } from '@justlook/uikit';

import cx from './table-popup.module.scss';

export const TablePopup = ({ content, controls, onClose }) => {
    return (
        <div className={cx.base}>
            <div className={cx.shadow}/>
            <div className={cx.content}>
                <div className={cx.header}>
                    <Button styling='hollow' icon={<Icons24.IconClose/>} onClick={onClose}/>
                </div>

                {content.map(item => {
                    if(!item.props.custom) {
                        return (
                            <Paragraph.LH24 className={cx.paragraph} key={item.props.children}>
                                {item.props.children}
                            </Paragraph.LH24>
                        )
                    }
                    return null;
                })}

                <div className={cx.buttons}>
                    {controls}
                </div>
            </div>
        </div>
    )
}

TablePopup.propTypes = {
    content: PropTypes.array,
    onClose: PropTypes.func,
    controls: PropTypes.any,

}
