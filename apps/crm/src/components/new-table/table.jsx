import React from 'react';
import PropTypes from 'prop-types';

import { Ellipsis } from '@justlook/uikit';

import { TableHead } from './table-head';
import { TableRow } from './table-row';
import { TableCell } from './table-cell';

import cx from './table.module.scss';

export const Table = ({ loading, children, className, ...rest }) => {
    if(loading) {
        return (
            <div className={cx.loading}>
                <Ellipsis/>
            </div>
        )
    }

    return (
        <div className={`${cx.base} ${className}`} {...rest}>
            {children}
        </div>
    )
}

Table.Head = TableHead;
Table.Row = TableRow;
Table.Cell = TableCell;

Table.propTypes = {
    loading: PropTypes.bool,
    children: PropTypes.any.isRequired,
    className: PropTypes.string,
}
