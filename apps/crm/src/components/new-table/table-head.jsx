import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import { Description, Icons16 } from '@justlook/uikit';
import { useIfMediaScreen } from '@justlook/uikit/lib/hooks';

import { TableCell } from './table-cell';

import cx from './table-head.module.scss';

export const TableHead = ({ renderCheckBox, cells, onSort, sortField, ...rest }) => {
    const isMobile = useIfMediaScreen();

    const field = sortField.replace('-', '');
    const sign = sortField.charAt(0) === '-' ? '-' : '';
    const reversField = sign === '-' ? field : '-' + field;

    const onClickSort = (selectedField) => {
        if (!selectedField) return

        onSort(selectedField === field ? reversField : selectedField)
    }

    if (isMobile) return null;

    return (
        <div className={cx.base} {...rest}>
            {renderCheckBox && (
                <TableCell className={cx['checkbox-cell']}>
                    {renderCheckBox}
                </TableCell>
            )}
            {cells.map(item => {
                const cls = classnames({
                    [cx.hover]: item.sort,
                    [cx.active]: item.sort === field,
                    [cx['label-end']]: item.positionLabel === 'end',
                    [item.className]: item.className,
                })

                return (
                    <TableCell
                        key={item.label}
                        className={cls}
                        onClick={() => onClickSort(item?.sort)}
                    >
                        <Description.LH24 element='span'>{item.label}</Description.LH24>
                        {item?.sort === field
                            ? sign === ''
                                ? <Icons16.IconSortDown/>
                                : <Icons16.IconSortUp/>
                            : null
                        }
                    </TableCell>
                )
            })}
        </div>
    )
}

TableHead.defaultProps = {
    sortField: ''
}

TableHead.propTypes = {
    cells: PropTypes.array.isRequired,
    onSort: PropTypes.any,
    sortField: PropTypes.string,
    renderCheckBox: PropTypes.any,
}
