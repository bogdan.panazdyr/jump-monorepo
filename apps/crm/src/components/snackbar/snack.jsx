import React, { useState, useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';

import { ParagraphV2 as Paragraph, Icons24 } from '@justlook/uikit';

import cx from './snack.module.scss';

export const Snack = ({ id, text, onClick, show, hideAfter, onHide, icon, role, renderButton, cancelButton, imitationOfThen }) => {
    const marginType = 'marginBottom'
    const [ animStyles, setAnimStyles ] = useState({ opacity: 0, [marginType]: -10 });
    const [ hideTime, setHideTime ] = useState(hideAfter);

    const handleHide = useCallback(() => {
        setAnimStyles({ opacity: 0, [marginType]: '-40px' });
        setTimeout(() => onHide(id), 400);
    }, [onHide, id]);

    useEffect(() => {
        const animTimeout = setTimeout(() => {
            setAnimStyles({ opacity: 1, [marginType]: '10px' });
        }, 50)

        let hideTimeout;

        if (hideTime !== 0) {
            hideTimeout = setTimeout(() => {
                handleHide()
                imitationOfThen()
            }, hideTime * 1000);
        }

        if (!show) handleHide();

        return () => {
            clearTimeout(animTimeout);
            if (hideTimeout) clearTimeout(hideTimeout);
        };
        // eslint-disable-next-line
    }, [hideTime, show])

    const clickProps = { tabIndex: 0, onClick };

    return (
        <div
            className={cx.base}
            role={role ? role : 'status'}
            style={{ ...animStyles }}
            {...(onClick ? clickProps : {})}
            onMouseEnter={() => setHideTime(0)}
            onMouseLeave={() => setHideTime(hideAfter)}
        >
            {icon}
            <Paragraph.LH24>{text}</Paragraph.LH24>
            {renderButton && renderButton(() => onHide(id))}
            {cancelButton && <Icons24.IconCross onClick={() => onHide(id)}/>}
        </div>
    )
}

Snack.defaultProps = {
    id: undefined,
    show: true,
    onHide: () => {},
    hideAfter: 5,
    onClick: () => {},
    imitationOfThen: () => {},
    cancelButton: true,
};

Snack.propTypes = {
    text: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.node
    ]).isRequired,
    show: PropTypes.bool,
    onHide: PropTypes.func,
    id: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ]),
    hideAfter: PropTypes.number,
    onClick: PropTypes.func,
    icon: PropTypes.any,
    role: PropTypes.any,
    renderButton: PropTypes.func,
    imitationOfThen: PropTypes.func,
    cancelButton: PropTypes.bool,
}
