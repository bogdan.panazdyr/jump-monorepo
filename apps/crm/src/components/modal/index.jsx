import React from 'react';
import PropTypes from 'prop-types';

import { Popup } from '@justlook/uikit';

export const Modal = ({title, description, children, onClose, minorButton, majorButton, dismissButton}) => {
    return (
        <Popup onDismiss={onClose}>
            <Popup.Header description={description}>
                {title}
            </Popup.Header>

            <Popup.Content>
                {children}
            </Popup.Content>

            {(minorButton || majorButton || dismissButton) && (
                <Popup.Footer
                    minorButton={minorButton}
                    majorButton={majorButton}
                    onDismissButton={dismissButton}
                />
            )}
        </Popup>
    )
}

Modal.propTypes = {
    title: PropTypes.string,
    description: PropTypes.string,
    children: PropTypes.any,
    onClose: PropTypes.func,
    minorButton: PropTypes.node,
    majorButton: PropTypes.node,
    dismissButton: PropTypes.node,
}
