import React, { Fragment, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import classes from 'classnames';

import { Config } from '@justlook/core';
import { ParagraphV2 as Paragraph, Icons16, Icons24, Link } from '@justlook/uikit';

import { errorToString } from 'utils/add-server-errors';
import { useClickOutside } from 'hooks/use-click-outside';
import { snackBar } from 'components';
import { useHasPermission } from 'app/profile/store/selectors';
import { useCancelReceipt } from 'domains/payments/mutators';

import cx from './receipt-controlls.module.scss';

const openReceipt = (id, key) => {
    window.open(
        Config.api_url.v2 + `payments/${id}/receipt`,
        `Чек №${key}`,
        'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=330,height=700'
    );
}

const REASON_REFUND = 'REFUND';
const REASON_MISTAKE = 'REGISTRATION_MISTAKE';

const cancelReasons = [
    { title: 'Возврат средств', value: REASON_REFUND },
    { title: 'Чек сформирован ошибочно', value: REASON_MISTAKE },
]

const Receipt = ({ payment }) => {
    const canPay = useHasPermission('write-payment');

    if (!payment.receipt) {
        return null;
    }

    const { id } = payment;
    const { key, can_cancel, links: { fns_url, saved_url } } = payment.receipt;

    return (
        <div className={cx.base}>
            <Paragraph.LH24>Чек</Paragraph.LH24>
            <div className={cx.controls}>
                {
                    !saved_url &&
                    <Paragraph.LH24 className={cx.show} onClick={() => openReceipt(id, key)}>
                        Показать
                    </Paragraph.LH24>
                }
                {
                    saved_url &&
                    <Fragment>
                        <Link to={saved_url} target='_blank'>Сохраненная копия</Link>
                        <Link to={fns_url} target='_blank'>Оригинал</Link>
                    </Fragment>
                }
                { canPay && can_cancel && <CancelReceiptTooltip id={id}/> }
            </div>
        </div>
    )
}

const CancelReceiptTooltip = ({ id }) => {
    const tooltipRef = useRef(null);

    const [ isShowTooltip, setIsShowTooltip ] = useState(false);

    const showTooltip = () => setIsShowTooltip(true);
    const hideTooltip = () => setIsShowTooltip(false);

    const onErrorCancel = (error) => {
        snackBar(errorToString(error), { hideAfter: 0, icon: <Icons24.IconWarning className={cx.error}/> })
    }

    const { onCancelReceipt, isCancelReceiptMutating } = useCancelReceipt(onErrorCancel, hideTooltip);

    const handleCancelReceipt = (reason) => onCancelReceipt({ id, reason });

    useClickOutside(tooltipRef, hideTooltip);

    const cls = classes(cx.cancel, { [cx.loading]: isCancelReceiptMutating });

    return (
        <div className={cx.tooltip}>
            <Paragraph.LH24
                className={cls}
                onClick={showTooltip}
            >
                {isCancelReceiptMutating ? <Icons16.IconLoader/> : 'Отменить'}
            </Paragraph.LH24>
            {
                isShowTooltip && !isCancelReceiptMutating &&
                <div className={cx.content} ref={tooltipRef}>
                    <Paragraph.LH24 className={cx.title}>
                        Причина отмены чека
                    </Paragraph.LH24>
                    {
                        cancelReasons.map(reason =>
                            <Paragraph.LH24
                                key={reason.value}
                                className={cx.reason}
                                onClick={() => handleCancelReceipt(reason.value)}
                            >
                                {reason.title}
                            </Paragraph.LH24>
                        )
                    }
                </div>
            }
        </div>
    )
}

Receipt.propTypes = {
    payment: PropTypes.object,
}

CancelReceiptTooltip.propTypes = {
    id: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ])
}


export default Receipt;
