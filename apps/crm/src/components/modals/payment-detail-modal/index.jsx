import React, { Fragment, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import classes from 'classnames';

import { useIfMediaScreen } from '@justlook/uikit/lib/hooks';
import { Utils } from '@justlook/core';
import { Popup, Button, ParagraphV2 as Paragraph, Header } from '@justlook/uikit';

import { ListItem, List } from 'components/list';
import { NewTable as Table, Status, Indent, Font, DropActions } from 'components';
import { useMoney } from 'hooks/use-money';
import { usePaymentHasConfirmStatus, getStatusIcon } from 'domains/payments/helpers';
import {
    useConfirmItemPayment,
    useRefundPayments,
    useCancelPayments,
    useRepeatPayments,
} from 'domains/payments/mutators';
import { usePaymentItem } from 'domains/payments/queries';

import { PaymentItemConfirmModal } from '../payment-item-confirm-modal/index';

import Receipt from './receipt-controlls';
import ContentError from './content-error';

import cx from './payment-detail-modal.module.scss';

const infoItem = (label, field) => {
    return <Paragraph.LH24 className={cx.info}>{label} <span>{field}</span></Paragraph.LH24>
}

export const PaymentsDetailModal = ({ payment, onClose, onUpdatePayment }) => {
    const isMobile = useIfMediaScreen();
    const [ isShowConfirmModal, setIsShowConfirmModal ] = useState(false);

    const { currency } = useMoney();
    const canConfirmPayment = usePaymentHasConfirmStatus();
    const { paymentData, isPaymentLoading, isPaymentFetching, paymentHasError, paymentError, refreshPayment } = usePaymentItem(payment.id, payment);

    const onSuccessMutating = async (id) => {
        await onUpdatePayment(id);
        await refreshPayment();
    }

    const { onConfirmPayment, needConfirmOtp, ...confirm } = useConfirmItemPayment(onSuccessMutating);
    const { onCancel, isCancelMutating, mutateCancelHasError, mutateCancelError } = useCancelPayments(onSuccessMutating);
    const { onRepeat, isRepeatMutating, mutateRepeatHasError, mutateRepeatError } = useRepeatPayments(onSuccessMutating);
    const { onRefund, isRefundMutating, mutateRefundHasError, mutateRefundError } = useRefundPayments(onSuccessMutating);

    const isErrorMutating = paymentHasError || confirm.isError || mutateRepeatHasError || mutateRefundHasError || mutateCancelHasError;
    const errorContent = paymentError || confirm.error || mutateRepeatError || mutateRefundError || mutateCancelError;
    const disableControls = confirm.isLoading || isRepeatMutating || isRefundMutating || isPaymentLoading || isPaymentFetching || isCancelMutating;

    const canBeCancel = paymentData.canBeCancel();
    const canRefund = paymentData.canRefund();
    const canConfirm = canConfirmPayment(paymentData);
    const canRepeat = paymentData.canRepeat();

    const hasActions = canBeCancel || canRefund || canConfirm || canRepeat;

    const clsFooter = classes(cx.footer, { [cx.hidden]: !hasActions });
    const clsTable = classes({ [cx.table]: !hasActions });

    useEffect(() => {
        if (needConfirmOtp) setIsShowConfirmModal(true)
    }, [ needConfirmOtp ])

    const handleOnRetryOtp = () => {
        onConfirmPayment(payment.id);
    }

    const onSuccessConfirmOtp = () => {
        onUpdatePayment(payment.id)
        refreshPayment()
    }

    if (isErrorMutating) {
        const titles = [
            confirm.isError && 'Ошибка оплаты',
            mutateRepeatHasError && 'Ошибка повтора выплаты',
            mutateRefundHasError && 'Ошибка возврата средств'
        ].filter(Boolean).join('. ');

        return <ContentError title={titles} onClose={onClose} error={errorContent}/>
    }

    const renderHistoryTable = () => {
        return (
            <Table className={clsTable}>
                <Table.Head
                    cells={[
                        { label: 'Дата и время' },
                        { label: 'Действие' }
                    ]}
                />
                <div style={{ maxHeight: '40vh', overflow: 'auto' }}>
                    {
                        paymentData.getHistory().map((item, index) => (
                            <Table.Row key={index + item.updated_at}>
                                <Table.Cell>
                                    {Utils.Dates.formatDate(item.updated_at)}&nbsp;
                                    {Utils.Dates.getTime(item.updated_at)}
                                </Table.Cell>
                                <Table.Cell>
                                    {item.message}
                                </Table.Cell>
                            </Table.Row>
                        ))
                    }
                </div>
            </Table>
        );
    }

    const renderHistoryList = () => {
        return (<List fluid>
            {paymentData.getHistory().map((item, index) => (
                <ListItem key={index + item.updated_at} className={cx.item}>
                    <Indent outsideBottom='xs'>
                        <Font as='p' size={18}>
                            {item?.message}
                        </Font>
                    </Indent>
                    <Font as='p' size={18} muted>
                        {`${Utils.Dates.formatDate(item.updated_at)}, ${Utils.Dates.getTime(item.updated_at)}`}
                    </Font>
                </ListItem>
            ))}
        </List>)
    }

    return (
        <Fragment>
            <Popup onDismiss={onClose} className={cx.cancelReceipt}>
                <Popup.Header
                    description={
                        <Status
                            icon={getStatusIcon(paymentData.getStatusTheme())}
                            text={paymentData.getStatus()}
                            color={paymentData.getStatusTheme()}
                        />
                    }
                >
                    Выплата {paymentData.id}
                </Popup.Header>
                <Popup.Content>
                    {infoItem('Получатель', paymentData.getContractorFullName())}
                    {infoItem('Номер телефона', paymentData.getContractorPhone())}
                    {infoItem('Заявка', `${paymentData.getAmount()} ${currency}`)}
                    {infoItem('Выплата', `${paymentData.getAmountPaid()} ${currency}`)}
                    {infoItem('Удержано на налог', `${paymentData.getTaxAmount()} ${currency}`)}
                    {infoItem('Комиссия банка', `${paymentData.getCommissionBank()} ${currency}`)}
                    {infoItem('Комиссия с исполнителя', `${paymentData.getCommission()} ${currency}`)}
                    {paymentData.isRequisite() && infoItem(paymentData.getRequisiteTitle(), paymentData.getRequisiteDescription())}
                    {infoItem('Назначение платежа', paymentData.getPaymentPurpose())}

                    <Receipt payment={paymentData.getOriginal()}/>

                    {paymentData.isHistory() &&
                    <div className={cx.history}>
                        <Header.H2>История</Header.H2>
                        {isMobile ? renderHistoryList() : renderHistoryTable()}
                    </div>
                    }

                </Popup.Content>
                <Popup.Footer className={clsFooter}>
                    {
                        isMobile && canRefund &&
                        <DropActions
                            disabled={disableControls}
                            loading={isRefundMutating}
                            actions={[{
                                label: 'Зачислить на баланс исполнителю',
                                action: () => onRefund(payment.id)
                            }]}
                        />
                    }
                    {
                        canBeCancel && <Button
                            disabled={disableControls}
                            loading={isCancelMutating}
                            onClick={() => onCancel(payment.id)}
                            styling='hollow-border'
                        >
                            Отменить выплату
                        </Button>
                    }
                    {
                        !isMobile && canRefund && <Button
                            styling='hollow-border'
                            disabled={disableControls}
                            loading={isRefundMutating}
                            onClick={() => onRefund(payment.id)}
                        >
                            Зачислить на баланс исполнителю
                        </Button>
                    }

                    {
                        canConfirm && <Button
                            disabled={disableControls}
                            loading={confirm.isLoading}
                            onClick={() => onConfirmPayment(payment.id)}
                        >
                            Оплатить
                        </Button>
                    }

                    {
                        canRepeat &&
                        <Button
                            disabled={disableControls}
                            loading={isRepeatMutating}
                            onClick={() => onRepeat(payment.id)}
                        >
                            Повторить
                        </Button>
                    }

                </Popup.Footer>
            </Popup>

            {needConfirmOtp && isShowConfirmModal &&
            <PaymentItemConfirmModal
                isRetrying={confirm.isLoading}
                responseOtp={confirm.payment}
                onSuccessConfirm={onSuccessConfirmOtp}
                onRetryOtp={handleOnRetryOtp}
                onDismiss={() => setIsShowConfirmModal(false)}
            />
            }
        </Fragment>
    )
}

PaymentsDetailModal.propTypes = {
    payment: PropTypes.any.isRequired,
    onClose: PropTypes.func.isRequired,
    onUpdatePayment: PropTypes.func.isRequired,
}
