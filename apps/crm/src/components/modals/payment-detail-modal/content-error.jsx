import React from 'react';
import PropTypes from 'prop-types';

import { Popup } from '@justlook/uikit';

const ContentError = ({ onClose, error, title }) => {

    const {detail: errorDetail, title: errorTitle} = error?.errorData || {};

    return (
        <Popup onDismiss={onClose}>
            <Popup.Header description={errorDetail}>
                { title || 'Ошибка выплаты' }
            </Popup.Header>
            <Popup.Content>
                { errorTitle || 'Возникла неизвестная ошибка' }
            </Popup.Content>
        </Popup>
    )
}

ContentError.propTypes = {
    onClose: PropTypes.func.isRequired,
    title: PropTypes.string,
    error: PropTypes.any,
}

export default ContentError
