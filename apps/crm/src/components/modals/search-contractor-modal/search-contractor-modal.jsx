import React, { Fragment, useCallback, useState } from 'react';
import PropTypes from 'prop-types';
import { useForm, Controller } from 'react-hook-form';

import { Popup, Button, Form, Input, Description } from '@justlook/uikit';

import { getFio, hasFio } from 'utils/person';
import { SearchContractor, SplashScreen } from 'components';
import { validatePhone } from 'utils/helpers';

import cx from './search-contractor-modal.module.scss';

const FORM_ID = 'search-contractor-form';

const SearchContractorModal = ({ initialState, onDismiss, onSubmit, onChange, isLoading }) => {
    const [ searching, setSearching ] = useState(false);
    const [ contractor, setContractor ] = useState(initialState || null);
    const [ description, setDescription ] = useState(initialState?.description || null);
    const [ isShowNameFields, setIsShowNameFields ] = useState(initialState?.isShowNameFields || false);
    const [ needCreateContractor, setNeedCreateContractor ] = useState(initialState?.needCreateContractor || false);

    const { handleSubmit, control, setValue, errors, watch, getValues } = useForm();

    const isDisabled = searching || !contractor || isLoading;

    const needFillFullName = needCreateContractor || isShowNameFields;

    const searchValue = watch('search');

    const setSearchState = async (searchValue, description) => {
        await setValue('search', searchValue);
        await setDescription(description);
    }

    const handleOnSelect = async (contractor) => {
        setContractor(contractor);

        if(!hasFio(contractor)) {
            setValue('search', contractor?.phone);
            setIsShowNameFields(true);

            return;
        }

        if(validatePhone(searchValue)) {
            setSearchState(searchValue, getFio(contractor))
                .then(submit({
                    description: getFio(contractor),
                    ...contractor,
                    ...getValues(),
                }));
        } else {
            setSearchState(getFio(contractor), searchValue)
                .then(submit({
                    description: contractor?.phone,
                    ...contractor,
                    ...getValues(),
                }));
        }
    }

    const handleOnManualChange = (value) => {
        setContractor(null);
        setDescription(null);
        setNeedCreateContractor(false);
        setIsShowNameFields(false);

        onChange(value);
    }

    const handleNeedCreate = (value) => {
        setContractor(value);
        setNeedCreateContractor(value);
    }

    const submit = useCallback((values) => {
        onSubmit({
            isShowNameFields,
            needCreateContractor,
            description,
            ...contractor,
            ...initialState,
            ...values,
        });
    }, [ contractor, description, initialState, isShowNameFields, needCreateContractor, onSubmit ]);

    return (
        <Popup onDismiss={onDismiss}>
            <Popup.Header>
                Новая выплата
            </Popup.Header>
            <Popup.Content>
                { isLoading && <SplashScreen/> }
                {
                    !isLoading &&
                    <Form id={FORM_ID} onSubmit={handleSubmit(submit)}>
                        <Controller
                            as={SearchContractor}
                            control={control}
                            label='Получатель'
                            name='search'
                            placeholder='ФИО или номер телефона'
                            defaultValue={initialState?.search ?? null}
                            onSelect={handleOnSelect}
                            onManualChange={handleOnManualChange}
                            changeNeedCreate={handleNeedCreate}
                            changeIsSearching={setSearching}
                        />
                        {
                            !needFillFullName &&
                            <Description.LH16 className={cx.description}>{description}</Description.LH16>
                        }

                        {
                            needFillFullName &&
                            <Fragment>
                                <Controller
                                    as={Input}
                                    control={control}
                                    name='last_name'
                                    label='Фамилия'
                                    defaultValue={initialState?.last_name || contractor?.last_name || null}
                                    className={cx.grid}
                                    maxLength={50}
                                    rules={{ required: 'Обязательно для ввода' }}
                                    error={errors.last_name && errors.last_name.message}
                                />

                                <Controller
                                    as={Input}
                                    control={control}
                                    name='first_name'
                                    label='Имя'
                                    defaultValue={initialState?.first_name || contractor?.first_name || null}
                                    className={cx.grid}
                                    maxLength={50}
                                    rules={{ required: 'Обязательно для ввода' }}
                                    error={errors.first_name && errors.first_name.message}
                                />

                                <Controller
                                    as={Input}
                                    control={control}
                                    name='middle_name'
                                    label='Отчество'
                                    className={cx.grid}
                                    maxLength={50}
                                    defaultValue={initialState?.middle_name || contractor?.middle_name || null}
                                />
                            </Fragment>
                        }
                    </Form>
                }
            </Popup.Content>
            <Popup.Footer>
                <Button
                    styling='hollow-border'
                    onClick={onDismiss}
                >
                    Отмена
                </Button>

                <Button
                    form={FORM_ID}
                    type='submit'
                    disabled={isDisabled}
                >
                    Далее
                </Button>
            </Popup.Footer>
        </Popup>
    )
}

SearchContractorModal.propTypes = {
    initialState: PropTypes.object,
    onDismiss: PropTypes.func.isRequired,
    onSubmit: PropTypes.func,
    onChange: PropTypes.func,
    isLoading: PropTypes.bool,
}

export default SearchContractorModal;
