import React, { Fragment, useState } from 'react';
import PropTypes from 'prop-types';

import { Popup, Button, ParagraphV2 as Paragraph, InfoMessage } from '@justlook/uikit';

import { SplashScreen, FormOtp } from 'components';
import { useSelectedPaymentsContext } from 'domains/payments/contexts';
import { useConfirmListPayments, useConfirmOtp } from 'domains/payments/mutators';
import { errorToString } from 'utils/add-server-errors';

import cx from './payment-list-confirm-modal.module.scss';

export const PaymentListConfirmModal = ({ onSuccess, onDismiss }) => {
    const [ code, setCode ] = useState('');

    const { listPayments, statistic, isLoadingInfo, onClearAll } = useSelectedPaymentsContext();

    const onSuccessConfirm = () => {
        onClearAll();
        reset();
        resetOtp();
        onSuccess();
        onDismiss();
    }

    const {
        onConfirm, isConfirming, needConfirmOtp, confirmError, confirmIsError, reset, response
    } = useConfirmListPayments(onSuccessConfirm);

    const {
        onConfirmOtp, isConfirmingOtp, resetOtp, confirmOtpError
    } = useConfirmOtp(onSuccessConfirm)

    const onCloseHandler = () => {
        reset()
        resetOtp()
        onDismiss();
    }

    const onConfirmPayments = () => onConfirm(listPayments.join(','))

    const onConfirmOtpPayments = () => {
        const { hash } = response;

        onConfirmOtp({ code, hash })
    }

    const onConfirmHandler = () => needConfirmOtp ? onConfirmOtpPayments() : onConfirmPayments();

    return (
        <Popup onDismiss={isConfirming || isConfirmingOtp ? null : onCloseHandler}>
            <Popup.Header>
                Подтверждение выплат
            </Popup.Header>

            <Popup.Content>
                {
                    isLoadingInfo
                        ? <SplashScreen/>
                        : (
                            <Fragment>
                                <Paragraph.LH24 className={cx.info}>
                                    { statistic }
                                </Paragraph.LH24>

                                {needConfirmOtp && response?.message &&
                                <Paragraph.LH24>
                                    {response?.message}
                                </Paragraph.LH24>
                                }

                                {needConfirmOtp && !isConfirming &&
                                <FormOtp
                                    seconds={response?.timer}
                                    code={code}
                                    setCode={setCode}
                                    onRetryOtp={onConfirmPayments}
                                    error={errorToString(confirmOtpError)}
                                />
                                }
                            </Fragment>
                        )
                }

                { confirmIsError &&
                <InfoMessage type='error'>
                    { errorToString(confirmError) }
                </InfoMessage>
                }
            </Popup.Content>

            <Popup.Footer
                majorButton={
                    <Button
                        loading={isConfirming || isConfirmingOtp}
                        disabled={isLoadingInfo || isConfirming || isConfirmingOtp}
                        onClick={onConfirmHandler}
                    >
                        Подтвердить
                    </Button>
                }
                onDismissButton={
                    <Button
                        disabled={isConfirming || isConfirmingOtp}
                        styling='hollow-border'
                        onClick={onCloseHandler}
                    >
                        Отмена
                    </Button>
                }
            />
        </Popup>
    )
}

PaymentListConfirmModal.propTypes = {
    onSuccess: PropTypes.func.isRequired,
    onDismiss: PropTypes.func,
}
