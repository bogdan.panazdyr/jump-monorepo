import React from 'react';
import PropTypes from 'prop-types';

import { ParagraphV2 as Paragraph } from '@justlook/uikit';

import { getFio } from 'utils/person';
import { useMoney } from 'hooks/use-money';

import cx from './contractor-info.module.scss';

export const ContractorInfo = ({ contractor }) => {
    const { currency, format } = useMoney();

    return (
        <div className={cx.base}>
            <Paragraph.LH24 className={cx.title}>Получатель</Paragraph.LH24>
            <div className={cx.content}>
                <Paragraph.LH24 className={cx.fio}>
                    <b>{ getFio(contractor) }</b>
                </Paragraph.LH24>
                <Paragraph.LH24>
                    { contractor?.phone ? contractor?.phone : null }
                    { ' · На балансе: ' + format(contractor?.balance?.value || 0) + ' ' + currency }
                </Paragraph.LH24>
            </div>
        </div>
    )
}

ContractorInfo.propTypes = {
    contractor: PropTypes.shape({
        first_name: PropTypes.string.isRequired,
        last_name: PropTypes.string.isRequired,
        middle_name: PropTypes.string,
        phone: PropTypes.string.isRequired,
        balance: PropTypes.object,
    }),
}
