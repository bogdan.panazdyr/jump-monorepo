import React, { useCallback, Fragment } from 'react';
import PropTypes from 'prop-types';
import { debounce } from 'debounce';

import { Input, ParagraphV2 as Paragraph } from '@justlook/uikit';

import cx from './validate-sum-field.module.scss';

export const ValidateAmount = ({ onChange, onValidate, validationText, ...rest }) => {
    // eslint-disable-next-line
    const handleOnSearch = useCallback(debounce((value) => {
        if(value) {
            onValidate(value);
        }
    }, 500), [onValidate]);

    const handleOnChange = (value) => {
        handleOnSearch.clear();
        handleOnSearch(value);
        onChange(value);
    }

    return (
        <Fragment>
            <Input type='number' className={cx.middle} onChange={handleOnChange} {...rest}/>
            { validationText && <Paragraph.LH24 className={cx.infoPay}>{ validationText }</Paragraph.LH24> }
        </Fragment>
    )
}

ValidateAmount.propTypes = {
    onChange: PropTypes.func.isRequired,
    onValidate: PropTypes.func.isRequired,
    validationText: PropTypes.string,
}
