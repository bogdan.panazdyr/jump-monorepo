import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Controller, useFormContext } from 'react-hook-form';

import { Forms } from '@justlook/core';
import { Select } from '@justlook/uikit';

import { useMoney } from 'hooks/use-money';
import { Box } from 'components';
import { useInitialBanksFields, useBankFieldsValues } from 'app/payments/hooks';
import { useValidateAmount } from 'domains/payments/services';
import { prepareRequestCustomFields, prepareStepFormRequestValues } from 'domains/payments/helpers';
import { WITHDRAW_BALANCE_COMPANY, WITHDRAW_BALANCE_CONTRACTOR } from 'domains/payments/constants';

import { AccountNumber } from './account-number-field';
import { ValidateAmount } from './validate-sum-field';
import { CustomFields } from './custom-fields';

import cx from './banks-fields.module.scss';

export const BanksFields = ({ formValues, onLoading, onValidating, customFields }) => {
    const { control, setError, clearErrors, watch, setValue, getValues, errors } = useFormContext()

    const { currency, format } = useMoney();

    const { requisites, agents, defaultAgent, isLoading } = useInitialBanksFields(formValues?.id);
    const { fetchValidating, validateInfo, isValidating } = useValidateAmount(setError, clearErrors);
    const bankFieldsHasValues = useBankFieldsValues();

    const PAYMENT_FROM_OPTIONS = [
        {
            label: `С баланса исполнителя · ${format(formValues?.balance?.value || 0)} ${currency}`,
            value: WITHDRAW_BALANCE_CONTRACTOR
        },
        {
            label: 'Со счета компании без учета баланса исполнителя',
            value: WITHDRAW_BALANCE_COMPANY
        }
    ]

    const isEmptyBalance = !formValues?.needCreateContractor && formValues?.balance?.value === 0;
    const isPaymentFromContractor = watch('payment_from')?.value === WITHDRAW_BALANCE_CONTRACTOR;

    const isDisabledPaymentFrom = isEmptyBalance || formValues?.needCreateContractor;
    const isDisabledAgent = !formValues?.needCreateContractor && isPaymentFromContractor;

    const contractorAgent = formValues?.agent && Forms.itemToOption(formValues?.agent);
    const initialAgent = defaultAgent && Forms.itemToOption(defaultAgent);

    useEffect(() => onLoading(isLoading), [ isLoading, onLoading ]);
    useEffect(() => onValidating(isValidating), [ isValidating, onValidating ]);

    useEffect(() => {
        if (isDisabledAgent) {
            setValue('agent_id', contractorAgent)
        }
        // eslint-disable-next-line
    }, [isDisabledAgent]);

    const handleChangeAccountNumber = (name, value) => {
        setValue(name, value);
        clearErrors(name);
    }

    const handleChangePaymentFrom = (option, callback) => {
        callback(option);
        fetchCommission(option.value);
    }

    const fetchCommission = (withdrawType) => {
        if (bankFieldsHasValues()) {
            fetchValidating(prepareStepFormRequestValues({
                ...formValues,
                ...getValues(),
                agent_id: withdrawType ===  WITHDRAW_BALANCE_CONTRACTOR
                    ? contractorAgent?.value
                    : getValues('agent_id')?.value,
                ...prepareRequestCustomFields(customFields, getValues())
            }))
        }
    }

    if (isLoading) {
        return null;
    }

    return (
        <Box mt={16}>
            <div className={cx.large}>
                <label>
                    Списать
                </label>
                <Controller
                    name='payment_from'
                    control={control}
                    defaultValue={isDisabledPaymentFrom ? PAYMENT_FROM_OPTIONS[1] : PAYMENT_FROM_OPTIONS[0]}
                    render={({ onChange, ...rest }) => (
                        <Select
                            options={PAYMENT_FROM_OPTIONS}
                            onChange={(option) => handleChangePaymentFrom(option, onChange)}
                            rules={{ required: 'Обязательно для ввода' }}
                            error={errors.payment_from && errors.payment_from.message}
                            disabled={isDisabledPaymentFrom}
                            {...rest}
                        />
                    )}
                />
            </div>

            <div className={cx.large}>
                <label>
                    Юрлицо
                </label>
                <Controller
                    name='agent_id'
                    control={control}
                    defaultValue={contractorAgent || initialAgent}
                    render={({ onChange, ...rest }) => (
                        <Select
                            options={Forms.makeOptions(agents)}
                            onChange={(option) => {
                                onChange(option);
                                fetchCommission();
                            }}
                            rules={{ required: 'Обязательно для ввода' }}
                            error={errors.agent_id && errors.agent_id.message}
                            disabled={isDisabledAgent}
                            {...rest}
                        />
                    )}
                />
            </div>

            { customFields && !isLoading && !isPaymentFromContractor && <CustomFields fields={customFields}/> }

            <Box mt={40}>
                <Controller
                    as={AccountNumber}
                    control={control}
                    label='Номер карты получателя'
                    name='requisite.account_number'
                    options={requisites}
                    onSelect={handleChangeAccountNumber}
                    onManualChange={handleChangeAccountNumber}
                    actionAfterChange={fetchCommission}
                    rules={{ required: 'Обязательно для ввода' }}
                    error={errors?.requisite?.account_number?.message}
                />

                <Controller
                    as={ValidateAmount}
                    control={control}
                    type='number'
                    name='amount'
                    label='Сумма выплаты'
                    placeholder={'0 ' + currency}
                    onValidate={fetchCommission}
                    validationText={validateInfo}
                    rules={{ required: 'Обязательно для ввода' }}
                    error={errors.amount && errors.amount.message}
                />
            </Box>
        </Box>
    )
}

BanksFields.propTypes = {
    id: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.string,
    ]),
    onLoading: PropTypes.func,
    onValidating: PropTypes.func,
    formValues: PropTypes.object,
    customFields: PropTypes.object,
}
