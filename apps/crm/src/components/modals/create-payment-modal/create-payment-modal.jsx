import React, { useCallback, useState } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { useForm, FormProvider } from 'react-hook-form';

import { Popup, Button, Icons24, Form } from '@justlook/uikit';

import { Box, ErrorMessage, SplashScreen } from 'components';
import { getFeatures } from 'app/init/store/selectors';
import { useCreateSmartPayment } from 'domains/payments/mutators';
import {
    getCustomFieldsInitialValues,
    prepareRequestCustomFields,
    getContractorInfo,
    prepareStepFormRequestValues
} from 'domains/payments/helpers';

import { BanksFields } from './banks-fields';
import { ContractorInfo } from './contractor-info';

const FORM_ID = 'create-payment-form';

const CreatePaymentModal = ({ initialState, onBack, onDismiss, onSuccess, submitLabel, isLoading: loading }) => {
    const [ isLoading, setIsLoading ] = useState(false);
    const [ isValidating, setIsValidating ] = useState(false);

    const features = useSelector(getFeatures);
    const customFields = features?.payment_purpose || null;

    const methods = useForm({
        defaultValues: {
            payment_from: initialState?.payment_from,
            agent_id: initialState?.agent_id,
            requisite: {
                account_number: initialState?.requisite?.account_number,
            },
            amount: initialState?.amount,
            ...getCustomFieldsInitialValues(customFields, initialState),
        }
    });

    const { onCreate, error, isLoading: isCreating } = useCreateSmartPayment(methods.setError, onSuccess);

    const isLoadingControl = isLoading || isValidating || isCreating || loading;

    const onGoBack = useCallback(() => {
        onBack({ ...initialState, ...methods.getValues() });
    }, [ initialState, methods, onBack ]);

    const onSubmit = (values) => onCreate(prepareStepFormRequestValues({
        ...initialState,
        ...values,
        agent_id: values?.agent_id?.value,
        ...prepareRequestCustomFields(customFields, values),
    }));

    return (
        <Popup onDismiss={isCreating ? null : onDismiss}>
            <Popup.Header>
                Новая выплата
            </Popup.Header>
            <Popup.Content>
                { loading && <SplashScreen/> }
                {
                    !loading && (
                        <FormProvider {...methods}>
                            <Form id={FORM_ID} onSubmit={methods.handleSubmit(onSubmit)}>
                                <ContractorInfo contractor={getContractorInfo(initialState)}/>

                                {
                                    isLoading &&
                                    <Box mt={42}>
                                        <SplashScreen/>
                                    </Box>
                                }

                                <BanksFields
                                    formValues={initialState}
                                    onLoading={setIsLoading}
                                    onValidating={setIsValidating}
                                    customFields={customFields}
                                />

                                {error?.responseCode === 500 && <ErrorMessage message={error?.message}/>}
                                {methods.errors?.permission && <ErrorMessage message={methods.errors?.permission?.message}/>}
                            </Form>
                        </FormProvider>
                    )
                }
            </Popup.Content>
            <Popup.Footer
                majorButton={
                    <Button
                        type='submit'
                        form={FORM_ID}
                        disabled={isLoadingControl}
                        loading={isLoadingControl}
                    >
                        {submitLabel}
                    </Button>
                }
                onDismissButton={
                    <Button
                        styling='hollow-border'
                        onClick={onDismiss}
                        disabled={isLoadingControl}
                    >
                        Отмена
                    </Button>
                }
                minorButton={
                    onBack &&
                    <Button
                        icon={<Icons24.IconBack/>}
                        styling='hollow-border'
                        onClick={onGoBack}
                        disabled={isLoadingControl}
                    >
                        Назад
                    </Button>
                }
            />
        </Popup>
    )
}

CreatePaymentModal.defaultProps = {
    submitLabel: 'Создать'
}

CreatePaymentModal.propTypes = {
    initialState: PropTypes.object,
    onBack: PropTypes.func,
    onDismiss: PropTypes.func.isRequired,
    onSuccess: PropTypes.func.isRequired,
    submitLabel: PropTypes.string,
    isLoading: PropTypes.bool,
}

export default CreatePaymentModal;
