import React, { useState, useRef } from 'react';
import PropTypes from 'prop-types';

import { Input } from '@justlook/uikit';

import { useClickOutside } from 'hooks/use-click-outside';

import cx from './account-number-field.module.scss';

export const AccountNumber = (
    { options, onSelect, onManualChange, actionAfterChange, onChange: onChangeController, ...rest }
) => {
    const [ isShowOptions, setIsShowOptions ] = useState(false);
    const ref = useRef(null);

    const handleManualChangeValue = (value) => {
        const prepareValue = value.split(' ').join('');

        onManualChange(rest.name, prepareValue);
        onChangeController(prepareValue);

        const isValidAccountNumber = value.length === 19 || value.length === 22;

        if(isValidAccountNumber) {
            actionAfterChange(value)
        }

        return null;
    }

    const handleSelectOption = (option, callback) => {
        if(option) {
            onSelect(rest.name, option);
            actionAfterChange(option);
            onChangeController(option);
        }

        callback(false);
    }

    const renderOptions = (options) => {
        return (
            <div className={cx.options}>
                {options.map((item, index) => (
                    <div
                        key={index}
                        className={cx.option}
                        onClick={() => handleSelectOption(item.requisite.account_number, setIsShowOptions)}
                    >
                        {item.requisite.account_number}
                    </div>
                ))}
            </div>
        )
    }

    useClickOutside(ref, () => setIsShowOptions(false));

    return (
        <div className={cx.base} ref={ref}>
            <Input
                {...rest}
                className={cx.grid}
                mask='9999 9999 9999 99?????'
                maskChar={null}
                formatChars={{
                    '9': '[0-9]',
                    '?': '[-\0-9]'
                }}
                onFocus={() => setIsShowOptions(true)}
                onChange={handleManualChangeValue}
            />

            {isShowOptions && options.length > 0 && renderOptions(options) }
        </div>
    )
}

AccountNumber.defaultProps = {
    onChange: () => {}
}

AccountNumber.propTypes = {
    options: PropTypes.array,
    onSelect: PropTypes.func,
    onManualChange: PropTypes.func,
    actionAfterChange: PropTypes.func,
    onChange: PropTypes.func,
}
