import React from 'react';
import PropTypes from 'prop-types';
import { Controller, useFormContext } from 'react-hook-form';

import { CalendarV2 as Calendar, Input } from '@justlook/uikit';

import { parseValidator } from 'utils/validator-parser';

import cx from './custom-fields.module.scss';

export const Fields = ({ fields }) => {
    const { control, errors } = useFormContext();

    return fields.map(field => (
        <Controller
            key={field.key}
            as={field.validator.match('iso_date') ? Calendar : Input}
            control={control}
            defaultValue={''}
            name={field.key}
            label={field.title}
            className={cx.grid}
            rules={parseValidator(field.validator)}
            error={errors[field.key] && errors[field.key].message}
        />
    ))
}

Fields.propTypes = {
    fields: PropTypes.array.isRequired,
}
