import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Controller, useFormContext } from 'react-hook-form';

import { Forms } from '@justlook/core';
import { ParagraphV2 as Paragraph, Select } from '@justlook/uikit';

import { Fields } from './fields';

import cx from './custom-fields.module.scss';

export const FieldsByCondition = ({ fields }) => {
    const { control, errors, watch } = useFormContext();

    const { key, title, templates } = fields;

    const defaultCondition = Forms.itemToOption(templates[0], { label: 'title', value: 'key' });

    const [ selectedCondition, setSelectedCondition ] = useState(watch(key) || defaultCondition);

    const getResult = () => templates.filter(template => template.key === selectedCondition.value)[0];

    return (
        <>
            <div className={cx.grid}>
                <Paragraph.LH24>{ title }</Paragraph.LH24>
                <Controller
                    control={control}
                    name={key}
                    defaultValue={selectedCondition}
                    render={({ onChange, ...rest }) => (
                        <Select
                            options={Forms.makeOptions(templates, { label: 'title', value: 'key' })}
                            onChange={(option) => {
                                onChange(option);
                                setSelectedCondition(option);
                            }}
                            error={errors[key] && errors[key].message}
                            {...rest}
                        />
                    )}
                />
            </div>

            <Fields fields={getResult()?.placeholders}/>
        </>
    )
}

FieldsByCondition.propTypes = {
    fields: PropTypes.object.isRequired,
}
