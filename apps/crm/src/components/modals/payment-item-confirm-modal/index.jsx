import React, { useState } from 'react';
import PropTypes from 'prop-types';

import { Button, ParagraphV2 as Paragraph, Popup } from '@justlook/uikit';

import { errorToString } from 'utils/add-server-errors';
import { FormOtp } from 'components';
import { useConfirmOtp } from 'domains/payments/mutators';

export const PaymentItemConfirmModal = ({ responseOtp, onDismiss, onSuccessConfirm, onRetryOtp, isRetrying }) => {
    const [ code, setCode ] = useState('');

    const onSuccessConfirmOtp = () => {
        onSuccessConfirm();
        resetOtp();
        onDismiss();
    }

    const onCloseHandler = () => {
        resetOtp()
        onDismiss();
    }

    const {
        onConfirmOtp, isConfirmingOtp, resetOtp, confirmOtpError
    } = useConfirmOtp(onSuccessConfirmOtp)

    const onConfirmHandler = () => onConfirmOtp({ code, hash: responseOtp?.hash })

    return (
        <Popup onDismiss={isConfirmingOtp || isRetrying ? null : onCloseHandler}>
            <Popup.Header>
                Подтверждение выплаты
            </Popup.Header>

            <Popup.Content>
                <Paragraph.LH24>{responseOtp?.message || ''}</Paragraph.LH24>

                <FormOtp
                    seconds={responseOtp?.timer || 30}
                    code={code}
                    setCode={setCode}
                    onRetryOtp={onRetryOtp}
                    error={errorToString(confirmOtpError)}
                />
            </Popup.Content>

            <Popup.Footer
                majorButton={
                    <Button
                        loading={isConfirmingOtp}
                        disabled={isConfirmingOtp || isRetrying}
                        onClick={onConfirmHandler}
                    >
                        Подтвердить
                    </Button>
                }
                onDismissButton={
                    <Button
                        disabled={isConfirmingOtp || isRetrying}
                        styling='hollow-border'
                        onClick={onCloseHandler}
                    >
                        Отмена
                    </Button>
                }
            />
        </Popup>
    )
}

PaymentItemConfirmModal.propTypes = {
    responseOtp: PropTypes.object,
    onDismiss: PropTypes.func,
    onSuccessConfirm: PropTypes.func,
    onRetryOtp: PropTypes.func,
    isRetrying: PropTypes.bool,
}
