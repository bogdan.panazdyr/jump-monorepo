import React from 'react';

export function Logo () {
    return (
        <svg width='96' height='96' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <rect width='96' height='96' rx='16' fill='#2691FF'/>
            <path  fill='#fff' fillRule='evenodd' clipRule='evenodd' d='M58.2 18c4.38 0 7.8 3.477 7.8 7.8V60c0 11.7-9 18-18 18s-18-6.3-18-18v-9.6c0-4.322 3.42-7.8 7.8-7.8s7.8 3.477 7.8 7.8V60c0 1.182.9 2.4 2.4 2.4s2.4-1.218 2.4-2.4V25.8c0-4.323 3.42-7.8 7.8-7.8z'/>
            <path fill='#9CF' fillRule='evenodd' clipRule='evenodd' d='M30 50.4V60c0 5.633 4.567 10.2 10.2 10.2 5.633 0 10.2-4.567 10.2-10.2 0 1.182-.9 2.4-2.4 2.4s-2.4-1.218-2.4-2.4v-9.6c0-4.322-3.419-7.8-7.8-7.8S30 46.077 30 50.4z'/>
        </svg>
    )
}