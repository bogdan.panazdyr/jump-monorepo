import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Hooks } from '@justlook/core';
import { Icons24, Header } from '@justlook/uikit';

import cx from './drop-popup.module.scss';

export const DropPopup = ({ renderControl, header, description, customControls }) => {
    const { isShow, onOpenForm, onCloseForm } = Hooks.useModalState(false);

    return (
        <Fragment>
            {renderControl(onOpenForm)}
            {isShow &&
                <div className={cx.base}>
                    <div className={cx.shadow}/>
                    <div className={cx.wrapper}>
                        <div className={cx.header}>
                            <Header.H3>{header}</Header.H3>
                            <Icons24.IconClose onClick={onCloseForm}/>
                        </div>
                        <div className={cx.content}>
                            { description }
                        </div>
                        <div className={cx.controls}>
                            { customControls }
                        </div>
                    </div>
                </div>
            }
        </Fragment>
    )
}

DropPopup.propTypes = {
    renderControl: PropTypes.func.isRequired,
    header: PropTypes.string,
    description: PropTypes.string,
    customControls: PropTypes.any,
}
