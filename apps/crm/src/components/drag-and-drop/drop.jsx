import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import { Icons24, ParagraphV2 as Paragraph, Description, Button } from '@justlook/uikit';
import { useIfMediaScreen } from '@justlook/uikit/lib/hooks';

import { DropPopup } from './drop-popup';

import cx from './drop.module.scss';

const reorder = (list, startIndex, endIndex, setState) => {
    const result = Array.from(list);
    const [ removed ] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);

    return setState(result);
};

const getItemStyle = (isDragging, draggableStyle) => ({
    ...draggableStyle,
    userSelect: 'none',
    boxShadow: isDragging && '0px 4px 12px rgba(204, 207, 219, 0.5)',
    display: isDragging && 'inline-flex',
    paddingRight: isDragging && '16px',
    width: isDragging && 'auto',
});

const DragDrop = ({ elements, onSetElements, accessor, renderCustomControl }) => {
    const onDragEnd = (result) => {
        if (!result.destination) return;

        reorder(
            elements,
            result.source.index,
            result.destination.index,
            onSetElements
        );
    }

    return (
        <DragDropContext onDragEnd={onDragEnd}>
            <Droppable droppableId='droppable'>
                {(provided) => (
                    <div {...provided.droppableProps} ref={provided.innerRef}>
                        {elements.map((item, index) => (
                            <Draggable key={String(item.id)} draggableId={String(item.id)} index={index}>
                                {(provided, snapshot) => (
                                    <div
                                        ref={provided.innerRef}
                                        {...provided.draggableProps}
                                        {...provided.dragHandleProps}
                                        style={getItemStyle(snapshot.isDragging, provided.draggableProps.style)}
                                        className={cx.item}
                                    >
                                        <div className={cx.content}>
                                            <Icons24.IconKebabVertical/>
                                            <Paragraph.LH24>
                                                {item[accessor]}
                                            </Paragraph.LH24>
                                        </div>
                                        {!snapshot.isDragging && renderCustomControl(item)}
                                    </div>
                                )}
                            </Draggable>
                        ))}
                        {provided.placeholder}
                    </div>
                )}
            </Droppable>
        </DragDropContext>
    )
}

export const Drop = ({ elements, onSetElements, renderCustomControl, accessor, header }) => {
    const isMobile = useIfMediaScreen();

    if(isMobile) {
        return (
            <DragDrop
                elements={elements}
                onSetElements={onSetElements}
                accessor={accessor}
                renderCustomControl={(item) =>
                    <DropPopup
                        header={header}
                        description={item[accessor]}
                        renderControl={(open) => <Button
                            onClick={open}
                            icon={<Icons24.IconArrowRight className={cx.icon}/>}
                            styling='hollow'
                        />}
                        customControls={renderCustomControl(item)}
                    />
                }
            />
        )
    }

    return (
        <Fragment>
            {header &&
                <div className={cx.header}>
                    <Description.LH24>
                        {header}
                    </Description.LH24>
                </div>
            }

            <DragDrop
                elements={elements}
                onSetElements={onSetElements}
                accessor={accessor}
                renderCustomControl={renderCustomControl}
            />
        </Fragment>
    )
}

DragDrop.propTypes = {
    elements: PropTypes.array.isRequired,
    onSetElements: PropTypes.func,
    accessor: PropTypes.string,
    renderCustomControl: PropTypes.any,
}

Drop.propTypes = {
    elements: PropTypes.array.isRequired,
    onSetElements: PropTypes.func,
    renderCustomControl: PropTypes.any,
    accessor: PropTypes.string,
    header: PropTypes.string,
}
