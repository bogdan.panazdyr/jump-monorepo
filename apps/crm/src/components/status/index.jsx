import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import { ParagraphV2 as Paragraph } from '@justlook/uikit';

import cx from './status.module.scss';

export const Status = ({ text, icon, color, className }) => {
    const classes = classnames(cx.base, className, {
        [cx.success]: color === 'success',
        [cx.info]: color === 'info',
        [cx.warning]: color === 'warning',
        [cx.danger]: color === 'danger',
    })

    if(!text && !icon) return null;

    return (
        <div className={classes}>
            { icon }
            {
                text &&
                <Paragraph.LH18 className={cx.text}>
                    { text }
                </Paragraph.LH18>
            }
        </div>
    )
}

Status.propTypes = {
    text: PropTypes.string,
    icon: PropTypes.any,
    color: PropTypes.string,
    className: PropTypes.string,
}
