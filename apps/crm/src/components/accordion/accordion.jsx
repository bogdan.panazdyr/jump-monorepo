import React, { useState } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { Icons24 } from '@justlook/uikit';
import { useIfMediaScreen } from '@justlook/uikit/lib/hooks';

import cx from './accordion.module.scss'

export const Accordion = ({ initialOpen, title, badge, disabled, children }) => {
    const [ containerState, setContainerState ] = useState(initialOpen);
    const isMobile = useIfMediaScreen();

    if (!children) return null;

    const cls = classnames(cx.accordion, {[cx.mobile]: isMobile});
    const containerCls = classnames(cx.container, {[cx.visible]: containerState});
    const headerCls = classnames(cx.header, {[cx.disabled]: disabled});

    const onChangeCollapsed = () => {
        !disabled && setContainerState(!containerState);
    }

    return (
        <div className={cls}>
            <div className={headerCls} onClick={onChangeCollapsed}>
                {containerState && !disabled ? <Icons24.IconArrowDown/> : <Icons24.IconArrowRight/>}
                <span className={cx.title}>{title}</span>
                {Boolean(badge) && (<div className={cx.badge}>{badge}</div>) }
            </div>
            <div className={containerCls}>
                {children}
            </div>
        </div>
    );
}

Accordion.defaultProps = {
    initialOpen: false,
    title: null,
}

Accordion.propTypes = {
    initialOpen: PropTypes.bool,
    title: PropTypes.string.isRequired,
    badge: PropTypes.any,
    disabled: PropTypes.bool,
    children: PropTypes.node
}
