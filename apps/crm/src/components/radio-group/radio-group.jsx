import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { RadioButtonGroup, RadioButton, ParagraphV2 as Paragraph } from '@justlook/uikit';

import cx from 'components/radio-group/radio-group.module.scss';

export const RadioGroup = ({ fields, onActionChange }) => {
    return (
        <Fragment>
            {fields.map((item, index) => (
                <div className={cx.base} key={index}>
                    <Paragraph.LH24 className={cx.title}>{ item.title }</Paragraph.LH24>
                    <RadioButtonGroup
                        defaultSelected={item.value}
                        name={item.id}
                        onChange={(value) => onActionChange(item.id, value)}
                        vertical
                    >
                        {item.options.map((option, index) => (
                            <RadioButton
                                key={index}
                                className={cx.radio}
                                id={item.id + option.value}
                                label={option.title}
                                value={option.value}
                            />
                        ))}
                    </RadioButtonGroup>
                </div>
            ))}
        </Fragment>
    )
}

RadioGroup.propTypes = {
    fields: PropTypes.array.isRequired,
    onActionChange: PropTypes.func,
}
