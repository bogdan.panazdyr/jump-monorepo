import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import cx from './button-group.module.scss';

const ButtonGroup = ({ children, className, ...rest }) => {
    const cls = classnames(cx.base, className);

    return (
        <div className={cls} {...rest}>
            { children }
        </div>
    )
}

ButtonGroup.propTypes = {
    className: PropTypes.string,
    children: PropTypes.any
};

export default ButtonGroup;
