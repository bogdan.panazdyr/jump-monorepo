import React, { Fragment, useEffect } from 'react';
import { Helmet } from 'react-helmet';
import { useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import { Header, ParagraphV2 as Paragraph, OverflowMenu, Icons24, Icons16, Button } from '@justlook/uikit';
import { Hooks } from '@justlook/core';

import { errorToString } from 'utils/add-server-errors';

import { SplashScreen } from 'components';

import { UserAction } from 'app/users/store/actions';
import { useHasPermission } from 'app/profile/store/selectors';
import { usePermissionState, getFormState } from 'app/users/store/selectors';

import { PermissionSwitch, AgentPermissionSwitch } from 'app/users/components';
import UserModal from 'app/users/components/modal/user-modal';

import cx from './user-details-page.module.scss';

const FORM_UPDATE = 'update';

const Page = () => {
    const { id } = useParams();
    const { isOpen, onOpen, onClose } = Hooks.useModal();

    const onFetchPermissions = Hooks.useActionToDispatch(UserAction.fetchPermissions);
    const onRecoveryUser = Hooks.useActionToDispatch(UserAction.recoveryUser);
    const onDeleteUser = Hooks.useActionToDispatch(UserAction.deleteUser);

    const {user, permissions, agents, isRequest, error} = usePermissionState();

    const { isSubmitting } = useSelector(getFormState);
    const canWriteUser = useHasPermission('write-users');

    useEffect(() => {
        onFetchPermissions(id);
    }, [ id, onFetchPermissions ]);

    const recoveryUser = () => onRecoveryUser(id);
    const deleteUser = () => onDeleteUser(id);

    const renderPermissions = () => {
        return isRequest  ? <SplashScreen/> : (
            <Fragment>
                { error && errorToString(error) }

                <div className={cx.subHeader}>
                    <Header.H2>Доступ к юрлицам</Header.H2>
                </div>

                {
                    !isRequest && agents && agents.map(item => (
                        <AgentPermissionSwitch disabled={!canWriteUser} key={item.id} item={item} userId={user.id} />
                    ))
                }

                <div className={cx.subHeader}>
                    <Header.H2>Права пользователя</Header.H2>
                </div>

                {
                    !isRequest && permissions && permissions.map(group => {
                        return <div key={group.name} className={cx.group}>
                            <Paragraph.LH24>{group.name}</Paragraph.LH24>
                            <div className={cx.groupContent}>
                                {
                                    group.permissions.map(item => (
                                        <PermissionSwitch disabled={!canWriteUser} key={item.slug} item={item} userId={user.id}/>
                                    ))
                                }

                                {/*{ group.has_group && <a href="/">Доступ к группам: Все</a> }*/}
                            </div>
                        </div>
                    })
                }
            </Fragment>
        )
    }
    const renderNotActiveUser = () => {
        return (
            <Fragment>
                <Paragraph.LH24 className={cx.subHeader}>Пользователь ранее был удален из личного кабинета</Paragraph.LH24>
                <Button className={cx.recover} onClick={recoveryUser} loading={isSubmitting}>Восстановить</Button>
            </Fragment>
        )
    }

    return (
        <div className={cx.page}>
            {
                user && (
                    <Fragment>
                        <Helmet>
                            <title>{user?.name || 'Без имени'}</title>
                        </Helmet>

                        <div className={cx.header}>
                            <div className={cx.titleBlock}>
                                <Header.H1>{user.name}</Header.H1>
                                <Paragraph.LH24>{user.email}</Paragraph.LH24>
                            </div>

                            {
                                user?.is_active &&
                                    <OverflowMenu icon={<Icons24.IconKebab/>}>
                                        <OverflowMenu.Item onItemClick={() => onOpen(FORM_UPDATE)}>
                                            Редактировать
                                        </OverflowMenu.Item>
                                        <OverflowMenu.Item onItemClick={deleteUser}>
                                            <span className={cx.delete}>
                                                Деактивировать
                                                {isSubmitting && <Icons16.IconLoader className={cx['delete-loader']}/>}
                                            </span>
                                        </OverflowMenu.Item>
                                    </OverflowMenu>
                            }
                        </div>
                        { isOpen(FORM_UPDATE) && <UserModal user={user} onDismiss={() => onClose(FORM_UPDATE)}/> }
                    </Fragment>
                )
            }

            { user && !user.is_active ? renderNotActiveUser() : renderPermissions() }
        </div>
    );
};

export default Page;
