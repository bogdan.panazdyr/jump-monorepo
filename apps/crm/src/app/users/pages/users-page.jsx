import React, { Fragment } from 'react';
import { Helmet } from 'react-helmet';
import { useSelector } from 'react-redux';
import { useOutlet } from 'react-router-dom';
import { Config } from '@justlook/core';
import { TwoColumnsLayout } from 'components/menu';
import { withPermissions, Paginator } from 'components';
import { useContentView } from 'utils/browser';
import useUpdateSearchFilter from '../../../hooks/use-update-search-filter';

import { UserList, UserHeader } from '../components';
import { UserAction } from '../store/actions';
import { USER_FILTER } from '../store/filters';
import { getUsersMeta } from '../store/selectors';

const Page = () => {
    const child = useOutlet();
    const viewMode = useContentView(child);
    const { updateFilter, filterFull } = useUpdateSearchFilter(
        USER_FILTER,
        UserAction.fetchUserList
    );
    const meta = useSelector(getUsersMeta);

    return (
        <Fragment>
            <Helmet>
                <title>Пользователи - {Config.brandName}</title>
            </Helmet>

            <TwoColumnsLayout
                viewMode={viewMode}
                details={child}
                header={<UserHeader
                    viewMode={viewMode}
                    updateFilter={updateFilter}
                />}
            >
                <UserList/>

                <Paginator {...meta} {...filterFull}
                           onChangePage={updateFilter}/>
            </TwoColumnsLayout>
        </Fragment>
    );
};

export default withPermissions(Page, [ 'read-users' ]);
