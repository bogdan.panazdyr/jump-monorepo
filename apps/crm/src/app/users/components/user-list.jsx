import React from 'react';
import { useSelector } from 'react-redux';
import { Header, ParagraphV2 as Paragraph } from '@justlook/uikit';

import { ListLink, SplashScreen } from 'components';

import { errorToString } from 'utils/add-server-errors';

import useSearchFilter from '../../../hooks/use-search-filter';

import { getUsers, getUsersError, getUsersIsRequest } from 'app/users/store/selectors';
import { USER_FILTER } from 'app/users/store/filters';

const UserList = () => {
    const { filterQuery } = useSearchFilter(USER_FILTER);
    const isRequest = useSelector(getUsersIsRequest);
    const userList = useSelector(getUsers);
    const error = useSelector(getUsersError);

    if (error) { return errorToString(error) }

    if (isRequest) { return <SplashScreen/>; }

    return (
        <div>
            {
                userList && userList.map(user => (
                    <ListLink
                        key={user.id}
                        to={`/settings-users/${user.id}${filterQuery}`}
                        muted={!user.is_active}
                    >

                        <Header.H2>{user.name || 'Без имени'}</Header.H2>

                        <Paragraph.LH24>
                            {user.email}
                        </Paragraph.LH24>

                    </ListLink>
                ))
            }
        </div>
    );
}

export default UserList;
