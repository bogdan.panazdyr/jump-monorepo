import React, { useCallback, useEffect, useMemo } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { useForm, Controller } from 'react-hook-form';

import { Popup, Button, Form, Input, Password } from '@justlook/uikit';
import { Hooks } from '@justlook/core';

import { addServerErrors } from 'utils/add-server-errors';

import { UserAction } from '../../store/actions';
import { getFormState } from '../../store/selectors';

import cx from './user-modal.module.scss';

const FORM_ID = 'user-form';

const UserModal = ({ user, onDismiss }) => {
    const { handleSubmit, errors, control, setError } = useForm({
        defaultValues: {
            name: user && user.name,
            email: user && user.email,
            phone: user && user.phone,
        }
    });

    const { isSubmitted, isSubmitting, invalidFields } = useSelector(getFormState);
    const onCreateUser = Hooks.useActionToDispatch(UserAction.createUser);
    const onUpdateUser = Hooks.useActionToDispatch(UserAction.updateUser);
    const clearErrors = Hooks.useActionToDispatch(UserAction.clearErrors);

    const onCloseModal = useCallback(() => {
        onDismiss();
        clearErrors();
    }, [ onDismiss, clearErrors ])

    const header = user ? 'Редактирование пользователя' : 'Новый пользователь';
    const submitLabel = user ? 'Сохранить' : 'Добавить';

    useEffect(() => { if(isSubmitted) onCloseModal() }, [isSubmitted, onCloseModal]);

    useMemo(() => { addServerErrors(invalidFields, setError) } , [invalidFields, setError]);

    const onSubmit = (values) => user ? onUpdateUser(user.id, values) : onCreateUser(values);

    return (
        <Popup onDismiss={isSubmitting ? null : onCloseModal} className={cx.popup}>
            <Popup.Header>
                {header}
            </Popup.Header>
            <Popup.Content>
                <Form onSubmit={handleSubmit(onSubmit)} id={FORM_ID}>
                    <Form.Field>
                        <Controller
                            as={Input}
                            className={cx.grid}
                            control={control}
                            label='Имя'
                            name='name'
                            rules={{ required: 'Обязательно для ввода' }}
                            error={errors.name && errors.name.message}
                        />
                    </Form.Field>

                    <Form.Field>
                        <Controller
                            as={Input}
                            label='Номер телефона'
                            name='phone'
                            type='phone'
                            mask='+79999999999'
                            control={control}
                            className={cx.grid}
                            rules={{ required: 'Обязательно для ввода' }}
                            error={errors.phone && errors.phone.message}
                        />
                    </Form.Field>

                    <Form.Field>
                        <Controller
                            as={Input}
                            className={cx.grid}
                            control={control}
                            label='Эл. почта'
                            name='email'
                            type='email'
                            rules={{ required: 'Обязательно для ввода' }}
                            error={errors.email && errors.email.message}
                        />
                    </Form.Field>

                    <Form.Field>
                        <Controller
                            as={Password}
                            className={cx.grid}
                            control={control}
                            label='Пароль'
                            name='password'
                            rules={!user && { required: 'Обязательно для ввода' }}
                            error={errors.password && errors.password.message}
                        />
                    </Form.Field>

                </Form>
            </Popup.Content>

            <Popup.Footer
                onDismissButton={
                    <Button
                        styling='hollow-border'
                        onClick={onCloseModal}
                        disabled={isSubmitting}
                    >
                        Отмена
                    </Button>
                }
                majorButton={
                    <Button
                        form={FORM_ID}
                        styling='primary'
                        type='submit'
                        disabled={isSubmitting}
                        loading={isSubmitting}
                    >
                        {submitLabel}
                    </Button>
                }
            />
        </Popup>
    )
};

UserModal.propTypes = {
    onDismiss: PropTypes.func,
    user: PropTypes.object,
}

export default UserModal;
