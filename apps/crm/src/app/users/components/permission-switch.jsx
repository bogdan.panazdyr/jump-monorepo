import React, { useState } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Toggle } from '@justlook/uikit';
import Api from 'api/client';

import cx from './permission-switch.module.scss';

const PermissionSwitch = ({ item, userId, disabled = false }) => {
    const { slug, display_name: label, is_enable } = item;
    const [ checked, setChecked ] = useState(is_enable);
    const [ isRequest, setIsRequest ] = useState(false);

    const handleOnChange = (value) => {
        setChecked(value);
        setIsRequest(true);

        Api.users.updatePermissions(userId, { [slug]: value })
            .then(() => setIsRequest(false))
            .catch(() => {
                setIsRequest(false);
                setChecked(!value);
            });
    };

    return (
        <div className={classNames(cx.base, {[cx.disabled]: disabled})}>
            <Toggle
                disabled={isRequest || disabled}
                isLoading={isRequest}
                id={slug}
                label={label}
                checked={checked}
                onChange={handleOnChange}
            />
        </div>
    );
};

PermissionSwitch.propTypes = {
    disabled: PropTypes.bool,
    item: PropTypes.shape({
        slug: PropTypes.string,
        display_name: PropTypes.string,
        is_enable: PropTypes.bool,
    }).isRequired,
    userId: PropTypes.oneOfType([
        PropTypes.string, PropTypes.number,
    ]).isRequired,
};

export default PermissionSwitch;
