import React, { useState } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Toggle } from '@justlook/uikit';

import Api from 'api/client';

import cx from './permission-switch.module.scss';

const AgentPermissionSwitch = ({ item, userId, disabled = false }) => {
    const { id, name, is_enable } = item;
    const [ checked, setChecked ] = useState(is_enable);
    const [ isRequest, setIsRequest ] = useState(false);

    const handleOnChange = (is_enable) => {
        setChecked(is_enable);
        setIsRequest(true);

        Api.users.updateAgentPermissions(userId, [ { is_enable, id } ])
            .then(() => setIsRequest(false))
            .catch(() => {
                setIsRequest(false);
                setChecked(!is_enable);
            });
    };

    return (
        <div className={classNames(cx.base, {[cx.disabled]: disabled})}>
            <Toggle
                disabled={isRequest || disabled}
                isLoading={isRequest}
                id={id}
                label={name}
                checked={checked}
                onChange={handleOnChange}
            />
        </div>
    );
};

AgentPermissionSwitch.propTypes = {
    disabled: PropTypes.bool,
    item: PropTypes.shape({
        id: PropTypes.number,
        name: PropTypes.string,
        is_enable: PropTypes.bool,
    }).isRequired,
    userId: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]).isRequired,
};

export default AgentPermissionSwitch;
