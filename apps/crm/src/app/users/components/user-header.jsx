import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { useNavigate } from 'react-router-dom';
import { Hooks } from '@justlook/core';
import { Icons24, Button, Header as Heading } from '@justlook/uikit';
import { useIfMediaScreen } from '@justlook/uikit/lib/hooks';

import { useHasPermission } from 'app/profile/store/selectors';
import { useMenu } from 'components/menu';

import useSearchFilter from '../../../hooks/use-search-filter';

import { VIEW_MODE_DETAILS } from 'utils/browser';
import { USER_FILTER } from '../store/filters';

import UserModal from 'app/users/components/modal/user-modal';

import cx from './user-header.module.scss';

const FORM_CREATE = 'create';

const ItemHeader = ({ onExpandedMobile, filterQuery }) => {
    const navigate = useNavigate();

    return <div className={cx.base}>
        <Button styling='hollow' icon={<Icons24.IconBack/>}
                onClick={() => navigate(`/settings-users${filterQuery}`)}/>

        <div className={cx.titleBlock}>
            <Heading.H1 className={cx.title}>Пользователи</Heading.H1>
        </div>

        <Button styling='hollow' icon={<Icons24.IconMenu/>}
                onClick={onExpandedMobile}/>
    </div>;
};

const FilterHeader = ({ onExpandedMobile, isMobile }) => {
    const { isOpen, onOpen, onClose } = Hooks.useModal();

    const canCreateUser = useHasPermission('write-users')

    return (
        <Fragment>
            <div className={cx.base}>
                <div className={cx.dummy}/>
                <div className={cx.titleBlock}>
                    <Heading.H1 className={cx.title}>Пользователи</Heading.H1>
                </div>

                { canCreateUser && <Button
                    styling='hollow'
                    icon={<Icons24.IconPlus/>}
                    onClick={() => onOpen(FORM_CREATE)}
                />}

                { isMobile && <Button styling='hollow' icon={<Icons24.IconMenu/>} onClick={onExpandedMobile}/> }
            </div>
            { isOpen(FORM_CREATE) && <UserModal onDismiss={() => onClose(FORM_CREATE)} /> }
        </Fragment>
    )
};

const Header = ({ viewMode, updateFilter }) => {
    const isMobile = useIfMediaScreen();
    const { filterQuery, filterFull } = useSearchFilter(USER_FILTER);
    const { onExpandedMobile } = useMenu();

    if (isMobile && viewMode === VIEW_MODE_DETAILS) {
        return (
            <ItemHeader
                onExpandedMobile={onExpandedMobile}
                filterQuery={filterQuery}
            />
        );
    }

    return (
        <FilterHeader
            isMobile={isMobile}
            onExpandedMobile={onExpandedMobile}
            filter={filterFull}
            updateFilter={updateFilter}
        />
    );
};

ItemHeader.propTypes = {
    onExpandedMobile: PropTypes.func,
    filterQuery: PropTypes.string,
}

FilterHeader.propTypes = {
    onExpandedMobile: PropTypes.func,
    isMobile: PropTypes.bool,
}

Header.propTypes = {
    viewMode: PropTypes.string,
    updateFilter: PropTypes.func,
}

export default Header;
