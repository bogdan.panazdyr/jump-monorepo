import UsersPage from './pages/users-page';
import UserDetailPage from './pages/users-details-page';

export {
    UsersPage,
    UserDetailPage,
};
