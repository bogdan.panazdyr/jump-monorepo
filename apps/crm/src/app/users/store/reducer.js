import produce from 'immer';
import { USER_LIST, USER, PERMISSIONS, SUCCESS_SUBMIT_FORM, CLEAR_ERRORS } from 'app/users/store/action-types';
import { Store } from '@justlook/core';

const INITIAL_STATE = {
    items: {
        data: null,
        meta: null,
        isRequest: false,
        error: null,
    },
    permissions: {
        permissions: null,
        agents: null,
        groups: null,
        user: null,
        isRequest: false,
        error: null,
    },
    formState: {
        isSubmitting: false,
        isSubmitted: false,
        error: null,
    }
};

export const users = (state = INITIAL_STATE, action) =>
    produce(state, draft => {
        // eslint-disable-next-line default-case
        switch (action.type) {
            case USER_LIST + Store.REQUEST:
                draft.items.isRequest = true;
                break;
            case USER_LIST + Store.RECEIVE:
                draft.items.isRequest = false;
                draft.items.data = action.data.items;
                draft.items.meta = action.data.meta;
                break;
            case USER_LIST + Store.FAILURE:
                draft.items.isRequest = false;
                draft.items.data = null;
                draft.items.meta = null;
                draft.items.error = action.error;
                break;

            case USER + Store.CREATE_REQUEST:
                draft.formState.isSubmitting = true;
                draft.formState.error = null;
                break;
            case USER + Store.CREATE_RECEIVE:
                draft.formState.isSubmitting = false;
                draft.formState.isSubmitted = true;
                draft.items.data = [action.user, ...state.items.data]
                break;
            case USER + Store.CREATE_FAILURE:
                draft.formState.isSubmitting = false;
                draft.formState.isSubmitted = false;
                draft.formState.error = action.error;
                break;

            case USER + Store.UPDATE_REQUEST:
                draft.formState.isSubmitting = true;
                draft.formState.error = null;
                break;
            case USER + Store.UPDATE_RECEIVE:
                draft.items.data = state.items.data.map(obj => {
                    if(obj.id === action.user.id) {
                        return action.user;
                    }
                    return obj;
                });
                draft.permissions.user = { ...action.user }
                draft.formState.isSubmitting = false;
                draft.formState.isSubmitted = true;
                break;
            case USER + Store.UPDATE_FAILURE:
                draft.formState.isSubmitting = false;
                draft.formState.isSubmitted = false;
                draft.formState.error = action.error;
                break;

            case USER + Store.DELETE_REQUEST:
                draft.formState.isSubmitting = true;
                draft.formState.error = null;
                break;
            case USER + Store.DELETE_RECEIVE:
                draft.formState.isSubmitting = false;
                draft.formState.isSubmitted = true;
                draft.items.data = state.items.data.map(item => {
                    if(item.id === Number(action.id)) {
                        return { ...item, is_active: false }
                    }

                    return item;
                })
                break;
            case USER + Store.DELETE_FAILURE:
                draft.formState.isSubmitting = false;
                draft.formState.isSubmitted = false;
                draft.formState.error = action.error;
                break;

            case USER + '_RECOVERY_REQUEST':
                draft.formState.isSubmitting = true;
                break;
            case USER + '_RECOVERY_RECEIVE':
                draft.formState.isSubmitting = false;
                draft.formState.isSubmitted = true;
                draft.items.data = state.items.data.map(obj => {
                    if(obj.id === action.user.id) {
                        return action.user;
                    }
                    return obj;
                });
                break;
            case USER + '_RECOVERY_FAILURE':
                draft.formState.isSubmitting = false;
                draft.formState.isSubmitted = false;
                draft.items.error = action.error;
                break;

            case PERMISSIONS + Store.REQUEST:
                if (state.items.data && action?.userId) {
                    const activeUsers = state.items.data.filter(user => (
                        parseInt(user.id) === parseInt(action.userId)
                    ));

                    if (activeUsers.length) {
                        draft.permissions.user = activeUsers[0];
                    }
                }
                draft.permissions.isRequest = true;
                draft.permissions.error = null;
                break;
            case PERMISSIONS + Store.RECEIVE:
                draft.permissions.isRequest = false;
                draft.permissions.permissions = action.data.permissions;
                draft.permissions.agents = action.data.agents;
                draft.permissions.groups = action.data.groups;
                draft.permissions.user = action.data.customer;
                break;
            case PERMISSIONS + Store.FAILURE:
                draft.permissions.isRequest = false;
                draft.permissions.permissions = null;
                draft.permissions.agents = null;
                draft.permissions.groups = null;
                draft.permissions.user = null;
                draft.permissions.error = action.error;
                break;

            case SUCCESS_SUBMIT_FORM:
                draft.formState.isSubmitted = false;
                break;

            case CLEAR_ERRORS:
                draft.items.error = null;
                draft.permissions.error = null;
                draft.formState.error = null;
                break;
        }
    });
