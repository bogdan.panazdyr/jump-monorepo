import { useSelector } from 'react-redux';

export const getUsers = (state) => state?.users?.items?.data ?? null;
export const getUsersMeta = (state) => {
    const meta = state?.users?.items?.meta ?? {};
    const loading = state?.users?.items?.isRequest ?? false;

    return { ...meta, loading }
};
export const getUsersIsRequest = (state) => state?.users?.items?.isRequest ?? false;
export const getUsersError = (state) => state?.users?.items?.error ?? null;

export const usePermissionState = () => useSelector(state => ({
    user: state?.users?.permissions?.user ?? null,
    permissions: state?.users?.permissions?.permissions ?? null,
    agents: state?.users?.permissions?.agents ?? null,
    isRequest: state?.users?.permissions?.isRequest ?? false,
    error: state?.users?.permissions?.error ?? null,
}));

export const getFormState = state => {
    return {
        isSubmitting: state?.users?.formState?.isSubmitting ?? false,
        isSubmitted: state?.users?.formState?.isSubmitted ?? false,
        invalidFields: state?.users?.formState?.error?.errorData?.fields ?? [],
    }
};
