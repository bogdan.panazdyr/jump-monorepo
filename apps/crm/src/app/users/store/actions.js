import { Store } from '@justlook/core';
import { USER_LIST, USER, PERMISSIONS, SUCCESS_SUBMIT_FORM, CLEAR_ERRORS } from 'app/users/store/action-types';

const fetchUserList = (filter) => ({ type: USER_LIST + Store.FETCH, filter });
const requestUserList = () => ({ type: USER_LIST + Store.REQUEST });
const receiveUserList = (data) => ({ type: USER_LIST + Store.RECEIVE, data });
const failureUserList = (error) => ({ type: USER_LIST + Store.FAILURE, error });

const createUser = (user) => ({ type: USER + Store.CREATE, user });
const requestCreateUser = () => ({ type: USER + Store.CREATE_REQUEST });
const receiveCreateUser = (user) => ({ type: USER + Store.CREATE_RECEIVE, user: user.item});
const failureCreateUser = (error) => ({ type: USER + Store.CREATE_FAILURE, error });

const updateUser = (id, user) => ({ type: USER + Store.UPDATE, id, user });
const requestUpdateUser = () => ({ type: USER + Store.UPDATE_REQUEST });
const receiveUpdateUser = (user) => ({ type: USER + Store.UPDATE_RECEIVE, user: user.item });
const failureUpdateUser = (error) => ({ type: USER + Store.UPDATE_FAILURE, error });

const recoveryUser = (id) => ({ type: USER + '_RECOVERY', id });
const requestRecoveryUser = () => ({ type: USER + '_RECOVERY_REQUEST' });
const receiveRecoveryUser = (user) => ({ type: USER + '_RECOVERY_RECEIVE', user: user.item });
const failureRecoveryUser = (error) => ({ type: USER + '_RECOVERY_FAILURE', error });

const deleteUser = (id) => ({ type: USER + Store.DELETE, id });
const requestDeleteUser = () => ({ type: USER + Store.DELETE_REQUEST });
const receiveDeleteUser = (id) => ({ type: USER + Store.DELETE_RECEIVE, id });
const failureDeleteUser = (error) => ({ type: USER + Store.DELETE_FAILURE, error });

const fetchPermissions = (userId) => ({ type: PERMISSIONS + Store.FETCH, userId });
const requestPermissions = (userId) => ({ type: PERMISSIONS + Store.REQUEST, userId });
const receivePermissions = (data) => ({ type: PERMISSIONS + Store.RECEIVE, data });
const failurePermissions = (error) => ({ type: PERMISSIONS + Store.FAILURE, error });

const successSubmit = () => ({ type: SUCCESS_SUBMIT_FORM });
const clearErrors = () => ({ type: CLEAR_ERRORS });

export const UserAction = {
    fetchUserList,
    requestUserList,
    receiveUserList,
    failureUserList,

    createUser,
    requestCreateUser,
    receiveCreateUser,
    failureCreateUser,

    updateUser,
    requestUpdateUser,
    receiveUpdateUser,
    failureUpdateUser,

    recoveryUser,
    requestRecoveryUser,
    receiveRecoveryUser,
    failureRecoveryUser,

    deleteUser,
    requestDeleteUser,
    receiveDeleteUser,
    failureDeleteUser,

    fetchPermissions,
    requestPermissions,
    receivePermissions,
    failurePermissions,

    successSubmit,
    clearErrors,
}
