export const USER = '@@users/USER';
export const USER_LIST = '@@users/LIST';
export const PERMISSIONS = '@@users/PERMISSIONS';

export const SUCCESS_SUBMIT_FORM = '@@users/SUCCESS_SUBMIT_FORM';
export const CLEAR_ERRORS = '@@users/CLEAR_ERRORS';
