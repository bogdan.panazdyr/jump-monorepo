import { put, fork, call, takeLatest } from 'redux-saga/effects';
import { FETCH, CREATE, UPDATE, DELETE } from '@justlook/core/lib/store';

import Api from 'api/client';
import { USER_LIST, USER, PERMISSIONS } from './action-types';
import { UserAction } from './actions';

export function * fetchUsersList({ filter = {} }) {
    yield put(UserAction.requestUserList());

    try {
        const response = yield call([ Api.users, 'fetchUserList' ], filter);
        yield put(UserAction.receiveUserList(response));
    } catch (e) {
        yield put(UserAction.failureUserList(e));
    }
}

export function * fetchPermissionsList({ userId }) {
    yield put(UserAction.requestPermissions(userId));

    try {
        const response = yield call([ Api.users, 'fetchPermissions' ], userId);
        yield put(UserAction.receivePermissions(response.data));
    } catch (e) {
        yield put(UserAction.failurePermissions(e));
    }
}

export function * createUser({ user }) {
    yield put(UserAction.requestCreateUser());

    try {
        const response = yield call([ Api.users, 'create' ], user);
        yield put(UserAction.receiveCreateUser(response));
        yield put(UserAction.successSubmit());
    } catch (e) {
        yield put(UserAction.failureCreateUser(e));
    }
}

export function * updateUser({ id, user }) {
    yield put(UserAction.requestUpdateUser());

    try {
        const response = yield call([ Api.users, 'update' ], id, user);
        yield put(UserAction.receiveUpdateUser(response));
        yield put(UserAction.successSubmit());
    } catch (e) {
        yield put(UserAction.failureUpdateUser(e));
    }
}

export function * recoveryUser({ id }) {
    yield put(UserAction.requestRecoveryUser());

    try {
        const response = yield call([ Api.users, 'recovery' ], id);
        yield put(UserAction.receiveRecoveryUser(response));
        yield put(UserAction.successSubmit());
        yield put(UserAction.fetchPermissions(id));
    } catch (e) {
        yield put(UserAction.failureRecoveryUser(e));
    }
}

export function * deleteUser({ id }) {
    yield put(UserAction.requestDeleteUser());

    try {
        yield call([ Api.users, 'delete' ], id);
        yield put(UserAction.receiveDeleteUser(id));
        yield put(UserAction.successSubmit());
        yield put(UserAction.fetchPermissions(id));
    } catch (e) {
        yield put(UserAction.failureDeleteUser(e));
    }
}

function * watchFetchUsersList() {
    yield takeLatest(USER_LIST + FETCH, fetchUsersList);
}

function * watchFetchPermissionsList() {
    yield takeLatest(PERMISSIONS + FETCH, fetchPermissionsList);
}

function * watchCreateUser() {
    yield takeLatest(USER + CREATE, createUser);
}

function * watchUpdateUser() {
    yield takeLatest(USER + UPDATE, updateUser);
}

function * watchRecoveryUser() {
    yield takeLatest(USER + '_RECOVERY', recoveryUser);
}

function * watchDeleteUser() {
    yield takeLatest(USER + DELETE, deleteUser);
}

export default function* watcher() {
    yield fork(watchFetchUsersList);
    yield fork(watchFetchPermissionsList);
    yield fork(watchCreateUser);
    yield fork(watchUpdateUser);
    yield fork(watchRecoveryUser);
    yield fork(watchDeleteUser);
}
