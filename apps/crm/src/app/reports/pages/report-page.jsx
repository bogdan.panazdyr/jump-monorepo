import React, { Fragment, useCallback, useState } from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { NavLink } from 'react-router-dom';

import { Config } from '@justlook/core';
import { ErrorScreen404, Link } from '@justlook/uikit';

import { Box, ContentBox, PeriodControl, withPermissions, Notice } from 'components';
import { OneColumnsLayout } from 'components/menu';
import { useReport } from 'domains/reports/models';
import { useReportFilter } from 'domains/reports/services';
import { ReportsContextProvider, useReportsContext } from 'domains/reports/contexts';

import { Chart } from '../components/reports-chart';
import { ReportsHeader } from '../components/reports-header';
import ReportDetails from '../components/reports';
import { DownloadReportsButton } from '../components/reports/download-reports-button';

import cx from './reports-page.module.scss';

const ReportPage = ({ type }) => {
    const report = useReport(type);
    const filter = useReportFilter();
    const { isLoadingDetails, reportDownloadLinks } = useReportsContext();

    const [ loaderCharts, setLoaderCharts ] = useState({});

    const onSetLoadChart = useCallback((chartType, status) => {
        setLoaderCharts(prevState => ({...prevState, [chartType]: status}));
    }, [setLoaderCharts]);

    if (! report.isValid()) {
        return <ErrorScreen404 />;
    }

    const isFetchingData = report.isLoadingCharts(loaderCharts) || isLoadingDetails;

    return (
        <Fragment>
            <Helmet>
                <title>{report.title()} - отчет. {Config.brandName}</title>
            </Helmet>

            <OneColumnsLayout isProgress={isFetchingData} header={<ReportsHeader title={report.title()} />}>
                <ContentBox className={cx.base}>
                    {
                        report.oldLinkReport() &&
                        <Notice className={cx.notice}>
                            Вы можете воспользоваться старой версией отчета по <Link to={report.oldLinkReport()} target='_blank'> ссылке</Link>
                        </Notice>
                    }

                    <Box className={cx.control}>
                        <PeriodControl isRequest={isFetchingData}/>

                        <DownloadReportsButton
                            links={reportDownloadLinks}
                            isLoading={isLoadingDetails}
                        />
                    </Box>

                    <Box className={cx.charts} mt={8} mb={24}>
                        {
                            report.charts().map(chart => (<Chart
                                key={chart}
                                filter={filter}
                                chartName={chart}
                                reportName={report.type()}
                                changeIsRequest={onSetLoadChart}
                            />))
                        }
                    </Box>

                    <ReportDetails
                        reportType={report.type()}
                        filter={filter}
                    />

                    { process.env.NODE_ENV !== 'production' && (
                        <div>
                            <br/>
                            <h2>DEBUG INFO</h2>
                            <br/>
                            <NavLink to='/reports/decreases'>/reports/decreases</NavLink>
                            <br/>
                            <NavLink to='/reports/payments'>/reports/payments</NavLink>
                            <br/>
                            <NavLink to='/reports/trips'>/reports/trips</NavLink>
                            <br/>
                            <NavLink to='/reports/404'>/reports/404</NavLink>
                        </div>
                    )}
                </ContentBox>
            </OneColumnsLayout>
        </Fragment>

    )
}

ReportPage.propTypes = {
    type: PropTypes.string.isRequired,
}

const PageWithProviders = ({ type }) => {
    return (
        <ReportsContextProvider>
            <ReportPage type={type}/>
        </ReportsContextProvider>
    )
}

PageWithProviders.propTypes = {
    type: PropTypes.string.isRequired,
}

export default withPermissions(PageWithProviders, ['read-report']);
