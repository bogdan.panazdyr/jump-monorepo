import PropTypes from 'prop-types';
import React, { useState } from 'react';

import { useMoney } from 'hooks/use-money';
import { CollapsibleTable, EmptyContent, TreeShift } from 'components';
import { useAgents } from 'domains/reports/report-mock';
import { EMPTY_ID } from 'domains/reports/models';

import { LoaderReports } from '../loader-reports';

import { Header } from './header';
import { Integrations } from './intergations';

//<NavLink to='/reports/payments#group_by=agent'>link 1</NavLink>
//<NavLink to='/reports/payments#group_by=agent&agent_id=1'>link 2</NavLink>

export const Agents = ({ filter, isShowHeader = false }) => {
    const [ activeId, setActiveId ] = useState(EMPTY_ID);
    const { format } = useMoney();

    const { agents, isLoading } = useAgents(filter);

    const isEmptyContent = isShowHeader && agents.length === 0 && !isLoading;

    if(isShowHeader && isLoading) {
        return <LoaderReports/>
    }

    return (
        <Header isShow={isShowHeader}>
            {
                isEmptyContent &&
                <EmptyContent message='Нет данных соответсвующих условиям.' isMute/>
            }

            {
                agents.map(item => {
                    return (
                        <CollapsibleTable.Row
                            key={item.id}
                            cells={[
                                item.name,
                                format(item.count),
                                format(item.count_non_cash),
                                format(item.sum_non_cash)
                            ]}
                            active={activeId === item.id}
                            onClick={() => setActiveId(item.id === activeId ? EMPTY_ID : item.id)}
                        >

                            <TreeShift>
                                {activeId === item.id && <Integrations filter={{...filter, agent_id: item.id}} />}
                            </TreeShift>
                        </CollapsibleTable.Row>
                    )
                })
            }

            { isLoading && <CollapsibleTable.Loader/> }
        </Header>
    )
}

Agents.propTypes = {
    isShowHeader: PropTypes.bool,
    filter: PropTypes.object.isRequired
}
