import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import { useMoney } from 'hooks/use-money';
import { CollapsibleTable, TreeShift, EmptyContent } from 'components';
import { useIntegrationsPayments } from 'domains/reports/queries';
import { EMPTY_ID } from 'domains/reports/models';
import { useReportsContext } from 'domains/reports/contexts';

import { LoaderReports } from '../loader-reports';
import { CellWithCounts } from '../cell-with-counts';

import { Header } from './header';
import { Banks } from './banks';

export const Integrations = ({ filter, isShowHeader = false }) => {
    const [ activeId, setActiveId ] = useState(EMPTY_ID);
    const { format } = useMoney();

    const { setIsLoadingDetails, setReportDownloadLinks } = useReportsContext();

    const { integrations, isLoading, isFetching, attachments } = useIntegrationsPayments(filter);

    useEffect(() => {
        setIsLoadingDetails(isLoading || isFetching);
        setReportDownloadLinks(attachments);
    }, [isLoading, isFetching, attachments, setIsLoadingDetails, setReportDownloadLinks]);

    const isEmptyContent = isShowHeader && integrations.length === 0 && !isLoading;

    if(isShowHeader && isLoading) {
        return <LoaderReports/>
    }

    return (
        <Header isShow={isShowHeader}>
            {
                isEmptyContent &&
                <EmptyContent message='Нет данных соответсвующих условиям.' isMute/>
            }

            {
                integrations.map(item => {
                    return (
                        <CollapsibleTable.Row
                            key={item.id}
                            cells={[
                                <CellWithCounts key={item.id} label={item.name} counts={format(item.drivers_count)}/>,
                                format(item.count),
                                format(item.sum),
                                format(item.commission),
                            ]}
                            active={activeId === item.id}
                            onClick={() => setActiveId(item.id === activeId ? EMPTY_ID : item.id)}
                        >
                            <TreeShift>
                                {activeId === item.id && <Banks filter={{...filter, integration_id: item.id}} />}
                            </TreeShift>
                        </CollapsibleTable.Row>
                    )
                })
            }

            { isLoading && <CollapsibleTable.Loader/> }
        </Header>
    )
}

Integrations.propTypes = {
    isShowHeader: PropTypes.bool,
    filter: PropTypes.object.isRequired
}
