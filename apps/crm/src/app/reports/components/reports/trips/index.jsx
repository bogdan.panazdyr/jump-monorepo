import React from 'react';
import PropTypes from 'prop-types';

import { EMPTY_ID } from 'domains/reports/models';

import { Agents } from './agents';
import { Drivers } from './drivers';
import { Integrations } from './intergations';

const ReportTrips = ({ filter }) => {
    const groupType = filter?.group_by ?? 'integration';
    const groupId = filter[groupType + '_id'] ?? EMPTY_ID;

    let Details = null;

    if (groupType === 'integration' && groupId === EMPTY_ID) {
        Details = Integrations;
    } else if (groupType === 'integration' && groupId !== EMPTY_ID) {
        Details = Drivers;
    } else if (groupType === 'agent' && groupId === EMPTY_ID) {
        Details = Agents;
    } else if (groupType === 'agent' && groupId !== EMPTY_ID) {
        Details = Integrations;
    }

    return <Details isShowHeader filter={filter} />
}

ReportTrips.propTypes = {
    filter: PropTypes.object.isRequired,
}

export default ReportTrips;
