import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import { useMoney } from 'hooks/use-money';
import { CollapsibleTable, EmptyContent, TreeShift } from 'components';
import { useIntegrationsTrips } from 'domains/reports/queries';
import { EMPTY_ID } from 'domains/reports/models';
import { useReportsContext } from 'domains/reports/contexts';

import { LoaderReports } from '../loader-reports';
import { CellWithCounts } from '../cell-with-counts';

import { Header } from './header';
import { Drivers } from './drivers';

export const Integrations = ({ filter, isShowHeader = false }) => {
    const [ activeId, setActiveId ] = useState(EMPTY_ID);
    const { format } = useMoney();

    const { setIsLoadingDetails, setReportDownloadLinks } = useReportsContext();

    const { integrations, isLoading, isFetching, attachments } = useIntegrationsTrips(filter);

    useEffect(() => {
        setIsLoadingDetails(isLoading || isFetching);
        setReportDownloadLinks(attachments);
    }, [isLoading, isFetching, attachments, setIsLoadingDetails, setReportDownloadLinks]);

    const isEmptyContent = isShowHeader && integrations.length === 0 && !isLoading;

    if(isShowHeader && isLoading) {
        return <LoaderReports/>
    }

    return (
        <Header isShow={isShowHeader}>
            {
                isEmptyContent &&
                <EmptyContent message='Нет данных соответсвующих условиям.' isMute/>
            }

            {
                integrations.map(item => {
                    return (
                        <CollapsibleTable.Row
                            key={item.id}
                            cells={[
                                <CellWithCounts key={item.id} label={item.name} counts={item.drivers_count}/>,
                                format(item.count),
                                format(item.count_non_cash),
                                format(item.sum_non_cash),
                                format(item.sum),
                                format(item.commission),
                            ]}
                            active={item.id ? activeId === item.id : false}
                            onClick={item.id ? () => setActiveId(item.id === activeId ? EMPTY_ID : item.id) : null}
                        >
                            <TreeShift>
                                {activeId === item.id && <Drivers filter={{...filter, integration_id: item.id}} />}
                            </TreeShift>
                        </CollapsibleTable.Row>
                    )
                })
            }

            { isLoading && <CollapsibleTable.Loader/> }
        </Header>
    )
}

Integrations.propTypes = {
    isShowHeader: PropTypes.bool,
    filter: PropTypes.object.isRequired
}
