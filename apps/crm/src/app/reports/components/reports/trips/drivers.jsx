import React from 'react';
import PropTypes from 'prop-types';

import { useMoney } from 'hooks/use-money';
import { CollapsibleTable, EmptyContent } from 'components';
import { useDriversTrips } from 'domains/reports/queries';

import { LoaderReports } from '../loader-reports';

import { Header } from './header';

export const Drivers = ({ filter, isShowHeader = false }) => {
    const { format } = useMoney();

    const { drivers, isLoading, hasNextPage, fetchNextPage } = useDriversTrips(filter);

    const isEmptyContent = isShowHeader && drivers.length === 0 && !isLoading;

    if(isShowHeader && isLoading) {
        return <LoaderReports/>
    }

    return (
        <Header isShow={isShowHeader}>
            {
                isEmptyContent &&
                <EmptyContent message='Нет данных соответсвующих условиям.' isMute/>
            }

            {
                drivers.map(driver => {
                    return (
                        <CollapsibleTable.Collapse key={driver.id}>
                            <CollapsibleTable.Cell style={{ paddingLeft: '40px' }}>
                                {driver.name}
                            </CollapsibleTable.Cell>
                            <CollapsibleTable.Cell>
                                {format(driver.count)}
                            </CollapsibleTable.Cell>
                            <CollapsibleTable.Cell>
                                {format(driver.count_non_cash)}
                            </CollapsibleTable.Cell>
                            <CollapsibleTable.Cell>
                                {format(driver.sum_non_cash)}
                            </CollapsibleTable.Cell>
                            <CollapsibleTable.Cell>
                                {format(driver.sum)}
                            </CollapsibleTable.Cell>
                            <CollapsibleTable.Cell>
                                {format(driver.commission)}
                            </CollapsibleTable.Cell>
                        </CollapsibleTable.Collapse>
                    )
                })

            }
            { isLoading && <CollapsibleTable.Loader/> }

            {
                hasNextPage && !isLoading &&
                <CollapsibleTable.Load
                    label='Показать ещё'
                    onClick={fetchNextPage}
                    style={{ paddingLeft: '40px' }}
                />
            }
        </Header>
    )
}

Drivers.propTypes = {
    isShowHeader: PropTypes.bool,
    filter: PropTypes.object.isRequired
}
