import React from 'react';
import PropTypes from 'prop-types';

import { Utils } from '@justlook/core';

import { useMoney } from 'hooks/use-money';
import { CollapsibleTable, EmptyContent } from 'components';
import { useDriversDecreases } from 'domains/reports/queries';

import { LoaderReports } from '../loader-reports';

import { Header } from './header';

export const Drivers = ({ filter, isShowHeader = false }) => {
    const { drivers, isLoading, hasNextPage, fetchNextPage } = useDriversDecreases(filter);

    const { format } = useMoney();

    const isEmptyContent = isShowHeader && drivers.length === 0 && !isLoading;

    if(isShowHeader && isLoading) {
        return <LoaderReports/>
    }

    return (
        <Header isShow={isShowHeader}>
            {
                isEmptyContent &&
                <EmptyContent message='Нет данных соответсвующих условиям.' isMute/>
            }

            {
                drivers.map(driver => {
                    return (
                        <CollapsibleTable.Collapse key={driver.name}>
                            <CollapsibleTable.Cell style={{ paddingLeft: '40px' }}>
                                {driver.name}
                            </CollapsibleTable.Cell>
                            <CollapsibleTable.Cell>
                                {`${Utils.Dates.formatDate(driver.date)}, ${Utils.Dates.getTime(driver.date)}`}
                            </CollapsibleTable.Cell>
                            <CollapsibleTable.Cell>
                                {format(driver.amount)}
                            </CollapsibleTable.Cell>
                        </CollapsibleTable.Collapse>
                    )
                })

            }
            { isLoading && <CollapsibleTable.Loader /> }

            {
                hasNextPage && !isLoading &&
                <CollapsibleTable.Load
                    label='Показать еще'
                    onClick={fetchNextPage}
                    style={{ paddingLeft: '40px' }}
                />
            }
        </Header>
    )
}

Drivers.propTypes = {
    isShowHeader: PropTypes.bool,
    filter: PropTypes.object.isRequired
}
