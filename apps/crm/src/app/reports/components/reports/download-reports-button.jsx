import React, { useState } from 'react';
import PropTypes from 'prop-types';

import { Icons24 } from '@justlook/uikit';

import { DropActions, snackBar } from 'components';
import { download } from 'utils/download';

export const DownloadReportsButton = ({ links = [], isLoading }) => {
    const [ downloading, setDownloading ] = useState(false);

    const actionDownload = (link, file) => {
        setDownloading(true);
        download({ url: link, attacheFile: file })
            .then(() => setDownloading(false))
            .catch((e) => {
                setDownloading(false)
                snackBar(e.message)
            })
    }

    const getActions = () => {
        return links.map(({ title, file, url }) => ({
            label: title,
            action: () => actionDownload(url, file)
        }))
    }

    return (
        <DropActions
            icon={Icons24.IconDownload}
            loading={downloading || isLoading}
            disabled={downloading || isLoading}
            actions={getActions()}
            positionDropDown='bottom-left'
        />
    )
}

DownloadReportsButton.propTypes = {
    links: PropTypes.array.isRequired,
    isLoading: PropTypes.bool,
}
