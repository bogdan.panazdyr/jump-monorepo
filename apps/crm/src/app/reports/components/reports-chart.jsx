import React, { Fragment, useEffect } from 'react';
import PropTypes from 'prop-types';

import { Header } from '@justlook/uikit';

import { useMoney } from 'hooks/use-money';
import { errorToString } from 'utils/add-server-errors';
import { BarChart } from 'components';
import { LoaderChart } from 'app/dashboard/components/loaders/loader-chart';
import { useChartReport } from 'domains/reports/queries';

import cx from './reports-chart.module.scss';

export const Chart = ({ filter, changeIsRequest, reportName, chartName }) => {
    const { currency, format } = useMoney();

    const { data, isLoading, isFetching, isError, error } = useChartReport(reportName, chartName, filter);

    const isValidData = !isError && !isLoading;

    useEffect(() => {
        changeIsRequest(chartName, isLoading || isFetching);
    }, [changeIsRequest, chartName, isLoading, isFetching]);

    if(isLoading) { return <LoaderChart/> }

    return (
        <div className={cx.base}>
            <div className={cx.header}>
                {
                    isValidData &&
                    <Fragment>
                        <Header.H1>{ data.meta.title }</Header.H1>
                        <Header.H1>
                            { format(data.meta.total) }&nbsp;
                            { data.meta.currency && currency }
                        </Header.H1>
                    </Fragment>
                }
                { !isValidData && errorToString(error) }
            </div>

            { isValidData && <BarChart isRequest={isLoading} data={data} XDataKey='tooltip'/> }
        </div>
    )
}

Chart.propTypes = {
    filter: PropTypes.object,
    changeIsRequest: PropTypes.func,
    reportName: PropTypes.string,
    chartName: PropTypes.string,
}
