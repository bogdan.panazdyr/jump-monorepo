import produce from 'immer';
import { errorToJs, AUTH_SCREENS as SCREENS, Utils } from '@justlook/core';
import {
    AUTH_LOGIN, AUTH_CONFIRM_OTP, AUTH_PASSWORD, AUTH_GET_OTP,
    AUTH_RECOVERY, REQUEST, RECEIVE, FAILURE, SHOW, AUTH, AUTH_LOGOUT
} from '@justlook/core/lib/store';

const initState = {
    screen: Utils.Request.isChangePasswordRequest() ? SCREENS.change_password : SCREENS.login,
    isRequest: false,
    error: null,
    credentials: null,
};

export const auth = (state = initState, action) => produce(state, draft => {
    // eslint-disable-next-line default-case
    switch (action.type) {
        case AUTH_LOGIN + SHOW:
            draft.screen = SCREENS.login;
            draft.error = null;
            break;
        case AUTH_CONFIRM_OTP + SHOW:
            draft.screen = SCREENS.confirm_otp;
            draft.error = null;
            break;
        case AUTH_RECOVERY + SHOW:
            draft.screen = SCREENS.recovery;
            draft.error = null;
            break;
        case AUTH_RECOVERY + '_SUCCESS' + SHOW:
            draft.screen = SCREENS.recovery_success;
            draft.error = null;
            break;
        case AUTH_PASSWORD + SHOW:
            draft.screen = SCREENS.change_password;
            draft.error = null;
            break;

        case AUTH + REQUEST:
            draft.isRequest = true;
            draft.error = null;
            break;
        case AUTH + RECEIVE:
            draft.isRequest = false;
            draft.error = null;
            draft.credentials = null;
            break;
        case AUTH_GET_OTP + RECEIVE:
            draft.isRequest = false;
            draft.credentials = action.data;
            draft.error = null;
            break;
        case AUTH + FAILURE:
            draft.isRequest = false;
            draft.error = errorToJs(action.error);
            draft.credentials = action.reset ? null : draft.credentials;
            break;

        case AUTH_LOGOUT + REQUEST:
            draft.isRequest = true;
            draft.error = null;
            break;
        case AUTH_LOGOUT + RECEIVE:
            draft.isRequest = false;
            draft.screen = SCREENS.login;
            draft.error = null;
            break;
        case AUTH_LOGOUT + FAILURE:
            draft.isRequest = false;
            draft.error = errorToJs(action.error);
            break;
    }
})
