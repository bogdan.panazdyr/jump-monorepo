import { put, fork, takeLatest, call } from 'redux-saga/effects';
import { Api as ApiUtils, Config, Utils } from '@justlook/core';
import { runApp } from 'app/init/store/actions';
import Api from 'api/client';

import {
    AUTH_LOGIN, AUTH_CONFIRM_OTP, FETCH, AUTH_RECOVERY, AUTH_PASSWORD,
    AUTH_RETRY_OTP, AUTH_LOGOUT
} from '@justlook/core/lib/store';

import {
    recoverySuccessScreen, confirmOtpScreen,
    requestAuth, successAuth, failureAuth,
    receivedOtpHash, logoutFailure,
    logoutRequest, logoutReceive,
} from './actions';

/**
 * Синхронизирует токен со старым кабинетом
 */
function * syncToken (bearerToken) {
    const { token } = yield call(ApiUtils.syncAuthCookies);

    const { token: sessionToken } = yield call(
        [ Api.authLK, 'sync' ],
        { token: bearerToken },
        token
    );

    Utils.Token.storeTokenSession(sessionToken);

    yield put(successAuth());
    yield put(runApp())
}

/**
 * Получение разрешенного сценария авторизации (sms, password)
 */
function * fetchScenario (action) {
    yield put(requestAuth());

    try {
        const { scenario } = yield call([Api.auth, 'scenario'], action.credentials);

        if (scenario === 'password') {
            yield call(fetchLogin, action.credentials);
        } else {
            yield call(fetchOtp, action.credentials);
        }
    } catch (e) {
        yield put(failureAuth(e));
    }
}

/**
 * Вход по логину и паролю
 */
function * fetchLogin (credentials) {
    yield put(requestAuth());

    try {
        const token = yield call([Api.auth, 'login'], credentials);
        yield call(syncToken, token);
    }catch (e) {
        yield put(failureAuth(e));
    }
}

/**
 * Запрос одноразового пароля
 */
function * fetchOtp (action) {
    const credentials = action?.credentials || action;
    yield put(requestAuth());

    try {
        const { hash } = yield call([Api.auth, 'getOtp'], credentials);

        if (hash) {
            yield put(receivedOtpHash({...credentials, hash}));
            yield put(confirmOtpScreen());
        } else {
            yield put(failureAuth(new Error('Не удалось отправить смс'), false));
        }

    } catch (e) {
        yield put(failureAuth(e, false));
    }
}

/**
 * Вход через одноразовый пароль - смс
 */
function * fetchConfirmOtp ({ credentials }) {
    yield put(requestAuth());

    try {
        const token = yield call([Api.auth, 'confirmOtp'], credentials);
        yield call(syncToken, token);
    } catch (e) {
        yield put(failureAuth(e, false));
    }
}

/**
 * Запрос на восстановление пароля
 */
function * fetchRecovery ({ email }) {
    yield put(requestAuth());
    try {
        const { need_auth } = yield call([Api.authLK, 'recoveryPassword'], { email });

        yield put(successAuth());

        if (need_auth) {
            yield put(recoverySuccessScreen());
        } else {
            yield put(runApp());
        }
    } catch (e) {
        yield put(failureAuth(e));
    }
}

/**
 * Запрос на смену пароля
 */
function * fetchUpdatePassword ({ credentials }) {
    yield put(requestAuth());

    try {
        const { token } = yield call([Api.authLK, 'changePassword'], credentials);

        if (token) {
            Utils.Token.storeTokenSession(token);

            yield put(successAuth());
            yield put(runApp());
        } else {
            window.location = Config.protocol + Config.auth_page;
        }
    } catch (e) {
        yield put(failureAuth(e));
    }
}

/**
 * Запрос на выход из кабинета
 */
function * fetchLogOut () {
    yield put(logoutRequest());

    try {
        yield call([Api.auth, 'logout']);
        yield put(logoutReceive());

        Utils.Token.destroyTokenStorage();

        yield put(runApp());
    } catch (e) {
        yield put(logoutFailure(e));
    }
}

// watchers
function * watchFetchLogin () {
    yield takeLatest(AUTH_LOGIN + FETCH, fetchScenario);
}

function * watchFetchRecovery () {
    yield takeLatest(AUTH_RECOVERY + FETCH, fetchRecovery);
}

function * watchFetchConfirmOtp () {
    yield takeLatest(AUTH_CONFIRM_OTP + FETCH, fetchConfirmOtp);
}

function * watchFetchRetryOtp () {
    yield takeLatest(AUTH_RETRY_OTP + FETCH, fetchOtp);
}

function * watchFetchUpdatePassword () {
    yield takeLatest(AUTH_PASSWORD + FETCH, fetchUpdatePassword);
}

function * watchFetchLogOut () {
    yield  takeLatest(AUTH_LOGOUT + FETCH, fetchLogOut);
}

export default function * watcher () {
    yield fork(watchFetchLogin);
    yield fork(watchFetchRecovery);
    yield fork(watchFetchConfirmOtp);
    yield fork(watchFetchRetryOtp);
    yield fork(watchFetchUpdatePassword);
    yield fork(watchFetchLogOut);
}
