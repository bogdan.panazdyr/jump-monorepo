import {
    AUTH_CONFIRM_OTP, AUTH_GET_OTP, AUTH_LOGIN, AUTH_LOGOUT, AUTH_PASSWORD, AUTH_RECOVERY,
    FAILURE, FETCH, RECEIVE, REQUEST, SHOW, AUTH, AUTH_RETRY_OTP, SUCCESS
} from '@justlook/core/lib/store';

export const requestAuth = () => ({ type: AUTH + REQUEST });
export const successAuth = () => ({ type: AUTH + RECEIVE });
export const failureAuth = (error, reset = true) => ({
    type: AUTH + FAILURE, error, reset,
});

// запрос на вход
export const login = (credentials) => ({ type: AUTH_LOGIN + FETCH, credentials });

// запрос на восстановление пароля через почту
export const recovery = (email) => ({ type: AUTH_RECOVERY + FETCH, email });

// запрос на сохранение нового пароля
export const updatePassword = (credentials) => ({ type: AUTH_PASSWORD + FETCH, credentials });

// запрос на подтверждение входа через смс
export const confirmOtp = (credentials) => ({ type: AUTH_CONFIRM_OTP + FETCH, credentials });
export const retryOtp = (credentials) => ({ type: AUTH_RETRY_OTP + FETCH, credentials });
export const receivedOtpHash = (data) => ({ type: AUTH_GET_OTP + RECEIVE, data });

// переключение экранов авторизации
export const loginScreen = () => ({ type: AUTH_LOGIN + SHOW});
export const confirmOtpScreen = (hash) => ({ type: AUTH_CONFIRM_OTP + SHOW, hash});
export const recoveryScreen = () => ({ type: AUTH_RECOVERY + SHOW});
export const recoverySuccessScreen = () => ({ type: AUTH_RECOVERY + SUCCESS + SHOW});

// запрос на выход
export const logout = () => ({ type: AUTH_LOGOUT + FETCH });
export const logoutRequest = () => ({ type: AUTH_LOGOUT + REQUEST });
export const logoutReceive = () => ({ type: AUTH_LOGOUT + RECEIVE });
export const logoutFailure = (error) => ({ type: AUTH_LOGOUT + FAILURE, error });
