import { AUTH_SCREENS } from '@justlook/core';

export const getScreen = (state) => state?.auth?.screen || AUTH_SCREENS.login;
export const getIsRequest = (state) => state?.auth?.isRequest;
export const getCredentials = (state) => state?.auth?.credentials || {};
export const getErrorMessage = (state) => state?.auth?.error?.detail;