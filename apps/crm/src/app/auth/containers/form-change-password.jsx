import React, { Fragment, useState } from 'react';
import { useSelector } from 'react-redux';
import { Helmet } from 'react-helmet';
import { Config, Hooks, Utils } from '@justlook/core';
import { Form, Button, Password, Icons24, InfoMessage } from '@justlook/uikit';
import { Logo } from 'components/logo';
import { getErrorMessage, getIsRequest } from '../store/selectors';
import { updatePassword } from '../store/actions';

const FormChangePassword = () => {
    const [password, setPassword] = useState('');
    const [confirmedPassword, setConfirmedPassword] = useState('');
    const [errorEquals, setError] = useState('');
    const isRequest = useSelector(getIsRequest);
    const error = useSelector(getErrorMessage);

    const onUpdatePassword = Hooks.useActionToDispatch(updatePassword);

    const checkEquals = (password, confirmedPassword) => {
        if (!password.length || !confirmedPassword.length) {
            setError('');
            return;
        }
        const equal = password === confirmedPassword;
        setError( equal ? '' : 'Пароли не совпадают');
    }

    const onChangePassword = (value) => {
        setPassword(value);
        checkEquals(value, confirmedPassword);
    }

    const onChangeConfirmedPassword = (value) => {
        setConfirmedPassword(value);
        checkEquals(password, value);
    }

    const disabledSubmit = () => {
        const hasEmpty = !password || !confirmedPassword;
        const equal = password === confirmedPassword;

        return hasEmpty || !equal || isRequest;
    }

    const onSubmit = (e) => {
        e.preventDefault();

        const login = Utils.Request.getEmail();
        const code = Utils.Request.getRecoveryCode();

        onUpdatePassword({password, code, login })
    }

    return (
        <Fragment>
            <Helmet>
                <title>{Config.brandName} - Новый пароль</title>
            </Helmet>

            <Logo /><br/><br/>

            <Form.Title>{`Новый пароль ${Config.brandName}`}</Form.Title>

            <Form onSubmit={onSubmit}>
                <Form.Field>
                    <Password
                        placeholder='Новый пароль'
                        onChange={onChangePassword}
                        value={password}
                        disabled={isRequest}
                        required
                    />
                </Form.Field>

                <Form.Field>
                    <Password
                        placeholder='Повторите новый пароль'
                        onChange={onChangeConfirmedPassword}
                        value={confirmedPassword}
                        error={errorEquals}
                        disabled={isRequest}
                        required
                    />
                </Form.Field>

                <Form.Field>
                    <Button
                        wide
                        type='submit'
                        icon={isRequest && <Icons24.IconLoader/>}
                        disabled={disabledSubmit()}
                    >
                        Подтвердить
                    </Button>
                </Form.Field>

                {
                    error &&
                    <Form.Field>
                        <InfoMessage icon={<Icons24.IconInfo />} style={{textAlign: 'left'}}>
                            {error}
                        </InfoMessage>
                    </Form.Field>
                }
            </Form>
        </Fragment>
    )
}

export default FormChangePassword;