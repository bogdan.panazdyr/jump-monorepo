import React, { Fragment, useState } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { Helmet } from 'react-helmet';
import { Hooks, Config } from '@justlook/core';
import {
    Form, ParagraphV2 as Paragraph, Input, Button, InfoMessage, Icons24,
} from '@justlook/uikit';

import { Logo } from 'components/logo';
import { Countdown } from 'components';
import { confirmOtp, retryOtp } from '../store/actions';
import { getIsRequest, getErrorMessage, getCredentials } from '../store/selectors';

const ButtonRetry = (props) => {
    return (
        <Button type='button' styling='hollow' onClick={props.onClick}>
            <span style={{color: '#2691FF'}}>Выслать код повторно</span>
        </Button>
    )
}

ButtonRetry.propTypes = {
    onClick: PropTypes.func
}

const FormConfirmation = () => {
    const [code, setCode] = useState('');
    const [isCountDown, setIsCountDown] = useState(true);

    const { hash, password, login } = useSelector(getCredentials);
    const error = useSelector(getErrorMessage);
    const isRequest = useSelector(getIsRequest);

    const onConfirm = Hooks.useActionToDispatch(confirmOtp);
    const onRetryOtp = Hooks.useActionToDispatch(retryOtp);
    const showCountDown = () => setIsCountDown(true);
    const hideCountDown = () => setIsCountDown(false);

    const onChangeCode = (code) => {
        setCode(code);

        if (code.length === 4) {
            onConfirm({ code, hash });
        }
    }

    const onSubmit = (e) => {
        e && e.preventDefault();

        onConfirm({ code, hash });
    }

    const onRetry = () => {
        setCode('');
        showCountDown();
        onRetryOtp({password, login})
    }

    return (
        <Fragment>
            <Helmet>
                <title>{Config.brandName} - Вход</title>
            </Helmet>

            <Logo /><br/><br/>

            <Form.Title>{`Введите код из СМС. ${Config.brandName}`}</Form.Title>
            <Paragraph.LH24>Мы отправили смс-код на ваш номер телефона</Paragraph.LH24>

            <Form onSubmit={onSubmit}>
                <Form.Field>
                    <Input
                        type='text'
                        value={code}
                        onChange={onChangeCode}
                        disabled={isRequest}
                        placeholder='Код из смс'
                        required
                    />
                </Form.Field>

                <Form.Field>
                    <Button
                        wide
                        icon={isRequest && <Icons24.IconLoader/>}
                        disabled={isRequest}
                        type='submit'
                    >
                        {!isRequest && 'Готово'}
                    </Button>
                </Form.Field>

                <Form.Field>
                    {
                        isCountDown
                            ? <Countdown
                                onTimeout={hideCountDown}
                                template='Повторная отправка через $ cек.'/>
                            : <ButtonRetry onClick={onRetry}/>
                    }
                </Form.Field>

                {
                    error &&
                    <Form.Field>
                        <InfoMessage icon={<Icons24.IconInfo />} style={{textAlign: 'left'}}>
                            {error}
                        </InfoMessage>
                    </Form.Field>
                }
            </Form>
        </Fragment>
    )
}

export default FormConfirmation;
