import React, { useState, useEffect, Fragment } from 'react';
import { useSelector } from 'react-redux';
import { useQueryClient } from 'react-query';
import { Helmet } from 'react-helmet';
import { Hooks, Config } from '@justlook/core';
import {
    Input, Button, Form, Icons24, Password, InfoMessage
} from '@justlook/uikit';

import { login as actionLogin, recoveryScreen } from '../store/actions';
import { getIsRequest, getErrorMessage } from '../store/selectors';
import { Logo } from 'components/logo';

const FormLogin = () => {
    const enableRecoveryForm = Config.features.enableRecoveryPassword;
    const queryClient = useQueryClient();

    const [login, setLogin] = useState('');
    const [password, setPassword] = useState('');
    const isRequest = useSelector(getIsRequest);
    const error = useSelector(getErrorMessage);

    const onLogin = Hooks.useActionToDispatch(actionLogin);
    const onRecoveryScreen = Hooks.useActionToDispatch(recoveryScreen);

    const onSubmit = async (e) => {
        e.preventDefault();

        onLogin({login, password});
    };

    useEffect(() => {
        queryClient.removeQueries();
    }, [queryClient]);

    return (
        <Fragment>
            <Helmet>
                <title>{Config.brandName} - Вход</title>
            </Helmet>

            <Logo /><br/><br/>

            <Form.Title>{`Вход в ${Config.brandName}`}</Form.Title>

            <Form onSubmit={onSubmit}>
                <Form.Field>
                    <Input
                        type='email'
                        value={login}
                        disabled={isRequest}
                        onChange={setLogin}
                        placeholder='Эл. почта'
                        required
                    />
                </Form.Field>

                <Form.Field>
                    <Password
                        value={password}
                        disabled={isRequest}
                        onChange={setPassword}
                        placeholder='Пароль'
                        required
                    />
                </Form.Field>

                <Form.Field>
                    <Button
                        wide
                        icon={isRequest && <Icons24.IconLoader/>}
                        disabled={isRequest}
                        type='submit'
                    >
                        {!isRequest && 'Войти'}
                    </Button>
                </Form.Field>
            </Form>

            {
                enableRecoveryForm && (
                    <Button
                        styling='hollow'
                        onClick={onRecoveryScreen}>
                        <span style={{color: '#2691FF'}}>Восстановление пароля</span>
                    </Button>
                )
            }

            {
                error &&
                <Form.Field>
                    <InfoMessage icon={<Icons24.IconInfo />} style={{textAlign: 'left'}}>
                        {error}
                    </InfoMessage>
                </Form.Field>
            }
        </Fragment>

    );
};

export default FormLogin;
