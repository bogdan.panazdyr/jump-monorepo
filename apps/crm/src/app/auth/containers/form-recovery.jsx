import React, { Fragment, useState } from 'react';
import { useSelector } from 'react-redux';
import { Helmet } from 'react-helmet';
import { Input, Button, Form, Icons24, InfoMessage } from '@justlook/uikit';
import { Config, Hooks } from '@justlook/core';

import { recovery as actionRecovery, loginScreen } from '../store/actions';
import { getIsRequest, getErrorMessage } from '../store/selectors';
import { Logo } from 'components/logo';

const FormConfirmation = () => {
    const [email, setEmail] = useState('');
    const isRequest = useSelector(getIsRequest);
    const error = useSelector(getErrorMessage);

    const onRecovery = Hooks.useActionToDispatch(actionRecovery);
    const onLoginScreen = Hooks.useActionToDispatch(loginScreen);

    const onSubmit = (e) => {
        e.preventDefault();
        onRecovery(email);
    };

    return (
        <Fragment>
            <Helmet>
                <title>{Config.brandName} - Восстановление пароля</title>
            </Helmet>

            <Logo/><br/><br/>

            <Form.Title>
                {`Восстановление пароля от ${Config.brandName}`}
            </Form.Title>

            <Form onSubmit={onSubmit}>
                <Form.Field>
                    <Input
                        type='email'
                        value={email}
                        disabled={isRequest}
                        onChange={setEmail}
                        placeholder='Эл. почта'
                        required
                    />
                </Form.Field>

                <Form.Field>
                    <Button
                        wide
                        icon={isRequest && <Icons24.IconLoader/>}
                        disabled={isRequest}
                        type='submit'
                    >
                        {!isRequest && 'Восстановить'}
                    </Button>
                </Form.Field>
            </Form>

            <Button
                styling='hollow'
                onClick={onLoginScreen}>
                <span style={{ color: '#2691FF' }}>Вспомнил пароль</span>
            </Button>

            {
                error &&
                <Form.Field>
                    <InfoMessage icon={<Icons24.IconInfo />} style={{textAlign: 'left'}}>
                        {error}
                    </InfoMessage>
                </Form.Field>
            }
        </Fragment>
    );
}

export default FormConfirmation;
