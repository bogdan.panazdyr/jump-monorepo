import React from 'react';
import { useSelector } from 'react-redux';
import { AUTH_SCREENS as SCREENS } from '@justlook/core';

import { getScreen } from '../store/selectors';
import FormLogin from './form-login';
import FormConfirmation from './form-confirmation';
import FormRecovery from './form-recovery';
import FormRecoverySuccess from './form-recovery-success';
import FormChangePassword from './form-change-password';

function Auth () {
    const screenRender = {
        [SCREENS.login]: FormLogin,
        [SCREENS.recovery]: FormRecovery,
        [SCREENS.recovery_success]: FormRecoverySuccess,
        [SCREENS.change_password]: FormChangePassword,
        [SCREENS.confirm_otp]: FormConfirmation,
    };

    const screen = useSelector(getScreen);
    const Form = screenRender[screen] || null;

    return Form && <Form/>;
}

Auth.propTypes = {};

export default Auth;
