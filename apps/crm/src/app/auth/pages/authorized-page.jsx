import React, { Component } from 'react';
import { Helmet } from 'react-helmet';
import { Config } from '@justlook/core';
import Auth from '../containers/auth';

import cx from './authorized-page.module.scss';

class AuthorizedPage extends Component {
    render () {
        return (
            <div className={cx.wrapper}>
                <Helmet>
                    <title>{Config.brandName} - Авторизация</title>
                </Helmet>

                <div className={cx.form}>
                    <Auth/>
                </div>
            </div>
        );
    }
}

export default AuthorizedPage;
