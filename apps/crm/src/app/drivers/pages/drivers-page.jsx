import React, { Fragment }                 from 'react';
import { Helmet }                          from 'react-helmet';
import { NavLink, useNavigate, useOutlet } from 'react-router-dom';
import { Config }                          from '@justlook/core/src';
import { Icons24, MobileHeader }           from '@justlook/uikit';
import { useIfMediaScreen }                from '@justlook/uikit/lib/hooks';
import { TwoColumnsLayout, useMenu }       from 'components/menu';
import { withPermissions }                 from 'components';
import { useContentView }                  from 'utils/browser';

const Header = () => {
    const isMobile = useIfMediaScreen();
    const { onExpandedMobile } = useMenu();
    const navigate = useNavigate();

    return <MobileHeader header={'Водители'}
                         onGoBack={() => navigate('/drivers')}>
        {
            isMobile &&
            <MobileHeader.Button icon={<Icons24.IconMenu/>}
                                 onClick={onExpandedMobile}/>
        }
    </MobileHeader>;
};

const Page = () => {
    const child = useOutlet();
    const viewMode = useContentView(child);

    return (
        <Fragment>
            <Helmet>
                <title>Водители - {Config.brandName}</title>
            </Helmet>

            <TwoColumnsLayout
                viewMode={viewMode}
                details={child}
                header={<Header viewMode={viewMode}/>}
            >
                <NavLink to='/drivers/100'>drivers-100</NavLink>
                <br/>
                <NavLink to='/drivers/101'>drivers-101</NavLink>
                <br/>
                <NavLink to='/drivers/102'>drivers-102</NavLink>
            </TwoColumnsLayout>
        </Fragment>
    );
};

export default withPermissions(Page, [ 'read-contractor' ]);
