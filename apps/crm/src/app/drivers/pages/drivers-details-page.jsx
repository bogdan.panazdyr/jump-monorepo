import React, { Fragment } from 'react';
import { Helmet }          from 'react-helmet';
import { useParams }       from 'react-router-dom';
import { Config }          from '@justlook/core/src';

const Page = () => {
    const { id } = useParams();

    return (
        <Fragment>
            <Helmet>
                <title>Водитель {id}- {Config.brandName}</title>
            </Helmet>

            <h1>Водитель: {id}</h1>

        </Fragment>
    );
};

export default Page;
