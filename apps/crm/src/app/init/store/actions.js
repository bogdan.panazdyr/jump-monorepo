import {
    APP_RUN, APP_AUTHORIZE, APP_LOADING, APP_READY,
    APP_FATAL, APP_CONFIG, APP_DICT,
} from '@justlook/core/lib/store';

export const runApp = () => ({ type: APP_RUN });
export const stateLoading = () => ({ type: APP_LOADING });
export const stateAuthorizing = () => ({ type: APP_AUTHORIZE });
export const stateReady = () => ({ type: APP_READY });
export const stateFatal = (error) => ({ type: APP_FATAL, error });
export const fillConfig = (config) => ({ type: APP_CONFIG, config });
export const fillDictionaries = (dictionaries) => ({ type: APP_DICT, dictionaries, });

export const fetchGroupsDictionary = (groups) => ({ type: APP_DICT + '_GROUPS', groups });
