import { fetchProfile } from 'app/profile/store/sagas';
import { put, select, fork, call, takeEvery } from 'redux-saga/effects';
import { Api as ApiUtils, Utils, Store } from '@justlook/core';
import {
    stateLoading, stateAuthorizing, stateFatal, stateReady,
    fillConfig, fillDictionaries, fetchGroupsDictionary
} from './actions';


import Api from 'api/client';

function * runApp () {
    yield put(stateLoading());

    const { token: sessionToken, isNewDevice } = yield call(ApiUtils.syncAuthCookies);

    if (isNewDevice) {
        yield put(stateAuthorizing());

    } else {
        try {
            // получение access-токена из старого кабинета
            const token = yield call([Api.authLK, 'getAccessToken'], sessionToken);

            // сохраняем токен в LocalStorage для того чтоб api клиент мог иметь доступ
            Utils.Token.storeTokenStorage(token);

            const config = yield call([Api.app, 'config']);
            yield put(fillConfig(config));

            const dictionaries = yield call([Api.app, 'dictionaries']);
            yield put(fillDictionaries(dictionaries));

            const groups = yield call([Api.groups, 'fetchList'])
            yield put(fetchGroupsDictionary(groups));

            yield call(fetchProfile);
            const profile = yield select(state => state?.profile);

            if (profile?.data) {
                yield put(stateReady());
            } else {
                yield put(stateFatal(profile?.error || 'Неизвестная ошибка'));
            }
        } catch (e) {
            if (e?.responseCode === 403) {
                yield put(stateAuthorizing());
            } else {
                yield put(stateFatal(e));
            }
        }
    }
}

function * watchRunApp () {
    yield takeEvery(Store.APP_RUN, runApp);
}

export default function * watcher () {
    yield fork(watchRunApp);
}
