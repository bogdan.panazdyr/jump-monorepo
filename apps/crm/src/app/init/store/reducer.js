import produce from 'immer';
import {
    APP_AUTHORIZE, APP_LOADING, APP_READY, APP_FATAL, APP_CONFIG, APP_DICT,
} from '@justlook/core/lib/store';

import {Config} from '@justlook/core';

const INITIAL_STATE = {
    isLoading: !Config?.develop,
    isAuthorized: !!Config?.develop,
    fatalError: null,
    config: {},
    dictionaries: {},
    dynamicDictionaries: {
        groups: [],
    },
};

export const app = (state = INITIAL_STATE, action) =>
    produce(state, draft => {
        // eslint-disable-next-line default-case
        switch (action.type) {
            case APP_LOADING:
            case APP_AUTHORIZE:
            case APP_READY:
                draft.fatalError = null;
                break;
            case APP_FATAL:
                draft.fatalError = action.error;
                break;
        }

        // eslint-disable-next-line default-case
        switch (action.type) {
            case APP_LOADING:
                draft.isLoading = true;
                break;
            case APP_AUTHORIZE:
                draft.isLoading = false;
                draft.isAuthorized = false;
                break;
            case APP_READY:
                draft.isLoading = false;
                draft.isAuthorized = true;
                break;
            case APP_CONFIG:
                draft.config = action.config;
                break;
            case APP_DICT:
                draft.dictionaries = action.dictionaries;
                break;
            case APP_DICT + '_GROUPS':
                draft.dynamicDictionaries.groups = action.groups.items;
                break;
        }
    });
