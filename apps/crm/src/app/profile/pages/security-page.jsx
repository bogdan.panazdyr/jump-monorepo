import React, { Fragment }           from 'react';
import { Helmet }                    from 'react-helmet';
import { useOutlet, useNavigate }    from 'react-router-dom';
import { Config }                    from '@justlook/core/src';
import { Icons24, MobileHeader }     from '@justlook/uikit';
import { useIfMediaScreen }          from '@justlook/uikit/lib/hooks';
import { useMenu }                   from 'components/menu';
import { TwoColumnsLayout }          from 'components/menu/content-layouts';
import { withPermissions, ListLink } from 'components';
import { useContentView }            from 'utils/browser';

const Header = () => {
    const isMobile = useIfMediaScreen();
    const { onExpandedMobile } = useMenu();
    const navigate = useNavigate();

    return <MobileHeader header={'Безопасность'}
                         onGoBack={() => navigate('/settings-security',
                             { replace: true })}>
        {
            isMobile &&
            <MobileHeader.Button icon={<Icons24.IconMenu/>}
                                 onClick={onExpandedMobile}/>
        }
    </MobileHeader>;
};

const Page = () => {
    const child = useOutlet();
    const viewMode = useContentView(child);

    return (
        <Fragment>
            <Helmet>
                <title>Безопасность - {Config.brandName}</title>
            </Helmet>

            <TwoColumnsLayout
                viewMode={viewMode}
                details={child}
                header={<Header viewMode={viewMode}/>}
            >
                <ListLink to='/settings-security/sessions'>
                    Доступ к аккаунту
                </ListLink>

                <ListLink to='/settings-security/two-auth'>
                    Двухфакторная аутентификация
                </ListLink>
            </TwoColumnsLayout>
        </Fragment>
    );
};

export default withPermissions(Page);
