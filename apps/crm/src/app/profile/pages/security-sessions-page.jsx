import React, { Fragment } from 'react';
import { Helmet }          from 'react-helmet';
import { Config }          from '@justlook/core/src';
import { ContentBox }      from 'components';

const Page = () => {
    return (
        <Fragment>
            <Helmet>
                <title>Доступ к аккаунту - {Config.brandName}</title>
            </Helmet>

            <ContentBox>
                <h1>Доступ к аккаунту</h1>
                <br/>
                <br/>
                <p>Раздел находится в разработке</p>
            </ContentBox>
        </Fragment>
    );
};

export default Page;
