import { PROFILE } from './action-types';
import {
    FETCH, REQUEST, RECEIVE, FAILURE,
} from '@justlook/core/lib/store';

export const fetchProfile = () => ({type: PROFILE + FETCH});
export const requestProfile = () => ({type: PROFILE + REQUEST});
export const receiveProfile = (profile) => ({type: PROFILE + RECEIVE, profile});
export const failureProfile = (error) => ({type: PROFILE + FAILURE, error});

