import produce from 'immer';
import { PROFILE } from './action-types';
import { REQUEST, RECEIVE, FAILURE } from '@justlook/core/lib/store';

const INITIAL_STATE = {
    data: null,
    isRequest: false,
    error: null,
};

export const profile = (state = INITIAL_STATE, action) =>
    produce(state, draft => {
        // eslint-disable-next-line default-case
        switch (action.type) {
            case PROFILE + REQUEST:
                draft.isRequest = true;
                break;
            case PROFILE + RECEIVE:
                draft.isRequest = false;
                draft.data = action.profile;
                break;
            case PROFILE + FAILURE:
                draft.isRequest = false;
                draft.error = action.error;
                break;
        }
    });