import { put, fork, call, takeEvery } from 'redux-saga/effects';
import { FETCH } from '@justlook/core/lib/store';
import Api from 'api/client';

import { PROFILE } from './action-types';
import { requestProfile, receiveProfile, failureProfile } from './actions';

export function * fetchProfile () {
    yield put(requestProfile());

    try {
        const profile = yield call([Api.profile, 'fetchProfile']);
        yield put(receiveProfile(profile?.data || null));
    } catch (e) {
        yield put(failureProfile(e));
    }
}

function * watchFetchProfile () {
    yield takeEvery(PROFILE + FETCH, fetchProfile);
}

export default function * watcher () {
    yield fork(watchFetchProfile);
}
