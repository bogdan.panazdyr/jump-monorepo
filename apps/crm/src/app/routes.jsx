import React from 'react';
import { Helmet } from 'react-helmet';
import { Routes } from 'react-router';
import { Route } from 'react-router-dom';
import { parse } from 'query-string';

import { Config } from '@justlook/core';
import { ErrorScreen404 } from '@justlook/uikit';

import { MenuPageLayout } from 'components/menu';
import { AgentPage, AgentDetailPage } from 'app/agents';
import { WorkDashboardPage, TaxiDashboardPage, DashboardPage} from 'app/dashboard';
import { DocumentsPage } from 'app/documents';
import { DriversPage, DriverDetailPage } from 'app/drivers';
import { OrdersPage } from 'app/orders';
import { UsersPage, UserDetailPage } from 'app/users';
import {
    GeneralSettingsPage, LegalFormPage, PaymentsPage, PaymentWizard
} from 'app/general-settings';
import {
    SecurityPage, SecuritySessionsPage, SecurityTwoAuthPage
} from 'app/profile';
import {
    ContractorDetailsPage,
    ContractorsPage,
    Documents,
    History,
    Orders,
    Payments as PaymentsTab,
    Requisites,
} from 'app/contractors';
import { ReportPage } from 'app/reports';
import { PaymentsPage as SimplePaymentsPage } from 'app/payments';
import { StatementPage, StatementDetailsPage } from 'app/taxes'
import { IntegrationsPage } from 'app/integrations';
import { BankAccounts } from 'app/bank-accounts';

const NotFoundPage = () => {
    return (
        <React.Fragment>
            <Helmet>
                <title>404 - {Config.brandName}</title>
            </Helmet>

            <ErrorScreen404/>
        </React.Fragment>
    );
};

const userMenu = [
    // {
    //     to: '/setting-profile',
    //     label: 'Профиль',
    // },
];

const routes = (menu, user, features) => {
    const isShowDashboard = features.show_dashboard || false;
    const isSimplePayments = features.show_simple_payments || false;

    const url = parse(window.location.search);
    const isTest = url?.isTest || false;

    return (
        <MenuPageLayout menu={menu} userMenu={userMenu} user={user}>
            <Routes>
                { isShowDashboard && Config.crm_type !== 'taxi' && <Route element={<WorkDashboardPage type='work'/>} path='/'/> }
                { isShowDashboard && Config.crm_type === 'taxi' && <Route element={<TaxiDashboardPage type='taxi'/>} path='/'/> }
                { !isShowDashboard && <Route element={<DashboardPage />} path='/'/> }

                { Config.crm_type === 'taxi' && <Route element={<ReportPage type='trips'/>} path='reports/trips'/> }
                { Config.crm_type === 'taxi' && <Route element={<ReportPage type='decreases'/>} path='reports/decreases'/> }
                { Config.crm_type === 'taxi' && <Route element={<ReportPage type='payments'/>} path='reports/payments'/> }

                { Config.crm_type === 'taxi' && <Route element={<IntegrationsPage />} path='integrations'/> }

                { isSimplePayments && <Route element={<SimplePaymentsPage />} path='payments'/> }
                <Route element={<StatementPage />} path='taxes/statements'/>
                <Route element={<StatementDetailsPage />} path='taxes/statements/:id'/>
                {
                    Config.crm_type !== 'taxi' &&
                    <Route element={<ContractorsPage />} path='contractors'>
                        <Route element={<ContractorDetailsPage />} path=':id'>
                            <Route element={<PaymentsTab />} index />
                            <Route element={<PaymentsTab />} path='payments' />
                            <Route element={<Orders />} path='orders'/>
                            <Route element={<Requisites />} path='requisites' />
                            <Route element={<Documents />} path='documents' />
                            <Route element={<History />} path='history' />
                        </Route>
                    </Route>
                }

                {
                    Config.crm_type !== 'taxi' &&
                    <Route element={<DocumentsPage />} path='settings-documents'/>
                }

                {
                    Config.crm_type === 'taxi' &&
                    <Route element={<DriversPage />} path='drivers'>
                        <Route element={<DriverDetailPage />} path=':id'/>
                    </Route>
                }

                {
                    Config.crm_type !== 'taxi' &&
                    <Route element={<OrdersPage />} path='orders' />
                }

                <Route element={<UsersPage />} path='settings-users'>
                    <Route element={<UserDetailPage />} path=':id'/>
                </Route>

                <Route element={<GeneralSettingsPage />} path='settings-company'>
                    { Config.crm_type !== 'taxi' && <Route element={<LegalFormPage />} path='legal_form' /> }
                    { Config.crm_type !== 'taxi' && <Route element={<PaymentsPage />} path='payments' /> }

                    { isTest && <Route element={<PaymentWizard />} path='payment_wizard'/> }
                </Route>

                <Route element={<SecurityPage />} path='settings-security'>
                    <Route element={<SecuritySessionsPage />} path='sessions'/>
                    <Route element={<SecurityTwoAuthPage />} path='two-auth'/>
                </Route>

                <Route element={<AgentPage />} path='settings-agents'>
                    <Route element={<AgentDetailPage />} path=':id'/>
                </Route>

                <Route element={<BankAccounts />} path={`${Config.crm_type === 'taxi' ? '' : 'settings-'}banks`}/>

                <Route element={<NotFoundPage />} path='*'/>
            </Routes>
        </MenuPageLayout>
    );
}

export default routes;
