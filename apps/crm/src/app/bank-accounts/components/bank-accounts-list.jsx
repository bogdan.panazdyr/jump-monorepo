import React, { Fragment, useState } from 'react';
import PropTypes from 'prop-types';
import { Config } from '@justlook/core';
import { Icons24 } from '@justlook/uikit';

import { EmptyContent, NewTable as Table, IconBar, DefaultMarker } from 'components';

import { useMoney } from 'hooks/use-money';

import { StatusSwitcher } from './status-switcher';
import { LinkToArchivedAccounts } from './link-archived-accounts';
import { BankAccountModal } from './bank-accounts-modal';
import { MobileRowBankAccounts } from './mobile-row-bank-accounts';

import cx from './bank-accounts-list.module.scss';

export const BankAccountsList = ({ items, onSelect, isEditable, hasArchived }) => {
    const [ isShowBankAccountModal, setIsShowBankAccountModal ] = useState(false);

    const { currency, format } = useMoney();

    const isLinkToArchivedAccounts = hasArchived && Config.crm_type === 'taxi';
    const emptyMessage = isLinkToArchivedAccounts
        ? 'Вы можете добавить новый счет или управлять архивными счетами'
        : 'Вы еще не добавили ни одного счета';

    const renderEmptyContent = () => {
        return (
            <EmptyContent
                message={emptyMessage}
                action={() => setIsShowBankAccountModal(true)}
                actionLabel={isEditable && 'Добавить счет'}
            >
                {isLinkToArchivedAccounts && <LinkToArchivedAccounts/>}
            </EmptyContent>
        )
    }

    const renderList = () => {
        return (
            <Fragment>
                <Table>
                    <Table.Head
                        cells={[
                            { label: 'Название' },
                            { label: 'Баланс', className: cx.balanceCell  },
                            { label: 'Банк' },
                            { label: 'Юрлицо' },
                        ]}
                    />
                    {items.map(item => {
                        const renderName = () => {
                            return (
                                <Fragment>
                                    {item.name}
                                    {item.is_default && <IconBar data={{
                                        label: 'Счет по умолчанию для юрлица',
                                        icon: <DefaultMarker />
                                    }}/>}
                                </Fragment>
                            )
                        }

                        const renderMobileDescription = () => `${format(item?.balance)} ${currency} · ${item?.agent?.name}`;

                        const renderStatusSwitcher = () => {
                            return (
                                <StatusSwitcher
                                    id={item.id}
                                    isActive={item.is_active}
                                    disabled={!isEditable}
                                />
                            )
                        }

                        return (
                            <Table.Row
                                key={item.id}
                                mobileRow={
                                    <MobileRowBankAccounts
                                        title={renderName()}
                                        description={renderMobileDescription()}
                                        error={!item?.has_default_account && 'Юрлицо не имеет счета по умолчанию.'}
                                        onClick={() => onSelect(item.id)}
                                        statusControl={renderStatusSwitcher()}
                                    />
                                }
                                renderCheckboxCell={renderStatusSwitcher()}
                                renderCustomCell={
                                    isEditable &&
                                    <Icons24.IconPencil
                                        className={cx.edit}
                                        onClick={() => onSelect(item.id)}
                                    />
                                }
                            >
                                <Table.Cell>
                                    {renderName()}
                                </Table.Cell>
                                <Table.Cell>{format(item.balance)}&nbsp;{currency}</Table.Cell>
                                <Table.Cell>{item.payment_system.name}</Table.Cell>
                                <Table.Cell>
                                    { ! item?.has_default_account &&
                                    <IconBar
                                        data={{
                                            icon: <Icons24.IconWarning className={cx.warning}/>,
                                            label: 'Юрлицо не имеет счета по умолчанию.'
                                        }}
                                        positionBar='top-left'
                                    />
                                    }
                                    {item?.agent?.name || '–'}&nbsp;
                                </Table.Cell>
                            </Table.Row>
                        )
                    })}
                </Table>
                {isLinkToArchivedAccounts && <LinkToArchivedAccounts/>}
            </Fragment>
        )
    }

    return (
        <Fragment>
            { items.length === 0 && renderEmptyContent() }
            { items.length > 0 && renderList() }
            {isShowBankAccountModal && <BankAccountModal onDismiss={() => setIsShowBankAccountModal(false)}/>}
        </Fragment>
    )
}

BankAccountsList.propTypes = {
    items: PropTypes.array.isRequired,
    onSelect: PropTypes.func,
    isEditable: PropTypes.bool.isRequired,
    hasArchived: PropTypes.bool,
}

BankAccountsList.defaultProps = {
    onSelect: () => {},
    isEditable: false
}
