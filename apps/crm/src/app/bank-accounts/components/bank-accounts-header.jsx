import React, { Fragment, useState } from 'react';
import { Icons24, MobileHeader } from '@justlook/uikit';
import { useIfMediaScreen } from '@justlook/uikit/lib/hooks';

import { useMenu } from 'components/menu';
import { useHasPermission } from 'app/profile/store/selectors';

import { BankAccountModal } from 'app/bank-accounts/components/bank-accounts-modal';

export const BankAccountsHeader = () => {
    const [ isShowBankAccountModal, setIsShowBankAccountModal ] = useState(false);
    const canWriteBankAccount = useHasPermission('write-bank-account');
    const { onExpandedMobile } = useMenu();
    const isMobile = useIfMediaScreen();

    return (
        <Fragment>
            <MobileHeader header='Счета'>
                {canWriteBankAccount &&
                    <MobileHeader.Button icon={<Icons24.IconPlus/>} onClick={() => setIsShowBankAccountModal(true)}/>
                }

                { isMobile && <MobileHeader.Button icon={<Icons24.IconMenu/>} onClick={onExpandedMobile}/> }
            </MobileHeader>
            {isShowBankAccountModal && <BankAccountModal onDismiss={() => setIsShowBankAccountModal(false)}/>}
        </Fragment>
    )
}
