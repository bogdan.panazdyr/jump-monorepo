import React from 'react';
import PropTypes from 'prop-types';
import { Toggle } from '@justlook/uikit';

import { useStatusMutator } from '../mutators';

export const StatusSwitcher = ({ id, isActive, disabled = false }) => {
    const { isLoading, mutate } = useStatusMutator(id);

    return (
        <Toggle
            disabled={isLoading || disabled}
            isLoading={isLoading}
            id={id}
            checked={isActive}
            onChange={(value) => mutate(value)}
        />
    );
};

StatusSwitcher.propTypes = {
    id: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]).isRequired,
    disabled: PropTypes.bool,
    isActive: PropTypes.bool,
};
