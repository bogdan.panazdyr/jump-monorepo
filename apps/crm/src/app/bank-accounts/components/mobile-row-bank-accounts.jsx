import React from 'react';
import PropTypes from 'prop-types';
import { ParagraphV2 as Paragraph, Description } from '@justlook/uikit';

import cx from './mobile-row-bank-accounts.module.scss';

export const MobileRowBankAccounts = ({ title, description, error, statusControl, onClick }) => {
    return (
        <div className={cx.base}>
            <div onClick={onClick}>
                <Paragraph.LH24 className={cx.title}>{title}</Paragraph.LH24>
                <Description.LH24 className={cx.desc}>{description}</Description.LH24>
                { error && <Description.LH24 className={cx.error}>{error}</Description.LH24> }
            </div>
            <div className={cx.statusControl}>
                {statusControl}
            </div>
        </div>
    )
}

MobileRowBankAccounts.propTypes = {
    title: PropTypes.any,
    description: PropTypes.any,
    error: PropTypes.any,
    statusControl: PropTypes.node,
    onClick: PropTypes.func,
}
