import React from 'react';
import { Link } from '@justlook/uikit';

import { Box } from 'components/box';

export const LinkToArchivedAccounts = () => {
    return (
        <Box pt={16} pb={16}>
            <Link to='https://drivers.jump.taxi/banks' target='_blank'>
                Управлять архивными счетами
            </Link>
        </Box>
    )
}
