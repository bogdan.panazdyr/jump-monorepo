import React, { Fragment, useEffect, useReducer } from 'react';
import PropTypes from 'prop-types';
import { Controller, useForm } from 'react-hook-form';
import { Forms } from '@justlook/core';
import { Form, ParagraphV2 as Paragraph, Select, Input, } from '@justlook/uikit';

import { defaultFields, preparingDataForSubmit, selectForm } from '../service';
import { useEditMutator } from '../mutators';

import { AgentSelection } from 'app/bank-accounts/components/agent-selection-field';

import { compareLengthToArray } from 'utils/compare-length-to-array';

import cx from 'app/bank-accounts/components/bank-accounts-modal.module.scss';

export const BankForm = ({ formId, item, agents, banks, onSupport, onProcessing, onSuccessSubmit }) => {
    const hasItem = !!item?.id;
    const defaultBank = item ? item.bank : banks?.[0];
    const defaultAgent = item
        ? item.agent
        : compareLengthToArray(agents, 1) ? agents?.[0] : null;
    const defaultType = item ? item.payment_system_type : banks?.[0]?.payment_system_types?.[0];

    const defaultValues = {
        ...defaultFields,
        ...item,
        name: item ? item?.name : 'Счет',
        bank_id: Forms.itemToOption(defaultBank, { label: 'name', value: 'id' }),
        agent_id: defaultAgent && Forms.itemToOption(defaultAgent, { label: 'name', value: 'id' }),
        payment_system_type_id: Forms.itemToOption(defaultType, { label: 'name', value: 'id' }),
    }

    const { handleSubmit, errors, control, setValue, setError, watch } = useForm({ defaultValues });

    const initialState = {
        bankId: defaultBank?.id,
        typeId: defaultType?.id,
        form: selectForm(defaultBank?.id, defaultType?.id),
        paymentSystems: banks.filter(bank => bank?.id === defaultBank?.id)?.[0]?.payment_system_types || [],
    }

    const reducer = (state, action) => {
        switch (action.type){
            case 'bank':
                // eslint-disable-next-line no-case-declarations
                const paymentSystems = banks.filter(bank => bank.id === action.bankId)?.[0]?.payment_system_types;
                // eslint-disable-next-line no-case-declarations
                const typeId = paymentSystems?.[0]?.id;
                setValue('payment_system_type_id', Forms.itemToOption(paymentSystems?.[0], { label: 'name', value: 'id' }));
                return {
                    ...state,
                    typeId,
                    paymentSystems,
                    bankId: action.bankId,
                    form: selectForm(action.bankId, typeId)
                };
            case 'type':
                return {
                    ...state,
                    typeId: action.typeId,
                    form: selectForm(state.bankId, action.typeId)
                };
            default:
                return state;
        }
    }

    const [state, dispatch] = useReducer(reducer, initialState);

    const onSetBankId = (bankId) => { dispatch({type: 'bank', bankId}) }
    const onSetTypeId = (typeId) => { dispatch({type: 'type', typeId}) }

    const bankOptions = Forms.makeOptions(banks, { value: 'id', label: 'name' });
    const typeOptions = Forms.makeOptions(state.paymentSystems, { value: 'id', label: 'name' });

    const { onCreate, onUpdate, isProcessing, isSuccess } = useEditMutator(item?.id, setError);

    const getError = (field) => errors?.[field]?.message;
    const onSubmit = handleSubmit(values => item?.id
        ? onUpdate(preparingDataForSubmit(values))
        : onCreate(preparingDataForSubmit(values))
    )

    const BankForms = state.form;


    useEffect(() => onSupport(!!BankForms), [BankForms, onSupport]);
    useEffect(() => onProcessing(isProcessing), [isProcessing, onProcessing]);
    useEffect(() => { if (isSuccess) onSuccessSubmit() }, [isSuccess, onSuccessSubmit]);

    return (
        <Form id={formId} onSubmit={onSubmit}>
            <Form.Field>
                <Controller
                    as={Input}
                    name='name'
                    label='Название'
                    className={cx.large}
                    control={control}
                    rules={{ required: 'Обязательно для ввода' }}
                    error={getError('name')}
                />
            </Form.Field>

            <Form.Field>
                <div className={cx.large}>
                    <label>
                        <Paragraph.LH24>Банк</Paragraph.LH24>
                    </label>
                    <Controller
                        name='bank_id'
                        control={control}
                        rules={{ required: 'Обязательно для ввода' }}
                        render={({ onChange, ...rest }) => (
                            <Select
                                options={bankOptions}
                                onChange={(option) => {
                                    onSetBankId(option.value);
                                    onChange(option);
                                }}
                                error={getError('bank_id')}
                                disabled={hasItem}
                                placeholder={'Выберите банк'}
                                {...rest}
                            />
                        )}
                    />
                </div>
            </Form.Field>

            {state.bankId && <Form.Field>
                <div className={cx.large}>
                    <label>
                        <Paragraph.LH24>Тип</Paragraph.LH24>
                    </label>
                    <Controller
                        name='payment_system_type_id'
                        control={control}
                        rules={{ required: 'Обязательно для ввода' }}
                        render={({ onChange, ...rest }) => (
                            <Select
                                options={typeOptions}
                                onChange={(option) => {
                                    onSetTypeId(option.value);
                                    onChange(option);
                                }}
                                error={getError('payment_system_type_id')}
                                disabled={hasItem || typeOptions.length === 1}
                                placeholder={'Выберите тип'}
                                {...rest}
                            />
                        )}
                    />
                </div>
            </Form.Field>}

            {
                BankForms ? (
                    <Fragment>
                        <BankForms control={control} getError={getError} editForm={hasItem}/>
                        <AgentSelection
                            bankAccount={item}
                            items={Forms.makeOptions(agents, { value: 'id', label: 'name' })}
                            control={control}
                            watch={watch}
                            getError={getError}
                        />
                    </Fragment>
                ) : <p className={cx.noSupport}>Банк не поддерживается</p>
            }
        </Form>
    )
}

BankForm.propTypes = {
    onProcessing: PropTypes.func,
    onSupport: PropTypes.func,
    formId: PropTypes.string.isRequired,
    item: PropTypes.object,
    agents: PropTypes.array.isRequired,
    banks: PropTypes.array.isRequired,
    onSuccessSubmit: PropTypes.func,
}
