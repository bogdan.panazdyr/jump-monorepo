import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Input, Form, Password, Textarea, ParagraphV2 as Paragraph } from '@justlook/uikit';
import { Controller } from 'react-hook-form';

import { SecurityInput } from 'components/security-input';

import cx from 'app/bank-accounts/components/bank-accounts-modal.module.scss';

export const AlfaBankApi = ({ control, getError, editForm }) => {
    return (
        <Fragment>
            <br/>
            <Form.Field>
                <Controller
                    as={Input}
                    name='login'
                    label='Логин'
                    className={cx.large}
                    control={control}
                    rules={{ required: 'Обязательно для ввода' }}
                    error={getError('login')}
                />
            </Form.Field>
            <Form.Field>
                <Controller
                    as={editForm  ? SecurityInput : Password}
                    name='password'
                    label='Пароль'
                    className={cx.large}
                    control={control}
                    rules={!editForm && { required: 'Обязательно для ввода' }}
                    error={getError('password')}
                />
            </Form.Field>
            <Form.Field>
                <Controller
                    as={Input}
                    name='short_name'
                    label='Организация'
                    className={cx.large}
                    control={control}
                    rules={{ required: 'Обязательно для ввода' }}
                    error={getError('short_name')}
                />
            </Form.Field>
            <Form.Field>
                <Controller
                    as={Input}
                    name='inn'
                    label='ИНН'
                    className={cx.middle}
                    control={control}
                    rules={{ required: 'Обязательно для ввода' }}
                    error={getError('inn')}
                />
            </Form.Field>
            <Form.Field>
                <Controller
                    as={Input}
                    name='kpp'
                    label='КПП'
                    className={cx.middle}
                    control={control}
                    rules={{ required: 'Обязательно для ввода' }}
                    error={getError('kpp')}
                />
            </Form.Field>
            <Form.Field>
                <Controller
                    as={Input}
                    name='bik'
                    label='БИК'
                    className={cx.middle}
                    control={control}
                    rules={{ required: 'Обязательно для ввода' }}
                    error={getError('bik')}
                />
            </Form.Field>
            <Form.Field>
                <Controller
                    as={Input}
                    name='bank_name'
                    label='Название банка'
                    className={cx.middle}
                    control={control}
                    rules={{ required: 'Обязательно для ввода' }}
                    error={getError('bank_name')}
                />
            </Form.Field>
            <Form.Field>
                <Controller
                    as={Input}
                    name='rs'
                    label='Рассчетный счет'
                    className={cx.large}
                    control={control}
                    rules={{ required: 'Обязательно для ввода' }}
                    error={getError('rs')}
                />
            </Form.Field>
            <Form.Field>
                <Controller
                    as={Input}
                    name='ks'
                    label='Корр. счет'
                    className={cx.large}
                    control={control}
                    rules={{ required: 'Обязательно для ввода' }}
                    error={getError('ks')}
                />
            </Form.Field>
            <Form.Field>
                <Controller
                    as={Input}
                    name='last_id'
                    label='Следующий номер платежа'
                    className={cx.middle}
                    control={control}
                    rules={{ required: 'Обязательно для ввода' }}
                    error={getError('last_id')}
                />
            </Form.Field>
            <Form.Field>
                <div className={cx.large}>
                    <label>
                        <Paragraph.LH24>Назначение платежа</Paragraph.LH24>
                    </label>
                    <Controller
                        as={Textarea}
                        name='payment_text'
                        control={control}
                        rules={{ required: 'Обязательно для ввода' }}
                        error={getError('payment_text')}
                    />
                </div>
            </Form.Field>
        </Fragment>
    )
}

AlfaBankApi.propTypes = {
    control: PropTypes.any,
    getError: PropTypes.func,
    editForm: PropTypes.bool,
}
