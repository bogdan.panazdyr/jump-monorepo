import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Input, Form } from '@justlook/uikit';
import { Controller } from 'react-hook-form';

import { SecurityInput } from 'components/security-input';

import cx from 'app/bank-accounts/components/bank-accounts-modal.module.scss';

export const TinkoffMoment = ({ control, getError, editForm }) => {
    return (
        <Fragment>
            <br/>
            <Form.Field>
                <Controller
                    as={editForm  ? SecurityInput : Input}
                    name='terminal_id'
                    label='Номер терминала'
                    className={cx.large}
                    control={control}
                    rules={!editForm && { required: 'Обязательно для ввода' }}
                    error={getError('terminal_id')}
                />
            </Form.Field>
        </Fragment>
    )
}

TinkoffMoment.propTypes = {
    control: PropTypes.any,
    getError: PropTypes.func,
    editForm: PropTypes.bool,
}
