import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Input, Form } from '@justlook/uikit';
import { Controller } from 'react-hook-form';

import cx from 'app/bank-accounts/components/bank-accounts-modal.module.scss';

export const TinkoffSBP = ({ control, getError }) => {
    return (
        <Fragment>
            <Form.Field>
                <Controller
                    as={Input}
                    name='inn'
                    label='ИНН'
                    className={cx.large}
                    control={control}
                    rules={{ required: 'Обязательно для ввода' }}
                    error={getError('inn')}
                />
            </Form.Field>
            <Form.Field>
                <Controller
                    as={Input}
                    name='kpp'
                    label='КПП'
                    className={cx.large}
                    control={control}
                    error={getError('kpp')}
                />
            </Form.Field>
        </Fragment>
    )
}

TinkoffSBP.propTypes = {
    control: PropTypes.any,
    getError: PropTypes.func,
}
