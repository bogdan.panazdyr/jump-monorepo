import { AlfaBankApi } from './alfa-bank-api';
import { Kassa24 } from './kassa24';
import { KzMoment } from './kz-moment';
import { QiwiTransit } from './qiwi-transit';
import { SberMoment } from './sber-moment';
import { TinkoffMoment } from './tinkoff-moment';
import { TinkoffOpenApi } from './tinkoff-open-api';
import { TinkoffSBP } from './tinkoff-sbp';

export const BankForms = {
    AlfaBankApi,
    Kassa24,
    KzMoment,
    QiwiTransit,
    SberMoment,
    TinkoffMoment,
    TinkoffOpenApi,
    TinkoffSBP,
}
