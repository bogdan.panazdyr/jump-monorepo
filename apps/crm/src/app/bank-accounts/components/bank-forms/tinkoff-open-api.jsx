import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Input, Form, ParagraphV2 as Paragraph, Textarea } from '@justlook/uikit';
import { Controller } from 'react-hook-form';

import cx from 'app/bank-accounts/components/bank-accounts-modal.module.scss';

export const TinkoffOpenApi = ({ control, getError }) => {
    return (
        <Fragment>
            <Form.Field>
                <Controller
                    as={Input}
                    name='inn'
                    label='ИНН'
                    className={cx.middle}
                    control={control}
                    rules={{ required: 'Обязательно для ввода' }}
                    error={getError('inn')}
                />
            </Form.Field>
            <Form.Field>
                <Controller
                    as={Input}
                    name='kpp'
                    label='КПП'
                    className={cx.small}
                    control={control}
                    error={getError('kpp')}
                />
            </Form.Field>
            <Form.Field>
                <Controller
                    as={Input}
                    name='rs'
                    label='Расчетный счет'
                    className={cx.large}
                    control={control}
                    rules={{ required: 'Обязательно для ввода' }}
                    error={getError('rs')}
                />
            </Form.Field>
            <Form.Field>
                <div className={cx.large}>
                    <label>
                        <Paragraph.LH24>Назначение платежа</Paragraph.LH24>
                    </label>
                    <Controller
                        as={Textarea}
                        name='payment_text'
                        control={control}
                        rules={{ required: 'Обязательно для ввода' }}
                        error={getError('payment_text')}
                    />
                </div>
            </Form.Field>
        </Fragment>
    )
}

TinkoffOpenApi.propTypes = {
    control: PropTypes.any,
    getError: PropTypes.func,
}
