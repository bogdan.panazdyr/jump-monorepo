import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Input, Form, Password } from '@justlook/uikit';
import { Controller } from 'react-hook-form';

import { SecurityInput } from 'components/security-input';

import cx from 'app/bank-accounts/components/bank-accounts-modal.module.scss';

export const KzMoment = ({ control, getError, editForm }) => {
    return (
        <Fragment>
            <br/>
            <Form.Field>
                <Controller
                    as={Input}
                    name='terminal_id'
                    label='Номер терминала'
                    className={cx.large}
                    control={control}
                    rules={{ required: 'Обязательно для ввода' }}
                    error={getError('terminal_id')}
                />
            </Form.Field>
            <Form.Field>
                <Controller
                    as={Input}
                    name='login'
                    label='Логин'
                    className={cx.large}
                    control={control}
                    rules={{ required: 'Обязательно для ввода' }}
                    error={getError('login')}
                />
            </Form.Field>
            <Form.Field>
                <Controller
                    as={editForm  ? SecurityInput : Password}
                    name='password'
                    label='Пароль'
                    className={cx.large}
                    control={control}
                    rules={!editForm && { required: 'Обязательно для ввода' }}
                    error={getError('password')}
                />
            </Form.Field>
        </Fragment>
    )
}

KzMoment.propTypes = {
    control: PropTypes.any,
    getError: PropTypes.func,
    editForm: PropTypes.any,
}
