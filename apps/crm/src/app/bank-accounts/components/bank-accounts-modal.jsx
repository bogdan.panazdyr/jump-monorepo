import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Popup, Button, Icons24 } from '@justlook/uikit';
import { SplashScreen } from 'components'
import { BankForm } from './bank-form';

import { useModalState } from '../service';
import { useDeleteMutator } from '../mutators';

import cx from './bank-accounts-modal.module.scss';

const FORM_ID = 'bank_account_form';

export const BankAccountModal = ({ onDismiss, id }) => {
    const modal = useModalState(id);
    const { onDelete, isDeleting, isSuccess } = useDeleteMutator(id);

    const [isProcessing, setIsProcessing] = useState(false);
    const [isSupport, setIsSupport] = useState(true);

    const {item, agents = [], banks = []} = modal.data || {};

    const title = id ? 'Редактирование счета' : 'Новый счет';
    const submitLabel = id ? 'Сохранить' : 'Добавить';
    const isDisableControl = modal.isFetching || isProcessing || isDeleting;

    useEffect(() => { if (isSuccess) onDismiss() }, [isSuccess, onDismiss]);

    return (
        <Popup onDismiss={isProcessing ? null : onDismiss}>
            <Popup.Header>{title}</Popup.Header>
            <Popup.Content>
                { modal.isError && modal.error.message}
                { modal.isFetching && <SplashScreen /> }
                { modal.isSuccess && !modal.isFetching && (
                    <BankForm
                        formId={FORM_ID}
                        item={item}
                        agents={agents}
                        banks={banks}
                        onProcessing={setIsProcessing}
                        onSupport={setIsSupport}
                        onSuccessSubmit={onDismiss}
                    />
                )}

            </Popup.Content>
            <Popup.Footer
                majorButton={
                    <Button
                        form={FORM_ID}
                        type='submit'
                        disabled={isDisableControl || !isSupport}
                        loading={isProcessing}
                    >
                        {submitLabel}
                    </Button>
                }
                onDismissButton={
                    <Button
                        styling='hollow-border'
                        onClick={onDismiss}
                        disabled={isDisableControl}
                    >
                        Отмена
                    </Button>
                }
                minorButton={
                    item && isSupport &&
                        <Button
                            styling='hollow-border'
                            className={cx.delete}
                            onClick={() => onDelete(id)}
                            disabled={isDisableControl}
                            loading={isDeleting}
                            icon={<Icons24.IconTrash/>}
                        >
                            Удалить
                        </Button>
                }
            />
        </Popup>
    )
}

BankAccountModal.propTypes = {
    onDismiss: PropTypes.func,
    id: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ])
}
