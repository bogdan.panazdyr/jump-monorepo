import React from 'react';
import PropTypes from 'prop-types';
import { Controller } from 'react-hook-form';
import { Config } from '@justlook/core';
import { Checkbox, Form, ParagraphV2 as Paragraph, Select, Link, InfoMessage, Icons24 } from '@justlook/uikit';

import { Box, DefaultMarker as Marker } from 'components';

import { useHasIsDefaultAccountForAgent } from '../service';

import cx from './bank-accounts-modal.module.scss';

const DefaultMarker = () => {
    return (
        <div className={cx.large}>
            <div/>
            <div className={cx.flex}>
                <Marker/>
                <Paragraph.LH24>Счет по умолчанию для этого юрлица</Paragraph.LH24>
            </div>
        </div>
    )
}

const DefaultAccountInfo = () => {
    return (
        <Box mt={16}>
            <InfoMessage icon={<Icons24.IconInfo />} style={{maxWidth: '100%'}}>
                При сохранении этот счет станет основным для проведения выплат с агрегаторов, привязанных к выбранному юрлицу.&nbsp;
                <Link
                    to='https://intercom.help/jumptaxi/ru/articles/5179394-разделение-финансовых-потоков-по-юрлицам'
                    target='_blank'
                >
                    Подробнее
                </Link>
            </InfoMessage>
        </Box>
    )
}

export const AgentSelection = ({ bankAccount, items, control, watch, getError }) => {
    const watchAgent = watch('agent_id');
    const watchPaymentSystemTypeId = watch('payment_system_type_id');
    const watchIsDefault = watch('is_default');

    let isDefault = bankAccount?.is_default || false;
    const selectAgentId = watchAgent?.value;
    const selectPaymentSystemTypeId = watchPaymentSystemTypeId?.value;
    const hasIsDefaultAccount = useHasIsDefaultAccountForAgent(selectAgentId, selectPaymentSystemTypeId);

    if (bankAccount?.id) {
        isDefault = bankAccount?.agent?.id === selectAgentId ? isDefault : false;
    }

    const isDefaultAccount = isDefault || !hasIsDefaultAccount;
    const isDefaultAccountInfo = (watchIsDefault || !hasIsDefaultAccount)
        && !isDefault && Config.crm_type === 'taxi';

    const renderCheckbox = () => {
        return (
            <div className={cx.large}>
                <div/>
                <Controller
                    name='is_default'
                    control={control}
                    error={getError('is_default')}
                    render={({value, ...rest}) => (
                        <Checkbox
                            label='Счет по умолчанию для этого юрлица'
                            checked={value}
                            {...rest}
                        />
                    )}
                />
            </div>
        )
    }

    return (
        <Box mt={40}>
            <Form.Field>
                <div className={cx.large}>
                    <label>
                        <Paragraph.LH24>Юрлицо</Paragraph.LH24>
                    </label>

                    <Controller
                        as={Select}
                        name='agent_id'
                        control={control}
                        options={items}
                        error={getError('agent_id')}
                        rules={{ required: 'Обязательно для ввода' }}
                    />
                </div>
            </Form.Field>
            {selectAgentId &&
                <Form.Field>
                    { isDefaultAccount ? <DefaultMarker /> : renderCheckbox() }
                    { isDefaultAccountInfo && <DefaultAccountInfo/> }
                </Form.Field>
            }
        </Box>
    )
}

AgentSelection.propTypes = {
    control: PropTypes.any,
    watch: PropTypes.func,
    getError: PropTypes.func,
    items: PropTypes.array,
    bankAccount: PropTypes.any,
}
