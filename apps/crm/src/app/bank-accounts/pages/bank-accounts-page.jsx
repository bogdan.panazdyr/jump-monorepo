import { useSelectItem } from 'hooks/use-select-item';
import React, { Fragment } from 'react';
import { Helmet } from 'react-helmet';

import { Config } from '@justlook/core';

import { useHasPermission } from 'app/profile/store/selectors';
import { withPermissions, Box, SplashScreen } from 'components';
import { OneColumnsLayout } from 'components/menu';

import { useFetchList, makeListModels } from '../service';
import { BankAccountModal } from '../components/bank-accounts-modal';
import { BankAccountsList } from '../components/bank-accounts-list';
import { BankAccountsHeader } from '../components/bank-accounts-header';

const Page = () => {
    const urlSection = Config.crm_type === 'taxi' ? 'Справочники' : 'Настройки';

    const canEdit = useHasPermission('write-bank-account');
    const { itemId, isSelected, setItemId, clearSelection } = useSelectItem();
    const { data: items, isLoading, isError, hasArchived } = useFetchList();
    const data = makeListModels(items);

    return (
        <Fragment>
            <Helmet>
                <title>{`${urlSection} – Счета`}</title>
            </Helmet>

            <OneColumnsLayout header={<BankAccountsHeader/>}>
                <Box pr={24} pl={24}>
                    { isError && 'Ошибка' }
                    { isLoading && <SplashScreen/> }
                    { !isLoading && data && <BankAccountsList
                        items={data}
                        isEditable={canEdit}
                        onSelect={(id) => setItemId(id) }
                        hasArchived={hasArchived}
                    /> }
                </Box>

                {isSelected && <BankAccountModal
                    id={itemId}
                    onDismiss={clearSelection}
                />
                }
            </OneColumnsLayout>
        </Fragment>
    )
}

export default withPermissions(Page, [ 'read-bank-account' ]);
