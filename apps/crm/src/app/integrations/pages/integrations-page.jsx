import React, { Fragment } from 'react';
import { Helmet } from 'react-helmet';

import { Pagination } from '@justlook/uikit';

import { withPermissions, Box  } from 'components';
import { OneColumnsLayout } from 'components/menu';
import { IntegrationsHeader } from 'app/integrations/components/integrations-header';
import { IntegrationsList } from 'app/integrations/components/integrations-list';
import { useIntegrationsList } from 'domains/integrations';

import cx from './integrations-page.module.scss';

function Page() {
    const { items, meta, isLoading, updateFilter } = useIntegrationsList();

    const { last_page = 1, current_page = 1 } = meta;

    return (
        <Fragment>
            <Helmet>
                <title>Справочники - Интеграции</title>
            </Helmet>
            <OneColumnsLayout header={<IntegrationsHeader/>}>
                <Box pl={24} pr={24}>
                    <IntegrationsList
                        items={items}
                        isLoading={isLoading}
                    />
                </Box>
                <div className={cx.paginator}>
                    {last_page > 1 &&
                        <Pagination
                            handlePageClick={(value) => updateFilter({ page: value.selected + 1 })}
                            initialPage={current_page - 1}
                            pageCount={last_page}
                        />
                    }
                </div>
            </OneColumnsLayout>
        </Fragment>
    )
}

export default withPermissions(Page, [ 'write-system-settings' ]);
