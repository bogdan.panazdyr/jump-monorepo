import React from 'react';
import PropTypes from 'prop-types';

import { Toggle } from '@justlook/uikit';

import { useChangeStatus } from 'domains/integrations';

export const StatusSwitcher = ({ id, isActive, disabled = false }) => {
    const { mutate, isLoading } = useChangeStatus(id);

    return (
        <Toggle
            disabled={isLoading || disabled}
            isLoading={isLoading}
            id={id}
            checked={isActive}
            onChange={(value) => mutate(value)}
        />
    );
};

StatusSwitcher.propTypes = {
    id: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]).isRequired,
    disabled: PropTypes.bool,
    isActive: PropTypes.bool,
};
