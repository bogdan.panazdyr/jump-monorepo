import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Controller } from 'react-hook-form';

import { Input, Form, Password } from '@justlook/uikit';

import { SecurityInput } from 'components';

import cx from '../integration-form.module.scss';

export const DidiForm = ({ control, errors, editForm }) => {
    return (
        <Fragment>
            <Form.Field>
                <Controller
                    as={editForm ? SecurityInput : Input}
                    name='api_key'
                    label='API ключ'
                    className={cx.grid}
                    control={control}
                    rules={!editForm && { required: 'Обязательно для ввода' }}
                    error={errors?.api_key?.message}
                />
            </Form.Field>
            <Form.Field>
                <Controller
                    as={Input}
                    name='phone'
                    label='Телефон'
                    className={cx.grid}
                    control={control}
                    error={errors?.phone?.message}
                />
            </Form.Field>
            <Form.Field>
                <Controller
                    as={editForm ? SecurityInput : Password}
                    name='password'
                    label='Пароль'
                    className={cx.grid}
                    control={control}
                    error={errors?.password?.message}
                />
            </Form.Field>
        </Fragment>
    )
}

DidiForm.propTypes = {
    control: PropTypes.any,
    errors: PropTypes.object,
    editForm: PropTypes.any,
}
