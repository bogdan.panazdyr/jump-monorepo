import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Controller } from 'react-hook-form';

import { Input, Form } from '@justlook/uikit';

import { SecurityInput } from 'components/security-input';

import cx from '../integration-form.module.scss';

export const YandexForm = ({ control, errors, editForm }) => {
    return (
        <Fragment>
            <Form.Field>
                <Controller
                    as={editForm ? SecurityInput : Input}
                    name='api_key_v7'
                    label='API ключ'
                    className={cx.grid}
                    control={control}
                    rules={!editForm && { required: 'Обязательно для ввода' }}
                    error={errors?.api_key_v7?.message}
                />
            </Form.Field>
            <Form.Field>
                <Controller
                    as={Input}
                    name='park_id'
                    label='ID парка'
                    className={cx.grid}
                    control={control}
                    rules={{ required: 'Обязательно для ввода' }}
                    error={errors?.park_id?.message}
                />
            </Form.Field>
            <Form.Field>
                <Controller
                    as={Input}
                    name='client_id'
                    label='ID клиента'
                    className={cx.grid}
                    control={control}
                    rules={{ required: 'Обязательно для ввода' }}
                    error={errors?.client_id?.message}
                />
            </Form.Field>
        </Fragment>
    )
}

YandexForm.propTypes = {
    control: PropTypes.any,
    errors: PropTypes.object,
    editForm: PropTypes.bool,
}
