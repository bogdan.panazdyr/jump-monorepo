import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import { Popup, Button, Icons24, ParagraphV2 as Paragraph } from '@justlook/uikit';

import { FormOtp } from 'components';
import { errorToString } from 'utils/add-server-errors';
import { useRequestOtp, useConfirmOtp, STEP_SUCCESS } from 'domains/integrations';

export const Otp = ({ payload, onGoStep, onDismiss }) => {
    const [ code, setCode ] = useState(null);

    const { mutate: onRequestOtp, isLoading, error: errorOtp } = useRequestOtp();
    const { onConfirmOtp, isConfirming, error } = useConfirmOtp(payload?.id, () => onGoStep(STEP_SUCCESS, payload));

    useEffect(() => { onRequestOtp(payload?.id) }, [onRequestOtp, payload]);

    const onRetry = () => onRequestOtp(payload?.id);

    const onChangeCode = (code) => {
        setCode(code);

        if (code.length === 4) {
            onConfirmOtp({ id: payload?.id, code });
        }
    }

    const isRequest = isConfirming || isLoading;

    return (
        <Popup onDismiss={isRequest ? null : onDismiss}>
            <Popup.Header description='Подтверждение'>
                Новая интеграция
            </Popup.Header>
            <Popup.Content>
                {
                    !isLoading &&
                    <Paragraph.LH18>
                        На номер телефона {payload?.phone} отправлен смс-код от сервиса Wheely для подтверждения.
                    </Paragraph.LH18>
                }

                <FormOtp
                    code={code}
                    setCode={onChangeCode}
                    isRetrying={isRequest}
                    onRetryOtp={onRetry}
                    error={errorToString(error || errorOtp)}
                />
            </Popup.Content>
            <Popup.Footer
                minorButton={
                    <Button
                        disabled={isRequest}
                        styling='hollow-border'
                        icon={<Icons24.IconBack/>}
                        onClick={() => onGoStep(null, payload)}
                    >
                        Назад
                    </Button>
                }
            />
        </Popup>
    )
}

Otp.propTypes = {
    payload: PropTypes.object,
    onGoStep: PropTypes.func,
    onDismiss: PropTypes.func,
}
