import React from 'react';
import PropTypes from 'prop-types';

import { Button, Icons48, ParagraphV2 as Paragraph, Popup } from '@justlook/uikit';

const styleWrapper = {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column'
}

const styleInfoSuccess = {
    textAlign:'center',
    marginTop: '12px',
}

export const Success = ({ onGoStep, onDismiss, payload }) => {
    const onFinish = () => { onDismiss() };

    return (
        <Popup onDismiss={onDismiss}>
            <Popup.Header>
                Новая интеграция
            </Popup.Header>

            <Popup.Content>
                <div style={styleWrapper}>
                    <Icons48.IconCheck/>
                    <Paragraph.LH24 style={styleInfoSuccess}>
                        Интеграция подтверждена.
                    </Paragraph.LH24>
                </div>
            </Popup.Content>

            <Popup.Footer
                majorButton={
                    <Button onClick={onFinish}>
                        Готово
                    </Button>
                }
                onDismissButton={
                    <Button
                        styling='hollow-border'
                        onClick={() => onGoStep(null, payload)}
                    >
                        Отредактировать интеграцию
                    </Button>
                }
            />
        </Popup>
    )
}

Success.propTypes = {
    onGoStep: PropTypes.func,
    onDismiss: PropTypes.func,
    payload: PropTypes.object,
}
