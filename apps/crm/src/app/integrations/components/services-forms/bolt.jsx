import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Controller } from 'react-hook-form';

import { Input, Form, Password } from '@justlook/uikit';

import { SecurityInput } from 'components';

import cx from '../integration-form.module.scss';

export const BoltForm = ({ control, errors, editForm }) => {
    return (
        <Fragment>
            <Form.Field>
                <Controller
                    as={Input}
                    name='login'
                    label='Логин'
                    className={cx.grid}
                    control={control}
                    rules={{ required: 'Обязательно для ввода' }}
                    error={errors?.login?.message}
                />
            </Form.Field>
            <Form.Field>
                <Controller
                    as={editForm ? SecurityInput : Password}
                    name='password'
                    label='Пароль'
                    className={cx.grid}
                    control={control}
                    rules={!editForm && { required: 'Обязательно для ввода' }}
                    error={errors?.password?.message}
                />
            </Form.Field>
        </Fragment>
    )
}

BoltForm.propTypes = {
    control: PropTypes.any,
    errors: PropTypes.object,
    editForm: PropTypes.bool,
}
