import React from 'react';
import PropTypes from 'prop-types';
import { Controller } from 'react-hook-form';

import { Input, Form } from '@justlook/uikit';

import { SecurityInput } from 'components/security-input';

import cx from '../integration-form.module.scss';

export const CitymobilForm = ({ control, errors, editForm }) => {
    return (
        <Form.Field>
            <Controller
                as={editForm ? SecurityInput : Input}
                name='api_key'
                label='API ключ'
                className={cx.grid}
                control={control}
                rules={!editForm && { required: 'Обязательно для ввода' }}
                error={errors?.api_key?.message}
            />
        </Form.Field>
    )
}

CitymobilForm.propTypes = {
    control: PropTypes.any,
    errors: PropTypes.object,
    editForm: PropTypes.bool,
}
