import React, { Fragment, useState } from 'react';
import PropTypes from 'prop-types';

import { Icons24, Button } from '@justlook/uikit';

import { EmptyContent, NewTable as Table, SplashScreen, IconBar } from 'components';


import { IntegrationModal } from './integrations-modal';
import { IntegrationDelete } from './integrations-delete-modal';
import { StatusSwitcher } from './status-switcher';

import cx from './integrations-list.module.scss';


export const IntegrationsList = ({ items, isLoading }) => {
    const [ isShowIntegrationModal, setIsShowIntegrationModal ] = useState(false);
    const [ isShowDeleteIntegration, setIsShowDeleteIntegration ] = useState(false);
    const [ currentIntegrationId, setCurrentIntegrationId ] = useState(null);

    const selectIntegration = (id) => {
        setCurrentIntegrationId(id);
        setIsShowIntegrationModal(true);
    }

    const deleteIntegration = (id) => {
        setCurrentIntegrationId(id);
        setIsShowDeleteIntegration(true);
    }

    if (isLoading) return <SplashScreen/>

    if (items.length === 0) return <EmptyContent message='Интеграций нет'/>

    return (
        <Fragment>
            <Table>
                <Table.Head
                    cells={[
                        { label: 'Внутреннее название' },
                        { label: 'Сервис' },
                        { label: 'Юрлицо' },
                    ]}
                    renderCheckBox={true}
                />
                {items.map(item => {
                    const isErrorConnected = !item?.is_login_success || item?.is_connected === false;

                    return (
                        <Table.Row
                            key={item.id}
                            mobileTitle={item?.name}
                            mobileDescription={item?.aggregator?.name}
                            renderCheckboxCell={
                                <StatusSwitcher
                                    id={item.id}
                                    isActive={item.is_active}
                                />
                            }
                            renderCustomCell={
                                <Fragment>
                                    <Button
                                        styling='hollow'
                                        icon={<Icons24.IconPencil/>}
                                        onClick={() => selectIntegration(item.id)}
                                    />
                                    <Button
                                        styling='hollow'
                                        icon={<Icons24.IconTrash/>}
                                        onClick={() => deleteIntegration(item.id)}
                                    />
                                </Fragment>
                            }
                        >
                            <Table.Cell>
                                {item?.name}
                                {isErrorConnected &&
                                <IconBar
                                    data={{
                                        icon: <Icons24.IconWarning className={cx.warning}/>,
                                        label: 'Ошибка подключения. Проверьте параметры.'
                                    }}
                                    positionBar='top-right'
                                />
                                }
                            </Table.Cell>
                            <Table.Cell>{item?.aggregator?.name}</Table.Cell>
                            <Table.Cell>{item?.agent?.name}</Table.Cell>
                        </Table.Row>
                    )
                })}
            </Table>
            {isShowIntegrationModal &&
                <IntegrationModal
                    id={currentIntegrationId}
                    onDismiss={() => setIsShowIntegrationModal(false)}
                />
            }
            {isShowDeleteIntegration &&
                <IntegrationDelete
                    id={currentIntegrationId}
                    onDismiss={() => setIsShowDeleteIntegration(false)}
                />
            }
        </Fragment>
    )
}

IntegrationsList.propTypes = {
    items: PropTypes.array,
    isLoading: PropTypes.bool,
}
