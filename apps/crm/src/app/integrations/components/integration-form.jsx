import React from 'react';
import PropTypes from 'prop-types';
import { Controller } from 'react-hook-form';
import { differenceInDays } from 'date-fns'

import { Forms } from '@justlook/core';
import {
    Button, CalendarV2 as Calendar, Checkbox,
    Form, Icons24, InfoMessage, Input,
    Link, ParagraphV2 as Paragraph, Popup, Select
} from '@justlook/uikit';

import { SplashScreen } from 'components/splash-screen';

import cx from './integration-form.module.scss';

const FORM_ID = 'integration-form';

export const IntegrationForm = ({
    onDismiss, integration, integrations, agents, groups, getError,
    form, hasStartDateField, needConnection, isArchived, Services, defaultGroupId, defaultAgentId
}) => {
    const renderAggregatorForm = () => {
        if (isArchived) {
            return (
                <div className={cx.grid}>
                    <div/>
                    <Paragraph.LH24>
                        Архивный сервис.
                        Для редактирования перейдите на старую версию страницы&nbsp;
                        <Link to='https://drivers.jump.taxi/aggregators' target='_blank'>интеграции</Link>
                    </Paragraph.LH24>
                </div>
            );
        }

        return (
            <Services
                control={form.control}
                errors={form.errors}
                editForm={Boolean(integration)}
            />
        );
    }

    const renderForm = () => {
        return (
            <Form onSubmit={form.onSubmit} id={FORM_ID}>
                {
                    needConnection && !isArchived &&
                    <InfoMessage
                        className={cx.infoMessage}
                        icon={<Icons24.IconWarning/>}
                        type='error'
                    >
                        Требуется подтверждение интеграции.
                    </InfoMessage>
                }

                <Form.Field>
                    <Controller
                        as={Input}
                        name='name'
                        label='Внутреннее название'
                        className={cx.grid}
                        control={form.control}
                        rules={{ required: 'Обязательно для ввода' }}
                        error={getError('name')}
                    />
                </Form.Field>
                <Form.Field>
                    <div className={cx.grid}>
                        <label>
                            <Paragraph.LH24>Сервис</Paragraph.LH24>
                        </label>
                        <Controller
                            as={Select}
                            name='aggregator_id'
                            control={form.control}
                            disabled={integration}
                            options={Forms.makeOptions(integrations.dictionaries[0].aggregators.options, {
                                value: 'value',
                                label: 'title'
                            })}
                            error={getError('aggregator_id')}
                            rules={{ required: 'Обязательно для ввода' }}
                        />
                    </div>
                </Form.Field>

                {renderAggregatorForm()}
                <br/>

                {hasStartDateField &&
                <Form.Field>
                    <div className={cx.grid}>
                        <label>
                            <Paragraph.LH24>Учитывать поездки с</Paragraph.LH24>
                        </label>
                        <div className={cx.calendar}>
                            <Controller
                                as={Calendar}
                                name='integration_start_date'
                                control={form.control}
                                disabled={integration}
                                rules={{ required: 'Обязательно для ввода' }}
                                error={getError('integration_start_date')}
                            />
                        </div>
                    </div>
                </Form.Field>
                }

                {integration && hasStartDateField &&
                <Form.Field>
                    <div className={cx.grid}>
                        <div/>
                        <Controller
                            name='need_reload_trips'
                            control={form.control}
                            render={({ onChange, value, ...rest }) => (
                                <Checkbox
                                    checked={value}
                                    onChange={onChange}
                                    disabled={form.control.getValues('integration_start_date') && differenceInDays(new Date(), form.control.getValues('integration_start_date')) > 30}
                                    label='Пересчитать поездки и комиссии'
                                    {...rest}
                                />
                            )}
                        />
                    </div>
                </Form.Field>
                }

                <Form.Field>
                    <div className={cx.grid}>
                        <label>
                            <Paragraph.LH24>Группа водителей</Paragraph.LH24>
                        </label>
                        <Controller
                            as={Select}
                            name='default_group_id'
                            control={form.control}
                            options={Forms.makeOptions(groups, { value: 'id', label: 'name' })}
                            defaultValue={defaultGroupId}
                            rules={{ required: 'Обязательно для ввода' }}
                            error={getError('default_group_id')}
                        />
                    </div>
                </Form.Field>
                <Form.Field>
                    <div className={cx.grid}>
                        <label><Paragraph.LH24>Юридическое лицо</Paragraph.LH24></label>
                        <Controller
                            as={Select}
                            name='agent_id'
                            options={Forms.makeOptions(agents, { value: 'id', label: 'name' })}
                            control={form.control}
                            defaultValue={defaultAgentId}
                            rules={{ required: 'Обязательно для ввода' }}
                            error={getError('agent_id')}
                        />
                    </div>
                </Form.Field>
            </Form>
        )
    }

    return (
        <Popup onDismiss={form.isMutating ? null : onDismiss}>
            <Popup.Header>{form.title}</Popup.Header>
            <Popup.Content>
                {form.isLoading ? <SplashScreen/> : renderForm()}
            </Popup.Content>
            <Popup.Footer
                majorButton={
                    <Button
                        form={FORM_ID}
                        type='submit'
                        disabled={form.isMutating || form.isLoading || isArchived}
                        loading={form.isCreating || form.isUpdating}
                    >
                        {form.submitLabel}
                    </Button>
                }
                onDismissButton={
                    <Button
                        disabled={form.isMutating || form.isLoading}
                        styling='hollow-border'
                        onClick={onDismiss}
                    >
                        Отмена
                    </Button>
                }
                minorButton={
                    integration &&
                    <Button
                        disabled={form.isMutating || form.isLoading || isArchived}
                        loading={form.isDeleting}
                        className={cx.delete}
                        styling='hollow-border'
                        icon={<Icons24.IconTrash/>}
                        onClick={form.onDelete}
                    >
                        Удалить
                    </Button>
                }
            />
        </Popup>
    )
}

IntegrationForm.propTypes = {
    onDismiss: PropTypes.func,
    integration: PropTypes.object,
    integrations: PropTypes.object,
    agents: PropTypes.array,
    groups: PropTypes.array,
    getError: PropTypes.func,
    form: PropTypes.object,
    hasStartDateField: PropTypes.bool,
    needConnection: PropTypes.bool,
    isArchived: PropTypes.bool,
    Services: PropTypes.any,
    defaultGroupId: PropTypes.object,
    defaultAgentId: PropTypes.object,
}
