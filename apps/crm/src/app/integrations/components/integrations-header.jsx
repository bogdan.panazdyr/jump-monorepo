import React, { Fragment, useState } from 'react';

import { Icons24, MobileHeader } from '@justlook/uikit';
import { useIfMediaScreen } from '@justlook/uikit/lib/hooks';

import { useMenu } from 'components/menu';

import { IntegrationModal } from './integrations-modal';

export const IntegrationsHeader = () => {
    const [ isShowIntegrationModal, setIsShowIntegrationModal ] = useState(false);
    const { onExpandedMobile } = useMenu();
    const isMobile = useIfMediaScreen();

    return (
        <Fragment>
            <MobileHeader header='Интеграции'>
                <MobileHeader.Button icon={<Icons24.IconPlus/>} onClick={() => setIsShowIntegrationModal(true)}/>

                { isMobile && <MobileHeader.Button icon={<Icons24.IconMenu/>} onClick={onExpandedMobile}/> }
            </MobileHeader>
            {isShowIntegrationModal &&
                <IntegrationModal onDismiss={() => setIsShowIntegrationModal(false)}/>
            }
        </Fragment>
    )
}
