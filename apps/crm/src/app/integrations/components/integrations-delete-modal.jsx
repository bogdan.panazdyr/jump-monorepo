import React  from 'react';
import PropTypes from 'prop-types';

import { Popup, Button } from '@justlook/uikit';

import { useGetIntegrationById, useDeleteMutator } from 'domains/integrations';

export const IntegrationDelete = ({ onDismiss, id }) => {
    const { onDelete, isDeleting } = useDeleteMutator(id, onDismiss)

    const getIntegrationById = useGetIntegrationById();

    return (
        <Popup>
            <Popup.Header>Удаление интеграции</Popup.Header>
            <Popup.Content>
                {`Удалить интеграцию ${getIntegrationById(id)?.name}?`}
            </Popup.Content>
            <Popup.Footer
                majorButton={
                    <Button
                        loading={isDeleting}
                        disabled={isDeleting}
                        onClick={onDelete}
                    >
                        Удалить
                    </Button>
                }
                onDismissButton={
                    <Button
                        disabled={isDeleting}
                        styling='hollow-border'
                        onClick={onDismiss}
                    >
                        Отменить
                    </Button>
                }
            />
        </Popup>
    )
}

IntegrationDelete.propTypes = {
    onDismiss: PropTypes.func,
    id: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.string
    ]),
}
