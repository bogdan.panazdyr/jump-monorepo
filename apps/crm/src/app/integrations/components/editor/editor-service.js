import { useState } from 'react';
import { parseJSON } from 'date-fns';
import { useForm } from 'react-hook-form';
import { useSelector } from 'react-redux';

import { Forms } from '@justlook/core';

import { getDictionaries } from 'app/init/store/selectors';
import {
    useDictionaries,
    useIntegrationItem,
    useCreateMutator,
    useUpdateMutator,
    useDeleteMutator,
    useGetIntegrationById,
    servicesIdNeedConfirmation,
} from 'domains/integrations';
import {
    BoltForm,
    CitymobilForm,
    DidiForm,
    GettForm,
    Wheely,
    YandexForm
} from 'app/integrations/components/services-forms';

const services = {
    8: YandexForm,
    10: YandexForm,
    11: GettForm,
    21: CitymobilForm,
    23: GettForm,
    24: Wheely,
    25: BoltForm,
    29: YandexForm,
    30: GettForm,
    31: DidiForm,
    32: GettForm,
}

const preparingDataForSubmit = (values) => {
    return {
        ...values,
        aggregator_id: values.aggregator_id.value,
        default_group_id: values.default_group_id.value,
        agent_id: values.agent_id.value,
    }
}

export const useInitialForm = (integrationId, onDismiss) => {
    const [ responseAtMutation, setResponseAtMutation ] = useState(null);

    const { isLoading: isLoadingDict, agents, groups, defaultAgent, defaultGroup } = useDictionaries();

    const getIntegrationById = useGetIntegrationById();

    const { integration, isLoading } = useIntegrationItem(integrationId, getIntegrationById(integrationId));

    const { integrations } = useSelector(getDictionaries);

    const defaultAggregator = integrations?.dictionaries[0]?.aggregators?.options[0];
    const defaultAgentId = integration ? Forms.itemToOption(integration?.agent) : defaultAgent;
    const defaultGroupId = integration ? Forms.itemToOption(integration?.default_group) : defaultGroup;

    const defaultValues = {
        ...integration,
        integration_start_date: parseJSON(integration?.integration_start_date || new Date()),
        aggregator_id: integration
            ? Forms.itemToOption(integration?.aggregator)
            : Forms.itemToOption(defaultAggregator, { value: 'value', label: 'title' }),
    }

    const { handleSubmit, errors, control, setError, watch, reset } = useForm({ defaultValues });

    const onResetCacheDefaultValues = (values) => reset({
        ...values,
        integration_start_date: parseJSON(values?.integration_start_date),
        default_group_id: Forms.itemToOption(values?.default_group, { value: 'id', label: 'name' }),
        agent_id: Forms.itemToOption(values?.agent, { value: 'id', label: 'name' }),
        aggregator_id: Forms.itemToOption(values?.aggregator, { value: 'id', label: 'name' })
    })

    const watchAggregatorId = watch('aggregator_id');

    const currentAggregatorId = responseAtMutation?.aggregator?.id || watchAggregatorId?.value;

    const needConfirm = servicesIdNeedConfirmation.some(item => currentAggregatorId === item);
    const needConnection = integrationId && needConfirm && getIntegrationById(integrationId)?.is_connected !== true;

    const title = integrationId ? 'Редактирование интеграции' : 'Новая интеграция';
    const submitLabel = integrationId
        ? needConnection ? 'Подтвердить' : 'Сохранить'
        : needConfirm ? 'Добавить и подтвердить' : 'Добавить';

    const hasStartDateField = !([31, 8, 21, 29].some(item => currentAggregatorId === item));

    const isArchived = !services[currentAggregatorId];
    const Services = services[currentAggregatorId];

    const { onCreate, isCreating, needConfirmationAfterCreation } = useCreateMutator(onDismiss, setResponseAtMutation, setError);
    const { onUpdate, isUpdating, needConfirmationAfterUpdate } = useUpdateMutator(integrationId, onDismiss, setResponseAtMutation, setError);
    const { onDelete, isDeleting } = useDeleteMutator(integrationId, onDismiss);

    const getError = field => errors?.[field]?.message;

    const onSubmit = handleSubmit((values) => integration
        ? onUpdate(preparingDataForSubmit(values))
        : onCreate(preparingDataForSubmit(values)));

    return {
        integration, integrations, agents,
        groups, getError, defaultAggregator,
        needConfirmation: needConfirmationAfterCreation || needConfirmationAfterUpdate,
        hasStartDateField,
        needConnection,
        isArchived,
        Services,
        responseAtMutation,
        onResetCacheDefaultValues,
        defaultGroupId,
        defaultAgentId,
        form: {
            isLoading: isLoadingDict || isLoading,
            isMutating: isCreating || isUpdating || isDeleting,
            isCreating,
            isUpdating,
            isDeleting,
            onSubmit,
            onDelete,
            title,
            submitLabel,
            handleSubmit,
            errors,
            setError,
            control,
            watch,
        },
    };
}
