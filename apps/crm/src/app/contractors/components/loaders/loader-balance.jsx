import React from 'react';
import PropTypes from 'prop-types';
import ContentLoader from 'react-content-loader';

export const LoaderBalance = ({ props }) => {
    return (
        <ContentLoader
            speed={1}
            width={250}
            height={24}
            viewBox='0 0 250 24'
            backgroundColor='#E7EAEE'
            foregroundColor='#DBDDE6'
            style={{ display: 'block' }}
            {...props}
        >
            <rect x='0' y='0' rx='4' ry='4' width='250' height='24' />
        </ContentLoader>
    )
}

LoaderBalance.propTypes = {
    props: PropTypes.any
}
