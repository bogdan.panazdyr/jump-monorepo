import React from 'react';
import PropTypes from 'prop-types';

import { Toggle } from '@justlook/uikit';

import { Box, snackBar } from 'components';
import { useContractorPermissions, useUpdateContractorPermission } from 'domains/contractors';

import { LoaderWithdrawalAccess } from './loaders/loader-withdrawal-access';

const WITHDRAW_PERMISSION = 1;

export const WithdrawalAccess = ({ contractorId, readOnly }) => {
    const { isEnable, isLoading } = useContractorPermissions(contractorId, WITHDRAW_PERMISSION);

    const { onUpdatePermission, isUpdatingPermission } = useUpdateContractorPermission(contractorId, snackBar);

    const handleOnChange = (value) => {
        onUpdatePermission({ permissionId: WITHDRAW_PERMISSION, value })
    }

    if(isLoading) return <LoaderWithdrawalAccess/>

    return (
        <Box mt={8}>
            <Toggle
                isLoading={isUpdatingPermission}
                label='Разрешить вывод средств'
                checked={isEnable}
                onChange={handleOnChange}
                disabled={readOnly}
            />
        </Box>
    )
}

WithdrawalAccess.propTypes = {
    contractorId: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]),
    readOnly: PropTypes.bool,
}
