import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { useNavigate } from 'react-router-dom';
import { Hooks } from '@justlook/core';
import { Popup, Button } from '@justlook/uikit';

import { ContractorActions } from 'app/contractors/store/actions';

import cx from 'app/contractors/components/modal/contractor-modal.module.scss';

export const DeleteModal = ({ renderProps, id, name }) => {
    const { isShow, onCloseForm, onOpenForm } = Hooks.useModalState(false);
    const navigate = useNavigate();

    const onDeleteContractor = Hooks.useActionToDispatch(ContractorActions.deleteContractor);

    const deleteContractor = () => {
        onDeleteContractor(id);
        navigate('/contractors');
    };

    return (
        <Fragment>
            {renderProps(onOpenForm)}
            {isShow &&
                <Popup onDismiss={onCloseForm} className={cx.popup}>
                    <Popup.Header>{ name }</Popup.Header>
                    <Popup.Content>
                        Удалить исполнителя?
                    </Popup.Content>
                    <Popup.Footer
                        majorButton={<Button onClick={deleteContractor}>Удалить</Button>}
                        onDismissButton={<Button styling='hollow-border' onClick={onCloseForm}>Отмена</Button>}
                    />
                </Popup>
            }
        </Fragment>
    )
}

DeleteModal.propTypes = {
    renderProps: PropTypes.func,
    id: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ]),
    name: PropTypes.string,
}
