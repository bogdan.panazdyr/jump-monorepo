import React, { useEffect, useMemo, useState } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import classnames from 'classnames';
import { useForm, Controller } from 'react-hook-form';

import { Forms, Hooks } from '@justlook/core';
import { Popup, Button, Input, Select, Form, Checkbox, ParagraphV2 as Paragraph } from '@justlook/uikit';

import { addServerErrors } from 'utils/add-server-errors';
import { Notice } from 'components/notice';
import { ContractorSelectors } from 'app/contractors/store/selectors';
import { ContractorActions } from 'app/contractors/store/actions';
import { usePayTaxes } from 'domains/contractors';
import { useAgentsList } from 'domains/agents';

import cx from './contractor-modal.module.scss';

const FORM_ID = 'edit_contractor_form';

const RefundInfo = () => {
    return (
        <>
            <div />
            <div className={cx.refund_info}>
                <svg width="16" height="16" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill="#F59F00" fillRule="evenodd" clipRule="evenodd" d="M8 0a8 8 0 110 16A8 8 0 018 0zm1 11a1 1 0 10-2 0 1 1 0 002 0zm0-3a1 1 0 11-2 0V5a1 1 0 012 0v3z"/>
                </svg>
                <span>Текущие налоговые удержания будут зачислены на баланс исполнителя</span>
            </div>
        </>
    )
}

export const EditContractorModal = ({ onDismiss, onSuccess }) => {
    const { agents: agentsList } = useAgentsList();

    const { legal_form } = useSelector(ContractorSelectors.getContractorsDict);
    const { has_exclusive_access } = useSelector(ContractorSelectors.getContractorAbilities);
    const { isSubmitting, isSubmitted } = useSelector(ContractorSelectors.getFormState);
    const { isVerified, isSelfEmployed } = useSelector(ContractorSelectors.getLegalFormData);
    const contractor = useSelector(ContractorSelectors.getContractor);
    const legalFormId = useSelector(ContractorSelectors.getLegalFormId);
    const errorFields = useSelector(ContractorSelectors.getContractorInvalidFields);

    const onUpdateContractor = Hooks.useActionToDispatch(ContractorActions.updateContractor);
    const onClearsError = Hooks.useActionToDispatch(ContractorActions.clearsError);

    const [isShowTaxesRefundInfo, setIsShowTaxesRefundInfo] = useState(null);

    const {
        isPaymentOfTaxes,
        showCompanyAgreesPayTaxes,
        setShowCompanyAgreesPayTaxes,
        paymentOfTaxesDefault,
    } = usePayTaxes();

    const { handleSubmit, control, errors, setError, getValues } = useForm({
        defaultValues: {
            phone: contractor?.phone ?? '',
            last_name: contractor?.last_name ?? '',
            first_name: contractor?.first_name ?? '',
            middle_name: contractor?.middle_name ?? '',
            legal_form_id: Forms.itemToOption(contractor?.legal_form, {
                value: 'id',
                label: 'title',
            }),
            company_agrees_pay_taxes: contractor?.legal_form?.id !== 2 ? paymentOfTaxesDefault : contractor?.tax?.has_company_agrees_pay_taxes,
            inn: contractor?.inn ?? '',
            agent_id: Forms.itemToOption(contractor?.agent, {
                value: 'id',
                label: 'name',
            }),
        }
    });

    const existsLabel = 'Исполнитель работает в нескольких компаниях, поэтому изменить правовую информацию может только сам исполнитель через приложение «Jump.Работа».';

    const [ legalForm, setLegalForm ] = useState(legalFormId);


    useEffect(() => {
        if (isSubmitted) {
            onSuccess(contractor);
        }

        setShowCompanyAgreesPayTaxes(getValues('legal_form_id'));
    }, [isSubmitted, onSuccess, getValues, setShowCompanyAgreesPayTaxes, contractor]);

    useMemo(() => {
        addServerErrors(errorFields, setError)
    }, [ errorFields, setError ]);

    const checkboxFieldCls = classnames(cx.grid, { [cx.hidden]: !isPaymentOfTaxes || !showCompanyAgreesPayTaxes })

    const prepareValues = (values) => {
        return {
            ...values,
            legal_form_id: Number(values.legal_form_id.value),
            agent_id: values.agent_id.value,
        }
    }

    const handleOnDismiss = () => {
        onClearsError();
        onDismiss();
    }

    const onShowTaxesRefundInfo = (isAgreesPayTaxes) => {
        const hasAgrees = contractor?.tax?.has_company_agrees_pay_taxes;
        const needShow = hasAgrees && !isAgreesPayTaxes && contractor?.id;

        setIsShowTaxesRefundInfo(needShow);
    }

    const onSubmit = (values) => onUpdateContractor(contractor.id, prepareValues(values));

    return (
        <Popup onDismiss={handleOnDismiss} className={cx.popup}>
            <Popup.Header>Редактирование исполнителя</Popup.Header>
            <Popup.Content>
                <Form onSubmit={handleSubmit(onSubmit)} id={FORM_ID}>
                    <Form.Field>
                        <Controller
                            as={Input}
                            label='Номер телефона'
                            name='phone'
                            type='phone'
                            mask='+79999999999'
                            control={control}
                            className={cx.grid}
                            disabled={!has_exclusive_access}
                            rules={{ required: 'Обязательно для ввода' }}
                            error={errors.phone && errors.phone.message}
                        />
                    </Form.Field>
                    <Form.Field>
                        <Controller
                            as={Input}
                            label='Фамилия'
                            name='last_name'
                            control={control}
                            className={cx.grid}
                            rules={{ required: 'Обязательно для ввода' }}
                            error={errors.last_name && errors.last_name.message}
                        />
                    </Form.Field>
                    <Form.Field>
                        <Controller
                            as={Input}
                            label='Имя'
                            name='first_name'
                            control={control}
                            className={cx.grid}
                            rules={{ required: 'Обязательно для ввода' }}
                            error={errors.first_name && errors.first_name.message}
                        />
                    </Form.Field>
                    <Form.Field>
                        <Controller
                            as={Input}
                            label='Отчество'
                            name='middle_name'
                            control={control}
                            className={cx.grid}
                            error={errors.middle_name && errors.middle_name.message}
                        />
                    </Form.Field>
                    <Form.Field>
                        <div className={cx.grid}>
                            <label><Paragraph.LH24>Правовой статус</Paragraph.LH24></label>
                            <Controller
                                name='legal_form_id'
                                control={control}
                                rules={has_exclusive_access && { required: 'Обязательно для ввода' }}
                                render={({ onChange, ...rest }) => (
                                    <Select
                                        options={
                                            Forms.makeOptions(
                                                legal_form.options,
                                                { value: 'value', label: 'title' }
                                            )
                                        }
                                        onChange={(option) => {
                                            onChange(option)
                                            setLegalForm(option.value)
                                            setShowCompanyAgreesPayTaxes(option);
                                        }}
                                        disabled={!has_exclusive_access}
                                        error={errors.legal_form_id && errors.legal_form_id.message}
                                        {...rest}
                                    />
                                )}
                            />
                        </div>
                        <div className={checkboxFieldCls}>
                            <label/>
                            <Controller
                                name='company_agrees_pay_taxes'
                                control={control}
                                error={errors?.company_agrees_pay_taxes?.message}
                                render={({ value, onChange, ...rest }) => (
                                    <Checkbox
                                        label='Уплачивать налоги за исполнителя'
                                        checked={value}
                                        onChange={(checked) => {
                                            onShowTaxesRefundInfo(checked);
                                            onChange(checked);
                                        }}
                                        {...rest}
                                    />
                                )}
                            />

                        </div>
                        <div className={checkboxFieldCls} style={{marginTop: '-0.5rem'}}>
                            { isShowTaxesRefundInfo && <RefundInfo />}
                        </div>
                    </Form.Field>
                    <Form.Field>
                        <Controller
                            name='inn'
                            control={control}
                            render={({ value, ...rest }) => {
                                const isDisable = has_exclusive_access && isSelfEmployed && isVerified && legalForm === 2;

                                return (
                                    <Input
                                        label='ИНН'
                                        disabled={isDisable || !has_exclusive_access}
                                        value={(legalForm === 2 && isSelfEmployed && isVerified) ? contractor?.inn : value}
                                        className={cx.grid}
                                        error={errors.inn && errors.inn.message}
                                        {...rest}
                                    />
                                )
                            }}
                        />
                    </Form.Field>
                    <Form.Field>
                        <div className={cx.grid}>
                            <label><Paragraph.LH24>Юрлицо</Paragraph.LH24></label>
                            <Controller
                                as={Select}
                                name='agent_id'
                                options={Forms.makeOptions(agentsList, { value: 'id', label: 'name' })}
                                control={control}
                                rules={{ required: 'Обязательно для ввода' }}
                                error={errors.agent_id && errors.agent_id.message}
                            />
                        </div>
                    </Form.Field>
                    {!has_exclusive_access && <Notice label={existsLabel}/>}
                </Form>
            </Popup.Content>
            <Popup.Footer
                majorButton={<Button form={FORM_ID} type='submit' loading={isSubmitting}>Сохранить</Button>}
                onDismissButton={
                    <Button styling='hollow-border' onClick={handleOnDismiss} disabled={isSubmitting}>
                        Отмена
                    </Button>
                }
            />
        </Popup>
    )
}

EditContractorModal.propTypes = {
    onDismiss: PropTypes.func,
    onSuccess: PropTypes.func,
}
