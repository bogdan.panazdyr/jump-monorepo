import React, { Fragment, useEffect, useState, useMemo } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { useForm, Controller } from 'react-hook-form';

import { Forms, Hooks } from '@justlook/core';
import { Popup, Button, Form, Input, Select, Checkbox, ParagraphV2 as Paragraph } from '@justlook/uikit';

import { Notice } from 'components/notice';
import { addServerErrors } from 'utils/add-server-errors';
import { ContractorActions } from 'app/contractors/store/actions';
import { ContractorSelectors } from 'app/contractors/store/selectors';
import { SearchPhone } from 'app/contractors/components/search-phone';
import { usePayTaxes } from 'domains/contractors';
import { useAgentsMy } from 'domains/agents';

import cx from './contractor-modal.module.scss';

const FORM_ID = 'create_contractor';

export const ContractorModal = ({ renderControl }) => {
    const { isShow, onOpenForm, onCloseForm } = Hooks.useModalState();
    const { agents: agentsList } = useAgentsMy();

    const { handleSubmit, control, errors, setError, getValues } = useForm();
    const [ executor, setExecutor ] = useState({});
    const [ successSearch, setSuccessSearch ] = useState(false);

    const { item, abilities } = executor;
    const { isPaymentOfTaxes, showCompanyAgreesPayTaxes, setShowCompanyAgreesPayTaxes } = usePayTaxes();

    const { isSubmitting, isSubmitted } = useSelector(ContractorSelectors.getFormState);
    const { legal_form } = useSelector(ContractorSelectors.getContractorsDict);
    const errorFields = useSelector(ContractorSelectors.getContractorInvalidFields);

    const onCreateContractor = Hooks.useActionToDispatch(ContractorActions.createContractor);
    const onAttachContractor = Hooks.useActionToDispatch(ContractorActions.attachContactor);

    useEffect(() => {
        if (isSubmitted) {
            onCloseForm();
        }

        setShowCompanyAgreesPayTaxes(getValues('legal_form_id'));
    }, [isSubmitted, onCloseForm, getValues, setShowCompanyAgreesPayTaxes]);

    useMemo(() => { addServerErrors(errorFields, setError) }, [errorFields, setError]);

    const existsLabel = 'Исполнитель работает в нескольких компаниях, поэтому изменить правовую информацию может только сам исполнитель через приложение «Jump.Работа»';

    const setOptionsAgent = Forms.makeOptions(agentsList, { value: 'id', label: 'name' });
    const setValueAgent = () => {
        if(item?.agent) {
            return Forms.itemToOption(item.agent, {label: 'name', value: 'id'})
        }

        if(!item?.agent && setOptionsAgent.length === 1) {
            return setOptionsAgent[0]
        }
    }

    const prepareValues = (values) => {
        return {
            ...values,
            legal_form_id: values.legal_form_id.value,
            agent_id: values.agent_id.value,
        }
    }

    const onSubmit = (values) => abilities.can_create
        ? onCreateContractor(prepareValues(values))
        : onAttachContractor(item.id, prepareValues(values))

    return (
        <Fragment>
            {renderControl(onOpenForm)}
            {isShow &&
            <Popup onDismiss={onCloseForm} className={cx.popup}>
                <Popup.Header>Новый исполнитель</Popup.Header>
                <Popup.Content>
                    <Form onSubmit={handleSubmit(onSubmit)} id={FORM_ID}>
                        <Controller
                            as={SearchPhone}
                            control={control}
                            name='phone'
                            successSearch={setSuccessSearch}
                            actionSearchContactor={setExecutor}
                        />

                        {successSearch &&
                        <Fragment>
                            <Form.Field>
                                <Controller
                                    as={Input}
                                    name='last_name'
                                    label='Фамилия'
                                    defaultValue={item && item.last_name}
                                    control={control}
                                    className={cx.grid}
                                    rules={{ required: 'Обязательно для ввода' }}
                                    error={errors.last_name && errors.last_name.message}
                                />
                            </Form.Field>
                            <Form.Field>
                                <Controller
                                    as={Input}
                                    name='first_name'
                                    label='Имя'
                                    defaultValue={item && item.first_name}
                                    control={control}
                                    className={cx.grid}
                                    rules={{ required: 'Обязательно для ввода' }}
                                    error={errors.first_name && errors.first_name.message}
                                />
                            </Form.Field>
                            <Form.Field>
                                <Controller
                                    as={Input}
                                    name='middle_name'
                                    label='Отчество'
                                    defaultValue={item && item.middle_name}
                                    control={control}
                                    className={cx.grid}
                                />
                            </Form.Field>
                            <Form.Field>
                                <div className={cx.grid}>
                                    <label><Paragraph.LH24>Правовой статус</Paragraph.LH24></label>
                                    <Controller
                                        name='legal_form_id'
                                        control={control}
                                        defaultValue={item && Forms.itemToOption(
                                            item.legal_form,
                                            { label: 'title', value: 'id' }
                                        )}
                                        disabled={abilities.can_attach}
                                        rules={{ required: 'Обязательно для ввода' }}
                                        render={({ onChange, ...rest }) => (
                                            <Select
                                                options={
                                                    Forms.makeOptions(
                                                        legal_form.options,
                                                        { value: 'value', label: 'title' }
                                                    )
                                                }
                                                onChange={(option) => {
                                                    onChange(option);
                                                    setShowCompanyAgreesPayTaxes(option);
                                                }}
                                                disabled={abilities.can_attach}
                                                error={errors.legal_form_id && errors.legal_form_id.message}
                                                {...rest}
                                            />
                                        )}
                                    />
                                </div>
                                {isPaymentOfTaxes && showCompanyAgreesPayTaxes && (
                                    <div className={cx.grid}>
                                        <label/>
                                        <Controller
                                            name='company_agrees_pay_taxes'
                                            defaultValue={item ? item?.company_agrees_pay_taxes : true}
                                            control={control}
                                            disabled={abilities.can_attach}
                                            error={errors?.company_agrees_pay_taxes?.message}
                                            render={({ value, ...rest }) => (
                                                <Checkbox
                                                    label='Уплачивать налоги за исполнителя'
                                                    checked={value}
                                                    {...rest}
                                                />
                                            )}
                                        />
                                    </div>)
                                }
                            </Form.Field>
                            <Form.Field>
                                <Controller
                                    as={Input}
                                    name='inn'
                                    label='ИНН'
                                    defaultValue={item && item.inn}
                                    control={control}
                                    className={cx.grid}
                                    disabled={abilities.can_attach}
                                    error={errors.inn && errors.inn.message}
                                />
                            </Form.Field>
                            <Form.Field>
                                <div className={cx.grid}>
                                    <label><Paragraph.LH24>Юрлицо</Paragraph.LH24></label>
                                    <Controller
                                        as={Select}
                                        name='agent_id'
                                        options={setOptionsAgent}
                                        defaultValue={setValueAgent()}
                                        control={control}
                                        rules={{ required: 'Обязательно для ввода' }}
                                        error={errors.agent_id && errors.agent_id.message}
                                    />
                                </div>
                            </Form.Field>
                            {abilities.can_attach && <Notice label={existsLabel}/>}
                        </Fragment>
                        }
                    </Form>
                </Popup.Content>
                <Popup.Footer
                    majorButton={
                        <Button form={FORM_ID} type='submit' loading={isSubmitting} disabled={!successSearch}>
                            Добавить
                        </Button>
                    }
                    onDismissButton={
                        <Button styling='hollow-border' onClick={onCloseForm} disabled={isSubmitting}>
                            Отмена
                        </Button>
                    }
                />
            </Popup>
            }
        </Fragment>
    )
}

ContractorModal.propTypes =
{
    renderControl: PropTypes.func,
}
