import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Input, Icons24, Form } from '@justlook/uikit';

import Api from 'api/client';

import cx from 'app/contractors/components/search-phone.module.scss';

export const SearchPhone = ({ actionSearchContactor, successSearch, name, value, onChange }) => {
    const [isRequest, setIsRequest] = useState(false);
    const [error, setError] = useState(null);

    useEffect(() => successSearch(false), [successSearch]);

    const handleOnChange = (phone) => {

        if(!phone.includes('_') && phone.length) {
            setIsRequest(true);
            Api.contractors.searchContractor({ phone, include: 'abilities' })
                .then((response) => {
                    setIsRequest(false);

                    const { abilities } = response;

                    if(abilities.has_attached) {
                        setError('Исполнитель уже существует')
                    } else {
                        actionSearchContactor(response);
                        successSearch(true)
                        setError('')
                    }
                })
        }

        if(phone.includes('_')) successSearch(false);

        onChange(phone);
    }

    return (
        <Form.Field className={cx['search-phone']}>
            <Input
                label='Номер телефона'
                type='phone'
                mask='+79999999999'
                name={name}
                value={value}
                onChange={handleOnChange}
                className={cx.grid}
                error={error}
            />
            {isRequest && <Icons24.IconLoader/>}
        </Form.Field>
    )
}

SearchPhone.defaultProps = {
    actionSearchContactor: () => {}
}

SearchPhone.propTypes = {
    actionSearchContactor: PropTypes.func,
    successSearch: PropTypes.func,
    name: PropTypes.string,
    value: PropTypes.string,
    onChange: PropTypes.func,
}
