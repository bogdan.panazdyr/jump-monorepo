import React from 'react';
import PropTypes from 'prop-types';
import classes from 'classnames';

import { Icons16, ParagraphV2 as Paragraph } from '@justlook/uikit';

import { IconBar } from 'components';
import { useSelfEmployer } from 'domains/contractors/services';
import { nl2br } from 'utils/helpers';
import { useTestMode } from 'hooks/use-testing';

import { LoaderLegalForm } from './loaders/loader-legal-form';
import { SelfEmployerStatus } from './self-employer-status';

import cx from './legal-form.module.scss';

export const LegalForm = ({ contractor, canWrite }) => {
    const { isTestMode } = useTestMode();

    const contractorId = contractor?.id;
    const isSelfEmployed = contractor?.legal_form?.is_self_employed;

    const {
        onSelfEmployerSync,
        isSelfEmployerInfoLoading,
        isSelfEmployerSyncMutating,
        isConfirmed,
        canCheck,
        syncInProcess,
        messages
    } = useSelfEmployer(contractorId, isSelfEmployed);

    const isPending = (syncInProcess || isSelfEmployerSyncMutating) && isTestMode;
    const isShowStatusIcon = isSelfEmployed && !isSelfEmployerInfoLoading && !isPending && isTestMode;
    const allowCheckStatus = canCheck && !isSelfEmployerSyncMutating && isTestMode;

    const iconBarData = isConfirmed ? {
        label: 'Статус подтвержден.',
        icon: <Icons16.IconCheckCircle className={cx.verified}/>
    } : {
        label: nl2br(messages?.tooltip || ''),
        icon: <Icons16.IconInfoFill className={cx.icon}/>
    };

    const cls = classes({ [cx.warning]: !isConfirmed && isSelfEmployed });
    const clsIconBar = classes({ [cx.info]: !isConfirmed && isSelfEmployed });

    if (isSelfEmployerInfoLoading) {
        return <LoaderLegalForm/>
    }

    return (
        <div className={cx.base}>
            <div className={cx.content}>
                <Paragraph.LH24 className={cls}>{ contractor?.legal_form?.title }</Paragraph.LH24>

                {
                    isShowStatusIcon && (
                        <IconBar className={clsIconBar} positionBar='bottom-right' data={iconBarData}/>
                    )
                }

                {
                    allowCheckStatus && (
                        <button type='button' className={cx.check} onClick={() => onSelfEmployerSync(contractorId)}>
                            Проверить
                        </button>
                    )
                }

                {
                    isPending && (
                        <Paragraph.LH24 className={cx.checking}>
                            Проверяется
                        </Paragraph.LH24>
                    )
                }
            </div>

            { !isTestMode && <SelfEmployerStatus contractor={contractor} canWrite={canWrite} /> }
        </div>
    )
}

LegalForm.propTypes = {
    contractor: PropTypes.object.isRequired,
    canWrite: PropTypes.bool.isRequired,
}
