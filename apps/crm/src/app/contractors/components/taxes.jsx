import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import classes from 'classnames';
import ContentLoader from 'react-content-loader';
import { Description } from '@justlook/uikit';

import { useMoney } from 'hooks/use-money';

import { useContractorTaxes } from 'domains/contractors';

import cx from './taxes.module.scss';

export const LoaderTaxes = () => {
    return (
        <ContentLoader
            speed={1}
            width={250}
            height={16}
            viewBox='0 0 250 16'
            backgroundColor='#E7EAEE'
            foregroundColor='#DBDDE6'
            style={{ display: 'block' }}
        >
            <rect x='0' y='0' rx='4' ry='4' width='250' height='16' />
        </ContentLoader>
    )
}

export const Taxes = ({ id }) => {
    const { currency, format } = useMoney();

    const { data, isLoading } = useContractorTaxes(id);

    const clsTaxes = classes({ [cx.warning]: data?.message_type === 'warning' });

    if(isLoading) { return <LoaderTaxes/> }

    return (
        <Fragment>
            {
                data?.hold !== null &&
                <Description.LH16 className={cx.base}>
                    Налогов удержано
                    <span>
                        {format(data.hold, 2)} {currency}
                    </span>
                </Description.LH16>
            }
            {
                data?.message &&
                <Description.LH16 className={clsTaxes}>
                    { data.message }
                </Description.LH16>
            }
        </Fragment>
    )
}

Taxes.propTypes = {
    id: PropTypes.any.isRequired,
}
