import React from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import { Hooks } from '@justlook/core';
import { Button, Icons48, ParagraphV2 as Paragraph, Popup } from '@justlook/uikit';

import { ContractorSelectors } from 'app/contractors/store/selectors';
import { ContractorActions } from 'app/contractors/store/actions';
import { getFio } from 'utils/person';

import cx from './steps.module.scss';

function Success({ id, onDismiss }) {
    const user = useSelector(ContractorSelectors.getContractor);
    const onFetchContractor = Hooks.useActionToDispatch(ContractorActions.fetchContractor);

    const onFinish = () => {
        onDismiss();
        onFetchContractor(id, { include: 'abilities' });
    }

    return (
        <Popup>
            <Popup.Header description='Шаг 3 из 3'>
                Подключение самозанятого
            </Popup.Header>

            <Popup.Content>
                <div className={cx.resultSubmit}>
                    <Icons48.IconCheck/>
                    <Paragraph.LH24 style={{textAlign:'center'}}>
                        { getFio(user) } <br/> успешно подтвердил статус самозанятого
                    </Paragraph.LH24>
                </div>
            </Popup.Content>

            <Popup.Footer
                majorButton={<Button onClick={onFinish}>Готово</Button>}
            />
        </Popup>
    );
}

Success.propTypes = {
    id: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ]),
    onDismiss: PropTypes.func,
}

export default Success;
