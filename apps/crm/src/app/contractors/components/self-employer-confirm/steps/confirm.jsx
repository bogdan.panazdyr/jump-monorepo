import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Button, ParagraphV2 as Paragraph, Popup, Link } from '@justlook/uikit';
import { useSelector } from 'react-redux';
import { sleep } from 'utils/helpers';
import { getFio } from 'utils/person';

import {ContractorSelectors} from 'app/contractors/store/selectors';

import Api from 'api/client';

import { FAILED, SUCCESS } from './index';

const nalogUrl = 'https://lknpd.nalog.ru/';
const iOSUrl = 'https://itunes.apple.com/ru/app/%D0%BC%D0%BE%D0%B9-%D0%BD%D0%B0%D0%BB%D0%BE%D0%B3/id1437518854?l=en&mt=8';
const androidUrl = 'https://play.google.com/store/apps/details?id=com.gnivts.selfemployed&hl=ru';
const partnerUrl = 'https://lknpd.nalog.ru/settings/partners';

const nalogLink = <Link to={nalogUrl} target='_blank'>личный кабинет</Link>
const partnerLink = <Link to={partnerUrl} target='_blank'>Партнёры</Link>
const androidLink = <Link to={androidUrl} target='_blank'>Android</Link>
const iOSLink = <Link to={iOSUrl} target='_blank'>iOS</Link>


function Confirm({id, onGoStep, onDismiss}) {
    const user = useSelector(ContractorSelectors.getContractor)
    const [ isFetch, setIsFetch ] = useState(false);
    const [ isFirstAttempt, setFirstAttempt ] = useState(true);

    const label = isFirstAttempt ? 'Подтвердить' : 'Повторить';

    const onConfirm = async () => {
        setIsFetch(true);
        setFirstAttempt(false);

        const check = await Api.contractors.fetchTaxCheck(id).catch(e => e);

        if (!check?.data?.id) {
            setIsFetch(false);
            onGoStep(FAILED, check);
            return;
        }

        let status = {};
        let attempts = 0;
        do {
            status = await Api.contractors.fetchTaxRequest(id, check?.data?.id).catch(e => e);
            attempts += 1;
            await sleep(2000);
        } while (status?.data?.status === 'processing' && attempts < 30);

        setIsFetch(false);

        if (status?.data?.status?.value === 'COMPLETED') {
            onGoStep(SUCCESS);
        } else if (status?.data?.status?.value === 'FAILED') {
            onGoStep(FAILED, status);
        } else if (status?.data?.status?.value !== 'IN_PROGRESS') {
            onGoStep(FAILED, status);
        }
    }

    return (
        <Popup onDismiss={onDismiss}>
            <Popup.Header description='Шаг 2 из 3'>
                Подключение самозанятого
            </Popup.Header>

            <Popup.Content>
                <Paragraph.LH24>
                    1. Откройте { nalogLink} или приложение «Мой налог» { androidLink } или { iOSLink },
                    исполнителя: <br/> { getFio(user) } ({user.phone}, ИНН: {user.inn})
                </Paragraph.LH24>
                <Paragraph.LH24>
                    2. Перейдите на экран «Настройки > { partnerLink }» и примите приглашение от Jump.Работа
                </Paragraph.LH24>
                <Paragraph.LH24>
                    3. Вернитесь обратно и нажмите «{ label }»
                </Paragraph.LH24>
            </Popup.Content>

            <Popup.Footer
                majorButton={<Button loading={isFetch} disabled={isFetch} onClick={onConfirm}>{label}</Button>}
            />
        </Popup>
    )
}

Confirm.propTypes = {
    id: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ]),
    onGoStep: PropTypes.func,
    onDismiss: PropTypes.func,
}

export default Confirm;
