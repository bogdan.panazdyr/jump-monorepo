import React from 'react';
import PropTypes from 'prop-types';
import { Button, Icons48, ParagraphV2 as Paragraph, Popup } from '@justlook/uikit';
import { errorToString } from 'utils/add-server-errors';

import { BIND } from './index';

import cx from './steps.module.scss';

function getErrorText (error) {
    const defaultText = 'Внутренняя ошибка сервера. Повторите позже.';

    if (error instanceof Error) {
        return errorToString(error, defaultText);
    }

    return error?.data?.status?.title || defaultText;
}

function Failed({ onGoStep, payload, onDismiss }) {
    const error = getErrorText(payload) ;

    return (
        <Popup onDismiss={onDismiss}>
            <Popup.Header description='Ошибка'>
                Подключение самозанятого
            </Popup.Header>

            <Popup.Content>
                <div className={cx.resultSubmit}>
                    <Icons48.IconCross/>
                    <Paragraph.LH24>{ error }</Paragraph.LH24>
                </div>
            </Popup.Content>

            <Popup.Footer
                majorButton={<Button onClick={() => onGoStep(BIND)}>Попробовать снова</Button>}
            />
        </Popup>
    );
}

Failed.propTypes = {
    onGoStep: PropTypes.func,
    payload: PropTypes.any,
    onDismiss: PropTypes.func,
}

export default Failed;
