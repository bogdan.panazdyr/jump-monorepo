import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Button, ParagraphV2 as Paragraph, Popup } from '@justlook/uikit';
import { sleep } from 'utils/helpers';

import Api from 'api/client';

import { FAILED, CONFIRM } from './index';

function Bind ({id, onGoStep, onDismiss}) {
    const [isFetch, setIsFetch] = useState(false)

    const onBind = async () => {
        setIsFetch(true);

        const bind = await Api.contractors.fetchTaxBind(id).catch(e => e);

        if (!bind?.data?.id) {
            setIsFetch(false);
            onGoStep(FAILED, bind);
            return;
        }

        let status = {};
        let attempts = 0;
        do {
            status = await Api.contractors.fetchTaxRequest(id, bind?.data?.id).catch(e => e);
            attempts += 1;
            await sleep(2000);
        } while (status?.data?.status === 'processing' && attempts < 30);

        setIsFetch(false);

        if (status?.data?.binding_id) {
            onGoStep(CONFIRM);
        } else {
            onGoStep(FAILED, status);
        }
    };

    return (
        <Popup onDismiss={onDismiss}>
            <Popup.Header description='Шаг 1 из 3'>
                Подключение самозанятого
            </Popup.Header>

            <Popup.Content>
                <Paragraph.LH24>
                    Для подключения самозанятого потребуется доступ в личный кабинет или приложение «Мой налог» исполнителя.
                    Нажмите «Подключить» и следуйте дальнейшим указаниям.
                </Paragraph.LH24>
            </Popup.Content>

            <Popup.Footer
                majorButton={<Button loading={isFetch} disabled={isFetch} onClick={onBind}>Подключить</Button>}
                onDismissButton={<Button disabled={isFetch} styling='hollow-border' onClick={onDismiss}>Отмена</Button>}
            />
        </Popup>
    )
}

Bind.propTypes = {
    id: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ]),
    onGoStep: PropTypes.func,
    onDismiss: PropTypes.func,
}

export default Bind;
