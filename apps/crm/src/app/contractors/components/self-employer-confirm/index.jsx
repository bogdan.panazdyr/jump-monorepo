import React, { Fragment, useState } from 'react';
import PropTypes from 'prop-types';
import { Hooks } from '@justlook/core';

import {
    BIND, CONFIRM, SUCCESS, FAILED,
    Success, Failed, Confirm, Bind
} from './steps';

const steps = {
    [BIND]: Bind,
    [CONFIRM]: Confirm,
    [SUCCESS]: Success,
    [FAILED]: Failed,
}

export const SelfEmployerConfirm = ({ renderControl, id }) => {
    const { isShow, onOpenForm, onCloseForm } = Hooks.useModalState(false);

    const [ step, setStep ] = useState(BIND);
    const [ payload, setPayload ] = useState();

    const onDismiss = () => {
        setStep(BIND);
        setPayload(null);
        onCloseForm();
    }

    const onGoStep = (step, payload = null) => {
        setStep(step);
        setPayload(payload);
    }

    const handleOpenForm = () => {
        setStep(BIND);
        setPayload(null);
        onOpenForm();
    }

    const Step = steps[step];

    return (
        <Fragment>
            { renderControl(handleOpenForm) }

            { isShow && <Step id={id} onDismiss={onDismiss} onGoStep={onGoStep} payload={payload} /> }
        </Fragment>
    )
}

SelfEmployerConfirm.propTypes = {
    renderControl: PropTypes.func,
    id: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ])
}
