import React, { Fragment } from 'react';
import { useSelector } from 'react-redux';
import classes from 'classnames';
import { Header, ParagraphV2 as Paragraph } from '@justlook/uikit';

import useSearchFilter from 'hooks/use-search-filter';

import { ListLink, SplashScreen, EmptyContent } from 'components';

import { CONTRACTORS_FILTER } from 'app/contractors/store/filters';
import { ContractorSelectors } from 'app/contractors/store/selectors';

import cx from './contractors-list.module.scss';

export const ContractorsList = () => {
    const { filterQuery, filter } = useSearchFilter(CONTRACTORS_FILTER);
    const contractors = useSelector(ContractorSelectors.getContractors);
    const isRequest = useSelector(ContractorSelectors.getContractorsIsRequest);

    if (isRequest) { return <SplashScreen/> }

    if(contractors.length === 0) {
        const message = filter?.search ? 'Никаких результатов' : 'Нет исполнителей';
        return <EmptyContent message={message}/>
    }

    return (
        <Fragment>
            {contractors && contractors.map((item, index) => {
                const isSelfEmployed = item.legal_form.is_self_employed;
                const needConfirmSelfEmployer = isSelfEmployed && !item?.legal_form.status?.is_verified;
                const noPermissionToPayTaxes = isSelfEmployed && !item.tax.has_granted_permission_pay_taxes;

                const isProblematicSelfEmployed = needConfirmSelfEmployer || noPermissionToPayTaxes;

                const cls = classes({ [cx.warning]: isProblematicSelfEmployed });

                return (
                    <ListLink key={index} to={`/contractors/${item.id}${filterQuery}`}>
                        <Header.H3>{`${ item.last_name || '' } ${ item.first_name || '' } ${item.middle_name || ''}\u00a0`}</Header.H3>

                        <Paragraph.LH24>
                            { item.phone } ·&nbsp;
                            <span className={cls}>
                                { item.legal_form.title }
                            </span>
                        </Paragraph.LH24>
                    </ListLink>
                )
            })}
        </Fragment>
    )
}
