import React from 'react';
import PropTypes from 'prop-types';

import { ParagraphV2 as Paragraph } from '@justlook/uikit';

import { SelfEmployerConfirm } from './self-employer-confirm';

import cx from './self-employer-status.module.scss';

export const SelfEmployerStatus = ({ contractor, canWrite }) => {
    const selfEmployerStatus = contractor?.legal_form?.status;
    const isVerifiedSelfEmployer = selfEmployerStatus?.is_verified;
    const needConfirmSelfEmployer = selfEmployerStatus && !isVerifiedSelfEmployer && canWrite;

    if(!needConfirmSelfEmployer) return null;

    return (
        <div className={cx.base}>
            <Paragraph.LH24 className={cx.status}>
                Не подтвержден
            </Paragraph.LH24>

            <SelfEmployerConfirm
                id={contractor.id}
                renderControl={(open) => (
                    <button type='button' className={cx.confirmStatus} onClick={open}>
                        Исправить
                    </button>
                )}
            />
        </div>
    )
}

SelfEmployerStatus.propTypes = {
    contractor: PropTypes.object.isRequired,
    canWrite: PropTypes.bool,
}
