import React from 'react';
import { useSelector } from 'react-redux';
import { ParagraphV2 as Paragraph } from '@justlook/uikit';

import { useMoney } from 'hooks/use-money';

import { ContractorSelectors } from '../store/selectors';

import { LoaderBalance } from './loaders/loader-balance';

import cx from './balance.module.scss';

export const Balance = () => {
    const { currency, format } = useMoney();

    const balance = useSelector(ContractorSelectors.getContractorBalance);
    const isFetching = useSelector(ContractorSelectors.getIsFetchingBalance);

    if(isFetching) {
        return <LoaderBalance/>
    }

    return (
        <Paragraph.LH24 className={cx.balance}>
            Баланс:
            <span>
                <b>{format(balance, 2)}</b> {currency}
            </span>
        </Paragraph.LH24>
    )
}
