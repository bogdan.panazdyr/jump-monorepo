import React from 'react';
import PropTypes from 'prop-types';
import { useNavigate } from 'react-router-dom';
import { useIfMediaScreen } from '@justlook/uikit/lib/hooks';
import { Button, Header as Heading, Icons24 } from '@justlook/uikit';

import { useMenu } from 'components/menu';
import { USER_FILTER } from 'app/users/store/filters';

import useSearchFilter from '../../../hooks/use-search-filter';
import { VIEW_MODE_DETAILS } from 'utils/browser';

import { ContractorModal } from 'app/contractors/components/modal/contractor-modal';

import cx from 'app/contractors/components/contractors-header.module.scss';

const ItemHeader = ({ onExpandedMobile, filterQuery }) => {
    const navigate = useNavigate();

    return (
        <div className={cx.base}>
            <Button styling='hollow' icon={<Icons24.IconBack/>} onClick={() => navigate(`/contractors${filterQuery}`)}/>

            <div className={cx.titleBlock}>
                <Heading.H1 className={cx.title}>Исполнители</Heading.H1>
            </div>

            <Button styling='hollow' icon={<Icons24.IconMenu/>} onClick={onExpandedMobile}/>
        </div>
    )
};

const FilterHeader = ({ onExpandedMobile, isMobile, canCreate, onClickSearch }) => {
    return <div className={cx.base}>
        <div className={cx.dummy}/>
        <div className={cx.titleBlock}>
            <Heading.H1 className={cx.title}>Исполнители</Heading.H1>
        </div>

        {canCreate &&
            <ContractorModal
                renderControl={(open) => <Button styling='hollow' icon={<Icons24.IconPlus/>} onClick={open}/>}
            />
        }

        { onClickSearch && <Button onClick={onClickSearch} styling='hollow' icon={<Icons24.IconSearch/>} /> }

        { isMobile && <Button styling='hollow' icon={<Icons24.IconMenu/>} onClick={onExpandedMobile}/> }
    </div>;
};

export const ContractorsHeader = ({ viewMode, updateFilter, canCreate, onClickSearch }) => {
    const isMobile = useIfMediaScreen();
    const { filterQuery, filterFull } = useSearchFilter(USER_FILTER);
    const { onExpandedMobile } = useMenu();

    if (isMobile && viewMode === VIEW_MODE_DETAILS) {
        return (
            <ItemHeader
                onExpandedMobile={onExpandedMobile}
                filterQuery={filterQuery}
            />
        );
    }

    return (
        <FilterHeader
            isMobile={isMobile}
            onExpandedMobile={onExpandedMobile}
            filter={filterFull}
            canCreate={canCreate}
            onClickSearch={onClickSearch}
            updateFilter={updateFilter}
        />
    );
};

ItemHeader.propTypes = {
    onExpandedMobile: PropTypes.func,
    filterQuery: PropTypes.any,
}

FilterHeader.propTypes = {
    onExpandedMobile: PropTypes.func,
    isMobile: PropTypes.bool,
    canCreate: PropTypes.bool,
    onClickSearch: PropTypes.func,
}

ContractorsHeader.propTypes = {
    viewMode: PropTypes.string,
    updateFilter: PropTypes.func,
    canCreate: PropTypes.bool,
    onClickSearch: PropTypes.func,
}
