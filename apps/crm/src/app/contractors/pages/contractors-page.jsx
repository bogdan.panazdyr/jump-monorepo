import React, { Fragment, useState } from 'react';
import { Helmet } from 'react-helmet';
import { useOutlet } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { Config } from '@justlook/core/src';

import { TwoColumnsLayout } from 'components/menu';
import { withPermissions, Paginator, Search } from 'components';
import { useContentView } from 'utils/browser';
import { useHasPermission } from 'app/profile/store/selectors';
import useUpdateSearchFilter from '../../../hooks/use-update-search-filter';

import { ContractorsHeader as Header } from 'app/contractors/components/contractors-header';
import { ContractorsList } from 'app/contractors/components/contractors-list';

import { CONTRACTORS_FILTER } from 'app/contractors/store/filters';
import { ContractorActions } from 'app/contractors/store/actions';
import { ContractorSelectors } from 'app/contractors/store/selectors';
import { CreateEntityContextProvider } from 'domains/common/contexts';

const Page = () => {
    const { updateFilter, filterFull } = useUpdateSearchFilter(
        CONTRACTORS_FILTER,
        ContractorActions.fetchContractorsList
    );

    const [ isOpenSearch, setIsOpenSearch ] = useState(filterFull.search);
    const child = useOutlet();
    const viewMode = useContentView(child);

    const meta = useSelector(ContractorSelectors.getContractorsMeta);
    const access = useHasPermission('write-contractor');

    return (
        <Fragment>
            <Helmet>
                <title>Исполнители - {Config.brandName}</title>
            </Helmet>

            <TwoColumnsLayout
                viewMode={viewMode}
                details={child}
                header={
                    <Header
                        viewMode={viewMode}
                        updateFilter={updateFilter}
                        canCreate={access}
                        onClickSearch={() => setIsOpenSearch(!isOpenSearch)}
                    />
                }
                search={ isOpenSearch &&
                    <Search
                        value={filterFull.search}
                        onSearch={updateFilter}
                        placeholder='Введите ФИО или телефон'
                    />
                }
            >
                <ContractorsList/>

                <Paginator {...meta} {...filterFull} onChangePage={updateFilter}/>
            </TwoColumnsLayout>
        </Fragment>
    );
};

const PageWithContext = () => {
    return (
        <CreateEntityContextProvider>
            <Page />
        </CreateEntityContextProvider>
    )
}

export default withPermissions(PageWithContext, [ 'read-contractor' ]);
