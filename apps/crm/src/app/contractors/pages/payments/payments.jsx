import React, { Fragment, useState } from 'react';
import { useParams } from 'react-router-dom';

import { Pagination } from '@justlook/uikit';
import { Hooks } from '@justlook/core';

import { Box, EmptyContent, NewTable as Table, ReceiptCell, SplashScreen, Status } from 'components';
import { useMoney } from 'hooks/use-money';
import { usePaymentHasConfirmStatus, getStatusIcon } from 'domains/payments/helpers';
import { usePayments } from 'domains/payments/queries';
import { useCreateEntityContext } from 'domains/common/contexts';
import { PaymentsDetailModal } from 'components/modals';
import { MobileRowPayment } from 'app/payments/components';
import { Payment } from 'domains/payments/models';
import { ContractorActions } from 'app/contractors/store/actions';

import { CreatePayment }  from './components/create-payment';

import cx from './payments.module.scss';

const Page = () => {
    const { id } = useParams();
    const { currency } = useMoney();
    const { paymentsData, paymentsMeta, isPaymentsLoading, refreshPayments, updateFilter, filter, getPaymentById } = usePayments();
    const { isShowEntityForm, onCloseEntityForm, onOpenEntityForm } = useCreateEntityContext();
    const canConfirmPayment = usePaymentHasConfirmStatus();

    const [ selectPayment, setSelectPayment ] = useState(null);

    const onFetchBalance = Hooks.useActionToDispatch(ContractorActions.fetchContractorBalance);

    const onSelectPayment = (id) => {
        setSelectPayment(getPaymentById(id));
    }

    const { last_page = 1, current_page = 1 } = paymentsMeta;
    const { order = '-created_at' } = filter;

    const onUpdatePayment = (payment) => {
        refreshPayments(payment);
        onFetchBalance(id, { include: 'abilities' })
    }

    const onSuccessCreatePayment = (payment) => {
        onCloseEntityForm();
        onUpdatePayment(payment);

        if (canConfirmPayment(payment)) {
            setSelectPayment(payment);
        }
    }

    const renderPayments = () => {
        if(paymentsData.length === 0) {
            return <EmptyContent
                isMute message='У исполнителя еще нет выплат'
                actionLabel={'Создать выплату'}
                action={onOpenEntityForm}
            />
        }

        return (
            <Fragment>
                <Table>
                    <Table.Head
                        cells={[
                            { label: 'Изменено', sort: 'updated_at', className: cx.date },
                            { label: `Заявка, ${currency}`, sort: 'amount', className: cx.amount },
                            { label: `Выплата, ${currency}`, className: cx.amount },
                            { label: 'Чек', className: cx.receipt },
                            { label: 'Статус', className: cx.status },
                            { label: 'Юрлицо', className: cx.common },
                        ]}
                        sortField={order}
                        onSort={(order) => updateFilter({ order })}
                    />

                    {paymentsData.map(item => {
                        const payment = new Payment(item);

                        return (
                            <Table.Row
                                key={payment.getId()}
                                onClick={() => onSelectPayment(payment.getId())}
                                mobileRow={
                                    <MobileRowPayment
                                        data={payment}
                                        onClick={() => onSelectPayment(payment.getId())}
                                    />
                                }
                            >
                                <Table.Cell className={cx.date}>{payment.getFullUpdatedAt()}</Table.Cell>
                                <Table.Cell className={cx.amount}>{payment.getAmount(true)}</Table.Cell>
                                <Table.Cell className={cx.amount}>{payment.getAmountPaid(true)}</Table.Cell>
                                <ReceiptCell payment={item} className={cx.receipt} />
                                <Table.Cell className={cx.status}>
                                    <Status
                                        icon={getStatusIcon(payment.getStatusTheme())}
                                        text={payment.getStatus()}
                                        color={payment.getStatusTheme()}
                                    />
                                </Table.Cell>
                                <Table.Cell className={cx.common}>{payment.getAgent()}</Table.Cell>
                            </Table.Row>
                        )
                    })}
                </Table>

                {
                    last_page > 1 && (
                        <Box mt={16}>
                            <Pagination
                                initialPage={current_page - 1}
                                handlePageClick={(value) => updateFilter({ page: value.selected + 1 })}
                                pageCount={last_page}
                            />
                        </Box>
                    )
                }
            </Fragment>
        );
    }

    return (
        <Fragment>
            { isPaymentsLoading && <SplashScreen/> }

            { !isPaymentsLoading && renderPayments() }

            {
                isShowEntityForm && <CreatePayment
                    onDismiss={onCloseEntityForm}
                    onSuccess={onSuccessCreatePayment}
                />
            }

            {
                selectPayment && <PaymentsDetailModal
                    payment={selectPayment}
                    onClose={() => setSelectPayment(null)}
                    onUpdatePayment={onUpdatePayment}
                />
            }
        </Fragment>
    )
}

export default Page;
