import React from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';

import { Button, ParagraphV2 as Paragraph } from '@justlook/uikit';

import { Modal } from 'components';
import { CreatePaymentModal } from 'components/modals';
import { useOnlinePermissions } from 'domains/payments/queries';
import { ContractorSelectors } from 'app/contractors/store/selectors';

export const CreatePayment = ({ onDismiss, onSuccess }) => {
    const { isAllow, isPermissionsLoading, message, actionLabel } = useOnlinePermissions();

    const contractor = useSelector(ContractorSelectors.getContractor);

    if (!isPermissionsLoading && !isAllow) {
        return (
            <Modal
                title='Новая выплата'
                onClose={onDismiss}
                majorButton={
                    <Button styling='hollow-border' onClick={onDismiss}>
                        Отмена
                    </Button>
                }
            >
                <Paragraph.LH24 style={{ textAlign: 'center' }}>
                    {message}
                </Paragraph.LH24>
            </Modal>
        )
    }

    return (
        <CreatePaymentModal
            initialState={contractor}
            submitLabel={actionLabel}
            isLoading={isPermissionsLoading}
            onSuccess={onSuccess}
            onDismiss={onDismiss}
        />
    )
}

CreatePayment.propTypes = {
    onDismiss: PropTypes.func,
    onSuccess: PropTypes.func,
}
