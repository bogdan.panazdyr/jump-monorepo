import React from 'react';

import cx from 'app/contractors/pages/history/history.module.scss';

const Page = () => {
    return (
        <div className={cx.base}>
            HISTORY
        </div>
    )
}

export default Page;
