import React from 'react';

import cx from 'app/contractors/pages/documents/documents.module.scss';

const Page = () => {
    return (
        <div className={cx.base}>
            DOCUMENTS
        </div>
    )
}

export default Page;
