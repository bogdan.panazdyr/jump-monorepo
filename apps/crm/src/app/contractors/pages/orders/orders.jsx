import React, { useEffect, useState, Fragment } from 'react';
import { useParams } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { Hooks } from '@justlook/core';
import { Button, Icons24, Pagination, ParagraphV2 as Paragraph } from '@justlook/uikit';

import { formattingDate } from 'utils/formatting-date';

import { SplashScreen, NewTable as Table } from 'components';

import { ViewOrder } from 'app/orders/components/modals/view-order-modal';

import { ContractorOrdersAction } from 'app/contractors/pages/orders/store/actions';
import { ContractorOrdersSelectors } from 'app/contractors/pages/orders/store/selectors';
import { ContractorSelectors } from 'app/contractors/store/selectors';

import cx from 'app/contractors/pages/orders/orders.module.scss';

const FORM_VIEW = 'view';

const Page = () => {
    const { id } = useParams();

    const { isOpen, onOpen, onClose, getPayload } = Hooks.useModal();

    const isRequest = useSelector(ContractorOrdersSelectors.getContractorOrdersIsRequest);
    const contractorOrders = useSelector(ContractorOrdersSelectors.getContractorOrders);
    const { last_page, current_page } = useSelector(ContractorOrdersSelectors.getContractorOrdersMeta);
    const contractor = useSelector(ContractorSelectors.getContractor);

    const onFetchContractorOrders = Hooks.useActionToDispatch(ContractorOrdersAction.fetchContractorOrders);

    const [page, setPage] = useState(current_page);

    useEffect(() => {
        (contractor?.id === parseInt(id)) && onFetchContractorOrders({ contractor_id: id, page })
    }, [onFetchContractorOrders, id, contractor, page]);

    if(isRequest) { return <SplashScreen/> }

    if(contractorOrders.length === 0) {
        return (
            <div className={cx['empty-wrapper']}>
                <Paragraph.LH24 className={cx.empty}>
                    Заказов нет
                </Paragraph.LH24>
            </div>
        )
    }

    return (
        <Fragment>
            <Table>
                    <Table.Head
                        cells={[
                            { label: 'Дата' },
                            { label: 'Статус' },
                            { label: 'Тел. клиента' },
                            { label: 'Исполнитель' },
                            { label: 'Цена' },
                        ]}
                    />
                    {contractorOrders.map(item => (
                        <Table.Row
                            key={item.id}
                            renderCustomCell={
                                <Button
                                    styling='hollow'
                                    icon={<Icons24.IconPencil/>}
                                    onClick={() => onOpen(FORM_VIEW, item)}
                                />
                            }
                        >
                            <Table.Cell>
                                { item?.pickup_date && formattingDate(item?.pickup_date) }
                            </Table.Cell>
                            <Table.Cell>
                                { item?.status?.title || '' }
                            </Table.Cell>
                            <Table.Cell>
                                { item?.phone }
                            </Table.Cell>
                            <Table.Cell>
                                { item?.contractor?.name }
                            </Table.Cell>
                            <Table.Cell>
                                { item?.cost?.value || 0 }
                            </Table.Cell>
                        </Table.Row>
                    ))}
                </Table>

            {last_page > 1 &&
                <Pagination
                    handlePageClick={(value) => setPage(value.selected + 1)}
                    forcePage={current_page - 1}
                    pageCount={last_page}
                />
            }

            {isOpen(FORM_VIEW) && <ViewOrder order={getPayload(FORM_VIEW)} onDismiss={() => onClose(FORM_VIEW)}/>}
        </Fragment>
    )
}

export default Page;
