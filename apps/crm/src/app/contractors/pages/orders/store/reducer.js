import produce from 'immer';
import { Store } from '@justlook/core';

import { CONTRACTOR_ORDERS_LIST } from 'app/contractors/pages/orders/store/action-types';

const INITIAL_STATE = {
    data: null,
    meta: null,
    isRequest: false,
    error: null,
};

export const contractorOrders = (state = INITIAL_STATE, action) =>
    produce(state, draft => {
        // eslint-disable-next-line default-case
        switch (action.type) {
            case CONTRACTOR_ORDERS_LIST + Store.REQUEST:
                draft.isRequest = true;
                draft.error = null;
                break;
            case CONTRACTOR_ORDERS_LIST + Store.RECEIVE:
                draft.isRequest = false;
                draft.data = action.orders.items;
                draft.meta = action.orders.meta;
                break;
            case CONTRACTOR_ORDERS_LIST + Store.FAILURE:
                draft.isRequest = false;
                draft.data = null;
                draft.meta = null;
                draft.error = action.error;
                break;
        }
    });
