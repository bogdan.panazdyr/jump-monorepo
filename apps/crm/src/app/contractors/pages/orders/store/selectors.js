const getContractorOrders = state => state?.contractorOrders?.data ?? [];
const getContractorOrdersIsRequest = state => state?.contractorOrders?.isRequest ?? false;
const getContractorOrdersMeta = state => state?.contractorOrders?.meta ?? {current_page: 1};

export const ContractorOrdersSelectors = {
    getContractorOrders,
    getContractorOrdersIsRequest,
    getContractorOrdersMeta,
}
