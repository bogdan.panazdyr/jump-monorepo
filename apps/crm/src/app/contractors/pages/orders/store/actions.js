import { Store } from '@justlook/core';

import { CONTRACTOR_ORDERS_LIST } from 'app/contractors/pages/orders/store/action-types';

const fetchContractorOrders = (filter) => ({ type: CONTRACTOR_ORDERS_LIST + Store.FETCH, filter });
const requestContractorOrders = () => ({ type: CONTRACTOR_ORDERS_LIST + Store.REQUEST });
const receiveContractorOrders = (orders) => ({ type: CONTRACTOR_ORDERS_LIST + Store.RECEIVE, orders });
const failureContractorOrders = (error) => ({ type: CONTRACTOR_ORDERS_LIST + Store.FAILURE, error });

export const ContractorOrdersAction = {
    fetchContractorOrders,
    requestContractorOrders,
    receiveContractorOrders,
    failureContractorOrders,
}
