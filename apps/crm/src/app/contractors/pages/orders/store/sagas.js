import { put, fork, call, takeLatest } from 'redux-saga/effects';
import { Store } from '@justlook/core';

import Api from 'api/client';

import { CONTRACTOR_ORDERS_LIST } from 'app/contractors/pages/orders/store/action-types';
import { ContractorOrdersAction } from 'app/contractors/pages/orders/store/actions';

export function * fetchContractorOrders({ filter = {} }) {
    yield put(ContractorOrdersAction.requestContractorOrders());

    try {
        const response = yield call([ Api.orders, 'fetchOrdersList' ], filter);
        yield put(ContractorOrdersAction.receiveContractorOrders(response));
    } catch (e) {
        yield put(ContractorOrdersAction.failureContractorOrders(e));
    }
}

function * watchFetchContractorOrders() {
    yield takeLatest(CONTRACTOR_ORDERS_LIST + Store.FETCH, fetchContractorOrders);
}

export default function* watcher() {
    yield fork(watchFetchContractorOrders);
}
