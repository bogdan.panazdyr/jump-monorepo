import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Controller } from 'react-hook-form';
import { Form, Input, Tab } from '@justlook/uikit';

import { BeneficiaryBank } from 'components/field-beneficiary-bank';

import cx from 'app/contractors/pages/requisites/components/requisites-card-form.module.scss';

export const PhoneFields = ({errors, control}) => {
    return (
        <Fragment>
            <Form.Field>
                <Controller
                    as={BeneficiaryBank}
                    label='Банк получателя'
                    name='bik'
                    placeholder='Введите название банка'
                    control={control}
                    rules={{ required: 'Обязательно для ввода' }}
                    error={errors.bik && errors.bik.message}
                />
            </Form.Field>
            <Form.Field>
                <Controller
                    as={Input}
                    name='account_number'
                    label='Номер телефона'
                    mask='+79999999999'
                    className={cx.grid}
                    control={control}
                    rules={{ required: 'Обязательно для ввода' }}
                    error={errors.account_number && errors.account_number.message}
                />
            </Form.Field>
        </Fragment>
    )
}

export const CardNumber = ({errors, control}) => {
    return (
        <Form.Field>
            <Controller
                as={Input}
                name='account_number'
                label='Номер карты'
                className={cx.grid}
                control={control}
                rules={{ required: 'Обязательно для ввода' }}
                error={errors.account_number && errors.account_number.message}
            />
        </Form.Field>
    )
}

export const RequisitesCardForm = ({onChangeType, control, errors}) => {
    return (
        <Tab className={cx.tabs} actionChangeTab={(value) => onChangeType(value)}>
            <Tab.Content label='Телефон' value={10} disabled={true}>
                <PhoneFields control={control} errors={errors}/>
            </Tab.Content>
            <Tab.Content label='Номер карты' value={8}>
                <CardNumber control={control} errors={errors}/>
            </Tab.Content>
        </Tab>
    )
}

RequisitesCardForm.defaultProps = {
    onChangeType: () => {}
}

PhoneFields.propTypes = {
    errors: PropTypes.any,
    control: PropTypes.object,
}

CardNumber.propTypes = {
    errors: PropTypes.any,
    control: PropTypes.object,
}

RequisitesCardForm.propTypes = {
    errors: PropTypes.any,
    control: PropTypes.object,
    onChangeType: PropTypes.func,
}
