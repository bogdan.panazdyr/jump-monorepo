import React, { useEffect, useMemo } from 'react';
import PropTypes from 'prop-types';
import { useParams } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { Controller, useForm } from 'react-hook-form';
import { Forms, Hooks } from '@justlook/core';
import { useIfMediaScreen } from '@justlook/uikit/lib/hooks';
import { Form, Button, Icons24, Input } from '@justlook/uikit';

import { Modal } from 'components';

import { addServerErrors } from 'utils/add-server-errors';

import { RequisitesSelectors } from '../../store/selectors';
import { RequisitesActions } from '../../store/actions';

import { PhoneFields, CardNumber } from '../../components/requisites-card-form';
import { RequisitesScoreForm } from '../../components/requisites-score-form';

import cx from '../requisites-form.module.scss';

const FORM_ID = 'edit_contractor_requisite';

export const EditRequisites = ({ requisite, onDismiss }) => {
    const { id } = useParams();
    const isMobile = useIfMediaScreen();

    const { handleSubmit, errors, control, setError } = useForm({
        defaultValues: {
            name: requisite.display_name,
            bik: Forms.itemToOption(requisite.requisite, {
                value: 'bik',
                label: 'bank_name',
            }),
            account_number: requisite.requisite.account_number,
            first_name: requisite.requisite.first_name,
            last_name: requisite.requisite.last_name,
            middle_name: requisite.requisite.middle_name,
            inn: requisite.requisite.inn,
        }
    })

    const { type } = requisite;
    const { isSubmitting, isSubmitted } = useSelector(RequisitesSelectors.getFormState);
    const errorFields = useSelector(RequisitesSelectors.getInvalidFieldsRequisites);

    const onUpdateRequisite = Hooks.useActionToDispatch(RequisitesActions.updateRequisite);
    const onDeleteRequisite = Hooks.useActionToDispatch(RequisitesActions.deleteRequisite);

    useEffect(() => { if(isSubmitted) onDismiss() }, [isSubmitted, onDismiss]);

    useMemo(() => { addServerErrors(errorFields, setError) }, [errorFields, setError]);

    const prepareData = (values) => ({
        ...values,
        bik: values.bik instanceof Object ? values.bik.value : values.bik,
        type_id: type.id,
        is_default: true
    });

    const onSubmit = (values) => onUpdateRequisite(id, requisite.id, prepareData(values));
    const onDelete = () => onDeleteRequisite(id, requisite.id);

    return (
        <Modal
            title='Редактирование реквизита'
            description={requisite?.type?.title}
            onClose={onDismiss}
            majorButton={<Button form={FORM_ID} loading={isSubmitting} type='submit'>Сохранить</Button>}
            dismissButton={
                <Button disabled={isSubmitting} onClick={onDismiss} styling='hollow-border'>
                    Отмена
                </Button>
            }
            minorButton={
                <Button
                    loading={isSubmitting}
                    onClick={onDelete}
                    styling='hollow-border'
                    icon={!isMobile && <Icons24.IconTrash/>}
                >
                    Удалить
                </Button>
            }

        >
            <Form onSubmit={handleSubmit(onSubmit)} id={FORM_ID}>
                <Form.Field>
                    <Controller
                        as={Input}
                        name='name'
                        label='Название'
                        className={cx.grid}
                        control={control}
                        rules={{ required: 'Обязательно для ввода' }}
                        error={errors.name && errors.name.message}
                    />
                </Form.Field>

                {type.id === 1 && <RequisitesScoreForm errors={errors} control={control}/>}
                {type.id === 8 && <CardNumber errors={errors} control={control}/>}
                {type.id === 10 && <PhoneFields errors={errors} control={control}/>}
            </Form>
        </Modal>
    )
}

EditRequisites.defaultProps = {
    onDismiss: () => {},
    requisite: {}
}

EditRequisites.propTypes = {
    onDismiss: PropTypes.func.isRequired,
    requisite: PropTypes.object.isRequired,
}
