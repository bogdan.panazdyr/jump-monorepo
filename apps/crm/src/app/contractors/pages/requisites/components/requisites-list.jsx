import React, { Fragment } from 'react';
import { useSelector } from 'react-redux';
import { Hooks } from '@justlook/core';
import { Button, Icons24 } from '@justlook/uikit';

import { SplashScreen, NewTable as Table, EmptyContent } from 'components';

import { useCreateEntityContext } from 'domains/common/contexts';

import { RequisitesSelectors } from '../store/selectors';

import { EditRequisites } from './modals/edit-requisite-modal';

const FORM_UPDATE = 'update';

export const RequisitesList = () => {
    const { isOpen, onOpen, onClose, getPayload } = Hooks.useModal()

    const { onOpenEntityForm } = useCreateEntityContext();
    const requisites = useSelector(RequisitesSelectors.getRequisites);
    const isRequest = useSelector(RequisitesSelectors.getIsRequest);

    if(isRequest) { return  <SplashScreen/> }

    if(requisites.length === 0) {
        return <EmptyContent
            isMute message='У исполнителя еще нет реквизитов'
            actionLabel={'Добавить реквизиты'}
            action={onOpenEntityForm}
        />
    }

    return (
        <Fragment>
            <Table>
                <Table.Head cells={[ { label: 'Название' }, { label: 'Тип' } ]}/>
                {requisites.map(item => (
                    <Table.Row
                        key={item.id}
                        renderCustomCell={
                            <Button
                                styling='hollow'
                                icon={<Icons24.IconPencil/>}
                                onClick={() => onOpen(FORM_UPDATE, item)}
                            />
                        }
                    >
                        <Table.Cell mobilelabel='Название'>
                            {item.display_name}
                        </Table.Cell>
                        <Table.Cell mobilelabel='Тип'>
                            {item.description}
                        </Table.Cell>
                    </Table.Row>
                ))}
            </Table>

            {isOpen(FORM_UPDATE) && <EditRequisites
                requisite={getPayload(FORM_UPDATE)} onDismiss={() => onClose(FORM_UPDATE)}
            />}
        </Fragment>
    )
}
