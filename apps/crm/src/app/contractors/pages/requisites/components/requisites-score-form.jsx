import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Controller } from 'react-hook-form';
import { Form, Header, Input } from '@justlook/uikit';

import { BeneficiaryBank } from 'components/field-beneficiary-bank';

import cx from 'app/contractors/pages/requisites/components/requisites-score-form.module.scss';

export const RequisitesScoreForm = ({ control, errors }) => {
    return (
        <Fragment>
            <Form.Field>
                <Controller
                    as={BeneficiaryBank}
                    label='Банк получателя'
                    name='bik'
                    placeholder='Введите название банка'
                    control={control}
                    rules={{ required: 'Обязательно для ввода' }}
                    error={errors.bik && errors.bik.message}
                />
            </Form.Field>
            <Form.Field>
                <Controller
                    as={Input}
                    name='account_number'
                    label='Номер счета'
                    control={control}
                    className={cx.grid}
                    rules={{ required: 'Обязательно для ввода' }}
                    error={errors.account_number && errors.account_number.message}
                />
            </Form.Field>

            <Header.H3 className={cx.title}>Получатель</Header.H3>

            <Form.Field>
                <Controller
                    as={Input}
                    name='last_name'
                    label='Фамилия'
                    control={control}
                    className={cx.grid}
                    rules={{ required: 'Обязательно для ввода' }}
                    error={errors.last_name && errors.last_name.message}
                />
            </Form.Field>
            <Form.Field>
                <Controller
                    as={Input}
                    name='first_name'
                    label='Имя'
                    control={control}
                    className={cx.grid}
                    rules={{ required: 'Обязательно для ввода' }}
                    error={errors.first_name && errors.first_name.message}
                />
            </Form.Field>
            <Form.Field>
                <Controller
                    as={Input}
                    name='middle_name'
                    label='Отчество'
                    control={control}
                    className={cx.grid}
                    rules={{ required: 'Обязательно для ввода' }}
                    error={errors.middle_name && errors.middle_name.message}
                />
            </Form.Field>
            <Form.Field>
                <Controller
                    as={Input}
                    name='inn'
                    label='ИНН'
                    control={control}
                    className={cx.grid}
                    rules={{ required: 'Обязательно для ввода' }}
                    error={errors.inn && errors.inn.message}
                />
            </Form.Field>
        </Fragment>
    )
}

RequisitesScoreForm.propTypes = {
    control: PropTypes.object,
    errors: PropTypes.any,
}
