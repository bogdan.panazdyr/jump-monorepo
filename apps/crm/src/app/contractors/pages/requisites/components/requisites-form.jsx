import React, { useEffect, useMemo, useState } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import { useForm, Controller } from 'react-hook-form';
import { Form, Input } from '@justlook/uikit';
import { Hooks } from '@justlook/core';

import { RequisitesCardForm } from 'app/contractors/pages/requisites/components/requisites-card-form';
import { RequisitesScoreForm } from 'app/contractors/pages/requisites/components/requisites-score-form';

import { RequisitesActions } from 'app/contractors/pages/requisites/store/actions';
import { RequisitesSelectors } from 'app/contractors/pages/requisites/store/selectors';

import { addServerErrors } from 'utils/add-server-errors';

import cx from 'app/contractors/pages/requisites/components/requisites-form.module.scss';

export const RequisitesForm = ({ type, onSuccessSubmit, formId }) => {
    const { id } = useParams();
    const [ typeId, setTypeId ] = useState(8);
    const { handleSubmit, control, errors, setError } = useForm();

    const { isSubmitted } = useSelector(RequisitesSelectors.getFormState);
    const errorFields = useSelector(RequisitesSelectors.getInvalidFieldsRequisites);

    const onCreateRequisite = Hooks.useActionToDispatch(RequisitesActions.createRequisite);

    useEffect(() => { if(isSubmitted) onSuccessSubmit()}, [onSuccessSubmit, isSubmitted]);

    useMemo(() => { addServerErrors(errorFields, setError) }, [errorFields, setError]);

    const prepareFormData = (values) => ({
        ...values,
        type_id: type === 'score' ? 1 : typeId,
        is_default: true
    });

    const onSubmit = (values) => onCreateRequisite(id, prepareFormData(values));

    return (
        <Form onSubmit={handleSubmit(onSubmit)} id={formId}>
            <Form.Field>
                <Controller
                    as={Input}
                    name='name'
                    label='Название'
                    className={cx.grid}
                    control={control}
                    rules={{ required: 'Обязательно для ввода' }}
                    error={errors.name && errors.name.message}
                />
            </Form.Field>

            { type === 'card' &&
                <RequisitesCardForm
                    control={control}
                    errors={errors}
                    onChangeType={setTypeId}
                />
            }
            { type === 'score' && <RequisitesScoreForm control={control} errors={errors}/> }
        </Form>
    )
}

RequisitesForm.propTypes = {
    formId: PropTypes.string,
    onDismiss: PropTypes.func,
    type: PropTypes.string,
    onSuccessSubmit: PropTypes.func,
}
