import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Button, Tab } from '@justlook/uikit';
import { useSelector } from 'react-redux';

import { RequisitesForm } from 'app/contractors/pages/requisites/components/requisites-form';

import { Modal } from 'components';

import { RequisitesSelectors } from 'app/contractors/pages/requisites/store/selectors';

import cx from 'app/contractors/pages/requisites/components/modals/requisites-modal.module.scss';

const FORM_ID = 'requisites_contractor_form';

export const RequisitesModal = ({ onDismiss }) => {
    const [type, setType] = useState('card');

    const { isSubmitting } = useSelector(RequisitesSelectors.getFormState);

    return (
        <Modal
            title='Новый счет'
            onClose={onDismiss}
            majorButton={<Button form={FORM_ID} loading={isSubmitting} type='submit'>Добавить</Button>}
            dismissButton={<Button disabled={isSubmitting} styling='hollow-border' onClick={onDismiss}>Отмена</Button>}
        >
            <Tab className={cx.tabs} actionChangeTab={(value) => setType(value)}>
                <Tab.Content label='Карта' value={'card'}>
                    <RequisitesForm
                        formId={FORM_ID}
                        type={type}
                        onDismiss={onDismiss}
                        onSuccessSubmit={onDismiss}
                    />
                </Tab.Content>
                <Tab.Content label='Счет' value={'score'}>
                    <RequisitesForm
                        formId={FORM_ID}
                        type={type}
                        onDismiss={onDismiss}
                        onSuccessSubmit={onDismiss}
                    />
                </Tab.Content>
            </Tab>
        </Modal>
    )
}

RequisitesModal.propTypes = {
    onDismiss: PropTypes.func,
}

