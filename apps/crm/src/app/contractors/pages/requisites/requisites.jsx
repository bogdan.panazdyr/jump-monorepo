import React, { useEffect, Fragment } from 'react';
import { useParams } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { Pagination } from '@justlook/uikit';
import { Hooks } from '@justlook/core';

import { RequisitesModal } from './components/modals/requisites-modal';
import { RequisitesActions } from 'app/contractors/pages/requisites/store/actions';
import { RequisitesSelectors } from 'app/contractors/pages/requisites/store/selectors';
import { ContractorSelectors } from 'app/contractors/store/selectors';
import { useCreateEntityContext } from 'domains/common/contexts';

import { RequisitesList } from 'app/contractors/pages/requisites/components/requisites-list';

const Page = () => {
    const { id } = useParams();
    const { isShowEntityForm, onCloseEntityForm } = useCreateEntityContext();
    const onFetchRequisites = Hooks.useActionToDispatch(RequisitesActions.fetchRequisites)
    const { last_page, current_page } = useSelector(RequisitesSelectors.getRequisitesMeta);
    const contractor = useSelector(ContractorSelectors.getContractor);

    useEffect(() => {
        (contractor?.id === parseInt(id)) && onFetchRequisites(id)
    }, [onFetchRequisites, id, contractor]);

    return (
        <Fragment>
            <RequisitesList/>

            {
                last_page > 1 &&
                <Pagination
                    handlePageClick={(value) => onFetchRequisites(id, {page: value.selected + 1})}
                    forcePage={current_page - 1}
                    pageCount={last_page}
                />
            }

            {
                isShowEntityForm && <RequisitesModal onDismiss={onCloseEntityForm} />
            }
        </Fragment>
    )
}

export default Page;
