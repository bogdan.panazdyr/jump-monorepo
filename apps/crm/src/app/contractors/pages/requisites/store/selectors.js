const getRequisites = state => state?.contractorRequisites?.items?.data ?? [];
const getIsRequest = state => state?.contractorRequisites?.items?.isRequest ?? false;
const getRequisitesMeta = (state) => state?.contractorRequisites?.items?.meta ?? {current_page: 1};
const getInvalidFieldsRequisites = state => state?.contractorRequisites?.items?.error?.errorData?.fields ?? [];

const getFormState = (state) => ({
    isSubmitting: state?.contractorRequisites?.formState?.isSubmitting ?? false,
    isSubmitted: state?.contractorRequisites?.formState?.isSubmitted ?? false,
})

export const RequisitesSelectors = {
    getRequisites,
    getIsRequest,
    getRequisitesMeta,
    getFormState,
    getInvalidFieldsRequisites,
}
