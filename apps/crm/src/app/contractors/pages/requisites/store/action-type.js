export const REQUISITE_LIST = '@@requisites/REQUISITE_LIST';
export const REQUISITE = '@@requisites/REQUISITE';

export const SUCCESS_SUBMIT_FORM = '@@requisites/SUCCESS_SUBMIT_FORM';
