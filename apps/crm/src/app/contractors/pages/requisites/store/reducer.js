import produce from 'immer';
import { Store } from '@justlook/core';

import { REQUISITE_LIST, REQUISITE, SUCCESS_SUBMIT_FORM } from 'app/contractors/pages/requisites/store/action-type';

const INITIAL_STATE = {
    items: {
        data: null,
        meta: null,
        isRequest: false,
        error: null,
    },
    item: {
        data: null,
        isRequest: false,
        error: null
    },
    formState: {
        isSubmitting: false,
        isSubmitted: false,
    }
};

export const contractorRequisites = (state = INITIAL_STATE, action) =>
    produce(state, draft => {
        // eslint-disable-next-line default-case
        switch (action.type) {
            case REQUISITE_LIST + Store.REQUEST:
                draft.items.isRequest = true;
                draft.items.error = null;
                break;
            case REQUISITE_LIST + Store.RECEIVE:
                draft.items.isRequest = false;
                draft.items.data = action.requisite.items;
                draft.items.meta = action.requisite.meta;
                break;
            case REQUISITE_LIST + Store.FAILURE:
                draft.items.isRequest = false;
                draft.items.error = action.error;
                break;


            case REQUISITE + Store.CREATE_REQUEST:
                draft.formState.isSubmitting = true;
                draft.formState.isSubmitted = false;
                draft.items.error = null;
                break;
            case REQUISITE + Store.CREATE_RECEIVE:
                draft.items.data = [action.requisite.item, ...state.items.data];
                draft.formState.isSubmitting = false;
                draft.formState.isSubmitted = true;
                break;
            case REQUISITE + Store.CREATE_FAILURE:
                draft.formState.isSubmitting = false;
                draft.formState.isSubmitted = false;
                draft.items.error = action.error;
                break;


            case REQUISITE + Store.UPDATE_REQUEST:
                draft.formState.isSubmitting = true;
                draft.formState.isSubmitted = false;
                draft.items.error = null;
                break;
            case REQUISITE + Store.UPDATE_RECEIVE:
                draft.formState.isSubmitting = false;
                draft.formState.isSubmitted = true;
                draft.items.data = draft.items.data.map(item => {
                    if(item.id === action.requisite.item.id) {
                        return action.requisite.item;
                    }
                    return item;
                });
                break;
            case REQUISITE + Store.UPDATE_FAILURE:
                draft.formState.isSubmitting = false;
                draft.formState.isSubmitted = false;
                draft.items.error = action.error;
                break;


            case REQUISITE + Store.DELETE_REQUEST:
                draft.formState.isSubmitting = true;
                draft.formState.isSubmitted = false;
                draft.items.error = null;
                break;
            case REQUISITE + Store.DELETE_RECEIVE:
                draft.formState.isSubmitting = false;
                draft.formState.isSubmitted = true;
                draft.items.data = draft.items.data.filter(item => item.id !== action.id);
                draft.items.error = null;
                break;
            case REQUISITE + Store.DELETE_FAILURE:
                draft.formState.isSubmitting = false;
                draft.formState.isSubmitted = false;
                draft.items.error = action.error;
                break;


            case SUCCESS_SUBMIT_FORM:
                draft.formState.isSubmitted = false;
                draft.items.error = null;
        }
    });
