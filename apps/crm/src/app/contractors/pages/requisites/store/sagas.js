import { put, fork, call, takeLatest } from 'redux-saga/effects';
import { Store } from '@justlook/core';

import Api from 'api/client';

import { REQUISITE_LIST, REQUISITE } from 'app/contractors/pages/requisites/store/action-type';
import { RequisitesActions } from 'app/contractors/pages/requisites/store/actions';

export function * fetchRequisites ({ id, filter }) {
    yield put(RequisitesActions.requestFetchRequisites());

    try {
        const response = yield call([Api.contractorRequisites, 'fetchRequisitesList'], id, filter);
        yield put(RequisitesActions.receiveFetchRequisites(response));
    } catch (e) {
        yield put(RequisitesActions.failureFetchRequisites(e))
    }
}

export function * createRequisite({ id, data }) {
    yield put(RequisitesActions.requestCreateRequisite());

    try {
        const response = yield call([Api.contractorRequisites, 'create'], id, data)
        yield put(RequisitesActions.receiveCreateRequisite(response));
        yield put(RequisitesActions.successSubmitForm())
    } catch (e) {
        yield put(RequisitesActions.failureCreateRequisite(e))
    }
}

export function * updateRequisite({ id, req_id, data }) {
    yield put(RequisitesActions.requestUpdateRequisite());

    try {
        const response = yield call([Api.contractorRequisites, 'update'], id, req_id, data);
        yield put(RequisitesActions.receiveUpdateRequisite(response))
        yield put(RequisitesActions.successSubmitForm())
    } catch (e) {
        yield put(RequisitesActions.failureUpdateRequisite(e))
    }
}

export function * deleteRequisite({ id, req_id }) {
    yield put(RequisitesActions.requestDeleteRequisite());

    try {
        yield call([Api.contractorRequisites, 'delete'], id, req_id);
        yield put(RequisitesActions.receiveDeleteRequisite(req_id))
        yield put(RequisitesActions.successSubmitForm())
    } catch (e) {
        yield put(RequisitesActions.failureDeleteRequisite(e));
    }
}

function * watchFetchRequisites() {
    yield takeLatest(REQUISITE_LIST + Store.FETCH, fetchRequisites);
}

function * watchCreateRequisite() {
    yield takeLatest(REQUISITE + Store.CREATE, createRequisite);
}

function * watchUpdateRequisite() {
    yield takeLatest(REQUISITE + Store.UPDATE, updateRequisite);
}

function * watchDeleteRequisite() {
    yield takeLatest(REQUISITE + Store.DELETE, deleteRequisite);
}

export default function* watcher() {
    yield fork(watchFetchRequisites);
    yield fork(watchCreateRequisite);
    yield fork(watchUpdateRequisite);
    yield fork(watchDeleteRequisite);
}
