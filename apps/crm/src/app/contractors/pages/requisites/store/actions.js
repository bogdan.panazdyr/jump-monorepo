import { Store } from '@justlook/core';
import { REQUISITE_LIST, REQUISITE, SUCCESS_SUBMIT_FORM } from 'app/contractors/pages/requisites/store/action-type';

const fetchRequisites = (id, filter) => ({ type: REQUISITE_LIST + Store.FETCH, id, filter});
const requestFetchRequisites = () => ({ type: REQUISITE_LIST + Store.REQUEST });
const receiveFetchRequisites = (requisite) => ({ type: REQUISITE_LIST + Store.RECEIVE, requisite });
const failureFetchRequisites = (error) => ({ type: REQUISITE_LIST + Store.FAILURE, error });

const createRequisite = (id, data) => ({ type: REQUISITE + Store.CREATE, id, data });
const requestCreateRequisite = () => ({ type: REQUISITE + Store.CREATE_REQUEST });
const receiveCreateRequisite = (requisite) => ({ type: REQUISITE + Store.CREATE_RECEIVE, requisite });
const failureCreateRequisite = (error) => ({ type: REQUISITE + Store.CREATE_FAILURE, error });

const updateRequisite = (id, req_id, data) => ({ type: REQUISITE + Store.UPDATE, id, req_id, data });
const requestUpdateRequisite = () => ({ type: REQUISITE + Store.UPDATE_REQUEST });
const receiveUpdateRequisite = (requisite) => ({ type: REQUISITE + Store.UPDATE_RECEIVE, requisite });
const failureUpdateRequisite = (error) => ({ type: REQUISITE + Store.UPDATE_FAILURE, error });

const deleteRequisite = (id, req_id) => ({ type: REQUISITE + Store.DELETE, id, req_id });
const requestDeleteRequisite = () => ({ type: REQUISITE + Store.DELETE_REQUEST });
const receiveDeleteRequisite = (id) => ({ type: REQUISITE + Store.DELETE_RECEIVE, id });
const failureDeleteRequisite = (error) => ({ type: REQUISITE + Store.DELETE_FAILURE, error });

const successSubmitForm = () => ({ type: SUCCESS_SUBMIT_FORM });

export const RequisitesActions = {
    fetchRequisites,
    requestFetchRequisites,
    receiveFetchRequisites,
    failureFetchRequisites,

    createRequisite,
    requestCreateRequisite,
    receiveCreateRequisite,
    failureCreateRequisite,

    updateRequisite,
    requestUpdateRequisite,
    receiveUpdateRequisite,
    failureUpdateRequisite,

    deleteRequisite,
    requestDeleteRequisite,
    receiveDeleteRequisite,
    failureDeleteRequisite,

    successSubmitForm,
}
