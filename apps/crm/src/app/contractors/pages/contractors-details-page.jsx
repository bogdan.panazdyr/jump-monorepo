import React, { Fragment, useEffect, useState } from 'react';
import { useParams, useNavigate, useOutlet } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { Helmet } from 'react-helmet';

import { Hooks, Config } from '@justlook/core';
import { OverflowMenu, Icons24, ParagraphV2 as Paragraph, Button } from '@justlook/uikit';

import { ItemHeader, TabsNavigation as Tab, SplashScreen, Segment } from 'components';
import { useGetPathNameTabs } from 'hooks/use-get-pathname-tab';
import { useCreateEntityContext } from 'domains/common/contexts';
import { usePermissions } from 'domains/contractors';
import { getFio } from 'utils/person';
import { useSelfEmployer } from 'domains/contractors/services';
import { useTestMode } from 'hooks/use-testing';

import { Balance } from '../components/balance';
import { Taxes } from '../components/taxes';
import { EditContractorModal } from '../components/modal/edit-contractor-modal';
import { DeleteModal } from '../components/modal/delete-modal';
import { LegalForm } from '../components/legal-form';
import { WithdrawalAccess } from '../components/withdrawal-access';
import { ContractorActions } from '../store/actions';
import { ContractorSelectors } from '../store/selectors';

import cx from './contractors-details-page.module.scss';

const Page = () => {
    const { isTestMode } = useTestMode();

    const { id } = useParams();
    const navigate = useNavigate();
    const tabs = useOutlet();

    const [ isOpen, setIsOpen ] = useState(false);
    const { pathname, search } = useGetPathNameTabs();
    const { onOpenEntityForm } = useCreateEntityContext();

    const onFetchContractor = Hooks.useActionToDispatch(ContractorActions.fetchContractor);

    const isRequest = useSelector(ContractorSelectors.getContractorIsRequest);
    const contractor = useSelector(ContractorSelectors.getContractor);

    const { canReadOrders, canReadPayments, canWriteUsers, canReadTaxes } = usePermissions();

    const { onSelfEmployerSync } = useSelfEmployer(id);

    useEffect(() => { onFetchContractor(id, { include: 'abilities' }) }, [id, onFetchContractor]);

    const onSuccessEdit = (contractor) => {
        if(contractor?.legal_form?.is_self_employed && isTestMode) {
            onSelfEmployerSync(id);
        }

        setIsOpen(false);
        onFetchContractor(id, { include: 'abilities' });
    }

    if(isRequest) { return <SplashScreen/> }

    return (
        <Fragment>
            <Helmet>
                <title>{ getFio(contractor) } - { Config.brandName }</title>
            </Helmet>

            <div className={cx.base}>
                <ItemHeader
                    header={ getFio(contractor) }
                    description={
                        <Fragment>
                            { contractor?.phone + ' · ' }

                            <LegalForm contractor={contractor} canWrite={canWriteUsers}/>
                        </Fragment>
                    }
                >
                    {
                        canWriteUsers &&
                        <OverflowMenu icon={<Icons24.IconKebab />} >
                            <OverflowMenu.Item onItemClick={() => setIsOpen(true)}>
                                Редактировать
                            </OverflowMenu.Item>
                            <OverflowMenu.Item onItemClick={() => {}}>
                                <DeleteModal
                                    id={contractor.id}
                                    name={getFio(contractor) }
                                    renderProps={(open) => (
                                        <span className={cx.delete} onClick={open}>Удалить</span>
                                    )}
                                />
                            </OverflowMenu.Item>
                        </OverflowMenu>
                    }
                </ItemHeader>

                <Segment>
                    <Balance/>

                    { canReadTaxes && <Taxes id={id}/> }
                </Segment>

                <WithdrawalAccess
                    contractorId={contractor.id}
                    readOnly={!canWriteUsers}
                />

                <div className={cx['personal-info']}>
                    <Paragraph.LH24>ИНН: {contractor?.inn}</Paragraph.LH24>
                    <Paragraph.LH24>Юрлицо: {contractor?.agent?.name}</Paragraph.LH24>
                </div>

                <div className={cx.navigation}>
                    <div className={cx.tabs}>
                        {
                            canReadPayments &&
                            <Tab
                                navLabel='Выплаты'
                                onChangeTab={() => navigate('payments' + search)}
                                isActive={pathname === 'payments' || pathname === id}
                            />
                        }
                        {
                            canReadOrders &&
                            <Tab
                                navLabel='Заказы'
                                onChangeTab={() => navigate('orders' + search)}
                                isActive={pathname === 'orders'}
                            />
                        }
                        <Tab
                            navLabel='Реквизиты'
                            onChangeTab={() => navigate('requisites' + search)}
                            isActive={pathname === 'requisites'}
                        />
                    </div>

                    { pathname !== 'orders' && <Button styling='hollow' icon={<Icons24.IconPlus/>} onClick={onOpenEntityForm}/> }
                </div>
                {tabs}
            </div>

            {
                isOpen && (
                    <EditContractorModal
                        onDismiss={() => setIsOpen(false)}
                        onSuccess={onSuccessEdit}
                    />
                )
            }
        </Fragment>
    );
};

export default Page;
