export const CONTRACTORS_LIST = '@@contractors/CONTRACTORS_LIST';
export const CONTRACTOR = '@@contractors/CONTRACTOR';

export const CONTRACTOR_BALANCE = '@@contractors/CONTRACTOR_BALANCE';

export const ATTACH = '_ATTACH';

export const SUCCESS_SUBMIT_FORM = '@@contractors/SUCCESS_SUBMIT_FORM';
export const CLEARS_ERROR = '@@contractors/CLEARS_ERROR'
