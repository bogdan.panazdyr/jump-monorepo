import { Store } from '@justlook/core';
import { CONTRACTOR, CONTRACTORS_LIST, SUCCESS_SUBMIT_FORM, ATTACH, CLEARS_ERROR, CONTRACTOR_BALANCE } from 'app/contractors/store/action-type';

const fetchContractorsList = (filter) => ({ type: CONTRACTORS_LIST + Store.FETCH, filter });
const requestContractorsList = () => ({ type: CONTRACTORS_LIST + Store.REQUEST });
const receiveContractorsList = (data) => ({ type: CONTRACTORS_LIST + Store.RECEIVE, data });
const failureContractorsList = (error) => ({ type: CONTRACTORS_LIST + Store.FAILURE, error });

const fetchContractor = (id, filter) => ({ type: CONTRACTOR + Store.FETCH, id, filter });
const requestContractor = () => ({ type: CONTRACTOR + Store.REQUEST });
const receiveContractor = (contractor) => ({ type: CONTRACTOR + Store.RECEIVE, contractor });
const failureContractor = (error) => ({ type: CONTRACTOR + Store.FAILURE, error });

const createContractor = (contractor) => ({ type: CONTRACTOR + Store.CREATE, contractor });
const requestCreateContractor = () => ({ type: CONTRACTOR + Store.CREATE_REQUEST });
const receiveCreateContractor = (contractor) => ({ type: CONTRACTOR + Store.CREATE_RECEIVE, contractor });
const failureCreateContractor = (error) => ({ type: CONTRACTOR + Store.CREATE_FAILURE, error });

const attachContactor = (id, contractor) => ({ type: CONTRACTOR + ATTACH, id, contractor });
const requestAttachContactor = () => ({ type: CONTRACTOR + ATTACH + Store.REQUEST });
const receiveAttachContactor = (contractor) => ({ type: CONTRACTOR + ATTACH + Store.RECEIVE, contractor });
const failureAttachContractor = (error) => ({ type: CONTRACTOR + ATTACH + Store.FAILURE, error })

const updateContractor = (id, contractor) => ({ type: CONTRACTOR + Store.UPDATE, id, contractor });
const requestUpdateContractor = () => ({ type: CONTRACTOR + Store.UPDATE_REQUEST });
const receiveUpdateContractor = (contractor) => ({ type: CONTRACTOR + Store.UPDATE_RECEIVE, contractor });
const failureUpdateContractor = (error) => ({ type: CONTRACTOR + Store.UPDATE_FAILURE, error });

const deleteContractor = (id) => ({ type: CONTRACTOR + Store.DELETE, id });
const requestDeleteContractor = () => ({ type: CONTRACTOR + Store.DELETE_REQUEST });
const receiveDeleteContractor = (id) => ({ type: CONTRACTOR + Store.DELETE_RECEIVE, id });
const failureDeleteContractor = (error) => ({ type: CONTRACTOR + Store.DELETE_FAILURE, error });

const fetchContractorBalance = (id, filter) => ({ type: CONTRACTOR_BALANCE + Store.FETCH, id, filter });
const requestContractorBalance = () => ({ type: CONTRACTOR_BALANCE + Store.REQUEST });
const receiveContractorBalance = (contractor) => ({ type: CONTRACTOR_BALANCE + Store.RECEIVE, contractor });
const failureContractorBalance = (error) => ({ type: CONTRACTOR_BALANCE + Store.FAILURE, error });

const successSubmitForm = () => ({ type: SUCCESS_SUBMIT_FORM });
const clearsError = () => ({ type: CLEARS_ERROR });

export const ContractorActions = {
    fetchContractorsList,
    requestContractorsList,
    receiveContractorsList,
    failureContractorsList,

    fetchContractor,
    requestContractor,
    receiveContractor,
    failureContractor,

    createContractor,
    requestCreateContractor,
    receiveCreateContractor,
    failureCreateContractor,

    attachContactor,
    requestAttachContactor,
    receiveAttachContactor,
    failureAttachContractor,

    updateContractor,
    requestUpdateContractor,
    receiveUpdateContractor,
    failureUpdateContractor,

    deleteContractor,
    requestDeleteContractor,
    receiveDeleteContractor,
    failureDeleteContractor,

    fetchContractorBalance,
    requestContractorBalance,
    receiveContractorBalance,
    failureContractorBalance,

    successSubmitForm,
    clearsError,
}
