import produce from 'immer';
import { Store } from '@justlook/core';

import { CONTRACTORS_LIST, CONTRACTOR, SUCCESS_SUBMIT_FORM, ATTACH, CLEARS_ERROR, CONTRACTOR_BALANCE } from 'app/contractors/store/action-type';

const INITIAL_STATE = {
    items: {
        data: null,
        meta: null,
        isRequest: false,
        error: null,
    },
    item: {
        data: null,
        abilities: null,
        isRequest: false,
        isFetchingBalance: false,
        error: null
    },
    formState: {
        isSubmitting: false,
        isSubmitted: false,
    }
};

export const contractors = (state = INITIAL_STATE, action) =>
    produce(state, draft => {
        // eslint-disable-next-line default-case
        switch (action.type) {
            case CONTRACTORS_LIST + Store.REQUEST:
                draft.items.isRequest = true;
                draft.items.error = null;
                break;
            case CONTRACTORS_LIST + Store.RECEIVE:
                draft.items.isRequest = false;
                draft.items.data = action.data.items;
                draft.items.meta = action.data.meta;
                break;
            case CONTRACTORS_LIST + Store.FAILURE:
                draft.items.isRequest = false;
                draft.items.data = null;
                draft.items.meta = null;
                draft.items.error = action.error;
                break;

            case CONTRACTOR + Store.REQUEST:
                draft.item.isRequest = true;
                draft.items.error = null;
                break;
            case CONTRACTOR + Store.RECEIVE:
                draft.item.isRequest = false;
                draft.item.data = action.contractor.item;
                draft.item.abilities = action.contractor.abilities;
                break;
            case CONTRACTOR + Store.FAILURE:
                draft.item.isRequest = false;
                draft.item.error = action.error;
                break;

            case CONTRACTOR + Store.CREATE_REQUEST:
                draft.formState.isSubmitting = true;
                draft.formState.isSubmitted = false;
                draft.items.error = null;
                break;
            case CONTRACTOR + Store.CREATE_RECEIVE:
                draft.items.data = [action.contractor.item, ...state.items.data];
                draft.formState.isSubmitting = false;
                draft.formState.isSubmitted = true;
                break;
            case CONTRACTOR + Store.CREATE_FAILURE:
                draft.formState.isSubmitting = false;
                draft.formState.isSubmitted = false;
                draft.items.error = action.error;
                break;

            case CONTRACTOR + ATTACH + Store.REQUEST:
                draft.formState.isSubmitting = true;
                draft.formState.isSubmitted = false;
                draft.items.error = null;
                break;
            case CONTRACTOR + ATTACH + Store.RECEIVE:
                draft.items.data = [action.contractor.item, ...state.items.data];
                draft.formState.isSubmitting = false;
                draft.formState.isSubmitted = true;
                break;
            case CONTRACTOR + ATTACH + Store.FAILURE:
                draft.formState.isSubmitting = false;
                draft.formState.isSubmitted = false;
                draft.items.error = action.error;
                break;

            case CONTRACTOR + Store.UPDATE_REQUEST:
                draft.formState.isSubmitting = true;
                draft.formState.isSubmitted = false;
                draft.items.error = null;
                break;
            case CONTRACTOR + Store.UPDATE_RECEIVE:
                draft.formState.isSubmitting = false;
                draft.formState.isSubmitted = true;
                draft.items.data = draft.items.data.map(item => {
                    if(item.id === action.contractor.item.id) {
                        return action.contractor.item;
                    }
                    return item;
                });
                draft.item.data = action.contractor.item;
                break;
            case CONTRACTOR + Store.UPDATE_FAILURE:
                draft.formState.isSubmitting = false;
                draft.formState.isSubmitted = false;
                draft.items.error = action.error;
                break;

            case CONTRACTOR + Store.DELETE_REQUEST:
                draft.formState.isSubmitting = true;
                draft.formState.isSubmitted = false;
                draft.items.error = null;
                break;
            case CONTRACTOR + Store.DELETE_RECEIVE:
                draft.formState.isSubmitting = false;
                draft.formState.isSubmitted = true;
                draft.items.data = state.items.data.filter(item => item.id !== Number(action.id));
                draft.item.data = null;
                draft.items.error = null;
                break
            case CONTRACTOR + Store.DELETE_FAILURE:
                draft.formState.isSubmitting = false;
                draft.formState.isSubmitted = false;
                draft.items.error = action.error;
                break;

            case CONTRACTOR_BALANCE + Store.REQUEST:
                draft.item.isFetchingBalance = true;
                draft.items.error = null;
                break;
            case CONTRACTOR_BALANCE + Store.RECEIVE:
                draft.item.isFetchingBalance = false;
                draft.item.data = action.contractor.item;
                draft.item.abilities = action.contractor.abilities;
                break;
            case CONTRACTOR_BALANCE + Store.FAILURE:
                draft.item.isFetchingBalance = false;
                draft.item.error = action.error;
                break;

            case SUCCESS_SUBMIT_FORM:
                draft.formState.isSubmitted = false;
                draft.items.error = null;
                break;
            case CLEARS_ERROR:
                draft.items.error = null;
                draft.item.error = null;
        }
    });
