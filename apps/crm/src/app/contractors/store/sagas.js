import { put, fork, call, takeLatest } from 'redux-saga/effects';

import { Store } from '@justlook/core';

import Api from 'api/client';
import { CONTRACTORS_LIST, CONTRACTOR, ATTACH, CONTRACTOR_BALANCE } from 'app/contractors/store/action-type';
import { ContractorActions } from 'app/contractors/store/actions';

export function * fetchContractorsList({ filter = {} }) {
    yield put(ContractorActions.requestContractorsList());

    try {
        const response = yield call([ Api.contractors, 'fetchContractorsList' ], filter);
        yield put(ContractorActions.receiveContractorsList(response));
    } catch (e) {
        yield put(ContractorActions.failureContractorsList(e));
    }
}

export function * fetchContractor ({ id, filter }) {
    yield put(ContractorActions.requestContractor());

    try {
        const response = yield call([Api.contractors, 'fetchContractor'], id, filter);
        yield put(ContractorActions.receiveContractor(response));
    } catch (e) {
        yield put(ContractorActions.failureContractor(e));
    }
}

export function * createContractor({ contractor }) {
    yield put(ContractorActions.requestCreateContractor());

    try {
        const response = yield call([Api.contractors, 'create'], contractor);
        yield put(ContractorActions.receiveCreateContractor(response));
        yield put(ContractorActions.successSubmitForm());
    } catch (error) {
        yield put(ContractorActions.failureCreateContractor(error))
    }
}

export function * attachContractor({ id, contractor }) {
    yield put(ContractorActions.requestAttachContactor());

    try {
        const response = yield call([Api.contractors, 'attach'], id, contractor);
        yield put(ContractorActions.receiveAttachContactor(response));
        yield put(ContractorActions.successSubmitForm());
    } catch (e) {
        yield put(ContractorActions.failureAttachContractor(e))
    }
}

export function * updateContractor({id, contractor}) {
    yield put(ContractorActions.requestUpdateContractor());

    try {
        const response = yield call([Api.contractors, 'update'], id, contractor);
        yield put(ContractorActions.receiveUpdateContractor(response))
        yield put(ContractorActions.successSubmitForm());
    } catch (e) {
        yield put(ContractorActions.failureUpdateContractor(e))
    }
}

export function * deleteContractor({ id }) {
    yield put(ContractorActions.requestDeleteContractor());

    try {
        yield call([Api.contractors, 'delete'], id);
        yield put(ContractorActions.receiveDeleteContractor(id));
        yield put(ContractorActions.successSubmitForm());
    } catch (e) {
        yield put(ContractorActions.failureDeleteContractor(e));
    }
}

export function * fetchBalanceContractor ({ id, filter }) {
    yield put(ContractorActions.requestContractorBalance());

    try {
        const response = yield call([Api.contractors, 'fetchContractor'], id, filter);
        yield put(ContractorActions.receiveContractorBalance(response));
    } catch (e) {
        yield put(ContractorActions.failureContractorBalance(e));
    }
}

function * watchFetchContractorsList() {
    yield takeLatest(CONTRACTORS_LIST + Store.FETCH, fetchContractorsList);
}

function * watchFetchContractor() {
    yield takeLatest(CONTRACTOR + Store.FETCH, fetchContractor);
}

function * watchCreateContractor() {
    yield takeLatest(CONTRACTOR + Store.CREATE, createContractor);
}

function * watchAttachContractor() {
    yield takeLatest(CONTRACTOR + ATTACH, attachContractor);
}

function * watchUpdateContractor() {
    yield takeLatest(CONTRACTOR + Store.UPDATE, updateContractor);
}

function * watchDeleteContractor() {
    yield takeLatest(CONTRACTOR + Store.DELETE, deleteContractor);
}

function * watchFetchContractorBalance() {
    yield takeLatest(CONTRACTOR_BALANCE + Store.FETCH, fetchBalanceContractor)
}

export default function* watcher() {
    yield fork(watchFetchContractorsList);
    yield fork(watchFetchContractor);
    yield fork(watchCreateContractor);
    yield fork(watchAttachContractor);
    yield fork(watchUpdateContractor);
    yield fork(watchDeleteContractor);
    yield fork(watchFetchContractorBalance);
}
