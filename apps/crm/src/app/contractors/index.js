import ContractorsPage from './pages/contractors-page';
import ContractorDetailsPage from './pages/contractors-details-page';

import { Payments } from './pages/payments';
import { Documents } from './pages/documents';
import { History } from './pages/history';
import { Orders } from './pages/orders';
import { Requisites } from './pages/requisites';

export {
    ContractorsPage,
    ContractorDetailsPage,

    Payments,
    Documents,
    History,
    Orders,
    Requisites,
};
