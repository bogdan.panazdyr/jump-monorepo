import React from 'react';
import { NavLink } from 'react-router-dom';

import {
    Header,
    ParagraphV2 as Paragraph,
    Icons24,
    Description,
} from '@justlook/uikit';
import { Config } from '@justlook/core';

import { RoundFrame } from 'components';
import { useBalance } from 'domains/dashboard';

import { LoaderBalance } from './loaders/loader-balance';
import { RefreshBalance } from './refresh-balance';

import cx from './balance-info.module.scss';

const banksUrl = `${Config.crm_type === 'taxi' ? '' : 'settings-'}banks`;

export const BalanceInfo = (props) => {
    const { balanceLabel, syncTime, isFetchingBalance, refreshBalance } = useBalance();

    if(isFetchingBalance) { return <LoaderBalance/> }

    return (
        <div className={cx.base} {...props}>
            <RoundFrame color='#CCE5FF'>
                <Icons24.IconWallet/>
            </RoundFrame>

            <div>
                <Paragraph.LH24>На счетах</Paragraph.LH24>
                <Header.H1 className={cx.balance}>
                    { balanceLabel }

                    <RefreshBalance onRefresh={ refreshBalance } />

                    <Description.LH16 className={cx.time}>{ syncTime }</Description.LH16>
                </Header.H1>

                <NavLink to={banksUrl}>
                    <Paragraph.LH24 className={cx.link}>Управление счетами</Paragraph.LH24>
                </NavLink>
            </div>
        </div>
    )
}
