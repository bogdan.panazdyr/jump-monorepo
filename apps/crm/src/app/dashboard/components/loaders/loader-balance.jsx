import React from 'react';
import PropTypes from 'prop-types';
import ContentLoader from 'react-content-loader';

export const LoaderBalance = ({ props }) => {
    return (
        <ContentLoader
            speed={1}
            width={332}
            height={72}
            viewBox='0 0 332 72'
            backgroundColor='#E7EAEE'
            foregroundColor='#DBDDE6'
            style={{ display: 'block' }}
            {...props}
        >
            <circle cx='36' cy='36' r='36' />
            <rect x='88' y='0' rx='4' ry='4' width='100' height='20' />
            <rect x='88' y='24' rx='4' ry='4' width='80' height='20' />
            <rect x='88' y='48' rx='4' ry='4' width='200' height='20' />
        </ContentLoader>
    )
}

LoaderBalance.propTypes = {
    props: PropTypes.any
}
