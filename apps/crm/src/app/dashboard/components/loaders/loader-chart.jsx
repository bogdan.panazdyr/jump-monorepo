import React from 'react';
import PropTypes from 'prop-types';
import ContentLoader from 'react-content-loader';

export const LoaderChart = ({ props }) => {
    return (
        <ContentLoader
            speed={1}
            width={482}
            height={256}
            viewBox='0 0 482 256'
            backgroundColor='#E7EAEE'
            foregroundColor='#DBDDE6'
            style={{ display: 'block' }}
            {...props}
        >
            <rect x='0' y='0' rx='4' ry='4' width={482} height={256} />
        </ContentLoader>
    )
}

LoaderChart.propTypes = {
    props: PropTypes.any,
}
