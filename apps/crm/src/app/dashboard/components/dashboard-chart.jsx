import React, { useEffect, useState } from 'react';
import classes from 'classnames';
import PropTypes from 'prop-types';

import { Header } from '@justlook/uikit';

import { useMoney } from 'hooks/use-money';
import { LineChart, PieChart, Button, ButtonGroup } from 'components';
import { useChartDashboard } from 'domains/dashboard';

import { LoaderChart } from './loaders/loader-chart';

import cx from './dashboard-chart.module.scss';

const charts = {
    line: LineChart,
    pie: PieChart,
}

export const Chart = ({ filter, queryTabs, query, type, changeIsRequest, title }) => {
    const [ fetchedCharts, setFetchedCharts ] = useState(queryTabs ? queryTabs[0].query : query);

    const { currency, format: formatMoney } = useMoney()
    const classHeader = classes(cx.header, { [cx.navHeader]: queryTabs });

    const ChartsComponent = charts[type];

    const needShowStorageInfo = ['aggregators_trips', 'aggregators_commissions'].indexOf(fetchedCharts) > -1;

    const { data, isLoading, isFetching } = useChartDashboard(fetchedCharts, filter);

    const onClick = (e) => {
        const query = e.currentTarget.dataset.query;

        setFetchedCharts(query);
    }

    useEffect(() => {
        changeIsRequest(fetchedCharts, isLoading || isFetching);
    }, [changeIsRequest, fetchedCharts, isLoading, isFetching]);

    if(!data) { return <LoaderChart/> }

    return (
        <div className={cx.base}>
            <div className={classHeader}>
                <Header.H1>{ title ? title : data.meta.title }</Header.H1>
                {
                    !isLoading &&
                    <Header.H1>
                        { data?.meta?.total && formatMoney(data.meta.total) }&nbsp;
                        { data?.meta?.currency && currency }
                    </Header.H1>
                }
            </div>

            {
                queryTabs &&
                <ButtonGroup style={{marginBottom: '1rem'}}>
                    {queryTabs.map(item => (
                        <Button
                            key={item.query}
                            data-query={item.query}
                            active={fetchedCharts === item.query}
                            onClick={onClick}
                        >
                            {item.label}
                        </Button>
                    ))}
                </ButtonGroup>
            }

            <ChartsComponent
                isRequest={isFetching}
                data={data}
                XDataKey='tooltip'
                isStorageInfo={needShowStorageInfo}
            />
        </div>
    )
}

Chart.propTypes = {
    filter: PropTypes.object,
    queryTabs: PropTypes.array,
    query: PropTypes.string,
    type: PropTypes.oneOf([ 'line', 'pie' ]),
    changeIsRequest: PropTypes.func,
    title: PropTypes.string,
}
