import React from 'react';
import PropTypes from 'prop-types';

import { Select } from '@justlook/uikit';

import { pureFilter } from 'hooks/use-search-filter';
import { useControlRoom } from 'domains/dashboard';

import { LoaderControlRoom } from './loaders/loader-control-room';

import cx from './control-room.module.scss';

export const ControlRoom = ({ onChange, filter, isRequest }) => {
    const { options, isLoading } = useControlRoom();

    if(isLoading) { return <LoaderControlRoom/> }

    const onChangeHandler = ({ value: exchanges_profiles_id }) => {
        onChange(pureFilter({...filter, exchanges_profiles_id}, {}));
    }

    return (
        <div className={cx.base}>
            <Select
                options={options}
                onChange={onChangeHandler}
                defaultValue={{ value: null, label: 'Все диспетчерские' }}
                disabled={isRequest}
            />
        </div>
    )
}

ControlRoom.propTypes = {
    onChange: PropTypes.func,
    filter: PropTypes.object,
    isRequest: PropTypes.bool
}
