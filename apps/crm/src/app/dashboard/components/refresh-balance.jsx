import React from 'react';
import PropTypes from 'prop-types';

import { Icons16 } from '@justlook/uikit';

import { useRefreshBalance } from 'domains/dashboard';

import cx from './refresh-balance.module.scss';

export const RefreshBalance = ({ onRefresh }) => {
    const { startSyncJob, isFetchingBalance } = useRefreshBalance(onRefresh);

    const Icon = Icons16[isFetchingBalance ? 'IconLoader' : 'IconRefresh'];

    return (
        <div className={cx.base}>
            <Icon onClick={startSyncJob} className={cx.refresh} />
        </div>
    )
}

RefreshBalance.propTypes = {
    onRefresh: PropTypes.func
}
