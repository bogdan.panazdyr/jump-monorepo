import React from 'react';

import { useMoney } from 'hooks/use-money';
import { SummaryBox } from 'components';
import { useSummary } from 'domains/dashboard';

import { LoaderEvents } from './loaders/loader-dasboard-events';

import cx from './dashboard-events.module.scss';

export const DashboardEvents = () => {
    const { format } = useMoney();

    const { summary, isLoading } = useSummary();

    if(isLoading) { return <LoaderEvents/> }

    return (
        <div className={cx.base}>
            { summary.map(item => <SummaryBox
                    key={item.description}
                    title={format(item.metric)}
                    description={item.description}
                    url={item.url}
                />
            )}
        </div>
    )
}
