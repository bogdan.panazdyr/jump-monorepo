import React, { Fragment, useCallback, useState } from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';

import { Config } from '@justlook/core';

import { useHasPermission } from 'app/profile/store/selectors';
import { OneColumnsLayout } from 'components/menu';
import { ContentBox, Box, withPermissions, PeriodControl } from 'components';
import { isLoadingCharts, useDashboardFilter } from 'domains/dashboard';

import { BalanceInfo } from '../components/balance-info';
import { DashboardHeader } from '../components/dashboard-header';
import { DashboardEvents } from '../components/dashboard-events';
import { Chart } from '../components/dashboard-chart';

import cx from './dashboard-page.module.scss';

const Page = ({ type }) => {
    const canViewBalance = useHasPermission('read-bank-account');

    const { filter } = useDashboardFilter();

    const [ loaderCharts, setLoaderCharts ] = useState({});

    const onSetLoadChart = useCallback((chartType, status) => {
        setLoaderCharts(prevState => ({...prevState, [chartType]: status}));
    }, [setLoaderCharts]);

    const isFetchingData = isLoadingCharts(type, loaderCharts);

    return (
        <Fragment>
            <Helmet>
                <title>Сводка - {Config.brandName}</title>
            </Helmet>

            <OneColumnsLayout header={<DashboardHeader/>}>
                <ContentBox className={cx.base}>
                    {
                        canViewBalance && (
                            <Box className={cx.content} mt={18} mb={24}>
                                <BalanceInfo/>
                            </Box>
                        )
                    }

                    <Box mb={32}>
                        <DashboardEvents />
                    </Box>

                    <PeriodControl isRequest={isFetchingData}/>

                    <Box className={cx.charts} mt={8}>
                        {/*Скрыто на время* (DEV-2905)*/}
                        {/*<Chart filter={filter} query='delivery_bids' type='line'*/}
                        {/*       changeIsRequest={onSetLoadChart}*/}
                        {/*/>*/}
                        <Chart filter={filter} query='drivers_payments' type='line'
                               changeIsRequest={onSetLoadChart}
                        />
                    </Box>
                </ContentBox>
            </OneColumnsLayout>
        </Fragment>
    );
};

Page.propTypes = {
    type: PropTypes.string.isRequired,
}

export default withPermissions(Page);
