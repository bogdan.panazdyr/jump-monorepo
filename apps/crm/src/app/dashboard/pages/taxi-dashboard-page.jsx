import React, { Fragment, useState, useCallback } from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';

import { Config } from '@justlook/core';

import { useHasPermission } from 'app/profile/store/selectors';
import { OneColumnsLayout } from 'components/menu';
import { ContentBox, Box, withPermissions, PeriodControl } from 'components';
import { useDashboardFilter, isLoadingCharts } from 'domains/dashboard';

import { DashboardHeader } from '../components/dashboard-header';
import { DashboardEvents } from '../components/dashboard-events';
import { ControlRoom } from '../components/control-room';
import { Chart } from '../components/dashboard-chart';
import { BalanceInfo } from '../components/balance-info'


import cx from './dashboard-page.module.scss';

const Page = ({ type }) => {
    const canViewBalance = useHasPermission('read-bank-account');

    const { filter, updateFilter } = useDashboardFilter();

    const [ loaderCharts, setLoaderCharts ] = useState({});

    const onSetLoadChart = useCallback((chartType, status) => {
        setLoaderCharts(prevState => ({...prevState, [chartType]: status}));
    }, [setLoaderCharts]);

    const isFetchingData = isLoadingCharts(type, loaderCharts);

    return (
        <Fragment>
            <Helmet>
                <title>Сводка - {Config.brandName}</title>
            </Helmet>
            <OneColumnsLayout header={<DashboardHeader/>} isProgress={isFetchingData}>
                <ContentBox className={cx.base}>
                    {
                        canViewBalance && (
                            <Box className={cx.content} mb={22}>
                                <BalanceInfo/>
                            </Box>
                        )
                    }

                    <Box mb={30}>
                        <DashboardEvents />
                    </Box>

                    <Box className={cx.control}>
                        <PeriodControl isRequest={isFetchingData}/>
                        <ControlRoom onChange={updateFilter} filter={filter} isRequest={isFetchingData}/>
                    </Box>

                    <Box className={cx.charts} mt={8}>
                        <Box className={cx['one-column']}>
                            <Chart filter={filter} queryTabs={[
                                { label: 'Выплаты', query: 'aggregators_payments' },
                                { label: 'Поездки', query: 'aggregators_trips' },
                                { label: 'Комиссии', query: 'aggregators_commissions' },
                            ]} type='pie' changeIsRequest={onSetLoadChart}/>

                            <Chart filter={filter} queryTabs={[
                                { label: 'Регистрации', query: 'drivers_registrations' },
                                { label: 'Активность', query: 'drivers_activities' },
                            ]} type='line' changeIsRequest={onSetLoadChart}/>
                        </Box>
                        <Box className={cx['two-column']}>
                            <Chart filter={filter} query='drivers_payments' type='line' changeIsRequest={onSetLoadChart}/>
                            <Chart filter={filter} query='drivers_commissions' type='line' changeIsRequest={onSetLoadChart}/>
                        </Box>
                    </Box>
                </ContentBox>
            </OneColumnsLayout>
        </Fragment>
    )
}

Page.propTypes = {
    type: PropTypes.string.isRequired,
}

export default withPermissions(Page);
