import React, { Fragment } from 'react';
import { Helmet } from 'react-helmet';

import { useIfMediaScreen } from '@justlook/uikit/lib/hooks';
import { Config } from '@justlook/core/src';
import { MobileHeader, Icons24, Header } from '@justlook/uikit';

import { OneColumnsLayout, useMenu } from 'components/menu';
import { ContentBox, withPermissions } from 'components';

const PageHeader = () => {
    const isMobile = useIfMediaScreen();
    const { onExpandedMobile } = useMenu();

    return <MobileHeader header={'Сводка'}>
        {
            isMobile &&
            <MobileHeader.Button icon={<Icons24.IconMenu/>}
                                 onClick={onExpandedMobile}/>
        }
    </MobileHeader>;
};

const Page = () => {
    if (Config.home_page) {
        window.location = Config.home_page;
    }

    return (
        <Fragment>
            <Helmet>
                <title>Сводка - {Config.brandName}</title>
            </Helmet>

            <OneColumnsLayout header={<PageHeader/>}>
                <ContentBox>
                    {Config.home_page && <Header.H3>{`Перенаправляем на ${Config.home_page}`}</Header.H3>}
                </ContentBox>
            </OneColumnsLayout>
        </Fragment>
    );
};

export default withPermissions(Page);
