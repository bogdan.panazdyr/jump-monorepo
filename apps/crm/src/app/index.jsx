import React, { Fragment, useEffect } from 'react';
import PropTypes from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';

import routes from 'app/routes';
import { AuthorizedPage } from 'app/auth';
import { User } from 'components/menu';
import { SplashScreen, ErrorScreen } from '@justlook/uikit';
import { Components, Utils } from '@justlook/core';
import { useIfMediaScreen } from '@justlook/uikit/src/hooks';

import { getProfile } from 'app/profile/store/selectors';
import {
    isAuthorized, isLoading, getError, intercomConfig, getMenu, getFeatures
} from 'app/init/store/selectors';
import { runApp } from 'app/init/store/actions';

const stateIsAuth = (state) => !state.isLoading && !state.error && !state.isAuthorized;
const stateIsLoading = (state) => state.isLoading && !state.error;
const stateIsReady = (state) => !state.isLoading && !state.error && state.isAuthorized;
const stateIsFatal = (state) => !!state.error;

const newUser = (profile) => {
    const user = new User();

    if (profile) {
        user.setId(profile.id)
            .setParentId(profile?.parent?.id)
            .setFullName(profile.name)
            .setEmail(profile?.contacts?.email)
            .setCompany(profile.name);
    }

    return user;
};

const App = () => {
    const isMobile = useIfMediaScreen();

    const features = useSelector(getFeatures);
    const user = newUser(useSelector(getProfile));
    const menu = useSelector(getMenu);
    const error = useSelector(getError);
    const intercom = useSelector(intercomConfig);
    const dispatch = useDispatch();

    const state = {
        error,
        isAuthorized: useSelector(isAuthorized),
        isLoading: useSelector(isLoading)
    };

    useEffect(() => {dispatch(runApp())}, [dispatch]);

    return (
        <Fragment>
            { stateIsFatal(state) && <ErrorScreen error={error} /> }
            { stateIsLoading(state) && <SplashScreen /> }
            { stateIsAuth(state) && <AuthorizedPage /> }
            { stateIsReady(state) && (
                <Fragment>
                    { routes(menu, user, features) }

                    <Components.ChatIntercom
                        config={intercom}
                        customLauncher={isMobile ? '#intercom-launcher' : null}
                        createdAt={Utils.Token.getTokenStorage()?.created_at}
                    />
                </Fragment>
            ) }
        </Fragment>
    )
};

App.propTypes = {
    children: PropTypes.any,
    app: PropTypes.object,
    user: PropTypes.object,
    history: PropTypes.object,
};

export default App;
