import React, { Fragment, useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import { ParagraphV2 as Paragraph, Button } from '@justlook/uikit';

import { Modal } from 'components';
import { SearchContractorModal, CreatePaymentModal } from 'components/modals';
import { useOnlinePermissions } from 'domains/payments/queries';

const SEARCH_CONTRACTOR = 'search_contractor';
const CREATE_PAYMENT = 'create_payment';

const SmartPaymentByStep = ({ onClose, onSuccess }) => {
    const { isAllow, isPermissionsLoading, message, actionLabel } = useOnlinePermissions();

    const [ store, setStore ] = useState(null);

    const [ step, setStep ] = useState(SEARCH_CONTRACTOR);

    const onGoStep = (step, payload) => {
        setStore(prevState => ({ ...prevState, ...payload }));

        setStep(step);
    }

    const clearStore = () => {
        setStore(null);
    }

    useEffect(() => {
        return () => setStore(null);
    }, []);

    if (!isPermissionsLoading && !isAllow) {
        return (
            <Modal
                title='Новая выплата'
                onClose={onClose}
                majorButton={
                    <Button styling='hollow-border' onClick={onClose}>
                        Отмена
                    </Button>
                }
            >
                <Paragraph.LH24 style={{ textAlign: 'center' }}>
                    {message}
                </Paragraph.LH24>
            </Modal>
        )
    }

    return (
        <Fragment>
            {
                step === SEARCH_CONTRACTOR &&
                <SearchContractorModal
                    initialState={store}
                    onChange={clearStore}
                    isLoading={isPermissionsLoading}
                    onSubmit={(payload) => onGoStep(CREATE_PAYMENT, payload)}
                    onDismiss={onClose}
                />
            }

            {
                step === CREATE_PAYMENT &&
                <CreatePaymentModal
                    initialState={store}
                    submitLabel={actionLabel}
                    onDismiss={onClose}
                    isLoading={isPermissionsLoading}
                    onBack={(payload) => onGoStep(SEARCH_CONTRACTOR, payload)}
                    onSuccess={onSuccess}
                />
            }
        </Fragment>
    )
}

SmartPaymentByStep.propTypes = {
    onClose: PropTypes.func,
    onSuccess: PropTypes.func,
}

export default SmartPaymentByStep;
