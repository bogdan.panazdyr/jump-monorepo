import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import classname from 'classnames';

import { Checkbox } from '@justlook/uikit';

import { useMoney } from 'hooks/use-money';
import { NewTable as Table, SplashScreen, ReceiptCell, Status } from 'components';
import { useSelectedPaymentsContext } from 'domains/payments/contexts';
import {
    useBulkActionsForPayments,
    getStatusIcon,
    isDeclinedWithActions, isWaitingConfirm
} from 'domains/payments/helpers';
import { Payment } from 'domains/payments/models';

import { PaymentEmptyContent } from './payment-empty-content';
import { MobileRowPayment } from './mobile-row-payment';

import cx from './payments-list.module.scss';

const PaymentsList = ({ isLoading, isFetching, isDisabled, payments, onSelectPayment, bulkActions }) => {
    const cls = classname(cx.paymentsList, {[cx.is_loading]: isFetching})

    const { currency } = useMoney();
    const { statusId, isSelected, hasSelection, hasPartialSelection,  onSelectAll, onTogglePayment } = useSelectedPaymentsContext();
    const { confirm, cancel, refund, repeat } = useBulkActionsForPayments(bulkActions);

    const canProcessForWaitingStatus = (confirm || cancel) && isWaitingConfirm(statusId);
    const canProcessForDeclinedStatus =  (cancel || refund || repeat) && isDeclinedWithActions(statusId);
    const hasBulkActions = canProcessForWaitingStatus || canProcessForDeclinedStatus;

    if (isLoading) return <SplashScreen/>

    if (payments.length === 0) return <PaymentEmptyContent/>

    return (
        <Fragment>
            <Table className={cls}>
                <Table.Head
                    cells={[
                        { label: 'Изменено', className: cx.date },
                        { label: 'Получатель', className: cx.contractor },
                        { label: `Заявка, ${currency}`, className: cx.right },
                        { label: `Выплата, ${currency}`, className: cx.right },
                        { label: 'Чек', className: cx.receipt },
                        { label: 'Статус', className: cx.status },
                        { label: 'Создатель' },
                        { label: 'Юрлицо' },
                    ]}
                    renderCheckBox={
                        hasBulkActions &&
                        <Checkbox
                            disabled={isDisabled}
                            id='selectAll'
                            checked={hasSelection}
                            mixed={hasPartialSelection}
                            onChange={(value) => onSelectAll(value)}
                        />
                    }
                />

                {payments.map(item => {
                    const payment = new Payment(item);

                    const renderCheckBox = () => (
                        <Checkbox
                            disabled={isDisabled}
                            id={payment.getId()}
                            checked={isSelected(payment.getId())}
                            onChange={(value) => onTogglePayment(payment.getId(), value)}
                        />
                    )

                    return (
                        <Table.Row
                            key={payment.getId()}
                            onClick={() => onSelectPayment(payment.getId())}
                            mobileRow={
                                <MobileRowPayment
                                    data={payment}
                                    confirm={ hasBulkActions && renderCheckBox() }
                                    onClick={() => onSelectPayment(payment.getId())}
                                />
                            }
                            renderCheckboxCell={ hasBulkActions && renderCheckBox() }
                        >
                            <Table.Cell className={cx.date}>
                                {payment.getFullUpdatedAt()}
                            </Table.Cell>
                            <Table.Cell className={cx.contractor}>
                                <span title={payment.getContractorFullName()}>
                                    {payment.getContractorShortName()}
                                </span>
                            </Table.Cell>
                            <Table.Cell className={cx.right}>{payment.getAmount(true)}</Table.Cell>
                            <Table.Cell className={cx.right}>{payment.getAmountPaid(true)}</Table.Cell>
                            <ReceiptCell payment={item} className={cx.receipt}/>
                            <Table.Cell className={cx.status}>
                                <Status
                                    icon={getStatusIcon(payment.getStatusTheme())}
                                    text={payment.getStatus()}
                                    color={payment.getStatusTheme()}
                                />
                            </Table.Cell>
                            <Table.Cell>{payment.getCreator()}</Table.Cell>
                            <Table.Cell>{payment.getAgent()}</Table.Cell>
                        </Table.Row>
                    )
                })}
            </Table>
        </Fragment>
    )
};

PaymentsList.propTypes = {
    onSelectPayment: PropTypes.func.isRequired,
    isLoading: PropTypes.bool.isRequired,
    isFetching: PropTypes.bool.isRequired,
    isDisabled: PropTypes.bool.isRequired,
    payments: PropTypes.array.isRequired,
    bulkActions: PropTypes.array,
}

export default PaymentsList
