import React from 'react';
import PropTypes from 'prop-types';
import classes from 'classnames';

import { Icons24, MobileHeader, Button, Checkbox, ParagraphV2 as Paragraph } from '@justlook/uikit';
import { useIfMediaScreen } from '@justlook/uikit/lib/hooks';

import { useMenu } from 'components/menu';
import { DropActions } from 'components';
import { useHasPermission } from 'app/profile/store/selectors';
import { useSelectedPaymentsContext, useNewPaymentContext } from 'domains/payments/contexts';
import { useBulkActionsForPayments } from 'domains/payments/helpers';
import { ACTION_CANCEL, ACTION_REFUND, ACTION_REPEAT } from 'domains/payments/constants';

import cx from './header-payments-page.module.scss';

const HeaderPaymentsPage = ({ setIsShowConfirmModal, setTypeActionWithPayment, bulkActions }) => {
    const isMobile = useIfMediaScreen();
    const canCreatePayment = useHasPermission('write-payment');

    const { onExpandedMobile } = useMenu();
    const { onOpenNewPaymentForm } = useNewPaymentContext();

    const { hasSelection, isLoadingInfo, onSelectAll, hasPartialSelection, listPayments } = useSelectedPaymentsContext();
    const { repeat, refund, confirm, cancel } = useBulkActionsForPayments(bulkActions);

    const clsSelectAll = classes(cx.selectAll, {
        [cx.disabled]: isLoadingInfo
    })

    if(isMobile && hasSelection) {
        return (
            <div className={cx.confirm}>
                <div className={cx.checkbox}>
                    <Checkbox
                        disabled={isLoadingInfo}
                        id='selectAll'
                        checked={hasSelection}
                        mixed={hasPartialSelection}
                        onChange={(value) => onSelectAll(value)}
                    />

                    <Paragraph.LH24
                        className={clsSelectAll}
                        onClick={() => onSelectAll(true)}
                    >
                        Выбрать все
                    </Paragraph.LH24>
                </div>

                <div className={cx.actionsWrap}>
                    {
                        confirm &&
                        <Button
                            disabled={isLoadingInfo}
                            onClick={setIsShowConfirmModal}
                        >
                            Подтвердить
                        </Button>
                    }

                    {
                        repeat &&
                        <Button
                            disabled={isLoadingInfo}
                            onClick={() => setTypeActionWithPayment(ACTION_REPEAT)}
                        >
                            Повторить
                        </Button>
                    }

                    <DropActions
                        disabled={isLoadingInfo}
                        positionDropDown='bottom-left'
                        className={cx.actions}
                        actions={[
                            {
                                label: `Отменить ${listPayments.length === 1 ? 'выплату' : 'выплаты'}`,
                                action: cancel ? () => setTypeActionWithPayment(ACTION_CANCEL) : null
                            },
                            {
                                label: 'Зачислить на баланс исполнителя',
                                action: refund ? () => setTypeActionWithPayment(ACTION_REFUND) : null,
                            }
                        ]}
                    />
                </div>
            </div>
        )
    }

    return (
        <MobileHeader header='Выплаты'>
            {
                canCreatePayment && <MobileHeader.Button
                    onClick={onOpenNewPaymentForm}
                    icon={<Icons24.IconPlus/>}
                />
            }

            { isMobile && <MobileHeader.Button icon={<Icons24.IconMenu/>} onClick={onExpandedMobile}/> }
        </MobileHeader>
    )
}

HeaderPaymentsPage.propTypes = {
    setIsShowConfirmModal: PropTypes.func,
    setTypeActionWithPayment: PropTypes.func,
    bulkActions: PropTypes.array,
}

export default HeaderPaymentsPage;
