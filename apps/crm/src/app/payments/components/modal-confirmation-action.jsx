import React from 'react';
import PropTypes from 'prop-types';

import { Button, ParagraphV2 as Paragraph } from '@justlook/uikit';

import { Modal } from 'components';
import { ACTION_REFUND, ACTION_CANCEL, ACTION_REPEAT } from 'domains/payments/constants';
import { useCancelPayments, useRefundPayments, useRepeatPayments } from 'domains/payments/mutators';
import { useSelectedPaymentsContext } from 'domains/payments/contexts';

import cx from './modal-confirmation-action.module.scss';

export const ModalConfirmationAction = ({ actionType, onSuccess, onDismiss }) => {
    const { listPayments, statistic, onClearAll } = useSelectedPaymentsContext();

    const onSuccessMutate = async () => {
        await onClearAll();
        await onSuccess();
        await onDismiss();
    }

    const isOne = listPayments.length === 1;

    const { onRepeat, isRepeatMutating } = useRepeatPayments(onSuccessMutate);
    const { onCancel, isCancelMutating } = useCancelPayments(onSuccessMutate);
    const { onRefund, isRefundMutating } = useRefundPayments(onSuccessMutate);

    const actions = {
        [ACTION_REFUND]: {
            title: 'Зачисление на баланс исполнителей',
            submitLabel: 'Зачислить на баланс исполнителя',
            action: onRefund,
        },
        [ACTION_CANCEL]: {
            title: `Отмена ${isOne ? 'выплаты' : 'выплат'}`,
            submitLabel: `Отменить ${isOne ? 'выплату' : 'выплаты'}`,
            action: onCancel,
        },
        [ACTION_REPEAT]: {
            title: `Повтор ${isOne ? 'выплаты' : 'выплат'}`,
            submitLabel: 'Повторить',
            action: onRepeat,
        },
    }

    const isLoading = isRepeatMutating || isCancelMutating || isRefundMutating;

    return (
        <Modal
            title={ actions[actionType].title }
            onClose={isLoading ? null : onDismiss}
            dismissButton={
                <Button
                    disabled={isLoading}
                    styling='hollow-border'
                    onClick={onDismiss}
                >
                    Отмена
                </Button>
            }
            majorButton={
                <Button
                    loading={isLoading}
                    disabled={isLoading}
                    onClick={() => actions[actionType].action(listPayments.join(','))}
                >
                    { actions[actionType].submitLabel }
                </Button>
            }
        >
            <Paragraph.LH24 className={cx.info}>
                { statistic }
            </Paragraph.LH24>
        </Modal>
    )
}

ModalConfirmationAction.propTypes = {
    actionType: PropTypes.string.isRequired,
    onSuccess: PropTypes.func.isRequired,
    onDismiss: PropTypes.func.isRequired,
}
