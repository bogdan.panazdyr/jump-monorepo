import React from 'react';

import { EmptyContent } from 'components';
import { useNewPaymentContext } from 'domains/payments/contexts';

import { useEmptyLabel } from '../hooks/use-empty-label';

export const PaymentEmptyContent = () => {
    const { onOpenNewPaymentForm } = useNewPaymentContext();

    const { label, canCreate } = useEmptyLabel();

    return (
        <EmptyContent
            message={label}
            actionLabel={canCreate && 'Создать'}
            action={onOpenNewPaymentForm}
            isMute
        />
    )
}
