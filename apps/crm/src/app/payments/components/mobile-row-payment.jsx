import React from 'react';
import PropTypes from 'prop-types';
import classes from 'classnames';

import { ParagraphV2 as Paragraph, Description, Icons24 } from '@justlook/uikit';

import { useMoney } from 'hooks/use-money';
import { Status, ReceiptCell } from 'components';
import { getStatusIcon } from 'domains/payments/helpers';

import cx from './mobile-row-payment.module.scss';

export const MobileRowPayment = ({ data, confirm, onClick }) => {
    const { currency } = useMoney();

    const cls = classes(cx.base, { [cx.confirm]: confirm })

    return (
        <div className={cls}>
            {confirm}
            <div onClick={onClick}>
                <div className={cx.row}>
                    <Paragraph.LH18>{ data.getContractorShortName() }</Paragraph.LH18>
                    <div className={cx.date}>
                        <Description.LH16 className={cx.mute}>{ data.getMobileDescription(', ', currency) }</Description.LH16>
                        <Icons24.IconArrowRight className={cx.mute}/>
                    </div>
                </div>

                <div className={cx.row}>
                    <Paragraph.LH18 className={cx.mute}>Заявка:</Paragraph.LH18>
                    <Paragraph.LH18>{ data.getAmount() + ' ' + currency }</Paragraph.LH18>
                </div>

                <div className={cx.row}>
                    <Paragraph.LH18 className={cx.mute}>Выплата:</Paragraph.LH18>
                    <Paragraph.LH18>{ data.getAmountPaid() + ' ' + currency }</Paragraph.LH18>
                </div>

                <div className={cx.row}>
                    <Status
                        icon={getStatusIcon(data.getStatusTheme())}
                        text={data.getStatus()}
                        color={data.getStatusTheme()}
                    />

                    <ReceiptCell payment={data.getOriginal()} className={cx.receipt}/>
                </div>
            </div>
        </div>
    )
}

MobileRowPayment.propTypes = {
    data: PropTypes.any,
    confirm: PropTypes.any,
    onClick: PropTypes.func,
}
