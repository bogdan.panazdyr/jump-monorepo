import React from 'react';
import PropTypes from 'prop-types';

import { useIfMediaScreen } from '@justlook/uikit/lib/hooks';
import { Select } from '@justlook/uikit';

import { Button, ButtonGroup } from 'components';
import { useQuerySearch } from 'hooks/use-search-filter';
import { PAYMENT_STATUS, isActiveStatus, getDefaultFilter, getFilterOptions } from 'domains/payments/helpers';

import cx from './payments-filter.module.scss';

const PaymentsFilter = ({ children, badges, disabled, onUpdate }) => {
    const query = useQuerySearch();
    const isMobile = useIfMediaScreen();

    const onClick = (e) => {
        const status_id = e.currentTarget.dataset.status;

        onUpdate({ status_id, page: null });
    }

    if(isMobile) {
        return (
            <div className={cx.mobileFilter}>
                <Select
                    defaultValue={getDefaultFilter(query)}
                    options={getFilterOptions(badges)}
                    onChange={({ value }) => onUpdate({ status_id: value, page: null })}
                    disabled={disabled}
                    isSearchable={false}
                />
            </div>
        )
    }

    return (
        <div className={cx.base}>
            <ButtonGroup style={{marginRight: '1rem'}}>
                <Button
                    data-status={PAYMENT_STATUS.ALL}
                    active={isActiveStatus(PAYMENT_STATUS.ALL, query)}
                    disabled={disabled}
                    onClick={onClick}
                >
                    Все
                </Button>

                <Button
                    data-status={PAYMENT_STATUS.WAITING_CONFIRMATION}
                    badge={badges.getCount(PAYMENT_STATUS.WAITING_CONFIRMATION)}
                    active={isActiveStatus(PAYMENT_STATUS.WAITING_CONFIRMATION, query)}
                    disabled={disabled}
                    onClick={onClick}
                >
                    Нужно подтверждение
                </Button>

                <Button
                    data-status={PAYMENT_STATUS.DECLINED_WITH_ACTIONS}
                    badge={badges.getCount(PAYMENT_STATUS.DECLINED_WITH_ACTIONS)}
                    active={isActiveStatus(PAYMENT_STATUS.DECLINED_WITH_ACTIONS, query)}
                    disabled={disabled}
                    onClick={onClick}
                >
                    Ошибка выплаты
                </Button>

                <Button
                    data-status={PAYMENT_STATUS.DECLINED}
                    active={isActiveStatus(PAYMENT_STATUS.DECLINED, query)}
                    disabled={disabled}
                    onClick={onClick}
                >
                    Отменённые
                </Button>

                <Button
                    data-status={PAYMENT_STATUS.PAYED}
                    active={isActiveStatus(PAYMENT_STATUS.PAYED, query)}
                    disabled={disabled}
                    onClick={onClick}
                >
                    Оплаченные
                </Button>

            </ButtonGroup>

            <div className={cx.children}>
                {children}
            </div>
        </div>
    )
}

PaymentsFilter.propTypes = {
    badges: PropTypes.object.isRequired,
    onUpdate: PropTypes.func.isRequired,
    disabled: PropTypes.bool,
    children: PropTypes.any,
}

export default PaymentsFilter;
