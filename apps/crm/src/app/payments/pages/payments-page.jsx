import React, { Fragment, useState } from 'react';
import { Helmet } from 'react-helmet';
import classes from 'classnames';

import { Config } from '@justlook/core';
import { Pagination, Button } from '@justlook/uikit';
import { useIfMediaScreen } from '@justlook/uikit/src/hooks';

import { OneColumnsLayout } from 'components/menu';
import { PaymentsDetailModal, PaymentListConfirmModal } from 'components/modals';
import { Box, withPermissions, SnackbarMessage, DropActions } from 'components';
import {
    NewPaymentFormProvider, useNewPaymentContext, useSelectedPaymentsContext,
    SelectedPaymentsContextProvider
} from 'domains/payments/contexts';
import { usePaymentHasConfirmStatus, useBulkActionsForPayments } from 'domains/payments/helpers';
import { usePayments, usePaymentStatusBadges } from 'domains/payments/queries';
import { ACTION_CANCEL, ACTION_REFUND, ACTION_REPEAT } from 'domains/payments/constants';

import {
    HeaderPaymentsPage,
    PaymentsFilter,
    PaymentsList,
    SmartPaymentByStep,
    ModalConfirmationAction
} from '../components';

import cx from './payments-page.module.scss';

const Page = () => {
    const isMedia = useIfMediaScreen();
    const { badgesData, refreshBadges } = usePaymentStatusBadges();
    const { paymentsData, paymentsMeta, isPaymentsLoading, isPaymentsFetching, refreshPayments, bulkActions, updateFilter, getPaymentById } = usePayments();
    const { isShowNewPaymentForm, onCloseNewPaymentForm } = useNewPaymentContext();
    const { statistic, hasSelection, isLoadingInfo, onTogglePayment, listPayments } = useSelectedPaymentsContext();
    const canConfirmPayment = usePaymentHasConfirmStatus();

    const { confirm, cancel, refund, repeat } = useBulkActionsForPayments(bulkActions);

    const [ selectPayment, setSelectPayment ] = useState(null);
    const [ isShowConfirmModal, setIsShowConfirmModal ] = useState(false);
    const [ typeActionWithPayment, setTypeActionWithPayment ] = useState(null);

    const clsScroll = classes(cx.scroll, {
        [cx.hasSelection]: hasSelection && confirm
    })

    const wrapperProps = {
        pt: isMedia ? 8 : 24,
        pr: isMedia ? 16 : 24,
        pb: 24,
        pl: isMedia ? 16 : 24,
    }

    const onSelectPayment = (id) => {
        setSelectPayment(getPaymentById(id));
    }

    const { last_page = 1, current_page = 1 } = paymentsMeta;

    const onSuccessCreatePayment = (payment) => {
        onCloseNewPaymentForm();
        refreshPayments();
        refreshBadges();
        canConfirmPayment(payment) && setSelectPayment(payment);
    }

    const onUpdatePayment = (id) => {
        onTogglePayment(id, false)
        refreshPayments();
        refreshBadges();
    }

    const onSuccessMutateListPayments = () => {
        refreshPayments();
        refreshBadges();
    }

    const onUpdateFilter = (filter) => {
        updateFilter(filter);
        refreshBadges();
    }

    return (
        <Fragment>
            <Helmet>
                <title>Выплаты - {Config.brandName}</title>
            </Helmet>

            <OneColumnsLayout
                header={
                    <HeaderPaymentsPage
                        bulkActions={bulkActions}
                        setIsShowConfirmModal={() => setIsShowConfirmModal(true)}
                        setTypeActionWithPayment={setTypeActionWithPayment}
                    />
                }
            >
                <Box {...wrapperProps}>
                    <PaymentsFilter badges={badgesData} disabled={isPaymentsLoading} onUpdate={onUpdateFilter}>
                        {
                            (confirm && hasSelection) &&
                            <Button
                                disabled={isPaymentsLoading || isLoadingInfo}
                                onClick={() => setIsShowConfirmModal(true)}
                            >
                                Подтвердить
                            </Button>
                        }
                        {
                            (repeat && hasSelection) &&
                            <Button
                                disabled={isPaymentsLoading || isLoadingInfo}
                                onClick={() => setTypeActionWithPayment(ACTION_REPEAT)}
                            >
                                Повторить
                            </Button>
                        }

                        {
                            (cancel && hasSelection) &&
                            <Button
                                styling='hollow-border'
                                disabled={isPaymentsLoading || isLoadingInfo}
                                onClick={() => setTypeActionWithPayment(ACTION_CANCEL)}
                            >
                                Отменить {listPayments.length === 1 ? 'выплату' : 'выплаты'}
                            </Button>
                        }

                        {
                            (refund && hasSelection) &&
                            <DropActions
                                disabled={isPaymentsLoading || isLoadingInfo}
                                positionDropDown='bottom-left'
                                actions={[{
                                    label: 'Зачислить на баланс исполнителя',
                                    action: () => setTypeActionWithPayment(ACTION_REFUND),
                                }]}
                            />
                        }
                    </PaymentsFilter>

                    <Box className={clsScroll}>
                        <Box>
                            <PaymentsList
                                isLoading={isPaymentsLoading}
                                isFetching={isPaymentsFetching}
                                isDisabled={isPaymentsLoading || isLoadingInfo}
                                payments={paymentsData}
                                onSelectPayment={onSelectPayment}
                                bulkActions={bulkActions}
                            />
                        </Box>

                        <Box pt={30}>
                            {
                                last_page > 1 && <Pagination
                                    handlePageClick={(value) => updateFilter({ page: value.selected + 1 })}
                                    initialPage={current_page - 1}
                                    pageCount={last_page}
                                />
                            }
                        </Box>
                    </Box>

                    {
                        isShowNewPaymentForm && <SmartPaymentByStep
                            onClose={onCloseNewPaymentForm}
                            onSuccess={onSuccessCreatePayment}
                        />
                    }

                    {
                        selectPayment && <PaymentsDetailModal
                            payment={selectPayment}
                            onClose={() => setSelectPayment(null)}
                            onUpdatePayment={onUpdatePayment}
                        />
                    }

                    {
                        (hasSelection || isLoadingInfo) && (
                            <SnackbarMessage isLoading={isLoadingInfo}>
                                {statistic}
                            </SnackbarMessage>
                        )
                    }
                </Box>
            </OneColumnsLayout>

            {
                isShowConfirmModal && confirm &&
                <PaymentListConfirmModal
                    onSuccess={onSuccessMutateListPayments}
                    onDismiss={() => setIsShowConfirmModal(false)}
                />
            }

            {
                typeActionWithPayment &&
                <ModalConfirmationAction
                    actionType={typeActionWithPayment}
                    onSuccess={onSuccessMutateListPayments}
                    onDismiss={() => setTypeActionWithPayment(null)}
                />
            }
        </Fragment>
    )
}

const PageWithProviders = () => {
    return (
        <NewPaymentFormProvider>
            <SelectedPaymentsContextProvider>
                <Page />
            </SelectedPaymentsContextProvider>
        </NewPaymentFormProvider>
    )
}

export default withPermissions(PageWithProviders, [ 'read-payment', 'read-self-payment']);
