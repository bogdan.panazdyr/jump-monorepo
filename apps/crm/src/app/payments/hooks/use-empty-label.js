import { useQuerySearch } from 'hooks/use-search-filter';

export const useEmptyLabel = () => {
    const search = useQuerySearch();

    const labels = {
        4: 'Неподтверждённых выплат нет',
        1: 'Оплаченных выплат нет',
        2: 'Отменённых выплат нет',
        5: 'Выплат с ошибкой нет',
    }

    const label = labels[search?.status_id];

    return {
        label: label || 'Выплат нет',
        canCreate: !label,
    }
}
