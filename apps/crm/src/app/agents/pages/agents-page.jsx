import React, { Fragment } from 'react';
import { Helmet } from 'react-helmet';
import { useOutlet } from 'react-router-dom';
import { Pagination } from '@justlook/uikit';
import { Config } from '@justlook/core';
import { useIfMediaScreen } from '@justlook/uikit/lib/hooks';

import { useContentView } from 'utils/browser';

import { OneColumnsLayout, TwoColumnsLayout } from 'components/menu';
import { withPermissions, Notice } from 'components';

import { AgentHeader } from 'app/agents/components/agents-header';
import { AgentsList } from 'app/agents/components/agents-list';

import { useAgentsList } from 'domains/agents';

import cx from 'app/agents/pages/agents-page.module.scss';

const userType = Config.crm_type === 'taxi' ? 'водителями' : 'исполнителями';

const Page = () => {
    const isMobile = useIfMediaScreen()
    const child = useOutlet();
    const viewMode = useContentView(child);

    const { agents, meta, isLoading, updateFilter } = useAgentsList();

    const { last_page, current_page } = meta;

    const Layout = isMobile ? TwoColumnsLayout : OneColumnsLayout;

    const desktopLayoutProps = {
        header: <AgentHeader canCreate/>,
    }

    const mobileLayoutProps = {
        header: <AgentHeader canCreate viewMode={viewMode}/>,
        details: child,
        viewMode,
    }

    const layoutProps = isMobile ? mobileLayoutProps : desktopLayoutProps;

    return (
        <Fragment>
            <Helmet>
                <title>Настройки - Юрлица</title>
            </Helmet>

            <Layout {...layoutProps}>
                <div className={cx.base}>
                    <Notice label={`Собственные юрлица для работы и расчетов с ${userType}`}/>
                    <AgentsList items={agents} isLoading={isLoading}/>
                </div>
                <div className={cx.paginator}>
                    {last_page > 1 &&
                        <Pagination
                            handlePageClick={(value) => updateFilter({ page: value.selected + 1 })}
                            initialPage={current_page - 1}
                            pageCount={last_page}
                        />
                    }
                </div>
            </Layout>
        </Fragment>
    )
}

export default withPermissions(Page, ['write-agent']);
