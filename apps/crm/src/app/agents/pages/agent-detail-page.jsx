import React from 'react';
import { useParams } from 'react-router-dom';
import { Hooks } from '@justlook/core';
import { Button, Icons24, Header, ParagraphV2 as Paragraph, Description } from '@justlook/uikit';

import { withPermissions, SplashScreen, ContentBox, Box } from 'components';

import { errorToString } from 'utils/add-server-errors';

import { useAgent, useDelete, Agent, useFilter } from 'domains/agents';

import { AgentsModal } from '../components/agents-modal';

import cx from './agents-page.module.scss';
import { useNavigate } from 'react-router';

const FORM_UPDATE = 'update';

const AgentDetailPage = () => {
    const { id } = useParams();
    const navigator = useNavigate()

    const { filterQuery } = useFilter();
    const { isOpen, onOpen, onClose, getPayload } = Hooks.useModal();

    const { data, isLoading } = useAgent(id);

    const { onDelete, isDeleting, error } = useDelete(id, () => navigator('/settings-agents' + filterQuery))

    const agent = new Agent(data);

    if(isLoading) { return <SplashScreen/> }

    return (
        <ContentBox>
            <Header.H1>{agent.getName()}</Header.H1>
            <Box mt={16} mb={16}>
                <Paragraph.LH24 className={cx.info}>
                    ИНН
                    <span>{agent.getInn()}</span>
                </Paragraph.LH24>
                <Paragraph.LH24 className={cx.info}>
                    ОГРН
                    <span>{agent.getOgrn()}</span>
                </Paragraph.LH24>
                <Paragraph.LH24 className={cx.info}>
                    Адрес
                    <span>{agent.getAddress()}</span>
                </Paragraph.LH24>
            </Box>
            <Box className={cx.buttons}>
                <Button
                    styling='hollow-border'
                    className={cx.delete}
                    icon={<Icons24.IconTrash/>}
                    loading={isDeleting}
                    disabled={isDeleting || error}
                    onClick={() => onDelete(agent.getId())}
                >
                    Удалить
                </Button>
                <Button
                    styling='hollow-border'
                    icon={<Icons24.IconPencil/>}
                    disabled={isDeleting}
                    onClick={() => onOpen(FORM_UPDATE, agent.getId())}
                >
                    Изменить
                </Button>
            </Box>
            { error && <Description.LH16 className={cx.error}>{errorToString(error)}</Description.LH16> }

            {
                isOpen(FORM_UPDATE) && <AgentsModal
                    id={getPayload(FORM_UPDATE)}
                    onDismiss={() => onClose(FORM_UPDATE)}
                    isDeletable
                />
            }
        </ContentBox>
    )
}

export default withPermissions(AgentDetailPage, ['write-agent']);
