import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { useNavigate } from 'react-router';
import { Hooks } from '@justlook/core';
import { Button, Icons24 } from '@justlook/uikit';
import { useIfMediaScreen } from '@justlook/uikit/lib/hooks';

import { EmptyContent, SplashScreen, NewTable as Table } from 'components';

import { Agent, useFilter } from 'domains/agents';

import { AgentsModal } from './agents-modal';

const FORM_UPDATE = 'update';

export const AgentsList = ({ items, isLoading }) => {
    const navigator = useNavigate();

    const isMobile = useIfMediaScreen();
    const { isOpen, onOpen, onClose, getPayload } = Hooks.useModal();

    const { filterQuery, pathname } = useFilter();

    if(isLoading) { return <SplashScreen/> }

    if(items.length < 1) {
        return <EmptyContent message='Вы еще не добавили ни одного юрлица'/>
    }

    return (
        <Fragment>
            <Table>
                <Table.Head
                    cells={[
                        { label: 'Название' },
                        { label: 'ИНН' },
                        { label: 'ОГРН / ОГРНИП' },
                        { label: 'Адрес' },
                    ]}
                />

                {items.map(item => {
                    const agent = new Agent(item);

                    return (
                        <Table.Row
                            key={agent.getId()}
                            mobileTitle={agent.getName()}
                            mobileDescription={agent.getMobileDescription(' · ')}
                            mobileIcon={Icons24.IconArrowRight}
                            renderCustomCell={
                                <Button
                                    styling='hollow'
                                    icon={<Icons24.IconPencil/>}
                                    onClick={() => onOpen(FORM_UPDATE, agent.getId())}
                                />
                            }
                            onClick={isMobile ? () => navigator(`${pathname}/${agent.getId()}${filterQuery}`) : null}
                        >
                            <Table.Cell>
                                {agent.getName()}
                            </Table.Cell>
                            <Table.Cell>
                                {agent.getInn()}
                            </Table.Cell>
                            <Table.Cell>
                                {agent.getOgrn()}
                            </Table.Cell>
                            <Table.Cell>
                                {agent.getAddress()}
                            </Table.Cell>
                        </Table.Row>
                    )
                })}
            </Table>

            {
                isOpen(FORM_UPDATE) && <AgentsModal
                    id={getPayload(FORM_UPDATE)}
                    onDismiss={() => onClose(FORM_UPDATE)}
                    isDeletable
                />
            }
        </Fragment>
    )
}

AgentsList.propTypes = {
    items: PropTypes.array,
    isLoading: PropTypes.bool,
}
