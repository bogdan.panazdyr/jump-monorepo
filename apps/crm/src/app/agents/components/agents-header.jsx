import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { useNavigate } from 'react-router-dom';
import { Hooks } from '@justlook/core';
import { Icons24, MobileHeader } from '@justlook/uikit';
import { useIfMediaScreen } from '@justlook/uikit/lib/hooks';

import { VIEW_MODE_DETAILS } from 'utils/browser';

import { useMenu } from 'components/menu';

import { useFilter } from 'domains/agents';

import { AgentsModal } from './agents-modal';

const FORM_CREATE = 'create';

export const AgentHeader = ({ viewMode, canCreate }) => {
    const navigate = useNavigate();
    const { filterQuery } = useFilter();
    const { isOpen, onOpen, onClose } = Hooks.useModal();

    const { onExpandedMobile } = useMenu();
    const isMobile = useIfMediaScreen();

    const detailMode = isMobile && viewMode === VIEW_MODE_DETAILS;

    return (
        <Fragment>
            <MobileHeader
                header='Юридические лица'
                onGoBack={detailMode ? () => navigate('/settings-agents' + filterQuery) : null}
            >
                {!detailMode && canCreate && <MobileHeader.Button icon={<Icons24.IconPlus/>} onClick={() => onOpen(FORM_CREATE)}/>}

                { isMobile && <MobileHeader.Button icon={<Icons24.IconMenu/>} onClick={onExpandedMobile}/> }
            </MobileHeader>

            { isOpen(FORM_CREATE) && <AgentsModal onDismiss={() => onClose(FORM_CREATE)}/> }
        </Fragment>
    )
}

AgentHeader.defaultProps = {
    canCreate: false,
}

AgentHeader.propTypes = {
    canCreate: PropTypes.bool,
    viewMode: PropTypes.string
}

