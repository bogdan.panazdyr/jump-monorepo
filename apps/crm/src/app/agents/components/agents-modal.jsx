import React from 'react';
import PropTypes from 'prop-types';
import { useIfMediaScreen } from '@justlook/uikit/lib/hooks';
import { Popup, Button, Icons24, Form, InfoMessage } from '@justlook/uikit';

import { InputController, SplashScreen, TextareaController } from 'components';

import { InnDadataField } from './inn-dadata-field';

import { useInitialForm } from 'domains/agents';

import cx from './agents-modal.module.scss';

const FORM_ID = 'agent-form';

export const AgentsModal = ({ id, onDismiss, isDeletable }) => {
    const isMobile = useIfMediaScreen();

    const {
        handleOnSelectAgent, form, header, submitLabel, isLoading, agent, errorDeletion
    } = useInitialForm(id, onDismiss);

    return (
        <Popup onDismiss={onDismiss}>
            <Popup.Header>{header}</Popup.Header>
            <Popup.Content>
                {isLoading && <SplashScreen/>}
                {
                    !isLoading &&
                        <Form onSubmit={form.onSubmit} id={FORM_ID}>
                        {agent
                            ? (
                                <InputController
                                    name='inn'
                                    label='ИНН'
                                    className={cx.small}
                                    control={form.control}
                                    error={form.getError('inn')}
                                    isRequired
                                />
                            )
                            : (
                                <InnDadataField
                                    name='inn'
                                    label='ИНН'
                                    control={form.control}
                                    error={form.getError('inn')}
                                    onSelect={handleOnSelectAgent}
                                    isRequired
                                />
                            )

                        }

                        <InputController
                            name='name'
                            label='Название'
                            className={cx.grid}
                            control={form.control}
                            error={form.getError('name')}
                            isRequired
                        />

                        <InputController
                            name='ogrn'
                            label='ОГРН / ОГРНИП'
                            className={cx.small}
                            control={form.control}
                            error={form.getError('ogrn')}
                            isRequired
                        />

                        <TextareaController
                            name='address'
                            label='Адрес'
                            className={cx.grid}
                            control={form.control}
                            error={form.getError('address')}
                            isRequired
                        />
                    </Form>
                }
                <Form.Field>
                    <div className={cx.grid}>
                        <div/>
                        {
                            errorDeletion &&
                            <InfoMessage icon={<Icons24.IconWarning/>} type='error'>{errorDeletion}</InfoMessage>
                        }
                    </div>
                </Form.Field>

            </Popup.Content>
            <Popup.Footer
                majorButton={
                    <Button
                        type='submit'
                        form={FORM_ID}
                        loading={form.isCreating || form.isUpdating}
                        disabled={form.isMutating}
                    >
                        {submitLabel}
                    </Button>
                }
                onDismissButton={
                    <Button
                        styling='hollow-border'
                        disabled={form.isMutating}
                        onClick={onDismiss}
                    >
                        Отмена
                    </Button>
                }
                minorButton={agent && isDeletable && (
                    <Button
                        icon={!isMobile && <Icons24.IconTrash/>}
                        styling='hollow-border'
                        loading={form.isDeleting}
                        disabled={form.isMutating}
                        onClick={form.onDelete}
                    >
                        Удалить
                    </Button>
                )}
            />
        </Popup>
    )
};

AgentsModal.defaultProps = {
    isDeletable: false,
}

AgentsModal.propTypes = {
    onDismiss: PropTypes.func,
    id: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]),
    isDeletable: PropTypes.bool,
}
