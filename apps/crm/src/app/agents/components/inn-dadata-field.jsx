import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import { debounce } from 'debounce';
import { Icons24 } from '@justlook/uikit';

import { InputController } from 'components/fields';

import { useAgentByInn } from 'domains/agents';

import cx from './inn-dadata-field.module.scss';

export const InnDadataField = ({ onSelect = () => {}, label, name, control, error, ...rest }) => {
    const { result, onFetch, isLoading, onClearState } = useAgentByInn();

    // eslint-disable-next-line
    const fetchAgent = useCallback(debounce((value) => {
        if(value.length >= 10) {
            onFetch(value)
        }
    }, 500), [ onFetch ]);

    const onSelectHint = (item) => {
        onSelect(item);
        onClearState();
    }

    const handleOnChangeField = (value) => {
        fetchAgent.clear();
        fetchAgent(value);
    }

    const renderResultDadata = () => {
        return (
            <div className={cx.results}>
                {result.map((item, index) => (
                    <div
                        className={cx.result}
                        key={index + item.inn}
                        onClick={() => onSelectHint(item)}
                    >
                        { item.data.name.short_with_opf || item.data.name.full }
                    </div>
                ))}
            </div>
        )
    }

    return (
        <div className={cx.base}>
            <div className={cx.inn}>
                <InputController
                    type='number'
                    name={name}
                    label={label}
                    className={cx.input}
                    control={control}
                    error={error}
                    onChange={handleOnChangeField}
                    isRequired
                    {...rest}
                />

                {isLoading && <Icons24.IconLoader/>}
            </div>

            {result.length > 0 && renderResultDadata()}
        </div>
    )
}

InnDadataField.propTypes = {
    control: PropTypes.any.isRequired,
    error: PropTypes.any,
    onSelect: PropTypes.func,
    label: PropTypes.string,
    name: PropTypes.string,
    className: PropTypes.string,
}
