import AgentPage from './pages/agents-page';
import AgentDetailPage from './pages/agent-detail-page';

export {
    AgentPage,
    AgentDetailPage,
}
