import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Toggle, ParagraphV2 as Paragraph } from '@justlook/uikit';

import Api from 'api/client';

import cx from 'app/general-settings/components/legal-form-settings.module.scss';

const RequisitesToggle = ({ requisite, id }) => {
    const { is_enable, title, id: reqId } = requisite;
    const [ checked, setChecked ] = useState(is_enable);
    const [ isRequest, setIsRequest ] = useState(false);

    const handleOnChange = (value) => {
        setChecked(value);
        setIsRequest(true);

        Api.generalSettings.updateRequisitesSetting(id, reqId, { is_enable: value })
            .then(() => setIsRequest(false))
            .catch(() => {
                setIsRequest(true);
                setChecked(is_enable);
            });
    }

    return (
        <Toggle
            id={id + title}
            label={title}
            defaultChecked={checked}
            onChange={handleOnChange}
            disabled={isRequest}
            isLoading={isRequest}
        />
    )
}

export const LegalFormSettings = ({ setting }) => {
    const { is_enable, title, requisites, id } = setting;
    const [ checked, setChecked ] = useState(is_enable);
    const [ isRequest, setIsRequest ] = useState(false);

    const handleOnChange = (value) => {
        setChecked(value)
        setIsRequest(true);

        Api.generalSettings.updateLegalFormSetting(id, { is_enable: value })
            .then(() => setIsRequest(false))
            .catch(() => {
                setIsRequest(true);
                setChecked(is_enable);
            });
    }

    return (
        <div className={cx.base}>
            <Toggle
                label={title}
                defaultChecked={checked}
                onChange={handleOnChange}
                disabled={isRequest}
                isLoading={isRequest}
            />
            {!isRequest && checked && (
                <div className={cx.requisites}>
                    <Paragraph.LH24>Реквизиты</Paragraph.LH24>

                    {requisites.map((item, index) => (
                        <RequisitesToggle requisite={item} key={index} id={id} />
                    ))}
                </div>
            )}
        </div>
    )
}

RequisitesToggle.propTypes = {
    requisite: PropTypes.object.isRequired,
    id: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ])
}

LegalFormSettings.propTypes = {
    setting: PropTypes.object.isRequired,
}
