import React from 'react';
import PropTypes from 'prop-types';
import { Forms } from '@justlook/core';

import { SelectController } from 'components';

import cx from '../common.module.scss';

export const Bank = ({ control, options, data, errors }) => {
    const getDefaultValue = () => {
        let defaultValue;

        if(options.length === 1 && !data) {
            defaultValue = Forms.itemToOption(options[0], { label: 'title', value: 'value' });

            return defaultValue;
        }

        defaultValue = options.find(item => item?.value === data?.value);

        return defaultValue && Forms.itemToOption(defaultValue, { label: 'title', value: 'value' })
    }

    return (
        <SelectController
            name='result.data.value'
            control={control}
            placeholder='Выберите банк'
            options={Forms.makeOptions(options, { label: 'title', value: 'value' })}
            defaultValue={getDefaultValue()}
            className={cx.middle}
            error={errors?.result?.data?.value?.message || ''}
            isRequired
        />
    )
}

Bank.propTypes = {
    control: PropTypes.object.isRequired,
    options: PropTypes.array,
    data: PropTypes.object,
    errors: PropTypes.object,
}
