import React, { Fragment, useState } from 'react';
import { Box } from 'components/box';
import { Button, Icons24, ParagraphV2 as Paragraph, Header } from '@justlook/uikit';

import { RulePopup } from './rule-popup';

import cx from './payment-wizard-header.module.scss';

export const PaymentWizardHeader = () => {
    const [ isOpenCreateRule, setIsOpenCreateRule ] = useState(false);

    return (
        <Fragment>
            <Box className={cx.header}>
                <Header.H1>Правила проведения выплат</Header.H1>
                <Button
                    styling='hollow'
                    icon={<Icons24.IconPlus/>}
                    onClick={() => setIsOpenCreateRule(true)}
                />
            </Box>
            <Paragraph.LH24>Чем выше правило, тем выше его приоритет. Выбор счета зависит от юрлица.</Paragraph.LH24>

            {isOpenCreateRule && <RulePopup onDismiss={() => setIsOpenCreateRule(false)}/>}
        </Fragment>
    )
}
