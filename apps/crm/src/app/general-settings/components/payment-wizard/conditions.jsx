import React, { useCallback, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Forms } from '@justlook/core';

import { SelectController } from 'components';

import { ConditionForms } from 'domains/payment-wizard';

import cx from './common.module.scss';

export const Conditions = ({ data, control, errors, getAvailableResult, initialRule }) => {
    const [ selectedCondition, setSelectedCondition ] = useState(null);

    const getResultsForType = useCallback((type) => {
        const condition = data.find(item => item?.type === type);

        return {
            condition,
            results: condition?.results,
        }
    }, [ data ])

    const getDefaultValue = () => {
        const { condition } = getResultsForType(initialRule?.type);

        return condition && Forms.itemToOption(condition, { label: 'title', value: 'type' });
    }

    const handleOnChangeCondition = (condition) => {
        setSelectedCondition(condition);

        const { results } = getResultsForType(condition);

        getAvailableResult(results);
    }

    const buildForm = (type) => {
        if(!type) return null;

        let Component = null;

        if(ConditionForms[type]) {
            Component = ConditionForms[type]
        } else {
            return null
        }

        const props = {
            control,
            errors,
            data: initialRule?.data
        }

        return <Component {...props}/>
    }

    useEffect(() => {
        if(initialRule) {
            const { results } = getResultsForType(initialRule?.type);

            getAvailableResult(results);
        }
    }, [ initialRule, getAvailableResult, getResultsForType ])

    return (
        <div className={cx.base}>
            <SelectController
                name='condition.type'
                control={control}
                placeholder='Выберите условие'
                defaultValue={getDefaultValue() || null}
                onSelect={handleOnChangeCondition}
                options={Forms.makeOptions(data, { label: 'title', value: 'type' })}
                className={cx.large}
                error={errors?.condition?.type?.message || ''}
                isRequired
            />

            { buildForm(selectedCondition || initialRule?.type) }
        </div>
    )
}

Conditions.propTypes = {
    data: PropTypes.array,
    control: PropTypes.object,
    getAvailableResult: PropTypes.func,
    initialRule: PropTypes.object,
    errors: PropTypes.object,
}
