import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Forms } from '@justlook/core';

import { SelectController } from 'components';

import { ResultForms } from 'domains/payment-wizard';

import cx from './common.module.scss';

export const Results = ({ control, data, errors, options, initialRule }) => {
    const [ selectedResult, setSelectedResult ] = useState(null);

    const getAvailableResults = () => {
        const results = options.map(option => data.find(item => item?.type === option))

        const preOptions = Forms.makeOptions(results, { label: 'title', value: 'type' })

        return {
            preOptions,
            results,
        }
    }

    const getDefaultValue = () => {
        const result = data.find(item => item?.type === initialRule?.type);

        return result && Forms.itemToOption(result, { label: 'title', value: 'type' })
    }

    const { preOptions, results } = getAvailableResults();

    const buildForm = (type) => {
        if(!type) return null;

        const propsFromConditions = results.find(item => item?.type === type);

        const Component = ResultForms[type];

        const props = {
            control,
            errors,
            ...propsFromConditions,
            data: initialRule?.data,
        }

        return <Component {...props} />
    }

    return (
        <div className={cx.base}>
            <SelectController
                name='result.type'
                control={control}
                placeholder='Выберите результат'
                options={preOptions}
                defaultValue={getDefaultValue()}
                onSelect={setSelectedResult}
                className={cx.middle}
                error={errors?.result?.type?.message || ''}
                isRequired
            />

            { buildForm(selectedResult || initialRule?.type) }
        </div>
    )
}

Results.propTypes = {
    control: PropTypes.object.isRequired,
    data: PropTypes.array,
    errors: PropTypes.object,
    options: PropTypes.array,
    initialRule: PropTypes.object,
}
