import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useForm } from 'react-hook-form';
import { Form, Popup, Button } from '@justlook/uikit';

import { SplashScreen } from 'components';

import { Conditions } from './conditions';
import { Results } from './results';

import { useConditions, useCreateRulePayment, useRulePayment, useUpdateRulePayment } from 'domains/payment-wizard';

const FORM_ID = 'payment-wizard-form';

export const RulePopup = ({ onDismiss, id }) => {
    const { conditions, results, isLoading: isLoadingConditions } = useConditions();
    const { rule, isLoading: isLoadingRule } = useRulePayment(id);

    const { control, handleSubmit, errors, setError } = useForm();

    const { onCreate, isCreating } = useCreateRulePayment(onDismiss, setError);
    const { onUpdate, isUpdating } = useUpdateRulePayment(onDismiss, setError);

    const submitLabel = id ? 'Сохранить' : 'Добавить';

    const onSubmit = (data) => id ? onUpdate({ id, data }) : onCreate(data);

    const isMutating = isCreating || isUpdating;
    const isLoading = isLoadingRule || isLoadingConditions;

    const [ availableResult, setAvailableResult ] = useState(null);

    const renderForm = () => {
        return (
            <Form id={FORM_ID} onSubmit={handleSubmit(onSubmit)}>
                <Conditions
                    control={control}
                    data={conditions}
                    initialRule={rule?.data?.condition}
                    getAvailableResult={setAvailableResult}
                    errors={errors}
                />

                {availableResult &&
                    <Results
                        control={control}
                        data={results}
                        initialRule={rule?.data?.result}
                        options={availableResult}
                        errors={errors}
                    />
                }
            </Form>
        )
    }

    return (
        <Popup onDismiss={isMutating ? null : onDismiss}>
            <Popup.Header>
                Правило проведения платежей
            </Popup.Header>
            <Popup.Content>
                {isLoading ? <SplashScreen/> : renderForm()}
            </Popup.Content>
            <Popup.Footer>
                <Button
                    styling='hollow-border'
                    disabled={isMutating}
                    onClick={onDismiss}
                >
                    Отмена
                </Button>
                <Button
                    disabled={isLoading || isMutating}
                    loading={isMutating}
                    form={FORM_ID}
                    type='submit'
                >
                    { submitLabel }
                </Button>
            </Popup.Footer>
        </Popup>
    )
}

RulePopup.propTypes = {
    onDismiss: PropTypes.func,
    id: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ])
}
