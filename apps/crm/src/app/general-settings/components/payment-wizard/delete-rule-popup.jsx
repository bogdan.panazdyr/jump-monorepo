import React from 'react';
import PropTypes from 'prop-types';
import { Popup, Button, InfoMessage, Icons24 } from '@justlook/uikit';

import { useDeleteRulePayment } from 'domains/payment-wizard';

export const DeleteRulePopup = ({ rule, onClose }) => {
    const { onDelete, isLoading, isError, error } = useDeleteRulePayment(onClose);

    return (
        <Popup onDismiss={isLoading ? null : onClose}>
            <Popup.Header description={rule?.name}>
                Подтвердите удаление
            </Popup.Header>
            <Popup.Content>
                Удалить правило: "{rule?.name}"?
                {isError &&
                    <InfoMessage icon={<Icons24.IconWarning/>} type='error'>
                        {error?.errorData?.detail}
                    </InfoMessage>
                }
            </Popup.Content>
            <Popup.Footer
                majorButton={
                    <Button
                        disabled={isLoading}
                        loading={isLoading}
                        onClick={() => onDelete(rule?.id)}
                    >
                        Удалить
                    </Button>
                }
                onDismissButton={
                    <Button
                        disabled={isLoading}
                        styling='hollow-border'
                        onClick={onClose}
                    >
                        Отмена
                    </Button>
                }
            />
        </Popup>
    )
}

DeleteRulePopup.propTypes = {
    rule: PropTypes.object.isRequired,
    onClose: PropTypes.func,
}
