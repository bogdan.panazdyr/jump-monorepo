import React, { Fragment } from 'react';
import { useSelector } from 'react-redux';
import { parse } from 'query-string';
import { ParagraphV2 as Paragraph } from '@justlook/uikit';

import { SettingsSelectors } from 'app/general-settings/store/selectors';

import { ListLink, SplashScreen } from 'components';

export const GeneralSettingsList = () => {
    const url = parse(window.location.search);
    const isTest = url?.isTest || false;

    const generalSettings = useSelector(SettingsSelectors.getGeneralSettings);
    const isRequest = useSelector(SettingsSelectors.getGeneralSettingsIsRequest);

    if(isRequest) { return <SplashScreen/> }

    return (
        <Fragment>
            {
                generalSettings && generalSettings
                .filter((item) => item.id !== 'payment_wizard' || isTest)
                .map((item, index) => (
                    <ListLink key={index} to={item.id + (isTest ? '?isTest=1' : '')}>
                        <Paragraph.LH24>{item.title}</Paragraph.LH24>
                    </ListLink>
                ))
            }
        </Fragment>
    )
}
