import React, { Fragment, useEffect } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { Hooks } from '@justlook/core';
import { Button } from '@justlook/uikit';

import { Modal } from 'components';

import { CommissionForm } from 'app/general-settings/components/commission-form';

import { SettingsSelectors } from 'app/general-settings/store/selectors';
import { GeneralSettings } from 'app/general-settings/store/actions';

const FORM_ID = 'edit-commission-form';

export const EditCommissionModal = ({ commission, renderControl }) => {
    const { isShow, onOpenForm, onCloseForm } = Hooks.useModalState(false);

    const onDeleteCommission = Hooks.useActionToDispatch(GeneralSettings.deletePaymentsCommission);

    const { isSubmitting, isSubmitted } = useSelector(SettingsSelectors.getFormState);

    useEffect(() => { if (isSubmitted) onCloseForm() }, [ isSubmitted, onCloseForm ]);

    const onDelete = () => onDeleteCommission();

    return (
        <Fragment>
            {renderControl(onOpenForm)}

            {isShow && (
                <Modal
                    title='Комиссия для исполнителей'
                    description={commission?.label}
                    onClose={onCloseForm}
                    majorButton={<Button form={FORM_ID} loading={isSubmitting} type='submit'>Сохранить</Button>}
                    minorButton={
                        commission && (
                            <Button onClick={onDelete} loading={isSubmitting} styling='hollow-border'>Удалить</Button>
                        )
                    }
                    dismissButton={<Button loading={isSubmitting} styling='hollow-border' onClick={onCloseForm}>Отмена</Button>}
                >
                    <CommissionForm formId={FORM_ID} commission={commission} />
                </Modal>
            )}
        </Fragment>
    );
};

EditCommissionModal.propTypes = {
    commission: PropTypes.object,
    renderControl: PropTypes.func
};
