import React, { Fragment, useEffect, useMemo } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { useForm, Controller } from 'react-hook-form';
import { Hooks } from '@justlook/core';
import { Popup, Form, Input, Button, Icons24 } from '@justlook/uikit';

import { GeneralSettings } from 'app/general-settings/store/actions';
import { SettingsSelectors } from 'app/general-settings/store/selectors';

import { addServerErrors } from 'utils/add-server-errors';

import cx from 'app/general-settings/components/modals/limit-modal.module.scss';

const FORM_ID = 'limits-form';

export const LimitModal = ({ renderControl, limit }) => {
    const { isShow, onOpenForm, onCloseForm } = Hooks.useModalState();
    const { handleSubmit, errors, control, setError } = useForm({
        defaultValues: {
            min_request: limit && String(limit?.min_request),
            max_request: limit && String(limit?.max_request),
            max_day: limit && String(limit?.max_day),
            max_week: limit && String(limit?.max_week),
            max_month: limit && String(limit?.max_month),
        }
    })

    const errorFields = useSelector(SettingsSelectors.getPaymentsInvalidFields);
    const { isSubmitting, isSubmitted } = useSelector(SettingsSelectors.getFormState);
    const onUpdateLimit = Hooks.useActionToDispatch(GeneralSettings.updatePaymentsLimit);
    const onDeleteLimit = Hooks.useActionToDispatch(GeneralSettings.deletePaymentsLimit);

    const submitLabel = limit ? 'Сохранить' : 'Добавить';

    useEffect(() => { if(isSubmitted) onCloseForm() }, [ isSubmitted, onCloseForm ]);

    useMemo(() => { addServerErrors(errorFields, setError) }, [errorFields, setError]);

    const onSubmit = (values) => onUpdateLimit(values);
    const onDelete = () => onDeleteLimit();

    return (
        <Fragment>
            {renderControl(onOpenForm)}
            {isShow &&
                <Popup onDismiss={onCloseForm}>
                    <Popup.Header>Лимит на вывод денег исполнителям</Popup.Header>
                    <Popup.Content className={cx.content}>
                        <Form onSubmit={handleSubmit(onSubmit)} id={FORM_ID}>
                            <Form.Field>
                                <Controller
                                    as={Input}
                                    type='number'
                                    name='min_request'
                                    label='Мин. заявка, ₽'
                                    className={cx.grid}
                                    control={control}
                                    error={errors.min_request && errors.min_request.message}
                                />
                            </Form.Field>
                            <Form.Field>
                                <Controller
                                    as={Input}
                                    type='number'
                                    name='max_request'
                                    label='Макс. заявка, ₽'
                                    className={cx.grid}
                                    control={control}
                                    error={errors.max_request && errors.max_request.message}
                                />
                            </Form.Field>
                            <br/>
                            <Form.Field>
                                <Controller
                                    as={Input}
                                    type='number'
                                    name='max_day'
                                    label='Макс. в сутки, ₽'
                                    className={cx.grid}
                                    control={control}
                                    error={errors.max_day && errors.max_day.message}
                                />
                            </Form.Field>
                            <Form.Field>
                                <Controller
                                    as={Input}
                                    type='number'
                                    name='max_week'
                                    label='Макс. в неделю, ₽'
                                    className={cx.grid}
                                    control={control}
                                    error={errors.max_week && errors.max_week.message}
                                />
                            </Form.Field>
                            <Form.Field>
                                <Controller
                                    as={Input}
                                    type='number'
                                    name='max_month'
                                    label='Макс. в месяц, ₽'
                                    className={cx.grid}
                                    control={control}
                                    error={errors.max_month && errors.max_month.message}
                                />
                            </Form.Field>
                        </Form>
                    </Popup.Content>

                    <Popup.Footer
                        majorButton={ <Button form={FORM_ID} loading={isSubmitting} type='submit'> {submitLabel} </Button> }
                        onDismissButton={
                            <Button disabled={isSubmitting} styling='hollow-border' onClick={onCloseForm}>
                                Отмена
                            </Button>
                        }
                        minorButton={
                            limit && (
                                <Button
                                    loading={isSubmitting}
                                    onClick={onDelete}
                                    styling='hollow-border'
                                    icon={<Icons24.IconTrash/>}
                                >
                                    Удалить
                                </Button>
                            )
                        }
                    />
                </Popup>
            }
        </Fragment>
    )
}

LimitModal.propTypes = {
    renderControl: PropTypes.func,
    limit: PropTypes.object,
}
