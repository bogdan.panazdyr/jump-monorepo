import React, { Fragment, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { Hooks } from '@justlook/core';
import { Button, Tab } from '@justlook/uikit';

import { Modal } from 'components';

import { CommissionForm } from 'app/general-settings/components/commission-form';

import { SettingsSelectors } from 'app/general-settings/store/selectors';
import { GeneralSettings } from 'app/general-settings/store/actions';

import cx from 'app/general-settings/components/modals/commission-modal.module.scss';

const FORM_ID = 'commission-form';

export const CommissionModal = ({ renderControl, commission }) => {
    const { isShow, onOpenForm, onCloseForm } = Hooks.useModalState();
    const [ type, setType ] = useState('percent');

    const onDeleteCommission = Hooks.useActionToDispatch(GeneralSettings.deletePaymentsCommission);

    const { isSubmitting, isSubmitted } = useSelector(SettingsSelectors.getFormState);

    useEffect(() => { if (isSubmitted) onCloseForm() }, [ isSubmitted, onCloseForm ]);

    const onDelete = () => onDeleteCommission();

    return (
        <Fragment>
            {renderControl(onOpenForm)}
            {isShow &&
            <Modal
                title='Комиссия для исполнителей'
                onClose={onCloseForm}
                majorButton={<Button form={FORM_ID} loading={isSubmitting} type='submit'>Добавить</Button>}
                minorButton={
                    commission && (
                        <Button onClick={onDelete} loading={isSubmitting} styling='hollow-border'>Удалить</Button>
                    )
                }
                dismissButton={<Button loading={isSubmitting} styling='hollow-border' onClick={onCloseForm}>Отмена</Button>}
            >
                <Tab className={cx.tabs} actionChangeTab={(value) => setType(value)}>
                    <Tab.Content label='Процентная' value='percent' isActive={false}>
                        <CommissionForm formId={FORM_ID} type={type} commission={commission} />
                    </Tab.Content>
                    <Tab.Content label='Фиксированная' value='fixed' isActive={false}>
                        <CommissionForm formId={FORM_ID} type={type} commission={commission} />
                    </Tab.Content>
                </Tab>
            </Modal>
            }
        </Fragment>
    )
}

CommissionModal.propTypes = {
    renderControl: PropTypes.func,
    commission: PropTypes.object,
}

