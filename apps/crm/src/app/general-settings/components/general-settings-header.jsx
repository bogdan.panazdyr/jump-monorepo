import React from 'react';
import PropTypes from 'prop-types';
import { useNavigate } from 'react-router-dom';
import { Icons24, Button, Header as Heading } from '@justlook/uikit';
import { useIfMediaScreen } from '@justlook/uikit/lib/hooks';

import { useMenu } from 'components/menu';
import { VIEW_MODE_DETAILS } from 'utils/browser';

import cx from './general-settings-header.module.scss';

const ItemHeader = ({ onExpandedMobile }) => {
    const navigate = useNavigate();

    return <div className={cx.base}>
        <Button styling='hollow' icon={<Icons24.IconBack/>}
                onClick={() => navigate('/settings-company')}/>

        <div className={cx.titleBlock}>
            <Heading.H1 className={cx.title}>Общие</Heading.H1>
        </div>

        <Button styling='hollow' icon={<Icons24.IconMenu/>} onClick={onExpandedMobile}/>
    </div>;
};

const DetailHeader = ({ onExpandedMobile, isMobile }) => {
    return <div className={cx.base}>
        <div className={cx.dummy}/>
        <div className={cx.titleBlock}>
            <Heading.H1 className={cx.title}>Общие</Heading.H1>
        </div>

        { isMobile && <Button styling='hollow' icon={<Icons24.IconMenu/>} onClick={onExpandedMobile}/> }
    </div>;
};

const Header = ({ viewMode }) => {
    const isMobile = useIfMediaScreen();
    const { onExpandedMobile } = useMenu();

    if (isMobile && viewMode === VIEW_MODE_DETAILS) {
        return <ItemHeader onExpandedMobile={onExpandedMobile}/>;
    }

    return (
        <DetailHeader
            isMobile={isMobile}
            onExpandedMobile={onExpandedMobile}
        />
    );
};

export default Header;

ItemHeader.propTypes = {
    onExpandedMobile: PropTypes.func,
}

DetailHeader.propTypes = {
    onExpandedMobile: PropTypes.func,
    isMobile: PropTypes.bool,
}

Header.propTypes = {
    viewMode: PropTypes.string,
}
