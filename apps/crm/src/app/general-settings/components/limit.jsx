import React from 'react';
import { useSelector } from 'react-redux';
import { Button, Header, Icons24, TableV2 as Table } from '@justlook/uikit';

import { LimitModal } from 'app/general-settings/components/modals/limit-modal';

import { SettingsSelectors } from 'app/general-settings/store/selectors';

import cx from 'app/general-settings/components/limit.module.scss';

export const Limit = () => {
    const limit = useSelector(SettingsSelectors.getPaymentsLimit);

    return (
        <div className={cx.wrapper}>
            <Header.H2 className={cx['sub-header']}>Лимиты для исполнителей</Header.H2>
            {limit
                ? (
                    <Table
                        accessor={['min_request', 'max_request', 'max_day', 'max_week', 'max_month']}
                        data={[limit]}
                        headerCell={[
                            {label: 'Мин. заявка, ₽'},
                            {label: 'Макс. заявка, ₽'},
                            {label: 'В сутки, ₽'},
                            {label: 'В неделю, ₽'},
                            {label: 'В месяц, ₽'},
                        ]}
                        renderCustomCells={(limit) => (
                            <div className={cx['custom-cell']}>
                                <LimitModal
                                    limit={limit}
                                    renderControl={(open) => (
                                        <Button icon={<Icons24.IconPencil/>} styling='hollow' onClick={open} />
                                    )}
                                />
                            </div>
                        )}
                    />
                )
                : (
                    <LimitModal
                        renderControl={(open) => (
                            <Button
                                styling='hollow-border'
                                icon={<Icons24.IconPlus/>}
                                onClick={open}
                            >
                                Добавить лимит
                            </Button>
                        )}
                    />
                )}
        </div>
    )
}
