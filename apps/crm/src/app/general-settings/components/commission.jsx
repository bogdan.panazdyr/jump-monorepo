import React from 'react';
import { useSelector } from 'react-redux';
import { Button, Header, Icons24, TableV2 as Table } from '@justlook/uikit';

import { CommissionModal } from 'app/general-settings/components/modals/commission-modal';
import { EditCommissionModal } from 'app/general-settings/components/modals/edit-commission-modal';

import { SettingsSelectors } from 'app/general-settings/store/selectors';

import cx from 'app/general-settings/components/commission.module.scss';

export const Commission = () => {
    const commissions = useSelector(SettingsSelectors.getPaymentsCommission);

    const prepareDataTable = (data) => {
        const obj = {
            ...data,
            label: data.type === 'percent'
                ? `Процентная, ${data.percent_value} %`
                : `Фиксированая, ${data.fix_value} ₽`,
            value: `${data.fix_value} ₽`,
        }

        return [obj];
    }

    return (
        <div className={cx.wrapper}>
            <Header.H2 className={cx['sub-header']}>Комиссии для исполнителей</Header.H2>
            {commissions
                ? (
                    <Table
                        accessor={['label', 'value']}
                        data={prepareDataTable(commissions)}
                        headerCell={[
                            {label: 'Вид'},
                            {label: 'Мин. сумма'},
                        ]}
                        renderCustomCells={(commission) => (
                            <div className={cx['custom-cell']}>
                                <EditCommissionModal
                                    commission={commission}
                                    renderControl={(open) => (
                                        <Button icon={<Icons24.IconPencil/>} styling='hollow' onClick={open} />
                                    )}
                                />
                            </div>
                        )}
                    />
                )
                : (
                    <CommissionModal
                        renderControl={(open) => (
                            <Button
                                styling='hollow-border'
                                icon={<Icons24.IconPlus/>}
                                onClick={open}
                            >
                                Добавить комиссию
                            </Button>
                        )}
                    />
                )
            }
        </div>
    )
}
