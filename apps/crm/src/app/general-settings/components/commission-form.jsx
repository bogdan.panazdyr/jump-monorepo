import React, { useMemo } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { useForm, Controller } from 'react-hook-form';
import { Form, Input } from '@justlook/uikit';
import { Hooks } from '@justlook/core';

import { GeneralSettings } from 'app/general-settings/store/actions';
import { SettingsSelectors } from 'app/general-settings/store/selectors';

import { addServerErrors } from 'utils/add-server-errors';

import cx from 'app/general-settings/components/commission-form.module.scss';

export const CommissionForm = ({ commission, type, formId }) => {
    const { handleSubmit, control, errors, setError } = useForm({
        defaultValues: {
            percent_value: commission && commission?.percent_value,
            fix_value: commission && commission?.fix_value,
        }
    });

    const errorFields = useSelector(SettingsSelectors.getPaymentsInvalidFields);
    const onUpdateCommission = Hooks.useActionToDispatch(GeneralSettings.updatePaymentsCommission);

    const commissionType = type || commission.type;

    useMemo(() => { addServerErrors(errorFields, setError) }, [errorFields, setError]);

    const onSubmit = (values) => onUpdateCommission({ type: commissionType, ...values });

    return (
        <Form onSubmit={handleSubmit(onSubmit)} id={formId}>
            {commissionType === 'percent' &&
                <div className={cx['percent-wrapper']}>
                    <Form.Field>
                        <Controller
                            as={Input}
                            name='percent_value'
                            label='Процент'
                            control={control}
                            rules={{ required: 'Обязательно для ввода' }}
                            error={errors.percent_value && errors.percent_value.message}
                        />
                    </Form.Field>
                    <Form.Field>
                        <Controller
                            as={Input}
                            name='fix_value'
                            label='Минимум, ₽'
                            control={control}
                            rules={{ required: 'Обязательно для ввода' }}
                            error={errors.fix_value && errors.fix_value.message}
                        />
                    </Form.Field>
                </div>
            }
            {commissionType === 'fixed' &&
                <Form.Field>
                    <Controller
                        as={Input}
                        name='fix_value'
                        label='Сумма, ₽'
                        control={control}
                        rules={{ required: 'Обязательно для ввода' }}
                        error={errors.fix_value && errors.fix_value.message}
                    />
                </Form.Field>
            }
        </Form>
    )
}

CommissionForm.propTypes = {
    formId: PropTypes.string,
    commission: PropTypes.object,
    onDismiss: PropTypes.func,
    type: PropTypes.string,
    onSuccessSubmit: PropTypes.func,
}
