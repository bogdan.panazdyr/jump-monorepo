import { put, fork, call, takeLatest } from 'redux-saga/effects';
import { Store } from '@justlook/core';

import Api from 'api/client';
import {
    COMMISSIONS,
    GENERAL_SETTINGS,
    SETTINGS_LEGAL_FORM,
    SETTINGS_PAYMENTS,
    SETTINGS_TAX,
    LIMITS,
} from './action-types';

import { GeneralSettings } from './actions';

export function* fetchGeneralSettings() {
    yield put(GeneralSettings.requestGeneralSettings());

    try {
        const response = yield call([ Api.generalSettings, 'fetchSettings' ]);
        yield put(GeneralSettings.receiveGeneralSettings(response));
    } catch (e) {
        yield put(GeneralSettings.failureGeneralSettings(e));
    }
}

export function* fetchLegalFormSettings() {
    yield put(GeneralSettings.requestSettingsLegalForm());

    try {
        const response = yield call([ Api.generalSettings, 'settingsLegalForm' ]);
        yield put(GeneralSettings.receiveSettingsLegalForm(response));
    } catch (e) {
        yield put(GeneralSettings.failureSettingsLegalForm(e));
    }
}

export function* fetchPaymentsSettings() {
    yield put(GeneralSettings.requestSettingsPayments());

    try {
        const response = yield call([ Api.generalSettings, 'settingsPayments' ]);
        yield put(GeneralSettings.receiveSettingsPayments(response));
    } catch (e) {
        yield put(GeneralSettings.failureSettingsPayments(e));
    }
}

export function* updateTaxSettings({ data }) {
    yield put(GeneralSettings.requestUpdateSettingsTax());

    try {
        const response = yield call([Api.generalSettings, 'updateSettingsTax'], data)
        yield put(GeneralSettings.receiveUpdateSettingsTax(response));
    } catch (e) {
        yield put(GeneralSettings.failureUpdateSettingsTax(e))
    }
}

export function* updateCommission({ data }) {
    yield put(GeneralSettings.requestPaymentsCommission());

    try {
        const response = yield call([Api.generalSettings, 'updatePaymentsCommission'], data);
        yield put(GeneralSettings.receivePaymentsCommission(response));
        yield put(GeneralSettings.successSubmit());
    } catch (e) {
        yield put(GeneralSettings.failurePaymentsCommission(e))
    }
}

export function* deleteCommission() {
    yield put(GeneralSettings.requestDeleteCommission());

    try {
        yield call([Api.generalSettings, 'deletePaymentsCommission'])
        yield put(GeneralSettings.receiveDeleteCommission());
        yield put(GeneralSettings.successSubmit());
    } catch (e) {
        yield put(GeneralSettings.failureDeleteCommission(e))
    }
}

export function* updateLimit({ data }) {
    yield put(GeneralSettings.requestPaymentsLimit());

    try {
        const response = yield call([Api.generalSettings, 'updatePaymentsLimit'], data);
        yield put(GeneralSettings.receivePaymentsLimit(response));
        yield put(GeneralSettings.successSubmit());
    } catch (e) {
        yield put(GeneralSettings.failurePaymentsLimit(e))
    }
}

export function* deleteLimit() {
    yield put(GeneralSettings.requestDeleteLimit());

    try {
        yield call([Api.generalSettings, 'deletePaymentsLimit'])
        yield put(GeneralSettings.receiveDeleteLimit());
        yield put(GeneralSettings.successSubmit());
    } catch (e) {
        yield put(GeneralSettings.failureDeleteLimit(e))
    }
}

function* watchFetchGeneralSettings() {
    yield takeLatest(GENERAL_SETTINGS + Store.FETCH, fetchGeneralSettings);
}

function* watchFetchLegalFormSettings() {
    yield takeLatest(SETTINGS_LEGAL_FORM + Store.FETCH, fetchLegalFormSettings);
}

function* watchFetchPaymentsSettings() {
    yield takeLatest(SETTINGS_PAYMENTS + Store.FETCH, fetchPaymentsSettings);
}

function* watchUpdateTaxSettings() {
    yield takeLatest(SETTINGS_TAX + Store.UPDATE, updateTaxSettings);
}

function* watchUpdateCommission() {
    yield takeLatest(COMMISSIONS + Store.UPDATE, updateCommission);
}

function* watchDeleteCommission() {
    yield takeLatest(COMMISSIONS + Store.DELETE, deleteCommission);
}

function* watchUpdateLimit() {
    yield takeLatest(LIMITS + Store.UPDATE, updateLimit);
}

function* watchDeleteLimit() {
    yield takeLatest(LIMITS + Store.DELETE, deleteLimit);
}

export default function* watcher() {
    yield fork(watchFetchGeneralSettings);
    yield fork(watchFetchLegalFormSettings);
    yield fork(watchFetchPaymentsSettings);
    yield fork(watchUpdateTaxSettings);
    yield fork(watchUpdateCommission);
    yield fork(watchDeleteCommission);
    yield fork(watchUpdateLimit);
    yield fork(watchDeleteLimit);
}
