import { Store } from '@justlook/core';
import {
    GENERAL_SETTINGS,
    SETTINGS_LEGAL_FORM,
    SETTINGS_PAYMENTS,
    SETTINGS_TAX,
    COMMISSIONS,
    LIMITS,
    SUCCESS_SUBMIT_FORM
} from './action-types';

const fetchGeneralSettings = () => ({ type: GENERAL_SETTINGS + Store.FETCH });
const requestGeneralSettings = () => ({ type: GENERAL_SETTINGS + Store.REQUEST });
const receiveGeneralSettings = (data) => ({ type: GENERAL_SETTINGS + Store.RECEIVE, data });
const failureGeneralSettings = (error) => ({ type: GENERAL_SETTINGS + Store.FAILURE, error });

const fetchSettingsLegalForm = () => ({ type: SETTINGS_LEGAL_FORM + Store.FETCH });
const requestSettingsLegalForm = () => ({ type: SETTINGS_LEGAL_FORM + Store.REQUEST });
const receiveSettingsLegalForm = (data) => ({ type: SETTINGS_LEGAL_FORM + Store.RECEIVE, data });
const failureSettingsLegalForm = (error) => ({ type: SETTINGS_LEGAL_FORM + Store.FAILURE, error });

const fetchSettingsPayments = () => ({ type: SETTINGS_PAYMENTS + Store.FETCH });
const requestSettingsPayments = () => ({ type: SETTINGS_PAYMENTS + Store.REQUEST });
const receiveSettingsPayments = (data) => ({ type: SETTINGS_PAYMENTS + Store.RECEIVE, data });
const failureSettingsPayments = (error) => ({ type: SETTINGS_PAYMENTS + Store.FAILURE, error });

const updateSettingsTax = (data) => ({ type: SETTINGS_TAX + Store.UPDATE, data });
const requestUpdateSettingsTax = () => ({ type: SETTINGS_TAX + Store.UPDATE_REQUEST });
const receiveUpdateSettingsTax = (resp) => ({ type: SETTINGS_TAX + Store.UPDATE_RECEIVE, resp });
const failureUpdateSettingsTax = (error) => ({ type: SETTINGS_TAX + Store.UPDATE_FAILURE, error });

const updatePaymentsCommission = (data) => ({ type: COMMISSIONS + Store.UPDATE, data });
const requestPaymentsCommission = () => ({ type: COMMISSIONS + Store.UPDATE_REQUEST });
const receivePaymentsCommission = (commission) => ({ type: COMMISSIONS + Store.UPDATE_RECEIVE, commission })
const failurePaymentsCommission = (error) => ({ type: COMMISSIONS + Store.UPDATE_FAILURE, error });

const deletePaymentsCommission = () => ({ type: COMMISSIONS + Store.DELETE });
const requestDeleteCommission = () => ({ type: COMMISSIONS + Store.DELETE_REQUEST });
const receiveDeleteCommission = () => ({ type: COMMISSIONS + Store.DELETE_RECEIVE });
const failureDeleteCommission = (error) => ({ type: COMMISSIONS + Store.DELETE_FAILURE, error });

const updatePaymentsLimit = (data) => ({ type: LIMITS + Store.UPDATE, data });
const requestPaymentsLimit = () => ({ type: LIMITS + Store.UPDATE_REQUEST });
const receivePaymentsLimit = (limit) => ({ type: LIMITS + Store.UPDATE_RECEIVE, limit });
const failurePaymentsLimit = (error) => ({ type: LIMITS + Store.UPDATE_FAILURE, error });

const deletePaymentsLimit = () => ({ type: LIMITS + Store.DELETE });
const requestDeleteLimit = () => ({ type: LIMITS + Store.DELETE_REQUEST });
const receiveDeleteLimit = () => ({ type: LIMITS + Store.DELETE_RECEIVE });
const failureDeleteLimit = (error) => ({ type: LIMITS + Store.DELETE_FAILURE, error });

const successSubmit = () => ({ type: SUCCESS_SUBMIT_FORM });

export const GeneralSettings = {
    fetchGeneralSettings,
    requestGeneralSettings,
    receiveGeneralSettings,
    failureGeneralSettings,

    fetchSettingsLegalForm,
    requestSettingsLegalForm,
    receiveSettingsLegalForm,
    failureSettingsLegalForm,

    fetchSettingsPayments,
    requestSettingsPayments,
    receiveSettingsPayments,
    failureSettingsPayments,

    updateSettingsTax,
    requestUpdateSettingsTax,
    receiveUpdateSettingsTax,
    failureUpdateSettingsTax,

    updatePaymentsCommission,
    requestPaymentsCommission,
    receivePaymentsCommission,
    failurePaymentsCommission,

    deletePaymentsCommission,
    requestDeleteCommission,
    receiveDeleteCommission,
    failureDeleteCommission,

    updatePaymentsLimit,
    requestPaymentsLimit,
    receivePaymentsLimit,
    failurePaymentsLimit,

    deletePaymentsLimit,
    requestDeleteLimit,
    receiveDeleteLimit,
    failureDeleteLimit,

    successSubmit,
}
