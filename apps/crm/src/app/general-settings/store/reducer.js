import produce from 'immer';
import {
    GENERAL_SETTINGS,
    SETTINGS_LEGAL_FORM,
    SETTINGS_PAYMENTS,
    SETTINGS_TAX,
    COMMISSIONS,
    LIMITS,
    SUCCESS_SUBMIT_FORM,
} from './action-types';

import { Store } from '@justlook/core';

const INITIAL_STATE = {
    items: {
        data: null,
        isRequest: false,
        error: null,
    },
    legal_form: {
        data: null,
        isRequest: false,
        error: null,
    },
    payments: {
        data: null,
        isRequest: false,
        error: null,
    },
    tax: {
        isRequest: false,
    },
    formState: {
        isSubmitting: false,
        isSubmitted: false,
    }
};

export const settings = (state = INITIAL_STATE, action) =>
    produce(state, draft => {
        // eslint-disable-next-line default-case
        switch (action.type) {
            case GENERAL_SETTINGS + Store.REQUEST:
                draft.items.isRequest = true;
                break;
            case GENERAL_SETTINGS + Store.RECEIVE:
                draft.items.isRequest = false;
                draft.items.data = action.data.items;
                break;
            case GENERAL_SETTINGS + Store.FAILURE:
                draft.items.isRequest = false;
                draft.items.data = null;
                draft.items.error = action.error;
                break;

            case SETTINGS_LEGAL_FORM + Store.REQUEST:
                draft.legal_form.isRequest = true;
                break;
            case SETTINGS_LEGAL_FORM + Store.RECEIVE:
                draft.legal_form.isRequest = false;
                draft.legal_form.data = action.data.items;
                break;
            case SETTINGS_LEGAL_FORM + Store.FAILURE:
                draft.legal_form.isRequest = false;
                draft.legal_form.data = null;
                draft.legal_form.error = action.error;
                break;

            case SETTINGS_PAYMENTS + Store.REQUEST:
                draft.payments.isRequest = true;
                draft.payments.error = null;
                break;
            case SETTINGS_PAYMENTS + Store.RECEIVE:
                draft.payments.isRequest = false;
                draft.payments.data = action.data.data;
                break;
            case SETTINGS_PAYMENTS + Store.FAILURE:
                draft.payments.isRequest = false;
                draft.payments.data = null;
                draft.payments.error = action.error;
                break;

            case SETTINGS_TAX + Store.UPDATE_REQUEST:
                draft.tax.isRequest = true;
                break;
            case SETTINGS_TAX + Store.UPDATE_RECEIVE:
                draft.tax.isRequest = false;
                draft.payments.data.receipt_default_service = action.resp.data.receipt_default_service;
                break;
            case SETTINGS_TAX + Store.UPDATE_FAILURE:
                draft.tax.isRequest = false;
                draft.payments.error = action.error;
                break

            case COMMISSIONS + Store.UPDATE_REQUEST:
                draft.formState.isSubmitted = false;
                draft.formState.isSubmitting = true;
                draft.payments.error = null;
                break;
            case COMMISSIONS + Store.UPDATE_RECEIVE:
                draft.formState.isSubmitted = true;
                draft.formState.isSubmitting = false;
                draft.payments.data.commission = action.commission.data;
                draft.payments.error = null;
                break;
            case COMMISSIONS + Store.UPDATE_FAILURE:
                draft.formState.isSubmitted = false;
                draft.formState.isSubmitting = false;
                draft.payments.error = action.error;
                break;

            case COMMISSIONS + Store.DELETE_REQUEST:
                draft.formState.isSubmitted = false;
                draft.formState.isSubmitting = true;
                break;
            case COMMISSIONS + Store.DELETE_RECEIVE:
                draft.formState.isSubmitted = true;
                draft.formState.isSubmitting = false;
                draft.payments.data.commission = null;
                draft.payments.error = null;
                break;
            case COMMISSIONS + Store.DELETE_FAILURE:
                draft.formState.isSubmitted = false;
                draft.formState.isSubmitting = false;
                draft.payments.error = action.error;
                break;

            case LIMITS + Store.UPDATE_REQUEST:
                draft.formState.isSubmitted = false;
                draft.formState.isSubmitting = true;
                draft.payments.error = null;
                break;
            case LIMITS + Store.UPDATE_RECEIVE:
                draft.formState.isSubmitted = true;
                draft.formState.isSubmitting = false;
                draft.payments.data.limit = action.limit.data;
                draft.payments.error = null;
                break;
            case LIMITS + Store.UPDATE_FAILURE:
                draft.formState.isSubmitted = false;
                draft.formState.isSubmitting = false;
                draft.payments.error = action.error;
                break;

            case LIMITS + Store.DELETE_REQUEST:
                draft.formState.isSubmitted = false;
                draft.formState.isSubmitting = true;
                break;
            case LIMITS + Store.DELETE_RECEIVE:
                draft.formState.isSubmitted = true;
                draft.formState.isSubmitting = false;
                draft.payments.data.limit = null;
                draft.payments.error = null;
                break;
            case LIMITS + Store.DELETE_FAILURE:
                draft.formState.isSubmitted = false;
                draft.formState.isSubmitting = false;
                draft.payments.error = action.error;
                break;

            case SUCCESS_SUBMIT_FORM:
                draft.formState.isSubmitted = false;
                break;

        }
    });
