import React, { Fragment, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { Helmet } from 'react-helmet';
import { Config, Hooks } from '@justlook/core';
import { Header, ParagraphV2 as Paragraph } from '@justlook/uikit';

import { SplashScreen } from 'components';

import { getProfile } from 'app/profile/store/selectors';

import { GeneralSettings } from 'app/general-settings/store/actions';
import { SettingsSelectors } from 'app/general-settings/store/selectors';

import { LegalFormSettings } from 'app/general-settings/components/legal-form-settings';

import cx from 'app/general-settings/pages/legal-form-page.module.scss';

const Page = () => {
    const onFetchLegalFormSettings = Hooks.useActionToDispatch(GeneralSettings.fetchSettingsLegalForm);

    const settings = useSelector(SettingsSelectors.getLegalFormSettings);
    const isRequest = useSelector(SettingsSelectors.getLegalFormSettingsIsRequest);
    const { is_demo } = useSelector(getProfile);

    useEffect(() => { onFetchLegalFormSettings() }, [onFetchLegalFormSettings])

    if(isRequest) { return <SplashScreen/> }

    return (
        <Fragment>
            <Helmet>
                <title>Правовые статусы и реквизиты - {Config.brandName}</title>
            </Helmet>
            <div className={cx.base}>
                <Header.H1>Правовые статусы и реквизиты</Header.H1>
                <Paragraph.LH24 className={cx.description}>
                    Правовые формы, которые могут принимать исполнители и доступные им реквизиты для вывода денег.
                </Paragraph.LH24>

                {settings.map((item, index) => (
                    is_demo && item.id === 1 ? null : <LegalFormSettings setting={item} key={index}/>
                ))}
            </div>
        </Fragment>
    )
}

export default Page;
