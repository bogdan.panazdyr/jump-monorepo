import React, { Fragment } from 'react';
import { useOutlet } from 'react-router-dom';
import { Config } from '@justlook/core';
import { Helmet } from 'react-helmet';

import { useContentView } from 'utils/browser';
import useUpdateSearchFilter from '../../../hooks/use-update-search-filter';

import { withPermissions } from 'components';
import { TwoColumnsLayout } from 'components/menu';

import Header from 'app/general-settings/components/general-settings-header';
import { GeneralSettingsList } from 'app/general-settings/components/general-settings-list';

import { GeneralSettings } from 'app/general-settings/store/actions';
import { SETTINGS_FILTER } from 'app/general-settings/store/filters';

const Page = () => {
    const child = useOutlet();
    const viewMode = useContentView(child);

    useUpdateSearchFilter(
        SETTINGS_FILTER,
        GeneralSettings.fetchGeneralSettings
    );

    return (
        <Fragment>
            <Helmet>
                <title>Общие - {Config.brandName}</title>
            </Helmet>

            <TwoColumnsLayout
                details={child}
                viewMode={viewMode}
                header={<Header viewMode={viewMode}/>}
            >
                <GeneralSettingsList/>
            </TwoColumnsLayout>
        </Fragment>
    )
}

export default withPermissions(Page, [ 'write-system-settings' ]);
