import React, { useState } from 'react';
import { Button, Icons24 } from '@justlook/uikit';

import { Box, Drop, EmptyContent, SplashScreen } from 'components';

import { PaymentWizardHeader } from '../components/payment-wizard/payment-wizard-header';
import { RulePopup } from '../components/payment-wizard/rule-popup';
import { DeleteRulePopup } from '../components/payment-wizard/delete-rule-popup';

import { useRulesPayments, useUpdateRulePriorities } from 'domains/payment-wizard';

import { useSelectItem } from 'hooks/use-select-item';

const Page = () => {
    const { rules, setRulesPriority, isLoading } = useRulesPayments();
    const { itemId, isSelected, setItemId, clearSelection } = useSelectItem();
    const { onUpdatePriorities } = useUpdateRulePriorities();

    const [ ruleToDelete, setRuleToDelete ] = useState(null);

    const handleChangeRulesPriority = (rules) => {
        setRulesPriority(rules);

        onUpdatePriorities(rules.map(rule => rule.id))
    }

    const renderListRule = () => {
        if(rules.length === 0) {
            return <EmptyContent isMute message='Правила не заданы'/>
        }

        return (
            <Drop
                header='Условие'
                accessor='name'
                elements={rules}
                onSetElements={handleChangeRulesPriority}
                renderCustomControl={(rule) => (
                    <div>
                        <Button
                            styling='hollow'
                            icon={<Icons24.IconPencil/>}
                            onClick={() => setItemId(rule?.id)}
                            disabled={rule?.is_readonly || false}
                        />
                        <Button
                            styling='hollow'
                            onClick={() => setRuleToDelete(rule)}
                            icon={<Icons24.IconTrash/>}
                        />
                    </div>
                )}
            />
        )
    }

    return (
        <Box pt={16} pr={24} pb={16} pl={24}>
            <PaymentWizardHeader/>

            <Box mt={24}>
                {isLoading ? <SplashScreen/> : renderListRule()}
            </Box>

            { isSelected && <RulePopup id={itemId} onDismiss={clearSelection}/> }
            { ruleToDelete && <DeleteRulePopup rule={ruleToDelete} onClose={() => setRuleToDelete(null)} /> }
        </Box>
    )
}

export default Page;
