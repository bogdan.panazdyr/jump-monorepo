import React, { useEffect, Fragment } from 'react';
import { useSelector } from 'react-redux';
import { Helmet } from 'react-helmet';
import { Config, Hooks } from '@justlook/core';
import { Header } from '@justlook/uikit';

import Api from 'api/client';

import { EditableField, RadioGroup, SplashScreen } from 'components';

import { GeneralSettings } from 'app/general-settings/store/actions';
import { SettingsSelectors } from 'app/general-settings/store/selectors';
import { getProfile } from 'app/profile/store/selectors';

import { Commission } from 'app/general-settings/components/commission';
import { Limit } from 'app/general-settings/components/limit';

import cx from 'app/general-settings/pages/payments-page.module.scss';

const Page = () => {
    const onFetchPaymentsSettings = Hooks.useActionToDispatch(GeneralSettings.fetchSettingsPayments);
    const onUpdatePaymentsSettingsTax = Hooks.useActionToDispatch(GeneralSettings.updateSettingsTax);

    const interplay = useSelector(SettingsSelectors.getPaymentsSettingsInterface);
    const isRequest = useSelector(SettingsSelectors.getPaymentsSettingsIsRequest);
    const { is_demo } = useSelector(getProfile);

    const taxValue = useSelector(SettingsSelectors.getPaymentsSettingsTax);
    const isRequestTax = useSelector(SettingsSelectors.getTaxSettingsIsRequest);

    useEffect(() => { onFetchPaymentsSettings() }, [onFetchPaymentsSettings]);

    const onUpdatePaymentsInterface = (field, value) => {
        Api.generalSettings.updatePaymentsInterface(field, { value })
    }

    if(isRequest) { return <SplashScreen/> }

    return (
        <Fragment>
            <Helmet>
                <title>Выплаты - {Config.brandName}</title>
            </Helmet>
            <div className={cx.base}>
                <Header.H1>Выплаты</Header.H1>
                <Header.H2 className={cx['sub-header']}>Доступ</Header.H2>

                <RadioGroup
                    fields={interplay}
                    onActionChange={onUpdatePaymentsInterface}
                />

                {!is_demo && <Commission/>}
                {!is_demo && <Limit/>}

                {is_demo &&
                    <div className={cx.wrapper}>
                        <Header.H2>Наименование услуг</Header.H2>

                        <EditableField
                            label='Название услуги передаваемое в ИФНС по самозанятым'
                            defaultValue={taxValue}
                            loading={isRequestTax}
                            onUpdateAction={(value) => onUpdatePaymentsSettingsTax({ receipt_default_service: value })}
                        />
                    </div>
                }

            </div>
        </Fragment>
    )
}

export default Page;
