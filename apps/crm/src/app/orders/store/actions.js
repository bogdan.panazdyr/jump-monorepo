import { Store } from '@justlook/core';
import { ORDERS_LIST, ORDER, SUCCESS_SUBMIT_FORM } from 'app/orders/store/action-types';

const fetchOrdersList = (filter) => ({ type: ORDERS_LIST + Store.FETCH, filter });
const requestOrdersList = () => ({ type: ORDERS_LIST + Store.REQUEST });
const receiveOrdersList = (orders) => ({ type: ORDERS_LIST + Store.RECEIVE, orders });
const failureOrdersList = (error) => ({ type: ORDERS_LIST + Store.FAILURE, error });

const createOrders = (order) => ({ type: ORDER + Store.CREATE, order });
const requestCreateOrder = () => ({ type: ORDER + Store.CREATE_REQUEST });
const receiveCreateOrder = (order) => ({ type: ORDER + Store.CREATE_RECEIVE, order });
const failureCreateOrder = (error) => ({ type: ORDER + Store.CREATE_FAILURE, error });

const successSubmit = () => ({ type: SUCCESS_SUBMIT_FORM })

export const OrdersActions = {
    fetchOrdersList,
    requestOrdersList,
    receiveOrdersList,
    failureOrdersList,

    createOrders,
    requestCreateOrder,
    receiveCreateOrder,
    failureCreateOrder,

    successSubmit,
}
