import produce from 'immer';
import { Store } from '@justlook/core';

import { ORDERS_LIST, ORDER, SUCCESS_SUBMIT_FORM } from 'app/orders/store/action-types';

const INITIAL_STATE = {
    items: {
        data: null,
        meta: null,
        isRequest: false,
        error: null,
    },
    item: {
        data: null,
        isRequest: false,
        error: null,
    },
    formState: {
        isSubmitting: false,
        isSubmitted: false
    }
};

export const orders = (state = INITIAL_STATE, action) =>
    produce(state, draft => {
        // eslint-disable-next-line default-case
        switch (action.type) {
            case ORDERS_LIST + Store.REQUEST:
                draft.items.isRequest = true;
                draft.items.error = null;
                break;
            case ORDERS_LIST + Store.RECEIVE:
                draft.items.isRequest = false;
                draft.items.data = action.orders.items;
                draft.items.meta = action.orders.meta;
                break;
            case ORDERS_LIST + Store.FAILURE:
                draft.items.isRequest = false;
                draft.items.data = null;
                draft.items.meta = null;
                draft.items.error = action.error;
                break;

            case ORDER + Store.CREATE_REQUEST:
                draft.formState.isSubmitting = true;
                draft.formState.isSubmitted = false;
                draft.items.error = null;
                break;
            case ORDER + Store.CREATE_RECEIVE:
                draft.formState.isSubmitting = false;
                draft.formState.isSubmitted = true;
                draft.items.data = [action.order.item, ...state.items.data]
                break;
            case ORDER + Store.CREATE_FAILURE:
                draft.formState.isSubmitting = false;
                draft.formState.isSubmitted = false;
                draft.items.error = action.error;
                break;

            case SUCCESS_SUBMIT_FORM:
                draft.formState.isSubmitted = false;
                break;
        }
    });
