import { put, fork, call, takeLatest } from 'redux-saga/effects';
import { Store } from '@justlook/core';

import { ORDERS_LIST, ORDER } from 'app/orders/store/action-types';
import { OrdersActions } from 'app/orders/store/actions';

import Api from 'api/client';

import { deliveryToRequest } from 'app/orders/domain/request-factory';

export function * fetchOrdersList({ filter }) {
    yield put(OrdersActions.requestOrdersList());

    try {
        const response = yield call([Api.orders, 'fetchOrdersList'], filter);
        yield put(OrdersActions.receiveOrdersList(response));
    } catch (e) {
        yield put(OrdersActions.failureOrdersList(e))
    }
}

export function * createDeliveryOrder({ order }) {
    yield put(OrdersActions.requestCreateOrder());

    try {
        const response = yield call([Api.orders, 'createDelivery'], deliveryToRequest(order))
        yield put(OrdersActions.receiveCreateOrder(response));
        yield put(OrdersActions.successSubmit());
    } catch (e) {
        yield put(OrdersActions.failureCreateOrder(e));
    }
}

function * watchFetchOrders() {
    yield takeLatest(ORDERS_LIST + Store.FETCH, fetchOrdersList);
}

function * watchCreateOrder() {
    yield takeLatest(ORDER + Store.CREATE, createDeliveryOrder);
}

export default function* watcher() {
    yield fork(watchFetchOrders);
    yield fork(watchCreateOrder);
}
