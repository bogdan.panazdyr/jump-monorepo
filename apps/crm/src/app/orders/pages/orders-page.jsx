import React, { Fragment } from 'react';
import { useSelector } from 'react-redux';
import { Helmet } from 'react-helmet';
import { Config } from '@justlook/core';
import { Pagination } from '@justlook/uikit';

import { withPermissions } from 'components';
import { OneColumnsLayout } from 'components/menu';
import useUpdateSearchFilter from '../../../hooks/use-update-search-filter';

import { OrdersActions } from 'app/orders/store/actions';
import { ORDERS_FILTER } from 'app/orders/store/filters';
import { OrdersSelectors } from 'app/orders/store/selectors';

import OrdersHeader from 'app/orders/components/orders-header';
import OrdersList from 'app/orders/components/orders-list';

import cx from 'app/orders/pages/orders-page.module.scss';

const Page = () => {

    const { updateFilter } = useUpdateSearchFilter(ORDERS_FILTER, OrdersActions.fetchOrdersList);
    const { last_page, current_page } = useSelector(OrdersSelectors.getOrdersMeta);

    return (
        <Fragment>
            <Helmet>
                <title>Заказы - {Config.brandName}</title>
            </Helmet>
            <OneColumnsLayout header={<OrdersHeader/>}>
                <div className={cx.base}>
                    <OrdersList/>
                </div>
                <div className={cx.paginator}>
                    {last_page > 1 &&
                        <Pagination
                            handlePageClick={(value) => updateFilter({ page: value.selected + 1 })}
                            initialPage={current_page - 1}
                            pageCount={last_page}
                        />
                    }
                </div>
            </OneColumnsLayout>
        </Fragment>
    )
}

export default withPermissions(Page, [ 'write-order' ]);
