import React from 'react';
import PropTypes from 'prop-types';
import { Popup, ParagraphV2 as Paragraph, Header } from '@justlook/uikit';

import { useMoney } from 'hooks/use-money';
import { formattingDate } from 'utils/formatting-date';

import cx from './view-order-modal.module.scss';

export const ViewOrder = ({ order, onDismiss }) => {
    const { currency, format } = useMoney();

    return (
        <Popup onDismiss={onDismiss}>
            <Popup.Header>Заказ</Popup.Header>
            <Popup.Content>
                <div className={cx.base}>
                    <Paragraph.LH24>Тип заказа
                        <span>{order.type.title}</span>
                    </Paragraph.LH24>

                    <Paragraph.LH24>Время отправления
                        <span>{formattingDate(order.pickup_date)}</span>
                    </Paragraph.LH24>

                    <Paragraph.LH24>Стоимость заказа
                        <span>{format(order.cost.value) + ' ' + currency}</span>
                    </Paragraph.LH24>
                </div>
                <div className={cx.base}>
                    <Header.H3>Отправитель</Header.H3>
                    <Paragraph.LH24>Коммент. к заказу
                        <span>{order.offers[1].comment}</span>
                    </Paragraph.LH24>
                    <Paragraph.LH24>Номер телефона <span>{order.offers[0].contacts.phone}</span></Paragraph.LH24>
                    <Paragraph.LH24>Адрес
                        <span>
                            {order.offers[0].address.city}&nbsp;
                            {order.offers[0].address.street}&nbsp;
                            {order.offers[0].address.house}&nbsp;
                            {order.offers[0].address.flat}&nbsp;
                        </span>
                    </Paragraph.LH24>
                    <Paragraph.LH24>Коммент. к адресу
                        <span>{order.offers[0].address.comment}</span>
                    </Paragraph.LH24>
                </div>

                <div className={cx.base}>
                    <Header.H3>Получатель</Header.H3>
                    <Paragraph.LH24>Имя <span>{order.offers[1].contacts.name}</span></Paragraph.LH24>
                    <Paragraph.LH24>Номер телефона <span>{order.offers[1].contacts.phone}</span></Paragraph.LH24>
                    <Paragraph.LH24>Адрес
                        <span>
                                {order.offers[1].address.city}&nbsp;
                            {order.offers[1].address.street}&nbsp;
                            {order.offers[1].address.house}&nbsp;
                            {order.offers[1].address.flat}&nbsp;
                            </span>
                    </Paragraph.LH24>
                    <Paragraph.LH24>Коммент. к адресу
                        <span>
                                {order.offers[1].address.comment}
                            </span>
                    </Paragraph.LH24>
                </div>

                {
                    order.contractor &&
                    <div className={cx.base}>
                        <Header.H3>Исполнитель</Header.H3>
                        <Paragraph.LH24>ФИО <span>{order.contractor.name}</span></Paragraph.LH24>
                        <Paragraph.LH24>Номер телефона <span>{order.contractor.phone}</span></Paragraph.LH24>
                    </div>
                }
            </Popup.Content>
        </Popup>
    )
}

ViewOrder.propTypes = {
    onDismiss: PropTypes.func,
    order: PropTypes.object,
}
