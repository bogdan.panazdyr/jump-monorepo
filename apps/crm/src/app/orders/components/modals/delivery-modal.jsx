import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { useImmer } from 'use-immer';
import { useSelector } from 'react-redux';
import { useForm, Controller } from 'react-hook-form';
import { Hooks } from '@justlook/core';
import { Popup, Form, Button, Input, CalendarV2 as Calendar, Header, Textarea } from '@justlook/uikit';
import { useIfMediaScreen } from '@justlook/uikit/lib/hooks';

import { HiddenContent, DaDataField } from 'components';

import { OrdersSelectors } from 'app/orders/store/selectors';
import { OrdersActions } from 'app/orders/store/actions';

import { onSelectAddress } from 'app/orders/utils/on-select-address';
import { setErrorsOrder } from 'app/orders/utils/set-errors-order';

import cx from 'app/orders/components/modals/delivery-modal.module.scss';

const FORM_ID = 'delivery_form';

export const DeliveryModal = ({ onDismiss }) => {
    const isMobile = useIfMediaScreen();
    const { handleSubmit, control, errors, setValue, setError } = useForm();

    const [state, setState] = useImmer({
        from_city: '',
        from_street: '',
        from_house: '',
        from_geo_lat: '',
        from_geo_lon: '',

        to_city: '',
        to_street: '',
        to_house: '',
        to_geo_lat: '',
        to_geo_lon: '',
    });

    const { isSubmitting, isSubmitted } = useSelector(OrdersSelectors.getFormState);
    const errorsField = useSelector(OrdersSelectors.getOrderInvalidFields);
    const onCreateDeliveryOrder = Hooks.useActionToDispatch(OrdersActions.createOrders);

    const onSelectFromAddress = (address) => onSelectAddress(address, setState, 'from', setValue, 'from-address');
    const onSelectToAddress = (address) => onSelectAddress(address, setState, 'to', setValue, 'to-address');

    useEffect(() => { if(isSubmitted) onDismiss() }, [isSubmitted, onDismiss]);
    useEffect(() => { setErrorsOrder(errorsField, setError) }, [setError, errorsField]);

    const preparationValues = (values) => {
        const copyValues = {...values}

        copyValues['from-city'] = state.from_city
        copyValues['from-street'] = state.from_street
        copyValues['from-house'] = state.from_house
        copyValues['from-geo-lat'] = state.from_geo_lat
        copyValues['from-geo-lon'] = state.from_geo_lon

        copyValues['type-id'] = 3
        copyValues.offers = [
            {
                client_phone: copyValues['to-phone'],
                client_name: copyValues['to-client-name'],
                declared_cost: 0,
                comment: copyValues['to-comment'],
                address: {
                    city: state.to_city,
                    street: state.to_street,
                    house: state.to_house,
                    flat: copyValues['to-flat'],
                    comment: copyValues['to-comment-address'],
                    coordinates: {
                        lat: state.to_geo_lat,
                        lon: state.to_geo_lon,
                    }
                }
            }
        ]

        return copyValues;
    }

    const onSubmit = (values) => onCreateDeliveryOrder(preparationValues(values));

    return (
        <Popup onDismiss={onDismiss}>
            <Popup.Header>
                Новый заказ
            </Popup.Header>
            <Popup.Content>
                <Form onSubmit={handleSubmit(onSubmit)} id={FORM_ID}>
                    <Form.Field className={cx.calendar}>
                        <Controller
                            as={Calendar}
                            label='Время отправления'
                            name='from-date'
                            control={control}
                            labelPosition='left'
                            className={cx.calendarField}
                            rules={{ required: 'Обязательно для ввода' }}
                            error={errors['from-date'] && errors['from-date'].message}
                        />
                        <Controller
                            as={Input}
                            name='from-time'
                            control={control}
                            mask='99:99'
                            placeholder='00:00'
                            rules={{ required: 'Обязательно для ввода' }}
                            error={errors['from-time'] && errors['from-time'].message}
                        />
                    </Form.Field>

                    <Form.Field>
                        <Controller
                            as={Input}
                            name='cost'
                            label='Стоимость заказа'
                            placeholder='0.00'
                            control={control}
                            className={cx.cost}
                            rules={{ required: 'Обязательно для ввода' }}
                            error={errors['cost'] && errors['cost'].message}
                        />
                    </Form.Field>

                    <Form.Field>
                        <HiddenContent label='Добавить описание' position={isMobile && 'standard'}>
                            <Controller
                                as={Textarea}
                                name='to-comment'
                                placeholder='Описание заказа'
                                control={control}
                            />
                        </HiddenContent>
                    </Form.Field>

                    <Header.H2 className={cx['sub-title']}>Отправитель</Header.H2>

                    <Form.Field>
                        <Controller
                            as={Input}
                            name='from-name'
                            label='Имя'
                            control={control}
                            className={cx.grid}
                        />
                    </Form.Field>

                    <Form.Field>
                        <Controller
                            as={Input}
                            name='from-phone'
                            label='Номер телефона'
                            mask='+79999999999'
                            control={control}
                            className={cx.grid}
                            rules={{ required: 'Обязательно для ввода' }}
                            error={errors['from-phone'] && errors['from-phone'].message}
                        />
                    </Form.Field>

                    <Form.Field>
                        <Controller
                            as={DaDataField}
                            name='from-address'
                            label='Адрес'
                            control={control}
                            onSelect={onSelectFromAddress}
                            rules={{ required: 'Обязательно для ввода' }}
                            error={errors['from-address'] && errors['from-address'].message}
                        />
                    </Form.Field>

                    <Form.Field>
                        <Controller
                            as={Input}
                            name='from-flat'
                            label='Квартира/Офис'
                            control={control}
                            className={cx.grid}
                        />
                    </Form.Field>

                    <Form.Field>
                        <HiddenContent label='Добавить комментарий' position={isMobile && 'standard'}>
                            <Controller
                                as={Textarea}
                                name='from-comment'
                                placeholder='Комментарий к адресу'
                                control={control}
                            />
                        </HiddenContent>
                    </Form.Field>

                    <Header.H2 className={cx['sub-title']}>Получатель</Header.H2>

                    <Form.Field>
                        <Controller
                            as={Input}
                            name='to-client-name'
                            label='Имя'
                            control={control}
                            className={cx.grid}
                        />
                    </Form.Field>

                    <Form.Field>
                        <Controller
                            as={Input}
                            name='to-phone'
                            label='Номер телефона'
                            mask='+79999999999'
                            control={control}
                            className={cx.grid}
                            rules={{ required: 'Обязательно для ввода' }}
                            error={errors['to-phone'] && errors['to-phone'].message}
                        />
                    </Form.Field>

                    <Form.Field>
                        <Controller
                            as={DaDataField}
                            name='to-address'
                            label='Адрес'
                            control={control}
                            onSelect={onSelectToAddress}
                            rules={{ required: 'Обязательно для ввода' }}
                            error={errors['to-address'] && errors['to-address'].message}
                        />
                    </Form.Field>

                    <Form.Field>
                        <Controller
                            as={Input}
                            name='to-flat'
                            label='Квартира/Офис'
                            control={control}
                            className={cx.grid}
                        />
                    </Form.Field>

                    <Form.Field>
                        <HiddenContent label='Добавить комментарий' position={isMobile && 'standard'}>
                            <Controller
                                as={Textarea}
                                name='to-comment-address'
                                placeholder='Комментарий к адресу'
                                control={control}
                            />
                        </HiddenContent>
                    </Form.Field>
                </Form>
            </Popup.Content>
            <Popup.Footer
                majorButton={
                    <Button form={FORM_ID} loading={isSubmitting} type='submit'>
                        Добавить
                    </Button>
                }
                onDismissButton={
                    <Button loading={isSubmitting} onClick={onDismiss} styling='hollow-border'>
                        Отмена
                    </Button>
                }
            />
        </Popup>
    )
}

DeliveryModal.propTypes = {
    onDismiss: PropTypes.func,
}
