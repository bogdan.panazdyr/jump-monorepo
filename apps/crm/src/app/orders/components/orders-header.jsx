import React, { Fragment } from 'react';
import { Hooks } from '@justlook/core';
import { Icons24, MobileHeader } from '@justlook/uikit';
import { useIfMediaScreen } from '@justlook/uikit/lib/hooks';

import { useMenu } from 'components/menu';

import { DeliveryModal } from 'app/orders/components/modals/delivery-modal';

const FORM_CREATE = 'create';

const OrdersHeader = () => {
    const { isOpen, onOpen, onClose } = Hooks.useModal();
    const { onExpandedMobile } = useMenu();
    const isMobile = useIfMediaScreen();

    return (
        <Fragment>
            <MobileHeader header='Заказы'>
                <MobileHeader.Button icon={<Icons24.IconPlus/>} onClick={() => onOpen(FORM_CREATE)}/>

                { isMobile && <MobileHeader.Button icon={<Icons24.IconMenu/>} onClick={onExpandedMobile}/> }
            </MobileHeader>

            {isOpen(FORM_CREATE) && <DeliveryModal onDismiss={() => onClose(FORM_CREATE)}/>}
        </Fragment>
    )
}

export default OrdersHeader;
