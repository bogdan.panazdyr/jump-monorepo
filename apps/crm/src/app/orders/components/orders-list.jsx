import React, { Fragment } from 'react';
import { useSelector } from 'react-redux';
import { Hooks } from '@justlook/core';
import { Icons24, Button } from '@justlook/uikit';

import { EmptyContent, SplashScreen, NewTable as Table } from 'components';

import { formattingDate } from 'utils/formatting-date';

import { OrdersSelectors } from 'app/orders/store/selectors';

import { ViewOrder } from 'app/orders/components/modals/view-order-modal';

const FORM_VIEW = 'view';

export const OrdersList = () => {
    const { isOpen, onOpen, onClose, getPayload } = Hooks.useModal();

    const isRequest = useSelector(OrdersSelectors.getIsRequest);
    const orders = useSelector(OrdersSelectors.getOrdersList);

    if(isRequest) { return  <SplashScreen/>}
    if(orders.length === 0) { return <EmptyContent message='Заказов нет'/> }

    return (
        <Fragment>
            <Table>
                <Table.Head
                    cells={[
                        { label: 'Дата' },
                        { label: 'Статус' },
                        { label: 'Тел. клиента' },
                        { label: 'Исполнитель' },
                        { label: 'Цена' },
                    ]}
                />
                {orders.map(item => (
                    <Table.Row
                        key={item.id}
                        renderCustomCell={
                            <Button
                                styling='hollow'
                                icon={<Icons24.IconPencil/>}
                                onClick={() => onOpen(FORM_VIEW, item)}
                            />
                        }
                    >
                        <Table.Cell>
                            { item?.pickup_date && formattingDate(item?.pickup_date) }
                        </Table.Cell>
                        <Table.Cell>
                            { item?.status?.title || '' }
                        </Table.Cell>
                        <Table.Cell>
                            { item?.phone }
                        </Table.Cell>
                        <Table.Cell>
                            { item?.contractor?.name }
                        </Table.Cell>
                        <Table.Cell>
                            { item?.cost?.value || 0 }
                        </Table.Cell>
                    </Table.Row>
                ))}
            </Table>

            {isOpen(FORM_VIEW) && <ViewOrder order={getPayload(FORM_VIEW)} onDismiss={() => onClose(FORM_VIEW)}/>}
        </Fragment>
    )
}

export default OrdersList;
