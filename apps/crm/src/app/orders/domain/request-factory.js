import { Dates } from '@justlook/core/lib/utils';

export function deliveryToRequest (data) {
    return {
        type_id: data['type-id'],
        phone: data['from-phone'],
        cost: data['cost'],
        pickup_date: Dates.toISOJoinedDate(data['from-date'], data['from-time']),
        address: {
            city: data['from-city'],
            street: data['from-street'],
            house: data['from-house'],
            flat: data['from-flat'],
            comment: data['from-comment'],
            coordinates: {
                lat: data['from-geo-lat'],
                lon: data['from-geo-lon'],
            }
        },
        offers: data.offers
    }
}
