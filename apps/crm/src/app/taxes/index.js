import StatementPage from './pages/statements-page';
import StatementDetailsPage from './pages/statement-details-page';

export {
    StatementPage,
    StatementDetailsPage
}
