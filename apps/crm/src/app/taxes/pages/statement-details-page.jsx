import React, { Fragment, useMemo } from 'react';
import { useNavigate } from 'react-router';
import { useParams, useLocation } from 'react-router-dom';
import { Helmet } from 'react-helmet';

import { Config, Hooks } from '@justlook/core';
import { InfoMessage, Icons24, ParagraphV2 as Paragraph } from '@justlook/uikit'
import { useIfMediaScreen } from '@justlook/uikit/lib/hooks';

import { getUri } from 'utils/get-uri';
import { SplashScreen, withPermissions, Indent, snackBar, Status } from 'components';
import { Accordion } from 'components/accordion/accordion';
import { HeaderWithBackBtn } from 'components/headers';
import {
    STATEMENT_DEDUCTION_STATUSES,
    STATEMENT_STATUSES,
    STATEMENT_STATUSES_STYLE,
    useStatementContext,
    StatementContextProvider,
    useRefreshStatement,
} from 'domains/taxes';
import { getStatusIcon } from 'domains/taxes/helpers';

import { DeductiosFilter } from '../components/deductions-filter';
import { DeductionsTotal } from '../components/deductions-total';
import { AboutPaymentTaxModal } from '../components/about-payment-tax-modal';
import RefreshConfirmModal from '../components/refresh-statement-modal';
import { DeductionsList, DeductionListHeader } from '../components'

const REFRESH_MODAL = 'refresh_modal';

const Page = () => {
    const { id } = useParams();
    const navigate = useNavigate();
    const isMobile = useIfMediaScreen();
    const { state: locationState } = useLocation();
    const {
        statementData,
        isStatementLoading,
        isStatementFetching,
        isEditable,
        isWriteTaxes,
        isPaid,
        deductionsGroupedByStatus,
        deductionFilter,
        isOpenAboutPaymentTax,
        setIsOpenAboutPaymentTax,
    } = useStatementContext();
    const { all: allDeductions, readyForPay, notEnoughMoney, rejected } = deductionsGroupedByStatus;
    const { isOpen, onOpen, onClose } = Hooks.useModal();

    const { onRefreshStatement, isRefreshingStatementMutating } = useRefreshStatement(
        () => {snackBar('Запущено обновление.')},
        () => {snackBar('Произошла ошибка! Попробуйте позже.')}
    );

    const handleRefreshStatement = () => {
        onRefreshStatement({id: statementData.id});
        onClose(REFRESH_MODAL);
    }

    const renderDescription = () => {
        return (
            <Fragment>
                <Status
                    icon={getStatusIcon(STATEMENT_STATUSES_STYLE[statementData?.status?.id || 1])}
                    text={statementData?.status?.title}
                    color={STATEMENT_STATUSES_STYLE[statementData?.status?.id || 1]}
                />

                <Paragraph.LH18>
                    &emsp;{statementData?.period?.title && `${statementData?.period?.title} · `}
                    {statementData?.agent?.name}
                </Paragraph.LH18>
            </Fragment>
        )
    }

    const deductions = useMemo(() => {
        if (!isPaid) return allDeductions;
        if (deductionFilter === STATEMENT_DEDUCTION_STATUSES.ready_pay) return readyForPay;
        if (deductionFilter === STATEMENT_DEDUCTION_STATUSES.not_enough_money) return notEnoughMoney;
        if (deductionFilter === STATEMENT_DEDUCTION_STATUSES.cannot_pay) return rejected;

    }, [ isPaid, allDeductions, deductionFilter, readyForPay, notEnoughMoney, rejected ]);

    const renderTable = () => {
        if (statementData.status.id === STATEMENT_STATUSES.payment || statementData.status.id === STATEMENT_STATUSES.wait_payment) {
            return (
                <Fragment>
                    <Accordion title='Недостаточно средств' initialOpen={Boolean(notEnoughMoney.length)}
                               badge={String(notEnoughMoney.length)}
                               disabled={!notEnoughMoney.length}>
                        <DeductionsList id='unpaid' isLoading={isStatementLoading} isFetching={isStatementFetching}
                                        deductions={notEnoughMoney}
                                        statusId={statementData?.status?.id} isSelected={true}/>
                    </Accordion>
                    <Accordion title='Готовы к оплате' initialOpen={Boolean(readyForPay.length)}
                               badge={String(readyForPay.length)}
                               disabled={!readyForPay.length}>
                        <DeductionsList id='paid' isLoading={isStatementLoading} isFetching={isStatementFetching} deductions={readyForPay}
                                        statusId={statementData?.status?.id} isSelected={true}/>
                    </Accordion>
                    <Accordion title='Невозможно уплатить налог' initialOpen={Boolean(rejected.length)}
                               badge={String(rejected.length)}
                               disabled={!rejected.length}>
                        <DeductionsList id='rejected' isLoading={isStatementLoading} isFetching={isStatementFetching} deductions={rejected}
                                        statusId={statementData?.status?.id}/>
                    </Accordion>
                </Fragment>
            )
        }

        return (
            <DeductionsList
                id='all'
                isLoading={isStatementLoading}
                isFetching={isStatementFetching}
                deductions={deductions}
                statusId={statementData?.status?.id}
            />
        )
    };

    if (isStatementLoading) return <SplashScreen/>

    return (
        <Fragment>
            <Helmet>
                <title>Ведомость - {statementData?.agent?.name || id} - {Config.brandName}</title>
            </Helmet>

            <HeaderWithBackBtn
                title='Ведомость по налогам самозанятых'
                description={renderDescription()}
                onClickBack={() => navigate(getUri('/taxes/statements', { params: locationState }))}
            />

            {
                isWriteTaxes &&
                <DeductionListHeader
                    onRefresh={() => onOpen(REFRESH_MODAL)}
                    isRefreshing={statementData?.refresh?.in_process || isRefreshingStatementMutating}
                />
            }
            {
                isEditable &&
                <Indent
                    as='section'
                    outsideLeft={isMobile ? 's' : 'm'}
                    outsideRight={isMobile ? 's' : 'm'}
                    outsideTop='s'
                    outsideBottom='s'
                >
                    <InfoMessage
                        style={{ width: '100%' }}
                        icon={<Icons24.IconInfo/>}
                        fluid={true}
                        type='default'
                    >
                        Если суммы удержания недостаточно, разница удерживается с баланса исполнителя.
                        При оплате счёта, удержания по всем невыбранным исполнителям будут возвращены на их балансы.
                    </InfoMessage>
                </Indent>
            }

            {
                !isEditable &&
                <Indent as='section' insideLeft={isMobile ? 's' : 'm'} insideRight={isMobile ? 's' : 'm'}>
                    <DeductionsTotal data={statementData}/>
                    {isPaid && <DeductiosFilter/>}
                </Indent>
            }

            <Indent as='section' insideLeft={isMobile ? null : 'm'} insideRight={isMobile ? null : 'm'} mb={80}>
                {renderTable()}
            </Indent>

            {
                isOpenAboutPaymentTax &&
                <AboutPaymentTaxModal onClose={() => setIsOpenAboutPaymentTax(false)} />
            }
            {
                isOpen(REFRESH_MODAL) &&
                <RefreshConfirmModal
                    onRefresh={handleRefreshStatement}
                    onDismiss={() => onClose(REFRESH_MODAL)}
                />
            }
        </Fragment>
    )
};

const PageWithProviders = () => {
    const { id } = useParams();

    return (
        <StatementContextProvider id={id}>
            <Page/>
        </StatementContextProvider>
    )
}

export default withPermissions(PageWithProviders, [ 'read-taxes' ]);
