import React, { Fragment } from 'react';
import { Helmet } from 'react-helmet';
import { useNavigate } from 'react-router';

import { Config } from '@justlook/core';
import { Pagination } from '@justlook/uikit';
import { useIfMediaScreen } from '@justlook/uikit/lib/hooks';

import { withPermissions, Indent, Box } from 'components';
import { OneColumnsLayout } from 'components/menu';
import { useStatements } from 'domains/taxes';

import HeaderStatementsPage from '../components/header-statements-page';
import { StatementsList } from '../components';

const Page = () => {
    const { statements, meta, filter, updateFilter, isLoading, isFetching } = useStatements();
    const isMobile = useIfMediaScreen();
    const { last_page = 1, current_page = 1 } = meta;
    const navigate = useNavigate();

    function onSelectStatement(id) {
        navigate(`/taxes/statements/${id}`, { state: filter })
    }

    return (
        <Fragment>
            <Helmet>
                <title>Налоги - {Config.brandName}</title>
            </Helmet>
            <OneColumnsLayout header={<HeaderStatementsPage/>}>
                <div id="content-statements">
                    <Indent
                        as='section'
                        insideBottom='m'
                        mb={30}
                        insideRight={isMobile ? null : 'm'}
                        insideLeft={isMobile ? null : 'm'}
                    >
                        <StatementsList
                            isLoading={isLoading}
                            isFetching={isFetching}
                            statements={statements}
                            onSelectStatement={onSelectStatement}
                        />
                        {
                            last_page > 1 &&
                            <Box
                                mt={24}
                                ml={isMobile ? 16 : 0}
                                mr={isMobile ? 16 : 0}
                            >
                                <Pagination
                                    handlePageClick={(value) => updateFilter({ page: value.selected + 1 })}
                                    initialPage={current_page - 1}
                                    pageCount={last_page}
                                />
                            </Box>
                        }
                    </Indent>
                </div>
            </OneColumnsLayout>
        </Fragment>
    )
}

export default withPermissions(Page, [ 'read-taxes' ]);
