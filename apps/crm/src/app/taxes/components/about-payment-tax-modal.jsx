import React from 'react';
import PropTypes from 'prop-types';

import { Icons16, Icons48, Button, Header, ParagraphV2 as Paragraph } from '@justlook/uikit';

import { Modal, Box } from 'components';

import cx from './about-payment-tax-modal.module.scss';

export const AboutPaymentTaxModal = ({ onClose }) => {
    return (
        <Modal
            onClose={onClose}
            majorButton={<Button onClick={onClose}>Понятно</Button>}
        >
            <Box className={cx.base}>
                <Icons48.IconDocDownload/>
                <Header.H1 className={cx.title}>Счет на оплату скачивается</Header.H1>
                <Paragraph.LH24 className={cx.desc}>
                    <Icons16.IconWarning/>
                    Не забудьте оплатить его в банке.
                </Paragraph.LH24>
            </Box>
        </Modal>
    )
}

AboutPaymentTaxModal.propTypes = {
    onClose: PropTypes.func.isRequired,
}
