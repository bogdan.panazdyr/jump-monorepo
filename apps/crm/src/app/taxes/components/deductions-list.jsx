import React, { useMemo } from 'react';
import PropTypes from 'prop-types';

import { Checkbox, Icons24, ParagraphV2 as Paragraph } from '@justlook/uikit';
import { useIfMediaScreen } from '@justlook/uikit/lib/hooks';

import { List, ListItem } from 'components/list';
import { round } from 'utils/helpers';
import { useMoney } from 'hooks/use-money';
import { IconBar, NewTable as Table } from 'components/index';
import { useStatementContext, STATEMENT_DEDUCTION_STATUSES } from 'domains/taxes';

import cx from './deductions-table.module.scss';

export const DeductionsList = ({ id, isSelected, deductions = [] }) => {
    const { currency, format } = useMoney();
    const isMobile = useIfMediaScreen();
    const {
        isEditable,
        isPaid,
        isWriteTaxes,
        isCompletedPay,
        onToggleDeduction,
        onToggleDeductions,
        isBlocked,
        isSelectAllDeduction,
        selectedDeductions,
        getGroupDeduction,
    } = useStatementContext();

    const cells = [
        { label: 'Исполнитель' },
        { label: `Удержано, ${currency}`, className: cx.currency },
    ];

    if (isEditable || isPaid) {
        cells.push({ label: `Начислен налог, ${currency}`, className: cx.currency });
    }

    if(isPaid) {
        cells.push({ label: `Уплачен налог, ${currency}`, className: cx.currency });
    }

    const selectCount = useMemo(() => {
        let count = 0;
        deductions.forEach(deduction => selectedDeductions[deduction.id] && count++)
        return count;
    }, [ deductions, selectedDeductions ]);

    const IconBarWrapper = (deduction) => {
        return () => {
            if ((isEditable || isPaid) && getGroupDeduction(deduction) === STATEMENT_DEDUCTION_STATUSES.cannot_pay) {
                return (
                    <IconBar
                        className={cx.info}
                        positionBar={isMobile ? 'left-center' : 'top-right'}
                        data={{
                            icon: <Icons24.IconInfo className={cx.warning}/>,
                            label: deduction?.decline_info?.message
                        }}
                    />
                )
            }

            return null;
        }
    };

    const renderList = () => {
        return (
            <List className={cx.mobileList}>
                {deductions.map(deduction => (
                    <ListItem
                        selectable={isSelected}
                        hoverable={isSelected}
                        className={cx.mobileDescription}
                        key={deduction.id}
                        icon={IconBarWrapper(deduction)()}
                        disabled={isBlocked || !isWriteTaxes}
                        id={`select-item-${id}-${deduction.id}`}
                        checked={selectedDeductions[deduction.id] || isSelectAllDeduction}
                        onSelect={(value) => onToggleDeduction(deduction.id, value)}
                        onClick={() => isSelected && onToggleDeduction(deduction.id, !(selectedDeductions[deduction.id] || isSelectAllDeduction)) }
                    >
                        <Paragraph.LH18>{deduction?.contractor?.full_name || ''}</Paragraph.LH18>
                        <Paragraph.LH18 className={cx.mobileDescriptionLine}>
                            <span>Удержано:</span>
                            <span>{round(deduction?.pre_amount || 0, false)}&nbsp;{currency}</span>
                        </Paragraph.LH18>
                        {(isEditable || isPaid) &&
                        <Paragraph.LH18 className={cx.mobileDescriptionLine}>
                            <span>{isPaid ? 'Начислен налог:' :'Налог:'}</span>
                            <span>{round(deduction?.real_amount || 0, false)}&nbsp;{currency}</span>
                        </Paragraph.LH18>
                        }

                        {
                            isPaid &&
                            <Paragraph.LH18 className={cx.mobileDescriptionLine}>
                                <span>Уплачен налог</span>
                                <span>
                                    {
                                        isCompletedPay(deduction)
                                            ? round(deduction?.real_amount || 0, false)
                                            : 0
                                    }&nbsp;{currency}
                                </span>
                            </Paragraph.LH18>
                        }
                    </ListItem>
                ))}
            </List>
        )
    };

    const renderTable = () => {
        return <Table className={cx.deductions_table}>
            <Table.Head
                cells={cells}
                renderCheckBox={
                    isSelected && isEditable &&
                    <Checkbox
                        disabled={isBlocked || !isWriteTaxes}
                        id={`selectAll-${id}`}
                        mixed={selectCount > 0 && selectCount < deductions.length}
                        checked={selectCount > 0}
                        onChange={(value) => onToggleDeductions(deductions, value)}
                    />
                }
            />
            {deductions.map(deduction => (
                <Table.Row
                    key={deduction.id}
                    mobileTitle={deduction?.contractor?.full_name || ''}
                    mobileDescription={
                        <div className={cx.mobileDescription}>
                            <Paragraph.LH18 className={cx.mobileDescriptionLine}>
                                <span>Удержано:</span>
                                <span>{format(deduction?.pre_amount || 0)}&nbsp;{currency}</span>
                            </Paragraph.LH18>
                            {(isEditable || isPaid) &&
                            <Paragraph.LH18 className={cx.mobileDescriptionLine}>
                                <span>Налог:</span>
                                <span>{format(deduction?.real_amount || 0)}&nbsp;{currency}</span>
                            </Paragraph.LH18>
                            }
                        </div>
                    }
                    disableMobilePopup={true}
                    mobileIcon={IconBarWrapper(deduction)}
                    mobileVAlign='center'
                    renderCheckboxCell={
                        isSelected && <Checkbox
                            disabled={isBlocked || !isWriteTaxes}
                            id={`select-item-${id}-${deduction.id}`}
                            checked={(selectedDeductions[deduction.id]) || isSelectAllDeduction}
                            onChange={(value) => onToggleDeduction(deduction.id, value)}/>
                    }
                >
                    <Table.Cell>
                        {(isEditable || isPaid)
                        && getGroupDeduction(deduction) === STATEMENT_DEDUCTION_STATUSES.cannot_pay
                        && IconBarWrapper(deduction)()}
                        {deduction?.contractor?.full_name || ''}
                    </Table.Cell>
                    <Table.Cell className={cx.currency}>
                        {format(deduction?.pre_amount || 0)}
                    </Table.Cell>

                    {
                        (isEditable || isPaid) &&
                        <Table.Cell className={cx.currency}>
                            {format(deduction?.real_amount || 0)}
                        </Table.Cell>
                    }

                    {
                        isPaid &&
                        <Table.Cell className={cx.currency}>
                            {
                                isCompletedPay(deduction)
                                    ? format(deduction?.real_amount || 0)
                                    : 0
                            }
                        </Table.Cell>
                    }
                </Table.Row>
            ))}
        </Table>
    };

    return isMobile ? renderList() : renderTable()
}

DeductionsList.propTypes = {
    id: PropTypes.any.isRequired,
    isSelected: PropTypes.bool,
    deductions: PropTypes.array.isRequired,
}
