import React from 'react';
import PropTypes from 'prop-types';

import { Header, Popup, Button, ParagraphV2 as Paragraph, Icons48 } from '@justlook/uikit';

import cx from './refresh-statement-modal.module.scss';

const RefreshConfirmModal = ({ onDismiss, onRefresh }) => {
    return (
        <Popup onDismiss={onDismiss}>
            <Popup.Header/>
            <Popup.Content>
                <div className={cx.content}>
                    <Icons48.IconWarning className={cx.icon}/>
                    <Header.H1>Обновление ведомости может занять время</Header.H1>
                    <Paragraph.LH24>
                        Будут проверены статусы исполнителей и налоговые начисления.
                        Время ожидания занимает в среднем 2 минуты, но зависит от количества исполнителей.
                    </Paragraph.LH24>
                </div>
            </Popup.Content>
            <Popup.Footer
                majorButton={<Button onClick={onRefresh}>Обновить</Button>}
                onDismissButton={<Button styling='hollow-border' onClick={onDismiss}>Отмена</Button>}
            />
        </Popup>
    )
}

RefreshConfirmModal.propTypes = {
    onDismiss: PropTypes.func,
    onRefresh: PropTypes.func,
};

export default RefreshConfirmModal;
