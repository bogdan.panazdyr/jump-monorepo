import React, { useEffect } from 'react';
import PropTypes from 'prop-types';

import { Select } from '@justlook/uikit';
import { useIfMediaScreen } from '@justlook/uikit/lib/hooks';

import { Button, ButtonGroup } from 'components';
import { useStatementContext, STATEMENT_DEDUCTION_STATUSES } from 'domains/taxes';

import cx from './deductions-filter.module.scss'

export const DeductiosFilter = ({ children }) => {
    const {
        deductionsGroupedByStatus,
        onChangeDeductionsFilter,
        isActiveDeductionsFilter,
        deductionFilter
    } = useStatementContext();
    const { readyForPay, notEnoughMoney, rejected } = deductionsGroupedByStatus;
    const isMobile = useIfMediaScreen();

    useEffect(() => {
        if (!readyForPay.length && !notEnoughMoney.length) onChangeDeductionsFilter(STATEMENT_DEDUCTION_STATUSES.cannot_pay)
        else if (!readyForPay.length) onChangeDeductionsFilter(STATEMENT_DEDUCTION_STATUSES.not_enough_money)
    }, [ readyForPay, notEnoughMoney, rejected, onChangeDeductionsFilter ]);

    const filtersPreset = [
        {
            value: STATEMENT_DEDUCTION_STATUSES.ready_pay,
            label: 'Оплаченные',
            badge: readyForPay.length,
            isDisabled: !readyForPay.length
        },
        {
            value: STATEMENT_DEDUCTION_STATUSES.not_enough_money,
            label: 'Неоплаченные',
            badge: notEnoughMoney.length,
            isDisabled: !notEnoughMoney.length
        },
        {
            value: STATEMENT_DEDUCTION_STATUSES.cannot_pay,
            label: 'Невозможно оплатить',
            badge: rejected.length,
            isDisabled: !rejected.length
        },
    ];

    const selectValue = filtersPreset.find((preset) => preset.value === deductionFilter)

    return (
        <div className={cx.deductions_filter}>
            {
                !isMobile &&
                <ButtonGroup className={cx.buttons}>
                    {filtersPreset.map((preset) => (
                        <Button
                            key={preset.value}
                            active={isActiveDeductionsFilter(preset.value)}
                            disabled={!preset.badge}
                            onClick={() => onChangeDeductionsFilter(preset.value)}>
                            {preset.label} {!!preset.badge && <span className={cx.muted}>{preset.badge}</span>}
                        </Button>
                    ))}
                </ButtonGroup>
            }

            <div className={isMobile ? cx.content_mobile : cx.content}>
                {children}
            </div>

            {
                isMobile &&
                <div className={cx.select}>
                    <Select
                        isSearchable={false}
                        options={filtersPreset}
                        value={selectValue}
                        onChange={(selectedOption) => {
                            onChangeDeductionsFilter(selectedOption.value);
                        }}
                    />
                </div>
            }
        </div>
    )
}

DeductiosFilter.propTypes = {
    onUpdate: PropTypes.func,
    children: PropTypes.node,
}
