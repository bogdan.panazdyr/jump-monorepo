import React from 'react';
import PropTypes from 'prop-types';
import { Toggle, Icons16 } from '@justlook/uikit';
import { useIfMediaScreen } from '@justlook/uikit/src/hooks';

import { IconBar } from 'components';

import cx from './deduction-return-taxes.module.scss';

export const DeductionReturnTaxes = ({ isDisabled, value, onChange }) => {
    const isMobile = useIfMediaScreen()

    const hint = () => (
        <div>
            Сумма уплаченного налога будет
            <br/>
            зачислена исполнителю на счёт.
        </div>
    )

    return (
        <div className={cx.base}>
            <Toggle
                label='Возместить налоги исполнителям'
                checked={value}
                onChange={onChange}
                disabled={isDisabled}
            />
            <IconBar
                className={cx.info}
                positionBar={isMobile ? 'top-center' : 'bottom-center'}
                data={{
                    icon: <Icons16.IconInfo/>,
                    label: hint()
                }}
            />
        </div>
    )
}

DeductionReturnTaxes.propTypes = {
    value: PropTypes.bool.isRequired,
    onChange: PropTypes.func.isRequired,
    isDisabled: PropTypes.bool,
}
