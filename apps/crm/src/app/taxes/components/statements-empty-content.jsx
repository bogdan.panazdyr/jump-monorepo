import React from 'react';

import { EmptyContent } from 'components';

export const StatementsEmptyContent = () => {

    return (
        <EmptyContent
            message='Ведомостей нет'
            isMute
        />
    )
}
