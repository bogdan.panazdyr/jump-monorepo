import React, { useMemo } from 'react';
import PropsTypes from 'prop-types';

import { Button, Checkbox, Icons24, OverflowMenu } from '@justlook/uikit';
import { useIfMediaScreen } from '@justlook/uikit/lib/hooks';

import { useMoney } from 'hooks/use-money';
import { STATEMENT_STATUSES } from 'domains/taxes';
import { useStatementContext } from 'domains/taxes/contexts/statement-context';

import { DeductionReturnTaxes } from './deduction-return-taxes';

import cx from './deduction-list-header.module.scss'

export const DeductionListHeader = ({isRefreshing, onRefresh}) => {
    const {
        statementData,
        isEditable,
        isBlocked,
        isStatementLoading,
        availableSelectDeduction,
        selectedDeductions,
        selectDeductionCount,
        isSelectAllDeduction,
        onSelectAllDeductions,
        needCompensationCompany,
        setNeedCompensationCompany,
        confirmStatement,
        deleteConfirmStatement,
        isConfirmationLoading,
        downloadInvoice,
        isDownloading,
    } = useStatementContext();

    const isMobile = useIfMediaScreen();
    const { format, currency } = useMoney();
    const total = useMemo(() => {
        let total = 0;
        statementData.deductions.forEach((deduction) => {
            if (selectedDeductions[deduction.id]) {
                total += deduction.real_amount;
            }
        });
        return total;
    }, [ statementData.deductions, selectedDeductions ]);

    const renderButtons = () => {
        return (
            <div className={cx.buttons}>
                {isEditable && isBlocked
                    ? <>
                        <Button
                            styling='hollow-border'
                            icon={<Icons24.IconDownload/>}
                            loading={isDownloading}
                            onClick={downloadInvoice}>
                            {!isMobile ? 'Скачать счет' : ''}
                        </Button>
                        <Button
                            styling='hollow-border'
                            icon={<Icons24.IconPencil/>}
                            loading={isConfirmationLoading || isStatementLoading}
                            onClick={deleteConfirmStatement}>
                            {!isMobile && 'Изменить счет'}
                        </Button>
                    </>
                    : <Button disabled={!selectDeductionCount}
                              loading={isConfirmationLoading || isStatementLoading}
                              icon={isMobile ? <Icons24.IconCheck/> : undefined}
                              onClick={confirmStatement}>
                        {isMobile ? 'Счет' : 'Сформировать счет'}
                    </Button>
                }
                {
                    statementData.status.id === STATEMENT_STATUSES.wait_payment && (
                        <OverflowMenu icon={<Icons24.IconKebab/>}>
                            <OverflowMenu.Item onItemClick={onRefresh}>
                                Обновить ведомость
                            </OverflowMenu.Item>
                        </OverflowMenu>
                    )
                }
            </div>
        )
    }

    if (!isEditable) return null;

    if(isRefreshing) return (
        <div className={cx.deduction_list_header}>
            <div className={cx.refreshing}><Icons24.IconProgress/>&nbsp;Ведомость обновляется</div>
        </div>
    )

    return (
        <div className={cx.deduction_list_header}>
            <div className={cx.state}>
                <Checkbox
                    disabled={isBlocked || availableSelectDeduction.length === 0}
                    id='selectAll'
                    mixed={!isSelectAllDeduction && selectDeductionCount < availableSelectDeduction.length}
                    checked={isSelectAllDeduction || selectDeductionCount > 0}
                    onChange={(value) => onSelectAllDeductions(value)}
                />
                <span className={cx.label}>
                        {`${selectDeductionCount} из ${availableSelectDeduction.length}. `}
                    <>Итого налог: <strong>{format(total)}</strong> {currency}</>
                    </span>
            </div>
            <div className={cx.buttonsWrapper}>
                <DeductionReturnTaxes
                    value={needCompensationCompany || statementData?.compensation_from_company}
                    onChange={setNeedCompensationCompany}
                    isDisabled={isBlocked}
                />
                {renderButtons()}
            </div>
        </div>
    )
}

DeductionListHeader.propTypes = {
    onRefresh: PropsTypes.func,
    isRefreshing: PropsTypes.bool,
}
