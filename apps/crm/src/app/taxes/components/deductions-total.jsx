import React from 'react';
import PropTypes  from 'prop-types';
import { ParagraphV2 as Paragraph } from '@justlook/uikit';

import { useMoney } from 'hooks/use-money';

import { Total } from 'domains/taxes/models';

import { round } from 'utils/helpers';

import cx from './deduction-total.module.scss';

export const DeductionsTotal = ({ data }) => {
    const { currency } = useMoney();

    const total = new Total(data);

    const totals = total.getTotals();

    return (
        <div className={cx.base}>
            <Paragraph.LH24><b>Итого</b></Paragraph.LH24>

            {totals.map(total =>
                <Paragraph.LH24 key={total.title}>
                    {total.title}
                    <span>{round(total.value, false)} {currency}</span>
                </Paragraph.LH24>
            )}
        </div>
    )
}

DeductionsTotal.propTypes = {
    data: PropTypes.object.isRequired
}
