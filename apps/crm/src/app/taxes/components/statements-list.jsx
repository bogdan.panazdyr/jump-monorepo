import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import classname from 'classnames';

import { Icons24, Description, InfoMessage } from '@justlook/uikit';
import { useIfMediaScreen } from '@justlook/uikit/lib/hooks';

import { List, ListItem } from 'components/list';
import { useMoney } from 'hooks/use-money';
import { NewTable as Table, SplashScreen, Font, Indent, Status } from 'components';
import { useStatements, STATEMENT_STATUSES_STYLE } from 'domains/taxes';
import { getStatusIcon } from 'domains/taxes/helpers';

import { StatementsEmptyContent } from './statements-empty-content';

import cx from './statements-list.module.scss';

const getStatusesStyle = (item) => {
    return STATEMENT_STATUSES_STYLE[item?.status?.id || 1];
}

const StatementsList = ({ isLoading, isFetching, statements, onSelectStatement }) => {
    const { currency, format } = useMoney();
    const { updateFilter, filter } = useStatements();
    const isMobile = useIfMediaScreen();

    const cls = classname(cx.statements_list, { [cx.is_loading]: isFetching })
    const { order = '-period' } = filter;

    if (isLoading) return <SplashScreen/>
    if (statements.length === 0) return <StatementsEmptyContent/>

    const onClickMobileSort = () => {
        updateFilter({ order: order === '-period' ? 'period' : '-period' })
    }

    const renderTable = () => {
        return <Table className={cls}>
            <Table.Head
                cells={[
                    { label: 'Дата', className: cx.date, sort: 'period' },
                    { label: 'Юрлицо', className: cx.agent },
                    { label: 'Статус ведомости', className: cx.status },
                    { label: `Удержано, ${currency}`, className: cx.currency },
                    { label: `Начислен налог, ${currency}`, className: cx.currency },
                    { label: `Уплачен налог, ${currency}`, className: cx.currency },
                ]}
                sortField={order}
                onSort={(order) => updateFilter({ order })}
            />
            {statements.map(item => (
                <Table.Row
                    key={item.id}
                    onClick={() => onSelectStatement(item.id)}
                >
                    <Table.Cell className={cx.date}>
                        {item?.period?.title || ''}
                    </Table.Cell>
                    <Table.Cell className={cx.agent}>
                        {item?.agent?.name}
                    </Table.Cell>
                    <Table.Cell className={cx.status}>
                        <Status
                            icon={getStatusIcon(getStatusesStyle(item))}
                            text={item?.status?.title}
                            color={getStatusesStyle(item)}
                        />
                    </Table.Cell>
                    <Table.Cell className={cx.currency}>
                        {format(item?.pre_amount || 0)}
                    </Table.Cell>
                    <Table.Cell className={cx.currency}>
                        {format(item?.tax_amount || 0)}
                    </Table.Cell>
                    <Table.Cell className={cx.currency}>
                        {format(item?.paid_amount || 0)}
                    </Table.Cell>
                </Table.Row>
            ))}
        </Table>
    }

    const renderList = () => {
        return (
            <List>
                {statements.map(item => (
                    <ListItem
                        key={item.id}
                        hoverable={!isMobile}
                        className={cx.listItem}
                        icon={<Icons24.IconArrowRight className={cx.icon}/>}
                        onClick={() => onSelectStatement(item.id)}
                    >
                        <Indent outsideBottom='xs'>
                            <Font as='p' size={18}>
                                {item?.period?.title || ''}
                            </Font>
                        </Indent>

                        <Font as='p' size={18} className={cx.description}>
                            <Status
                                icon={getStatusIcon(getStatusesStyle(item))}
                                text={item?.status?.title}
                                color={getStatusesStyle(item)}
                            />
                            &nbsp;{item?.agent?.name}
                        </Font>
                    </ListItem>
                ))}
            </List>
        )
    }

    return (
        <Fragment>
            {
                isMobile && (
                    <Description.LH16
                        onClick={onClickMobileSort}
                        className={cx.mobile_header}
                    >
                        {order === '-period' ? 'По дате (новый—старый)' : 'По дате (старый—новый)'}
                    </Description.LH16>
                )
            }

            <Indent
                insideBottom={isMobile ? 's' : null}
                insideTop={'s'}
                insideRight={isMobile ? 's' : null}
                insideLeft={isMobile ? 's' : null}
            >
                <InfoMessage
                    className={cx.message}
                    icon={<Icons24.IconInfo/>}
                    fluid={true}
                    type='default'
                >
                    Налоговые начисления происходят 13 числа за&nbsp;предыдущий отчётный период.
                    Уплату налогов необходимо произвести не&nbsp;позднее 25&nbsp;числа того же&nbsp;месяца, в&nbsp;котором сформирована ведомость.
                </InfoMessage>
            </Indent>

            {isMobile ? renderList() : renderTable()}
        </Fragment>
    )
};

StatementsList.propTypes = {
    onSelectStatement: PropTypes.func.isRequired,
    isLoading: PropTypes.bool.isRequired,
    isFetching: PropTypes.bool.isRequired,
    statements: PropTypes.array.isRequired,
}

export default StatementsList
