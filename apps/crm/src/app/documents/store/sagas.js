import { put, fork, call, takeLatest } from 'redux-saga/effects';
import { Store } from '@justlook/core';

import Api from 'api/client';

import { DOCUMENTS_LIST, DOCUMENT } from 'app/documents/store/action-types';
import { DocumentsAction } from 'app/documents/store/actions';

function * fetchDocumentsList ({filter = {}}) {
    yield put(DocumentsAction.requestDocumentsList());

    try {
        const response = yield call([Api.documents, 'fetchDocumentsList'], filter);
        yield put(DocumentsAction.receiveDocumentsList(response));
    } catch (error) {
        yield put(DocumentsAction.failureDocumentsList(error))
    }
}

function * fetchDocument ({ id }) {
    yield put(DocumentsAction.requestDocument());

    try {
        const response = yield call([Api.documents, 'fetchDocument'], id);
        yield put(DocumentsAction.receiveDocument(response));
    } catch (error) {
        yield put(DocumentsAction.failureDocument(error))
    }
}

function * createDocument({ document }) {
    yield put(DocumentsAction.requestCreateDocument());

    try {
        const response = yield call([Api.documents, 'create'], document);
        yield put(DocumentsAction.receiveCreateDocument(response));
        yield put(DocumentsAction.successSubmit());
    } catch (error) {
        yield put(DocumentsAction.failureCreateDocument(error))
    }
}

function * updateDocument({ id, document }) {
    yield put(DocumentsAction.requestUpdateDocument());

    try {
        const response = yield call([Api.documents, 'update'], id, document)
        yield put(DocumentsAction.receiveUpdateDocument(response));
        yield put(DocumentsAction.successSubmit());
    } catch (error) {
        yield put(DocumentsAction.failureUpdateDocument(error))
    }
}

function * deleteDocument({ id }) {
    yield put(DocumentsAction.requestDeleteDocument());

    try {
        yield call([Api.documents, 'delete'], id);
        yield put(DocumentsAction.receiveDeleteDocument(id));
        yield put(DocumentsAction.successSubmit());
    } catch (error) {
        yield put(DocumentsAction.failureDeleteDocument(error))
    }
}

function * watchFetchDocumentsList () {
    yield takeLatest(DOCUMENTS_LIST + Store.FETCH, fetchDocumentsList);
}

function * watchFetchDocument () {
    yield takeLatest(DOCUMENT + Store.FETCH, fetchDocument);
}

function * watcherCreateDocument () {
    yield takeLatest(DOCUMENT + Store.CREATE, createDocument);
}

function * watcherUpdateDocument () {
    yield takeLatest(DOCUMENT + Store.UPDATE, updateDocument);
}

function * watcherDeleteDocument () {
    yield takeLatest(DOCUMENT + Store.DELETE, deleteDocument);
}

export default function * watcher () {
    yield fork(watchFetchDocumentsList);
    yield fork(watchFetchDocument);
    yield fork(watcherCreateDocument);
    yield fork(watcherUpdateDocument);
    yield fork(watcherDeleteDocument);
}
