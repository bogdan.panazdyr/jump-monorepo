import produce from 'immer';
import { Store } from '@justlook/core';
import { DOCUMENTS_LIST, DOCUMENT, SUCCESS_SUBMIT_FORM } from './action-types';

const INITIAL_STATE = {
    items: {
        data: null,
        meta: null,
        isRequest: false,
        error: null
    },
    item: {
        data: null,
        isRequest: false,
        error: null
    },
    formState: {
        isSubmitting: false,
        isSubmitted: false
    }
}

export const documents = (state = INITIAL_STATE, action) =>
    produce(state, draft => {
        // eslint-disable-next-line default-case
        switch (action.type) {
            case DOCUMENTS_LIST + Store.REQUEST:
                draft.items.isRequest = true;
                draft.items.error = null;
                break;
            case DOCUMENTS_LIST + Store.RECEIVE:
                draft.items.isRequest = false;
                draft.items.data = action.data.items;
                draft.items.meta = action.data.meta;
                break;
            case DOCUMENTS_LIST + Store.FAILURE:
                draft.items.isRequest = false;
                draft.items.data = null;
                draft.items.meta = null;
                draft.items.error = action.error;
                break;

            case DOCUMENT + Store.REQUEST:
                draft.item.isRequest = true;
                draft.item.error = null;
                break;
            case DOCUMENT + Store.RECEIVE:
                draft.item.isRequest = false;
                draft.item.data = action.document;
                break;
            case DOCUMENT + Store.FAILURE:
                draft.item.isRequest = false;
                draft.item.data = null;
                draft.item.error = action.error;
                break;

            case DOCUMENT + Store.CREATE_REQUEST:
                draft.formState.isSubmitting = true;
                draft.formState.isSubmitted = false;
                break;
            case DOCUMENT + Store.CREATE_RECEIVE:
                draft.formState.isSubmitting = false;
                draft.formState.isSubmitted = true;
                draft.items.data = [action.document, ...state.items.data]
                draft.items.error = null;
                break;
            case DOCUMENT + Store.CREATE_FAILURE:
                draft.formState.isSubmitting = false;
                draft.formState.isSubmitted = false;
                draft.items.error = action.error;
                break;

            case DOCUMENT + Store.UPDATE_REQUEST:
                draft.formState.isSubmitting = true;
                draft.formState.isSubmitted = false;
                break;
            case DOCUMENT + Store.UPDATE_RECEIVE:
                draft.formState.isSubmitting = false;
                draft.formState.isSubmitted = true;
                draft.items.data = draft.items.data.map(item => {
                    if(item.id === action.document.id) {
                        return action.document;
                    }
                    return item;
                });
                draft.items.error = null;
                break;
            case DOCUMENT + Store.UPDATE_FAILURE:
                draft.formState.isSubmitting = false;
                draft.formState.isSubmitted = false;
                draft.items.error = action.error;
                break;

            case DOCUMENT + Store.DELETE_REQUEST:
                draft.formState.isSubmitting = true;
                draft.formState.isSubmitted = false;
                break;
            case DOCUMENT + Store.DELETE_RECEIVE:
                draft.formState.isSubmitting = false;
                draft.formState.isSubmitted = true;
                draft.items.data = draft.items.data.filter(item => item.id !== action.id);
                draft.items.error = null;
                break
            case DOCUMENT + Store.DELETE_FAILURE:
                draft.formState.isSubmitting = false;
                draft.formState.isSubmitted = false;
                draft.items.error = action.error;
                break;

            case SUCCESS_SUBMIT_FORM:
                draft.formState.isSubmitted = false;
                break;
        }
    })
