import { Store } from '@justlook/core';
import { DOCUMENTS_LIST, DOCUMENT, SUCCESS_SUBMIT_FORM } from './action-types';

const fetchDocumentsList = (filter) => ({ type: DOCUMENTS_LIST + Store.FETCH, filter });
const requestDocumentsList = () => ({ type: DOCUMENTS_LIST + Store.REQUEST });
const receiveDocumentsList = (data) => ({ type: DOCUMENTS_LIST + Store.RECEIVE, data });
const failureDocumentsList = (error) => ({ type: DOCUMENTS_LIST + Store.FAILURE, error });

const fetchDocument = (id) => ({ type: DOCUMENT + Store.FETCH, id });
const requestDocument = () => ({ type: DOCUMENT + Store.REQUEST });
const receiveDocument = (document) => ({ type: DOCUMENT + Store.RECEIVE, document: document.item });
const failureDocument = (error) => ({ type: DOCUMENT + Store.FAILURE, error });

const createDocuments = (document) => ({ type: DOCUMENT + Store.CREATE, document });
const requestCreateDocument = () => ({ type: DOCUMENT + Store.CREATE_REQUEST });
const receiveCreateDocument = (document) => ({ type: DOCUMENT + Store.CREATE_RECEIVE, document: document.item });
const failureCreateDocument = (error) => ({ type: DOCUMENT + Store.CREATE_FAILURE, error });

const updateDocument = (id, document) => ({ type: DOCUMENT + Store.UPDATE, id, document });
const requestUpdateDocument = () => ({ type: DOCUMENT + Store.UPDATE_REQUEST });
const receiveUpdateDocument = (document) => ({ type: DOCUMENT + Store.UPDATE_RECEIVE, document: document.item });
const failureUpdateDocument = (error) => ({ type: DOCUMENT + Store.UPDATE_FAILURE, error })

const deleteDocument = (id) => ({ type: DOCUMENT + Store.DELETE, id });
const requestDeleteDocument = () => ({ type: DOCUMENT + Store.DELETE_REQUEST });
const receiveDeleteDocument = (id) => ({ type: DOCUMENT + Store.DELETE_RECEIVE, id });
const failureDeleteDocument = (error) => ({ type: DOCUMENT + Store.DELETE_FAILURE, error });

const successSubmit = () => ({ type: SUCCESS_SUBMIT_FORM })

export const DocumentsAction = {
    fetchDocumentsList,
    requestDocumentsList,
    receiveDocumentsList,
    failureDocumentsList,

    fetchDocument,
    requestDocument,
    receiveDocument,
    failureDocument,

    createDocuments,
    requestCreateDocument,
    receiveCreateDocument,
    failureCreateDocument,

    updateDocument,
    requestUpdateDocument,
    receiveUpdateDocument,
    failureUpdateDocument,

    deleteDocument,
    requestDeleteDocument,
    receiveDeleteDocument,
    failureDeleteDocument,

    successSubmit,
}


