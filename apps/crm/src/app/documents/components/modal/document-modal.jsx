import React, { useEffect, useMemo } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { useForm, Controller } from 'react-hook-form';
import { Popup, Button, Input, Select, Icons24, Form, ParagraphV2 as Paragraph } from '@justlook/uikit';
import { Hooks, Forms } from '@justlook/core';
import { useIfMediaScreen } from '@justlook/uikit/lib/hooks';

import { SplashScreen } from 'components';

import { DocumentsAction } from 'app/documents/store/actions';
import { DocumentsSelectors } from 'app/documents/store/selectors';

import { addServerErrors } from 'utils/add-server-errors';

import { useAgentsMy } from 'domains/agents';

import cx from 'app/documents/components/modal/document-modal.module.scss';

const FORM_ID = 'documents-form';

const DocumentModal = ({ document, onDismiss }) => {
    const isMobile = useIfMediaScreen();
    const { agents: agentsList, isLoading } = useAgentsMy();

    const { handleSubmit, errors, control, setError } = useForm({
        defaultValues: {
            title: document && document?.title,
            url: document && document?.url,
            agentId: document && (
                Forms.itemToOption(document?.agent, {
                    label: 'name',
                    value: 'id'
                })
            )
        }
    });

    const { isSubmitting, isSubmitted } = useSelector(DocumentsSelectors.getFormState);
    const errorFields = useSelector(DocumentsSelectors.getDocumentInvalidFields);

    const onCreateDocument = Hooks.useActionToDispatch(DocumentsAction.createDocuments);
    const onUpdateDocument = Hooks.useActionToDispatch(DocumentsAction.updateDocument);
    const onDeleteDocument = Hooks.useActionToDispatch(DocumentsAction.deleteDocument);

    useEffect(() => { if (isSubmitted) onDismiss() }, [ isSubmitted, onDismiss ]);

    useMemo(() => { addServerErrors(errorFields, setError) }, [errorFields, setError]);

    const header = document ? 'Редактирование документа' : 'Новый документ';
    const submitLabel = document ? 'Сохранить' : 'Добавить';
    const defaultAgent = agentsList.length === 1 ? Forms.itemToOption(agentsList[0]) : null;

    const prepareDocumentForSubmit = (values) => ({ ...values, agent_id: values.agentId.value });

    const onSubmit = values => document
        ? onUpdateDocument(document.id, prepareDocumentForSubmit(values))
        : onCreateDocument(prepareDocumentForSubmit(values))

    const onDelete = () => onDeleteDocument(document.id);

    return (
        <Popup onDismiss={onDismiss}>
            <Popup.Header>{header}</Popup.Header>
            <Popup.Content>
                { isLoading && <SplashScreen/> }
                { !isLoading &&
                    <Form onSubmit={handleSubmit(onSubmit)} id={FORM_ID}>
                        <Form.Field>
                            <Controller
                                as={Input}
                                name='title'
                                label='Название'
                                className={cx.grid}
                                control={control}
                                rules={{ required: 'Обязательно для ввода' }}
                                error={errors.title && errors.title.message}
                            />
                        </Form.Field>
                        <Form.Field>
                            <Controller
                                as={Input}
                                name='url'
                                label='Ссылка на файл'
                                className={cx.grid}
                                control={control}
                                rules={{ required: 'Обязательно для ввода' }}
                                error={errors.url && errors.url.message}
                            />
                        </Form.Field>
                        <Form.Field>
                            <div className={cx.grid}>
                                <label><Paragraph.LH24>Юрлицо</Paragraph.LH24></label>
                                <Controller
                                    as={Select}
                                    name='agentId'
                                    control={control}
                                    defaultValue={defaultAgent}
                                    options={Forms.makeOptions(agentsList, { value: 'id', label: 'name' })}
                                    rules={{ required: 'Обязательно для ввода' }}
                                    error={errors.agentId && errors.agentId.message}
                                />
                            </div>
                        </Form.Field>
                </Form>
                }
            </Popup.Content>
            <Popup.Footer
                onDismissButton={
                    <Button styling='hollow-border' onClick={onDismiss} loading={isSubmitting}>
                        Отмена
                    </Button>
                }
                majorButton={<Button form={FORM_ID} type='submit' loading={isSubmitting}>{submitLabel}</Button>}
                minorButton={document && (
                    <Button
                        styling='hollow-border'
                        icon={!isMobile && <Icons24.IconTrash/>}
                        onClick={onDelete}
                        loading={isSubmitting}
                    >
                        Удалить
                    </Button>
                )}
            />
        </Popup>
    )
};

DocumentModal.propTypes = {
    document: PropTypes.object,
    onDismiss: PropTypes.func,
}

export default DocumentModal;
