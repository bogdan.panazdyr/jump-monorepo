import React, { Fragment } from 'react';
import { useSelector } from 'react-redux';
import { Hooks } from '@justlook/core';
import { Icons24, Link, Button } from '@justlook/uikit';

import { EmptyContent, SplashScreen, NewTable as Table } from 'components';

import { DocumentsSelectors } from '../store/selectors';

import DocumentModal from './modal/document-modal';

const FORM_EDIT = 'edit';

const DocumentsList = () => {
    const { isOpen, onOpen, onClose, getPayload } = Hooks.useModal();

    const isRequest = useSelector(DocumentsSelectors.getDocumentsIsRequest);
    const documentsList = useSelector(DocumentsSelectors.getDocuments);

    const prepareDataTable = documentsList.map(item => ({
        ...item,
        title: item.title,
        agent_name: item.agent.name,
        url: item.url
    }))

    if(isRequest) {
        return <SplashScreen/>
    }

    if(documentsList.length < 1) {
        return <EmptyContent message='Вы еще не добавили ни одного документа'/>
    }

    return (
        <Fragment>
            <Table>
                <Table.Head cells={[
                    { label: 'Название' },
                    { label: 'Юрлицо' },
                    { label: 'Файл' },
                ]}/>

                {prepareDataTable.map(item => (
                    <Table.Row
                        key={item.id}
                        renderCustomCell={
                            <Button
                                styling='hollow'
                                icon={<Icons24.IconPencil/>}
                                onClick={() => onOpen(FORM_EDIT, item)}
                            />
                        }
                    >
                        <Table.Cell>
                            { item.title }
                        </Table.Cell>
                        <Table.Cell>
                            { item.agent_name }
                        </Table.Cell>
                        <Table.Cell>
                            <Link key={item.id} to={item.url} target='_blank'>Документ</Link>
                        </Table.Cell>
                    </Table.Row>
                ))}
            </Table>

            {isOpen(FORM_EDIT) &&
                <DocumentModal
                    document={getPayload(FORM_EDIT)}
                    onDismiss={() => onClose(FORM_EDIT)}
                />
            }
        </Fragment>
    )
}

export default DocumentsList;
