import DocumentsHeader from './documents-header';
import DocumentsList from './documents-list';
import DocumentModal from './modal/document-modal';

export {
    DocumentsHeader,
    DocumentsList,
    DocumentModal,
}
