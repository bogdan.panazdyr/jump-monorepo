import React, { Fragment } from 'react';
import { useSelector } from 'react-redux';
import { Helmet } from 'react-helmet';
import { Pagination } from '@justlook/uikit';

import { withPermissions, Notice } from 'components';
import { OneColumnsLayout } from 'components/menu';
import useUpdateSearchFilter from '../../../hooks/use-update-search-filter';

import { DocumentsHeader, DocumentsList } from 'app/documents/components';
import { DocumentsAction } from 'app/documents/store/actions';
import { DocumentsSelectors } from 'app/documents/store/selectors';
import { DOCUMENTS_FILTER } from 'app/documents/store/filters';

import cx from './documents-page.module.scss';

const Page = () => {
    const { updateFilter } = useUpdateSearchFilter(DOCUMENTS_FILTER, DocumentsAction.fetchDocumentsList);
    const { last_page, current_page } = useSelector(DocumentsSelectors.getDocumentsMeta);

    return (
        <Fragment>
            <Helmet>
                <title>Настройки - Документы</title>
            </Helmet>

            <OneColumnsLayout header={<DocumentsHeader/>}>
                <div className={cx.base}>
                    <Notice label='Документы, которые исполнители подписывают перед началом работы'/>
                    <DocumentsList/>
                </div>
                <div className={cx.paginator}>
                    {last_page > 1 &&
                        <Pagination
                            handlePageClick={(value) => updateFilter({ page: value.selected + 1 })}
                            initialPage={current_page - 1}
                            pageCount={last_page}
                        />
                    }
                </div>
            </OneColumnsLayout>
        </Fragment>
    )
}

export default withPermissions(Page, [ 'write-document' ]);
