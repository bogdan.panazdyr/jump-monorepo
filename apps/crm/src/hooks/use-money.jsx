import { getCurrency } from 'app/init/store/selectors';
import { useSelector } from 'react-redux';
import { formatMoney } from 'utils/helpers';

export const useMoney = () => {
    const currency = useSelector(getCurrency);

    return {currency, format: formatMoney};
}
