import { useLocation } from 'react-router';
import { parse } from 'query-string';
import { useQuery, useQueryClient } from 'react-query';

import { Utils } from '@justlook/core';

export const useTesting = () => {
    const { search } = useLocation();
    const { isTest } = parse(search);

    return isTest || false;
}

export const useTestMode = () => {
    const queryClient = useQueryClient();

    const { data } = useQuery(
        ['just-dev-tools', 'test-mode'],
        () => Utils.LocalStorage.get('is_test_mode', false),
        {
            staleTime: 1000,
        }
    );

    const setTestMode = (value) => {
        Utils.LocalStorage.set('is_test_mode', value);

        queryClient.invalidateQueries(['just-dev-tools', 'test-mode'])
    }

    return { isTestMode: !!data, setTestMode };
}
