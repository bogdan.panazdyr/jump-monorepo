import { useState } from 'react';
import { LocalStorage } from '@justlook/core/lib/utils';

export const usePersist = (key, defaultValue = null) => {
    const { set, get } = LocalStorage;

    const [value, updateValue] = useState(() => get(key, defaultValue));

    const setValue = (value) => {
        updateValue(value);

        set(key, value);
    };

    return [ value, setValue ];
}
