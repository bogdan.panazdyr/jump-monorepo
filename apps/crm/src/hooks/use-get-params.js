import { useLocation } from 'react-router-dom';
import { parse } from 'query-string';

const UseGetParams = name => {
    const { search } = useLocation();
    const query = parse(search);

    return query[name] ?? '';
}

export default UseGetParams;
