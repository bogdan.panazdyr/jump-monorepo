import { useLocation } from 'react-router-dom';

export const useGetPathNameTabs = () => {
    const { pathname, ...rest } = useLocation();

    let currentPathTabs = pathname.split('/');
    return { pathname: currentPathTabs[currentPathTabs.length - 1], ...rest }
}
