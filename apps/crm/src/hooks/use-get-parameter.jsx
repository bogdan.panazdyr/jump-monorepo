import { useLocation } from 'react-router-dom';
import { parse } from 'query-string';

const useGetParameter = name => {
    const { hash } = useLocation();
    const query = parse(hash);

    return query[name];
}

export default useGetParameter;
