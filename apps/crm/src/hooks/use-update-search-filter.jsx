import { Hooks } from '@justlook/core';
import { stringify } from 'query-string';
import { useCallback, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import useSearchFilter, { pureFilter } from './use-search-filter';


export function useUpdateQueryFilter(defaultFilter) {
    const navigate = useNavigate();
    const { filter, filterQuery, filterFull, pathname } = useSearchFilter(defaultFilter);

    const updateFilter = useCallback((newFilter) => {
        const mergeFilter = { ...filter, ...newFilter };
        const nextFilter = stringify(pureFilter(mergeFilter, defaultFilter));

        navigate(pathname + (nextFilter ? `?${nextFilter}` : ''));
    }, [ defaultFilter, navigate, pathname, filter ]);

    return {
        pathname,
        filter,
        filterQuery,
        filterFull,
        updateFilter,
    };
}

export default function useUpdateSearchFilter(defaultFilter, fetchAction) {
    const navigate = useNavigate();
    const onFetchData = Hooks.useActionToDispatch(fetchAction);
    const { filter, filterQuery, filterFull, pathname } = useSearchFilter(defaultFilter);

    const updateFilter = useCallback((newFilter) => {
        const mergeFilter = { ...filter, ...newFilter };
        const nextFilter = stringify(pureFilter(mergeFilter, defaultFilter));

        navigate(pathname + (nextFilter ? `?${nextFilter}` : ''));
    }, [ defaultFilter, navigate, pathname, filter ]);

    useEffect(() => {
        onFetchData({ ...defaultFilter, ...filter });
    }, [ onFetchData, filter, defaultFilter ]);

    return {
        filter,
        filterQuery,
        filterFull,
        updateFilter,
    };
}
