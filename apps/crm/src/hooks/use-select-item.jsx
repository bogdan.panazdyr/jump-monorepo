import { useState } from 'react';

const NO_SELECT = -1;

export const useSelectItem = (initialId = NO_SELECT) => {
    const [itemId, setItemId] = useState(initialId);

    const isSelected = itemId !== NO_SELECT

    const clearSelection = () => {
        setItemId(NO_SELECT);
    }

    return {
        itemId,
        setItemId,
        isSelected,
        clearSelection
    }
}
