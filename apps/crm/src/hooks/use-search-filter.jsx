import { parse, stringify } from 'query-string';
import { useMemo }          from 'react';
import { useLocation }      from 'react-router-dom';

export function pureFilter (data, defaultFilter) {
    const params = {};

    for(let key in data) {
        if (!Object.prototype.hasOwnProperty.call(data, key)) {
            continue;
        }

        const val = data[key];

        // eslint-disable-next-line eqeqeq
        if (val === null || val === '' || (defaultFilter[key] && defaultFilter[key] == val)) {
            continue;
        }

        params[key] = val;
    }

    return params;
}

export function useQuerySearch () {
    const { search } = useLocation();

    return useMemo(() => {
        return parse(search);
    }, [ search ]);
}

export default function useSearchFilter (defaultFilter) {
    const { pathname, search } = useLocation();

    const filter = useMemo(() => {
        return pureFilter(parse(search), defaultFilter);
    }, [ defaultFilter, search ]);

    const filterFull = useMemo(() => {
        return {...defaultFilter, ...filter};
    }, [defaultFilter, filter])

    const filterQuery = useMemo(() => {
        const query = stringify(filter);
        return query ? `?${query}` : '';
    }, [ filter ]);

    return {
        pathname,
        filter,
        filterQuery,
        filterFull
    };
}
