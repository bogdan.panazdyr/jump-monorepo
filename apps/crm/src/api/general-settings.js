const GeneralSettings = {
    fetchSettings: function () {
        return this.get('companies/settings');
    },

    settingsLegalForm: function() {
        return this.get('companies/settings/legal_form');
    },

    updateLegalFormSetting: function(id, data) {
        return this.patch(`companies/settings/legal_form/${id}`, data);
    },

    updateRequisitesSetting: function(id, reqId, data) {
        return this.patch(`companies/settings/legal_form/${id}/requisites/${reqId}`, data);
    },

    settingsPayments: function() {
        return this.get('companies/settings/payments');
    },

    updatePaymentsInterface: function(field, data) {
        return this.patch(`companies/settings/payments/interface/${field}`, data);
    },

    updatePaymentsCommission: function(data) {
        return this.put('companies/settings/payments/commission', data)
    },

    deletePaymentsCommission: function() {
        return this.deleteRequest('companies/settings/payments/commission');
    },

    updatePaymentsLimit: function(data) {
        return this.put('companies/settings/payments/limit', data);
    },

    deletePaymentsLimit: function() {
        return this.deleteRequest('companies/settings/payments/limit');
    },

    updateSettingsTax: function(data) {
        return this.put('companies/settings/tax', data)
    }
};
export default GeneralSettings;
