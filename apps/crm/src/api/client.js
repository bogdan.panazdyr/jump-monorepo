import { Config as cnf } from '@justlook/core';
import { EndpointFactory, AuthEndpoint } from '@justlook/core/lib/api';

import Agents from './agent-api';
import App from './app-api';
import Documents from './documents-api';
import GeneralSettings from './general-settings';
import { Payments } from './payments-api';
import { Statements } from './statements-api';
import Profile from './profile-api';
import Users from './users-api';
import { Address } from './address-api';
import { AuthLK } from './auth-api';
import { Banks } from './banks-api';
import { Contractor, Requisites, Permissions } from './contractors-api';
import { Orders } from './orders-api';
import { Dashboard } from './dashboard-api';
import { Reports } from './reports-api';
import { Integrations } from './integrations-api';
import { Groups } from './groups-api';
import { BankAccounts } from './bank-accounts-api';
import { PaymentWizard } from './payment-wizard';

const { endpoint: lk } = new EndpointFactory(
    { baseURL: cnf.api_url.lk, storage: 'session' });

const { endpoint: v2 } = new EndpointFactory(
    { baseURL: cnf.api_url.v2, storage: 'session' });

const api = {
    address: Address,
    agents: v2(Agents),
    app: v2(App),
    auth: v2(AuthEndpoint),
    authLK: lk(AuthLK),
    banks: v2(Banks),
    contractorPermissions: v2(Permissions),
    contractorOrders: v2(Orders),
    contractorRequisites: v2(Requisites),
    contractors: v2(Contractor),
    documents: v2(Documents),
    generalSettings: v2(GeneralSettings),
    orders: v2(Orders),
    payments: v2(Payments),
    statements: v2(Statements),
    profile: v2(Profile),
    users: v2(Users),
    dashboard: v2(Dashboard),
    reports: v2(Reports),
    integrations: v2(Integrations),
    groups: v2(Groups),
    bankAccounts: v2(BankAccounts),
    paymentWizard: v2(PaymentWizard),
};

export default api;
