export const AuthLK = {
    sync (params, token) {
        return this.post('ajax/auth.php', params, token);
    },

    getAccessToken (token) {
        return this.post('ajax/auth-token.php', null, token)
    },

    recoveryPassword (params) {
        return this.post('ajax/auth-recovery.php', params)
    },

    changePassword (params) {
        return this.post('ajax/auth-password.php', params)
    }
};
