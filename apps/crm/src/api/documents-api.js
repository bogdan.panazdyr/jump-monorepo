const documents = {
    fetchDocumentsList: function (filter) {
        return this.get('agreements', filter)
    },

    fetchDocument: function(id) {
        return this.get(`agreements/${id}`)
    },

    create: function(document) {
        return this.post('agreements', document)
    },

    update: function(id, document) {
        return this.put(`agreements/${id}`, document)
    },

    delete: function(id) {
        return this.deleteRequest(`agreements/${id}`)
    }
};

export default documents;
