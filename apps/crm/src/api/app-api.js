const App = {
    config: async function () {
        return await this.get('settings');
    },

    dictionaries () {
        return this.get('dictionaries');
    },
};

export default App;
