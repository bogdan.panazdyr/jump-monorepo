export const BankAccounts = {
    fetchBanksDictionary: function () {
        return this.get('banks/list');
    },

    syncBalances: function () {
        return this.post('banks_accounts/sync_balances');
    },

    fetchList: function (filter) {
        return this.get('banks_accounts', filter);
    },

    fetchItem: function (id) {
        return this.get(`banks_accounts/${id}`);
    },

    create: function (data) {
        return this.post('banks_accounts', data);
    },

    update: function (id, data) {
        return this.put(`banks_accounts/${id}`, data);
    },

    delete: function (id) {
        return this.deleteRequest(`banks_accounts/${id}`);
    },

    changeStatus: function (id, value) {
        return this.patch(`banks_accounts/${id}/status`, { is_active: value });
    },
}
