'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var dateFns = require('date-fns');
var ru = require('date-fns/locale/ru');
var Cookies = require('js-cookie');
var index = require('../index');
var queryString = require('query-string');

function _interopDefaultLegacy (e) { return e && typeof e === 'object' && 'default' in e ? e : { 'default': e }; }

var ru__default = /*#__PURE__*/_interopDefaultLegacy(ru);
var Cookies__default = /*#__PURE__*/_interopDefaultLegacy(Cookies);

/**
 * Форматирует дату в строку указанного формата
 *
 * @param date string|Date дата
 * @param formatStr string формат
 * @returns {string}
 */

function formatDate(date) {
  var formatStr = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'dd.MM.yy';
  var dateInstance = date instanceof Date ? date : dateFns.parseISO(date);
  return dateFns.format(dateInstance, formatStr, {
    locale: ru__default['default']
  });
}
function toISO8601(date) {
  if (date instanceof Date) {
    return date.toISOString();
  }

  var dt = dateFns.parseISO(date);
  return dateFns.isValid(dt) ? dt.toISOString() : null;
}
function formatUnixTime(date) {
  return dateFns.getUnixTime(dateFns.parseISO(date));
}
function getTime(date) {
  var time = new Date(date);
  return dateFns.format(time, 'HH:mm');
}
function toISOJoinedDate(date, time) {
  var d = new Date(date).getDate();
  var m = new Date(date).getMonth();
  var y = new Date(date).getFullYear();
  var t = time.split(':');
  var fullTime = t.map(function (item) {
    return parseInt(item);
  });
  return toISO8601(new Date(y, m, d, fullTime[0], fullTime[1]));
}
function currentTime() {
  var timeFormat = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'HH:mm';
  return dateFns.format(new Date(), timeFormat);
}
function currentDate() {
  var dateFormat = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'dd.MM.yyyy';
  return dateFns.format(new Date(), dateFormat);
}
/**
 * Возвращает форматированную строку при валидной date иначе пустую строку
 *
 * @param date
 * @param dateFormat
 * @returns {string}
 */

function formatOrEmpty(date) {
  var dateFormat = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'dd.MM.yyyy';
  return date ? formatDate(date, dateFormat) : '';
}

var dates = /*#__PURE__*/Object.freeze({
    __proto__: null,
    formatDate: formatDate,
    toISO8601: toISO8601,
    formatUnixTime: formatUnixTime,
    getTime: getTime,
    toISOJoinedDate: toISOJoinedDate,
    currentTime: currentTime,
    currentDate: currentDate,
    formatOrEmpty: formatOrEmpty
});

var _window, _window$crypto;

function randomString(length) {
  var result = '';
  var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var charactersLength = characters.length;

  for (var i = 0; i < length; i++) {
    var position = Math.floor(Math.random() * charactersLength);
    result += characters.charAt(position);
  }

  return result;
}
var uuid = (_window = window) !== null && _window !== void 0 && (_window$crypto = _window.crypto) !== null && _window$crypto !== void 0 && _window$crypto.getRandomValues ? function () {
  return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, function (c) {
    var r = new Uint8Array(1);
    var v = c ^ crypto.getRandomValues(r)[0] & 15 >> c / 4;
    return v.toString(16);
  });
} : function () {
  var mask = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx';
  return mask.replace(/[xy]/g, function (c) {
    var r = Math.random() * 16 | 0;
    var v = c === 'x' ? r : r & 0x3 | 0x8;
    return v.toString(16);
  });
};

var utils = /*#__PURE__*/Object.freeze({
    __proto__: null,
    randomString: randomString,
    uuid: uuid
});

function set(key, value) {
  localStorage.setItem(key, JSON.stringify(value));
}
function get(key) {
  var defaultValue = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
  var json = localStorage.getItem(key);
  var value = defaultValue;

  if (json) {
    try {
      value = JSON.parse(json);
    } catch (e) {}
  }

  return value;
}

var localStorage$1 = /*#__PURE__*/Object.freeze({
    __proto__: null,
    set: set,
    get: get
});

var deviceStorageKey = 'deviceKey';
var deviceSessionKey = 'device_key';
function newDeviceKey() {
  return uuid().toUpperCase();
}
function getDeviceKey(store) {
  var key = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
  var map = {
    session: getDeviceKeySession,
    storage: getDeviceKeyStorage
  };

  if (map[store]) {
    return map[store](key);
  }

  throw new Error('Store type for "Device-Key" not support');
}
function getOrNewDeviceKey(store) {
  var key = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
  var map = {
    session: getOrNewDeviceKeySession,
    storage: getOrNewDeviceKeyStorage
  };

  if (map[store]) {
    return map[store](key);
  }

  throw new Error('Store type for "Device-Key" not support');
}
function getDeviceKeySession() {
  var key = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
  var sessionKey = key || deviceSessionKey;
  return Cookies__default['default'].get(sessionKey);
}
function getOrNewDeviceKeySession() {
  var key = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
  var value = getDeviceKeySession(key) || newDeviceKey();
  return storeDeviceKeySession(value, key);
}
function storeDeviceKeySession(value) {
  var key = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
  var sessionKey = key || deviceSessionKey;
  Cookies__default['default'].set(sessionKey, value, {
    expires: 365,
    path: '/',
    domain: index.Config.root_domain
  });
  return value;
}
function getDeviceKeyStorage() {
  var key = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
  return get(key || deviceStorageKey);
}
function getOrNewDeviceKeyStorage() {
  var key = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
  var value = getDeviceKeyStorage(key) || newDeviceKey();
  return storeDeviceKeyStorage(value, key);
}
function storeDeviceKeyStorage(value) {
  var key = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
  var storeKey = key || deviceStorageKey;
  set(storeKey, value);
  return value;
}

var deviceKey = /*#__PURE__*/Object.freeze({
    __proto__: null,
    deviceStorageKey: deviceStorageKey,
    deviceSessionKey: deviceSessionKey,
    newDeviceKey: newDeviceKey,
    getDeviceKey: getDeviceKey,
    getOrNewDeviceKey: getOrNewDeviceKey,
    getDeviceKeySession: getDeviceKeySession,
    getOrNewDeviceKeySession: getOrNewDeviceKeySession,
    storeDeviceKeySession: storeDeviceKeySession,
    getDeviceKeyStorage: getDeviceKeyStorage,
    getOrNewDeviceKeyStorage: getOrNewDeviceKeyStorage,
    storeDeviceKeyStorage: storeDeviceKeyStorage
});

/**
 * Вырезает все символы кроме цифр и + из номера телефона
 *
 * @param phone string
 * @return string
 */
function phoneToCalling(phone) {
  return phone.replace(/[^+0-9]/g, '');
}

var phone = /*#__PURE__*/Object.freeze({
    __proto__: null,
    phoneToCalling: phoneToCalling
});

function isChangePasswordRequest() {
  return getRecoveryCode() && getEmail();
}
function getRecoveryCode() {
  var search = queryString.parse(window.location.search);
  return search === null || search === void 0 ? void 0 : search.recovery_code;
}
function getEmail() {
  var search = queryString.parse(window.location.search);
  return search === null || search === void 0 ? void 0 : search.email;
}

var request = /*#__PURE__*/Object.freeze({
    __proto__: null,
    isChangePasswordRequest: isChangePasswordRequest,
    getRecoveryCode: getRecoveryCode,
    getEmail: getEmail
});

function _typeof(obj) {
  "@babel/helpers - typeof";

  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function (obj) {
      return typeof obj;
    };
  } else {
    _typeof = function (obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function ownKeys(object, enumerableOnly) {
  var keys = Object.keys(object);

  if (Object.getOwnPropertySymbols) {
    var symbols = Object.getOwnPropertySymbols(object);
    if (enumerableOnly) symbols = symbols.filter(function (sym) {
      return Object.getOwnPropertyDescriptor(object, sym).enumerable;
    });
    keys.push.apply(keys, symbols);
  }

  return keys;
}

function _objectSpread2(target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i] != null ? arguments[i] : {};

    if (i % 2) {
      ownKeys(Object(source), true).forEach(function (key) {
        _defineProperty(target, key, source[key]);
      });
    } else if (Object.getOwnPropertyDescriptors) {
      Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
    } else {
      ownKeys(Object(source)).forEach(function (key) {
        Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
      });
    }
  }

  return target;
}

var TOKEN_STATUS = {
  login: 'login',
  refresh: 'refresh',
  normal: 'normal'
};
var tokenSessionKey = 'token';
var tokenStorageKey = 'token';
function newToken() {
  return randomString(10);
}
var defaultToken = {
  access_token: null,
  refresh_token: null,
  status: TOKEN_STATUS.login,
  is_support_access: false,
  is_admin_access: false,
  expires_in: null,
  created_at: null
};
function getTokenSession() {
  var key = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
  var sessionKey = key || tokenStorageKey;
  return Cookies__default['default'].get(sessionKey);
}
function getOrNewTokenSession() {
  var key = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
  var value = getTokenSession(key) || newToken();
  return storeTokenSession(value, key);
}
function storeTokenSession(value) {
  var key = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
  var sessionKey = key || tokenStorageKey;
  Cookies__default['default'].set(sessionKey, value, {
    expires: 365,
    path: '/',
    domain: index.Config.root_domain
  });
  return value;
}
function destroyTokenSession() {
  var key = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
  var sessionKey = key || tokenStorageKey;
  Cookies__default['default'].remove(sessionKey, {
    path: '/',
    domain: index.Config.root_domain
  });
}
function getTokenStorage() {
  var key = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
  var storeKey = key || tokenStorageKey;
  var token = get(storeKey) || defaultToken;
  return _objectSpread2(_objectSpread2({}, token), {}, {
    status: tokenStatus(token)
  });
}
function storeTokenStorage(data) {
  var key = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
  var storeKey = key || tokenStorageKey;

  var token = _objectSpread2(_objectSpread2({}, data), {}, {
    status: tokenStatus(data)
  });

  set(storeKey, token);
}
function destroyTokenStorage() {
  var key = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
  var storeKey = key || tokenStorageKey;
  set(storeKey, null);
}
function tokenStatus(data) {
  var token = data === null || data === void 0 ? void 0 : data.access_token;
  var expiresIn = data === null || data === void 0 ? void 0 : data.expires_in;
  var refreshToken = data === null || data === void 0 ? void 0 : data.refresh_token;
  var date = dateFns.parseISO(expiresIn); // отсутствует токен

  if (!dateFns.isValid(date) || !token || !refreshToken) {
    return TOKEN_STATUS.login;
  }

  var isDieToken = dateFns.differenceInMinutes(date, new Date()) < 30; // если меньше 30 минут, то считаем устаревшим

  var isDieRefresh = dateFns.differenceInDays(date, new Date()) < -15; // 15 дней для refresh token'а
  // просрочился refresh token

  if (isDieRefresh) {
    return TOKEN_STATUS.login;
  } // просрочился access token


  if (isDieToken) {
    return TOKEN_STATUS.refresh;
  }

  return TOKEN_STATUS.normal;
}

var token = /*#__PURE__*/Object.freeze({
    __proto__: null,
    TOKEN_STATUS: TOKEN_STATUS,
    tokenSessionKey: tokenSessionKey,
    tokenStorageKey: tokenStorageKey,
    newToken: newToken,
    defaultToken: defaultToken,
    getTokenSession: getTokenSession,
    getOrNewTokenSession: getOrNewTokenSession,
    storeTokenSession: storeTokenSession,
    destroyTokenSession: destroyTokenSession,
    getTokenStorage: getTokenStorage,
    storeTokenStorage: storeTokenStorage,
    destroyTokenStorage: destroyTokenStorage,
    tokenStatus: tokenStatus
});

var _toString = Object.prototype.toString;
function toRawType(val) {
  return _toString.call(val).slice(8, -1);
}
function isFunction(val) {
  return toRawType(val) === 'Function';
}
function isPlainObject(obj) {
  return _toString.call(obj) === '[object Object]';
}
function isObject(obj) {
  return obj !== null && _typeof(obj) === 'object';
}
function isDef(val) {
  return val !== undefined && val !== null;
}
function toString(val) {
  var jsonable = Array.isArray(val) || isPlainObject(val) && val.toString === _toString;

  return val == null ? '' : jsonable ? JSON.stringify(val, null, 2) : String(val);
}

var types = /*#__PURE__*/Object.freeze({
    __proto__: null,
    toRawType: toRawType,
    isFunction: isFunction,
    isPlainObject: isPlainObject,
    isObject: isObject,
    isDef: isDef,
    toString: toString
});

exports.Dates = dates;
exports.Device = deviceKey;
exports.LocalStorage = localStorage$1;
exports.Phone = phone;
exports.Request = request;
exports.Token = token;
exports.Types = types;
exports.Utils = utils;
