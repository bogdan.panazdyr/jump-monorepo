'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

/**
 * Возвращает массив с объектами форматированными для использования в формах в поле select
 * Формат возвращаемых объектов: {label: '', value: ''}
 * Аргумент params меет два ключа:
 *  - value - название поля для получения значений (по умолчанию `id`)
 *  - label - название поля для получения подписи (по умолчанию `name`)
 *
 * @param list array массив объектов у которых можно забрать свойство через `.`
 * @param params object
 * @returns {*}
 */
var makeOptions = function makeOptions(list) {
  var params = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {
    value: 'id',
    label: 'name'
  };
  return list.map(function (item) {
    return itemToOption(item, params);
  });
};
/**
 * Преобразует входной объект в объект для использования в формах в поле select
 * В случае если item не объект, возвращает пустой объект {}
 * Формат возвращаемого объекта: {label: '', value: ''}
 * Аргумент params меет два ключа:
 *  - value - название поля для получения значений (по умолчанию `id`)
 *  - label - название поля для получения подписи (по умолчанию `name`)
 *
 * @param item object
 * @param params object
 * @returns {*}
 */

var itemToOption = function itemToOption(item) {
  var params = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {
    value: 'id',
    label: 'name'
  };
  return item ? {
    label: item[params.label],
    value: item[params.value]
  } : {};
};

exports.itemToOption = itemToOption;
exports.makeOptions = makeOptions;
