'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var axios = require('axios');
var index = require('../index');
var utils = require('../utils');

function _interopDefaultLegacy (e) { return e && typeof e === 'object' && 'default' in e ? e : { 'default': e }; }

var axios__default = /*#__PURE__*/_interopDefaultLegacy(axios);

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) {
  try {
    var info = gen[key](arg);
    var value = info.value;
  } catch (error) {
    reject(error);
    return;
  }

  if (info.done) {
    resolve(value);
  } else {
    Promise.resolve(value).then(_next, _throw);
  }
}

function _asyncToGenerator(fn) {
  return function () {
    var self = this,
        args = arguments;
    return new Promise(function (resolve, reject) {
      var gen = fn.apply(self, args);

      function _next(value) {
        asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value);
      }

      function _throw(err) {
        asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err);
      }

      _next(undefined);
    });
  };
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function ownKeys(object, enumerableOnly) {
  var keys = Object.keys(object);

  if (Object.getOwnPropertySymbols) {
    var symbols = Object.getOwnPropertySymbols(object);
    if (enumerableOnly) symbols = symbols.filter(function (sym) {
      return Object.getOwnPropertyDescriptor(object, sym).enumerable;
    });
    keys.push.apply(keys, symbols);
  }

  return keys;
}

function _objectSpread2(target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i] != null ? arguments[i] : {};

    if (i % 2) {
      ownKeys(Object(source), true).forEach(function (key) {
        _defineProperty(target, key, source[key]);
      });
    } else if (Object.getOwnPropertyDescriptors) {
      Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
    } else {
      ownKeys(Object(source)).forEach(function (key) {
        Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
      });
    }
  }

  return target;
}

function EndpointFactory(config) {
  var apiClientInstance = new ApiClient(config);

  var endpoint = function endpoint(EndPoint) {
    return Object.assign({}, apiClientInstance, EndPoint);
  };

  return {
    endpoint: endpoint
  };
}
/**
 *
 * @param string baseURL путь до api включая протокол
 * @param string storage тип хранилища [storage|session]
 * @constructor
 */

function ApiClient(_ref) {
  var baseURL = _ref.baseURL,
      _ref$storage = _ref.storage,
      storage = _ref$storage === void 0 ? 'storage' : _ref$storage,
      _ref$deviceStorageKey = _ref.deviceStorageKey,
      deviceStorageKey = _ref$deviceStorageKey === void 0 ? null : _ref$deviceStorageKey;
  this.baseURL = baseURL;
  this.appKey = index.Config.app_key;
  this.storage = storage;
  this.deviceStorageKey = deviceStorageKey;

  this.download = function (url, token) {
    return this.request({
      method: 'get',
      url: url,
      responseType: 'blob',
      token: token
    });
  };

  this.get = function (url) {
    var query = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    var token = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
    var cancelToken = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;
    return this.request({
      method: 'get',
      url: url,
      query: query,
      token: token,
      cancelToken: cancelToken
    });
  };

  this.getStatus = function (url) {
    var query = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    var token = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
    var cancelToken = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;
    return this.request({
      method: 'get',
      url: url,
      query: query,
      token: token,
      cancelToken: cancelToken,
      onlyStatus: true
    });
  };

  this.put = function (url) {
    var data = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    var token = arguments.length > 2 ? arguments[2] : undefined;
    return this.request({
      method: 'put',
      url: url,
      data: data,
      token: token
    });
  };

  this.patch = function (url) {
    var data = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    var token = arguments.length > 2 ? arguments[2] : undefined;
    return this.request({
      method: 'patch',
      url: url,
      data: data,
      token: token
    });
  };

  this.post = function (url) {
    var data = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    var token = arguments.length > 2 ? arguments[2] : undefined;
    var cancelToken = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;
    return this.request({
      method: 'post',
      url: url,
      data: data,
      token: token,
      cancelToken: cancelToken
    });
  };

  this.postMultipart = function (url) {
    var formData = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    var token = arguments.length > 2 ? arguments[2] : undefined;
    var cancelToken = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;
    return this.request({
      method: 'post',
      url: url,
      data: formData,
      token: token,
      cancelToken: cancelToken,
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    });
  };

  this.deleteRequest = function (url, data, token) {
    return this.request({
      method: 'delete',
      url: url,
      data: data,
      token: token
    });
  };

  this.getHeaders = function (token) {
    var headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    };

    if (this.appKey) {
      headers['Application-Key'] = this.appKey;
    }

    var deviceKey = utils.Device.getDeviceKey(this.storage, this.deviceStorageKey);

    if (deviceKey) {
      headers['Device-Key'] = deviceKey;
    }

    var tokenLocal = utils.Token.getTokenStorage();
    token = token || (tokenLocal === null || tokenLocal === void 0 ? void 0 : tokenLocal.access_token) || null;

    if (token) {
      headers['Authorization'] = 'Bearer ' + token;
    }

    return headers;
  };

  this.request = function (_ref2) {
    var url = _ref2.url,
        method = _ref2.method,
        _ref2$query = _ref2.query,
        query = _ref2$query === void 0 ? {} : _ref2$query,
        _ref2$data = _ref2.data,
        data = _ref2$data === void 0 ? {} : _ref2$data,
        token = _ref2.token,
        _ref2$responseType = _ref2.responseType,
        responseType = _ref2$responseType === void 0 ? 'json' : _ref2$responseType,
        cancelToken = _ref2.cancelToken,
        _ref2$onlyStatus = _ref2.onlyStatus,
        onlyStatus = _ref2$onlyStatus === void 0 ? false : _ref2$onlyStatus,
        _ref2$headers = _ref2.headers,
        additionalHeaders = _ref2$headers === void 0 ? {} : _ref2$headers;

    var headers = _objectSpread2(_objectSpread2({}, this.getHeaders(token)), additionalHeaders);

    var config = {
      baseURL: this.baseURL,
      url: url,
      method: method,
      responseType: responseType,
      headers: headers,
      data: data,
      params: query,
      validateStatus: function validateStatus(status) {
        return status < 500;
      },
      cancelToken: cancelToken
    };
    return axios__default['default'](config).then(function (res) {
      if (onlyStatus) {
        return res.status;
      }

      if (res.status >= 400) {
        throw new index.ApiError(res.data.error, res.status);
      }

      return res.data;
    })["catch"](function (e) {
      var error;

      try {
        error = e.name === 'ApiError' ? e : new index.ApiError(e.response.data.error, e.response.status);
      } catch (e) {
        error = new index.ApiError();
      }

      throw error;
    });
  };
}

var AuthEndpoint = {
  scenario: function () {
    var _scenario = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(_ref) {
      var login, password;
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              login = _ref.login, password = _ref.password;
              _context.next = 3;
              return this.post('auth/scenario', {
                login: login,
                password: password
              });

            case 3:
              return _context.abrupt("return", _context.sent);

            case 4:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, this);
    }));

    function scenario(_x) {
      return _scenario.apply(this, arguments);
    }

    return scenario;
  }(),
  login: function () {
    var _login2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(_ref2) {
      var _login, password;

      return regeneratorRuntime.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              _login = _ref2.login, password = _ref2.password;
              _context2.next = 3;
              return this.post('auth/login', {
                login: _login,
                password: password
              });

            case 3:
              return _context2.abrupt("return", _context2.sent);

            case 4:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2, this);
    }));

    function login(_x2) {
      return _login2.apply(this, arguments);
    }

    return login;
  }(),
  refresh: function refresh(refreshToken) {
    return this.patch('auth/token', {
      refreshToken: refreshToken
    });
  },
  logout: function logout() {
    return this.deleteRequest('auth/token');
  },
  patchPassword: function patchPassword(params) {
    return this.patch('auth/password', params);
  },
  getOtp: function () {
    var _getOtp = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(params) {
      return regeneratorRuntime.wrap(function _callee3$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              _context3.next = 2;
              return this.post('auth/otp', params);

            case 2:
              return _context3.abrupt("return", _context3.sent);

            case 3:
            case "end":
              return _context3.stop();
          }
        }
      }, _callee3, this);
    }));

    function getOtp(_x3) {
      return _getOtp.apply(this, arguments);
    }

    return getOtp;
  }(),
  confirmOtp: function () {
    var _confirmOtp = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(params) {
      return regeneratorRuntime.wrap(function _callee4$(_context4) {
        while (1) {
          switch (_context4.prev = _context4.next) {
            case 0:
              _context4.next = 2;
              return this.put('auth/otp', params);

            case 2:
              return _context4.abrupt("return", _context4.sent);

            case 3:
            case "end":
              return _context4.stop();
          }
        }
      }, _callee4, this);
    }));

    function confirmOtp(_x4) {
      return _confirmOtp.apply(this, arguments);
    }

    return confirmOtp;
  }()
};

function syncAuthCookies() {
  var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      tokenSessionKey = _ref.tokenSessionKey,
      deviceSessionKey = _ref.deviceSessionKey;

  var deviceStoreKey = deviceSessionKey || utils.Device.deviceSessionKey;
  var tokenStoreKey = tokenSessionKey || utils.Token.tokenSessionKey;
  var deviceKey = utils.Device.getDeviceKeySession(deviceStoreKey);
  var token = utils.Token.getTokenSession(tokenStoreKey);
  var isNewDevice = !deviceKey || !token;

  if (!deviceKey) {
    deviceKey = utils.Device.newDeviceKey();
    utils.Device.storeDeviceKeySession(deviceKey, deviceStoreKey);
  }

  if (!token) {
    token = utils.Token.newToken();
    utils.Token.storeTokenSession(token, tokenStoreKey);
  }

  return {
    deviceKey: deviceKey,
    token: token,
    isNewDevice: isNewDevice
  };
}

exports.ApiClient = ApiClient;
exports.AuthEndpoint = AuthEndpoint;
exports.EndpointFactory = EndpointFactory;
exports.syncAuthCookies = syncAuthCookies;
