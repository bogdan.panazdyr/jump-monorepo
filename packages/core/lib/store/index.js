'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var history$1 = require('history');
var redux = require('redux');
var thunk = require('redux-thunk');

function _interopDefaultLegacy (e) { return e && typeof e === 'object' && 'default' in e ? e : { 'default': e }; }

var thunk__default = /*#__PURE__*/_interopDefaultLegacy(thunk);

function _typeof(obj) {
  "@babel/helpers - typeof";

  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function (obj) {
      return typeof obj;
    };
  } else {
    _typeof = function (obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function ownKeys(object, enumerableOnly) {
  var keys = Object.keys(object);

  if (Object.getOwnPropertySymbols) {
    var symbols = Object.getOwnPropertySymbols(object);
    if (enumerableOnly) symbols = symbols.filter(function (sym) {
      return Object.getOwnPropertyDescriptor(object, sym).enumerable;
    });
    keys.push.apply(keys, symbols);
  }

  return keys;
}

function _objectSpread2(target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i] != null ? arguments[i] : {};

    if (i % 2) {
      ownKeys(Object(source), true).forEach(function (key) {
        _defineProperty(target, key, source[key]);
      });
    } else if (Object.getOwnPropertyDescriptors) {
      Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
    } else {
      ownKeys(Object(source)).forEach(function (key) {
        Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
      });
    }
  }

  return target;
}

var reduxStoreInstance = null;
var isDevTools = (typeof window === "undefined" ? "undefined" : _typeof(window)) === 'object' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ && process.env.NODE_ENV !== 'production';
var initialState = {};
var composeEnhancers = isDevTools ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({}) : redux.compose;
var history = history$1.createBrowserHistory();
function configureStore(createRootReducer) {
  var reducerPath = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : './reducers';
  var enhancer = composeEnhancers(redux.applyMiddleware(thunk__default['default']));
  reduxStoreInstance = redux.createStore(createRootReducer(history), initialState, enhancer);

  if (module.hot) {
    module.hot.accept(reducerPath, function () {
      reduxStoreInstance.replaceReducer(createRootReducer(history));
    });
  }

  return reduxStoreInstance;
}
function getStore() {
  return reduxStoreInstance;
}

var FETCH = '_FETCH';
var REQUEST = '_REQUEST';
var RECEIVE = '_RECEIVE';
var FAILURE = '_FAILURE';
var SUCCESS = '_SUCCESS';
var ERROR = '_ERROR';
var CREATE = '_CREATE';
var DELETE = '_DELETE';
var UPDATE = '_UPDATE';
var CREATE_FETCH = CREATE + FETCH;
var CREATE_REQUEST = CREATE + REQUEST;
var CREATE_RECEIVE = CREATE + RECEIVE;
var CREATE_FAILURE = CREATE + FAILURE;
var DELETE_FETCH = DELETE + FETCH;
var DELETE_REQUEST = DELETE + REQUEST;
var DELETE_RECEIVE = DELETE + RECEIVE;
var DELETE_FAILURE = DELETE + FAILURE;
var UPDATE_FETCH = UPDATE + FETCH;
var UPDATE_REQUEST = UPDATE + REQUEST;
var UPDATE_RECEIVE = UPDATE + RECEIVE;
var UPDATE_FAILURE = UPDATE + FAILURE;
var SHOW = '_SHOW';
var HIDE = '_HIDE';
var TOGGLE = '_TOGGLE';
var APP_RUN = '@@app/RUN';
var APP_LOADING = '@@app/LOADING';
var APP_AUTHORIZE = '@@app/AUTHORIZE';
var APP_READY = '@@app/READY';
var APP_FATAL = '@@app/FATAL';
var APP_CONFIG = '@@app/CONFIG';
var APP_DICT = '@@app/DICT';
var AUTH = '@@auth/AUTH';
var AUTH_LOGIN = '@@auth/LOGIN';
var AUTH_LOGOUT = '@@auth/LOGOUT';
var AUTH_LOGOUT_ALL = '@@auth/LOGOUT_ALL';
var AUTH_REFRESH = '@@auth/REFRESH';
var AUTH_RECOVERY = '@@auth/RECOVERY';
var AUTH_PASSWORD = '@@auth/PASSWORD';
var AUTH_GET_OTP = '@@auth/GET_OTP';
var AUTH_RETRY_OTP = '@@auth/RETRY_OTP';
var AUTH_CONFIRM_OTP = '@@auth/CONFIRM_OTP';

var INITIAL_STATE_LIST = {
  items: [],
  loading: false,
  error: null
};
var listReducer = function listReducer(TYPE) {
  return function () {
    var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : INITIAL_STATE_LIST;
    var action = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

    switch (action.type) {
      case TYPE + REQUEST:
        return _objectSpread2(_objectSpread2({}, INITIAL_STATE_LIST), {}, {
          loading: true
        });

      case TYPE + FAILURE:
        return _objectSpread2(_objectSpread2({}, INITIAL_STATE_LIST), {}, {
          error: action.error
        });

      case TYPE + RECEIVE:
        return _objectSpread2(_objectSpread2({}, INITIAL_STATE_LIST), {}, {
          items: action.items
        });

      default:
        return state;
    }
  };
};

var INITIAL_STATE_LIST_BY_KEY = {
  itemsByKey: {},
  loading: false,
  error: null
};
var listReducerByKey = function listReducerByKey(TYPE) {
  var enableCash = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
  return function () {
    var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : INITIAL_STATE_LIST_BY_KEY;
    var action = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    var itemsByKey = enableCash ? state.itemsByKey : INITIAL_STATE_LIST_BY_KEY.itemsByKey;

    switch (action.type) {
      case TYPE + REQUEST:
        return _objectSpread2(_objectSpread2({}, INITIAL_STATE_LIST_BY_KEY), {}, {
          loading: true,
          itemsByKey: _objectSpread2({}, itemsByKey)
        });

      case TYPE + FAILURE:
        return _objectSpread2(_objectSpread2({}, INITIAL_STATE_LIST_BY_KEY), {}, {
          error: action.error
        });

      case TYPE + RECEIVE:
        return _objectSpread2(_objectSpread2({}, INITIAL_STATE_LIST_BY_KEY), {}, {
          itemsByKey: _objectSpread2(_objectSpread2({}, itemsByKey), {}, _defineProperty({}, action.key, action.items))
        });

      default:
        return state;
    }
  };
};

var INITIAL_STATE_ITEM = {
  item: null,
  loading: false,
  error: null
};
var itemReducer = function itemReducer(TYPE) {
  return function () {
    var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : INITIAL_STATE_ITEM;
    var action = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

    switch (action.type) {
      case TYPE + REQUEST:
        return _objectSpread2(_objectSpread2({}, INITIAL_STATE_ITEM), {}, {
          loading: true
        });

      case TYPE + FAILURE:
        return _objectSpread2(_objectSpread2({}, INITIAL_STATE_ITEM), {}, {
          error: action.error
        });

      case TYPE + RECEIVE:
        return _objectSpread2(_objectSpread2({}, INITIAL_STATE_ITEM), {}, {
          item: action.item
        });

      default:
        return state;
    }
  };
};

exports.APP_AUTHORIZE = APP_AUTHORIZE;
exports.APP_CONFIG = APP_CONFIG;
exports.APP_DICT = APP_DICT;
exports.APP_FATAL = APP_FATAL;
exports.APP_LOADING = APP_LOADING;
exports.APP_READY = APP_READY;
exports.APP_RUN = APP_RUN;
exports.AUTH = AUTH;
exports.AUTH_CONFIRM_OTP = AUTH_CONFIRM_OTP;
exports.AUTH_GET_OTP = AUTH_GET_OTP;
exports.AUTH_LOGIN = AUTH_LOGIN;
exports.AUTH_LOGOUT = AUTH_LOGOUT;
exports.AUTH_LOGOUT_ALL = AUTH_LOGOUT_ALL;
exports.AUTH_PASSWORD = AUTH_PASSWORD;
exports.AUTH_RECOVERY = AUTH_RECOVERY;
exports.AUTH_REFRESH = AUTH_REFRESH;
exports.AUTH_RETRY_OTP = AUTH_RETRY_OTP;
exports.CREATE = CREATE;
exports.CREATE_FAILURE = CREATE_FAILURE;
exports.CREATE_FETCH = CREATE_FETCH;
exports.CREATE_RECEIVE = CREATE_RECEIVE;
exports.CREATE_REQUEST = CREATE_REQUEST;
exports.DELETE = DELETE;
exports.DELETE_FAILURE = DELETE_FAILURE;
exports.DELETE_FETCH = DELETE_FETCH;
exports.DELETE_RECEIVE = DELETE_RECEIVE;
exports.DELETE_REQUEST = DELETE_REQUEST;
exports.ERROR = ERROR;
exports.FAILURE = FAILURE;
exports.FETCH = FETCH;
exports.HIDE = HIDE;
exports.INITIAL_STATE_ITEM = INITIAL_STATE_ITEM;
exports.INITIAL_STATE_LIST = INITIAL_STATE_LIST;
exports.RECEIVE = RECEIVE;
exports.REQUEST = REQUEST;
exports.SHOW = SHOW;
exports.SUCCESS = SUCCESS;
exports.TOGGLE = TOGGLE;
exports.UPDATE = UPDATE;
exports.UPDATE_FAILURE = UPDATE_FAILURE;
exports.UPDATE_FETCH = UPDATE_FETCH;
exports.UPDATE_RECEIVE = UPDATE_RECEIVE;
exports.UPDATE_REQUEST = UPDATE_REQUEST;
exports.configureStore = configureStore;
exports.getStore = getStore;
exports.history = history;
exports.itemReducer = itemReducer;
exports.listReducer = listReducer;
exports.listReducerByKey = listReducerByKey;
