'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var utils = require('../utils');

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function ownKeys(object, enumerableOnly) {
  var keys = Object.keys(object);

  if (Object.getOwnPropertySymbols) {
    var symbols = Object.getOwnPropertySymbols(object);
    if (enumerableOnly) symbols = symbols.filter(function (sym) {
      return Object.getOwnPropertyDescriptor(object, sym).enumerable;
    });
    keys.push.apply(keys, symbols);
  }

  return keys;
}

function _objectSpread2(target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i] != null ? arguments[i] : {};

    if (i % 2) {
      ownKeys(Object(source), true).forEach(function (key) {
        _defineProperty(target, key, source[key]);
      });
    } else if (Object.getOwnPropertyDescriptors) {
      Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
    } else {
      ownKeys(Object(source)).forEach(function (key) {
        Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
      });
    }
  }

  return target;
}

function ChatIntercom(props) {
  var intercomSettings = _objectSpread2(_objectSpread2({}, props.config), {}, {
    custom_launcher_selector: props.customLauncher,
    hide_default_launcher: !!props.customLauncher,
    created_at: utils.Dates.formatUnixTime(props.createdAt)
  });

  process.env.NODE_ENV === 'production' && initIntercome(intercomSettings);
  return null;
}

function initIntercome(intercomSettings) {
  var w = window;
  var d = document;
  var ic = w.Intercom;
  w.APP_ID = intercomSettings.app_id;
  w.intercomSettings = intercomSettings;

  if (typeof ic === 'function') {
    ic('reattach_activator');
    ic('update', w.intercomSettings);
  } else {
    var i = function i() {
      i.c(arguments);
    };

    i.q = [];

    i.c = function (args) {
      i.q.push(args);
    };

    w.Intercom = i;
    w.Intercom('boot', intercomSettings);

    var l = function l() {
      var s = d.createElement('script');
      s.type = 'text/javascript';
      s.async = true;
      s.src = 'https://widget.intercom.io/widget/' + w.APP_ID;
      var x = d.getElementsByTagName('script')[0];
      x.parentNode.insertBefore(s, x);
    };

    if (document.readyState === 'complete') {
      l();
    } else if (w.attachEvent) {
      w.attachEvent('onload', l);
    } else {
      w.addEventListener('load', l, false);
    }
  }
}

exports.ChatIntercom = ChatIntercom;
