export const Config = window.__CONFIG__ || {
    protocol: 'https://',
    root_domain: 'nosite.ru',
    app_key: 'nosite-app-key',
};