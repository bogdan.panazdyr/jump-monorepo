import { useState } from 'react';

export function useModalState (state = false) {
    const hookData = useState(state);
    const isShow = hookData[0];
    const onShow = hookData[1];

    const onOpenForm = () => onShow(true);
    const onCloseForm = () => onShow(false);

    return { isShow, onOpenForm, onCloseForm };
}