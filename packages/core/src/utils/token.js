import Cookies from 'js-cookie';
import { Config } from '../index';
import { get, set } from './localStorage';
import { randomString } from './utils';
import {
    differenceInMinutes,
    differenceInDays,
    isValid,
    parseISO,
} from 'date-fns';

export const TOKEN_STATUS = {
    login: 'login',
    refresh: 'refresh',
    normal: 'normal',
};

export const tokenSessionKey = 'token';
export const tokenStorageKey = 'token';

export function newToken () {
    return randomString(10);
}

export const defaultToken = {
    access_token: null,
    refresh_token: null,
    status: TOKEN_STATUS.login,
    is_support_access: false,
    is_admin_access: false,
    expires_in: null,
    created_at: null,
};

export function getTokenSession (key = null) {
    const sessionKey = key || tokenStorageKey;

    return Cookies.get(sessionKey);
}

export function getOrNewTokenSession (key = null) {
    const value = getTokenSession(key) || newToken();

    return storeTokenSession(value, key);
}

export function storeTokenSession (value, key = null) {
    const sessionKey = key || tokenStorageKey;

    Cookies.set(
        sessionKey, value,
        { expires: 365, path: '/', domain: Config.root_domain },
    );

    return value;
}

export function destroyTokenSession (key = null) {
    const sessionKey = key || tokenStorageKey;

    Cookies.remove(sessionKey, { path: '/', domain: Config.root_domain });
}

export function getTokenStorage (key = null) {
    const storeKey = key || tokenStorageKey;

    const token = get(storeKey) || defaultToken;

    return { ...token, status: tokenStatus(token) };
}

export function storeTokenStorage (data, key = null) {
    const storeKey = key || tokenStorageKey;

    const token = { ...data, status: tokenStatus(data) };

    set(storeKey, token);
}

export function destroyTokenStorage (key = null) {
    const storeKey = key || tokenStorageKey;
    set(storeKey, null);
}

export function tokenStatus (data) {
    const token = data?.access_token;
    const expiresIn = data?.expires_in;
    const refreshToken = data?.refresh_token;
    const date = parseISO(expiresIn);

    // отсутствует токен
    if (!isValid(date) || !token || !refreshToken) {
        return TOKEN_STATUS.login;
    }

    const isDieToken = differenceInMinutes(date, new Date()) < 30; // если меньше 30 минут, то считаем устаревшим
    const isDieRefresh = differenceInDays(date, new Date()) < -15; // 15 дней для refresh token'а

    // просрочился refresh token
    if (isDieRefresh) {
        return TOKEN_STATUS.login;
    }

    // просрочился access token
    if (isDieToken) {
        return TOKEN_STATUS.refresh;
    }

    return TOKEN_STATUS.normal;
}
