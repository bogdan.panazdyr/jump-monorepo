import { randomString, uuid } from '../utils';

describe('Utils > utils.js', () => {
    it('makeId создает строку верной длины', () => {
        const length5 = randomString(5);
        const length10 = randomString(10);
        const length32 = randomString(32);

        expect(length5.length).toEqual(5);
        expect(length10.length).toEqual(10);
        expect(length32.length).toEqual(32);
    });

    it('uuid должен быть 36 символов', () => {
        const value = uuid();

        expect(value.length).toEqual(36);
    });
});