import addDays from 'date-fns/addDays';
import addMinutes from 'date-fns/addMinutes';
import { tokenStatus, TOKEN_STATUS } from '../token';

describe('Utils > token.js', ()=>{
    it('tokenStatus - токен имеет невалидную структуру', () => {
        const token = {};
        const status = tokenStatus(token);

        expect(status).toEqual(TOKEN_STATUS.login);
    });

    it('tokenStatus - токен просрочен', () => {
        const exp = new Date(2020, 1, 1);

        const token = {
            access_token: '1234567890',
            expires_in: exp,
            refresh_token: '1234567890'
        };

        const status = tokenStatus(token);

        expect(status).toEqual(TOKEN_STATUS.login);
    });

    it('tokenStatus - токен валидный', () => {
        const exp = addDays(new Date(), 1).toISOString();

        const token = {
            access_token: '1234567890',
            expires_in: exp,
            refresh_token: '1234567890'
        };

        const status = tokenStatus(token);

        expect(status).toEqual(TOKEN_STATUS.normal);
    });


    it('tokenStatus - токен надо обновить', () => {
        const exp = addMinutes(new Date(), 10).toISOString();

        const token = {
            access_token: '1234567890',
            expires_in: exp,
            refresh_token: '1234567890'
        };

        const status = tokenStatus(token);

        expect(status).toEqual(TOKEN_STATUS.refresh);
    });
});