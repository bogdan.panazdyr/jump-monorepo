/**
 * Возвращает массив с объектами форматированными для использования в формах в поле select
 * Формат возвращаемых объектов: {label: '', value: ''}
 * Аргумент params меет два ключа:
 *  - value - название поля для получения значений (по умолчанию `id`)
 *  - label - название поля для получения подписи (по умолчанию `name`)
 *
 * @param list array массив объектов у которых можно забрать свойство через `.`
 * @param params object
 * @returns {*}
 */
export const makeOptions = (list, params = { value: 'id', label: 'name' }) => {
    return list.map(item => itemToOption(item, params));
};

/**
 * Преобразует входной объект в объект для использования в формах в поле select
 * В случае если item не объект, возвращает пустой объект {}
 * Формат возвращаемого объекта: {label: '', value: ''}
 * Аргумент params меет два ключа:
 *  - value - название поля для получения значений (по умолчанию `id`)
 *  - label - название поля для получения подписи (по умолчанию `name`)
 *
 * @param item object
 * @param params object
 * @returns {*}
 */
export const itemToOption = (item, params = { value: 'id', label: 'name' }) => {
    return item ? {
        label: item[params.label],
        value: item[params.value],
    } : {};
};