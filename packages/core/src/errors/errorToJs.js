export default function errorToJs (error) {
    if (error.name === 'ApiError') {
        return {
            title: error.errorData.title,
            detail: error.errorData.detail,
            event: error.errorData.event,
            fields: error.errorData.fields,

            name: error.name,
            code: error.responseCode,
        };
    }

    return {
        title: 'Ошибка приложения',
        detail: error.message,
        event: null,
        fields: null,

        name: error.name,
        code: 0,
    };
}