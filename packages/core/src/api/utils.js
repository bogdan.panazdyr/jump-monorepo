import { Device, Token } from '../utils';

export function syncAuthCookies ({tokenSessionKey, deviceSessionKey} = {}) {
    const deviceStoreKey = deviceSessionKey || Device.deviceSessionKey;
    const tokenStoreKey = tokenSessionKey || Token.tokenSessionKey;

    let deviceKey = Device.getDeviceKeySession(deviceStoreKey);
    let token = Token.getTokenSession(tokenStoreKey);

    const isNewDevice = !deviceKey || !token;

    if (!deviceKey) {
        deviceKey = Device.newDeviceKey();
        Device.storeDeviceKeySession(deviceKey, deviceStoreKey)
    }

    if (!token) {
        token = Token.newToken();
        Token.storeTokenSession(token, tokenStoreKey)
    }

    return { deviceKey, token, isNewDevice };
}