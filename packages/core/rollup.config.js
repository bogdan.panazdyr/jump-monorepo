import resolve from '@rollup/plugin-node-resolve';
import babel from 'rollup-plugin-babel';

const plugins = [
    resolve(),
    babel({
        exclude: 'node_modules/**', // only transpile our source code
    }),
];

const external = [
    'axios',
    'date-fns',
    'date-fns/locale/ru',
    'history',
    'js-cookie',
    'prop-types',
    'query-string',
    'react',
    'react-redux',
    'redux',
    'redux-thunk',
];

export default [
    {
        input: 'src/index.js',
        output: {
            file: 'lib/index.js',
            format: 'cjs',
        },
        plugins,
        external,
    },
    {
        input: 'src/api/index.js',
        output: {
            file: 'lib/api/index.js',
            format: 'cjs',
        },
        plugins,
        external: [...external, '../index', '../utils', '../config'],
    },
    {
        input: 'src/components/index.js',
        output: {
            file: 'lib/components/index.js',
            format: 'cjs',
        },
        plugins,
        external: [...external, '../utils'],
    },
    {
        input: 'src/hooks/index.js',
        output: {
            file: 'lib/hooks/index.js',
            format: 'cjs',
        },
        plugins,
        external,
    },
    {
        input: 'src/utils/index.js',
        output: {
            file: 'lib/utils/index.js',
            format: 'cjs',
        },
        plugins,
        external: [...external, '../index'],
    },
    {
        input: 'src/store/index.js',
        output: {
            file: 'lib/store/index.js',
            format: 'cjs',
        },
        plugins,
        external,
    },
    {
        input: 'src/forms/index.js',
        output: {
            file: 'lib/forms/index.js',
            format: 'cjs',
        },
        plugins,
        external,
    },
];