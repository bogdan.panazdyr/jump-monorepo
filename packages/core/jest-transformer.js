const babelOptions = {
    presets: [
        "@babel/preset-react",
        [
            "@babel/preset-env",
            {
                debug: false,
                targets: {
                    browsers: [
                        "last 3 versions"
                    ]
                }
            }
        ],
    ],
    plugins: [
        '@babel/plugin-proposal-class-properties'
    ]
};
module.exports = require("babel-jest").createTransformer(babelOptions);