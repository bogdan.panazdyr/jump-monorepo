"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Chips = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _paragraph = require("../paragraph");

var _button = require("../button");

var _icons = require("../icons");

var _chipsModule = _interopRequireDefault(require("./chips.module.scss"));

var _excluded = ["element", "label", "onDelete", "color", "avatar", "onClick"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var Chips = function Chips(_ref) {
  var _classes;

  var Tag = _ref.element,
      label = _ref.label,
      onDelete = _ref.onDelete,
      color = _ref.color,
      avatar = _ref.avatar,
      onClick = _ref.onClick,
      attrs = _objectWithoutProperties(_ref, _excluded);

  var cls = (0, _classnames["default"])(_chipsModule["default"].base, (_classes = {}, _defineProperty(_classes, _chipsModule["default"].color, color), _defineProperty(_classes, _chipsModule["default"].alert, color === 'alert'), _defineProperty(_classes, _chipsModule["default"].warning, color === 'warning'), _defineProperty(_classes, _chipsModule["default"].success, color === 'success'), _defineProperty(_classes, _chipsModule["default"].primary, color === 'primary'), _defineProperty(_classes, _chipsModule["default"].onDelete, onDelete), _defineProperty(_classes, _chipsModule["default"].avatar, avatar), _defineProperty(_classes, _chipsModule["default"].onClick, onClick), _classes));
  return /*#__PURE__*/_react["default"].createElement(Tag, _extends({
    className: cls,
    onClick: onClick
  }, attrs), avatar && /*#__PURE__*/_react["default"].createElement("div", {
    className: _chipsModule["default"].status
  }, avatar), /*#__PURE__*/_react["default"].createElement(_paragraph.Paragraph, null, label), onDelete && /*#__PURE__*/_react["default"].createElement(_button.Button, {
    icon: /*#__PURE__*/_react["default"].createElement(_icons.Icons16.IconClose, null),
    styling: 'hollow',
    onClick: onDelete
  }));
};

exports.Chips = Chips;
Chips.defaultProps = {
  element: 'div',
  label: 'default',
  color: '',
  avatar: null,
  onClick: null
};
Chips.propTypes = {
  label: _propTypes["default"].string,
  onDelete: _propTypes["default"].func,
  element: _propTypes["default"].string,
  color: _propTypes["default"].string,
  avatar: _propTypes["default"].any,
  onClick: _propTypes["default"].func
};