"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "Toggle", {
  enumerable: true,
  get: function get() {
    return _toggle.Toggle;
  }
});

var _toggle = require("./toggle");