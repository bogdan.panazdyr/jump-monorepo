"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Image = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _imageModule = _interopRequireDefault(require("./image.module.scss"));

var _excluded = ["avatar", "size", "round"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var Image = function Image(_ref) {
  var _classes;

  var avatar = _ref.avatar,
      size = _ref.size,
      round = _ref.round,
      attrs = _objectWithoutProperties(_ref, _excluded);

  var cls = (0, _classnames["default"])((_classes = {}, _defineProperty(_classes, _imageModule["default"].mini, size === 'mini'), _defineProperty(_classes, _imageModule["default"].middle, size === 'middle'), _defineProperty(_classes, _imageModule["default"].big, size === 'big'), _defineProperty(_classes, _imageModule["default"].avatar, avatar), _defineProperty(_classes, _imageModule["default"].round, round), _classes));
  return /*#__PURE__*/_react["default"].createElement("img", _extends({
    className: cls,
    alt: ""
  }, attrs));
};

exports.Image = Image;
Image.defaultProps = {
  avatar: false,
  size: null,
  round: false
};
Image.propTypes = {
  avatar: _propTypes["default"].bool,
  size: _propTypes["default"].string,
  round: _propTypes["default"].bool
};