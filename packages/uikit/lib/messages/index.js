"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "ErrorMessage", {
  enumerable: true,
  get: function get() {
    return _message.ErrorMessage;
  }
});

var _message = require("./message");