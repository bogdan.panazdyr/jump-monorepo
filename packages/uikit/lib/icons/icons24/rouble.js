"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IconRouble = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var IconRouble = function IconRouble(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", _extends({}, props, {
    width: "24",
    height: "24",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    fill: "#333",
    fillRule: "evenodd",
    clipRule: "evenodd",
    d: "M12 20a8 8 0 110-16 8 8 0 010 16zM2 12C2 6.477 6.477 2 12 2s10 4.477 10 10-4.477 10-10 10S2 17.523 2 12zm8-6H9v8h-.5a1 1 0 100 2H9v2h2v-2h3.5a1 1 0 100-2H11v-1h2.6c1.916 0 3.4-1.606 3.4-3.5S15.516 6 13.6 6H10zm1 2v3h2.6c.735 0 1.4-.633 1.4-1.5S14.335 8 13.6 8H11z"
  }));
};

exports.IconRouble = IconRouble;