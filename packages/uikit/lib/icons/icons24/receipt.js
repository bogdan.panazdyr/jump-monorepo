"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IconReceipt = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var IconReceipt = function IconReceipt(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", _extends({}, props, {
    width: "24",
    height: "24",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    fill: "#333",
    fillRule: "evenodd",
    clipRule: "evenodd",
    d: "M4 4a1 1 0 011-1h14a1 1 0 011 1v16a1 1 0 01-1.371.928l-2.129-.851-2.129.851a1 1 0 01-.818-.034L12 20.118l-1.553.776a1 1 0 01-.818.035L7.5 20.076l-2.129.851A1 1 0 014 20V4zm2 1v13.523l1.129-.451a1 1 0 01.742 0l2.083.833 1.599-.8a1 1 0 01.894 0l1.599.8 2.083-.834a1 1 0 01.742 0l1.129.452V5H6z"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    fill: "#333",
    d: "M8 9a1 1 0 011-1h6a1 1 0 110 2H9a1 1 0 01-1-1zM8 12a1 1 0 011-1h6a1 1 0 110 2H9a1 1 0 01-1-1z"
  }));
};

exports.IconReceipt = IconReceipt;