"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IconWarning = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var IconWarning = function IconWarning(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", _extends({}, props, {
    width: "24",
    height: "24",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    fill: "#333",
    fillRule: "evenodd",
    clipRule: "evenodd",
    d: "M5 12a7 7 0 1114 0 7 7 0 01-14 0zm7 9a9 9 0 110-18 9 9 0 010 18zm0-7a1 1 0 001-1V8a1 1 0 10-2 0v5a1 1 0 001 1zm0 1a1 1 0 110 2 1 1 0 010-2z"
  }));
};

exports.IconWarning = IconWarning;