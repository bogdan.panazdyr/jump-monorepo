"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IconWatchRhombus = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var IconWatchRhombus = function IconWatchRhombus(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", _extends({}, props, {
    width: "24",
    height: "24",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    fill: "#333",
    d: "M13 8a1 1 0 10-2 0v4c0 .6.4 1 1 1h3a1 1 0 100-2h-2V8z"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    fill: "#333",
    fillRule: "evenodd",
    clipRule: "evenodd",
    d: "M4.1 14.1a3 3 0 010-4.2l5.7-5.7a3 3 0 014.2 0l5.7 5.7a3 3 0 010 4.2L14 19.8a3 3 0 01-4.2 0L4 14zm1.4-2.8l5.7-5.7a1 1 0 011.4 0l5.7 5.7c.4.4.4 1 0 1.4l-5.7 5.7a1 1 0 01-1.4 0l-5.7-5.7a1 1 0 010-1.4z"
  }));
};

exports.IconWatchRhombus = IconWatchRhombus;