"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IconArrowLeftLine = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var IconArrowLeftLine = function IconArrowLeftLine(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", _extends({}, props, {
    width: "24",
    height: "24",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    fill: "#333",
    d: "M20 12a1 1 0 00-1-1H7.83l4.88-4.88a1 1 0 00-1.415-1.415l-6.588 6.588a1 1 0 000 1.414l6.588 6.588a.997.997 0 001.41-1.41L7.83 13H19a1 1 0 001-1z"
  }));
};

exports.IconArrowLeftLine = IconArrowLeftLine;