"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IconSetting = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var IconSetting = function IconSetting(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", _extends({}, props, {
    width: "24",
    height: "24",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    fill: "#333",
    fillRule: "evenodd",
    clipRule: "evenodd",
    d: "M21 5H10.83a3.001 3.001 0 00-5.66 0H3a1 1 0 000 2h2.17a3.001 3.001 0 005.66 0H21a1 1 0 100-2zM8 7a1 1 0 110-2 1 1 0 010 2zm5.17 4H3a1 1 0 100 2h10.17a3.001 3.001 0 005.66 0H21a1 1 0 100-2h-2.17a3.001 3.001 0 00-5.66 0zM17 12a1 1 0 11-2 0 1 1 0 012 0zm-6.17 5H21a1 1 0 110 2H10.83a3.001 3.001 0 01-5.66 0H3a1 1 0 110-2h2.17a3.001 3.001 0 015.66 0zM7 18a1 1 0 102 0 1 1 0 00-2 0z"
  }));
};

exports.IconSetting = IconSetting;