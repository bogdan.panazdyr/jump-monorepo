"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IconLoaderSmall = void 0;

var _react = _interopRequireDefault(require("react"));

var _animateModule = _interopRequireDefault(require("../animate.module.scss"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var IconLoaderSmall = function IconLoaderSmall(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", _extends({}, props, {
    width: "24",
    height: "24",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    className: _animateModule["default"].base,
    fill: "#333",
    fillRule: "evenodd",
    clipRule: "evenodd",
    d: "M14.939 7.955A5 5 0 1017 12h2a7 7 0 11-2.886-5.663L14.94 7.955z"
  }));
};

exports.IconLoaderSmall = IconLoaderSmall;