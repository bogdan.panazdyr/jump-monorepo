"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IconClients = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var IconClients = function IconClients(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", _extends({}, props, {
    width: "24",
    height: "24",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    fill: "#333",
    fillRule: "evenodd",
    clipRule: "evenodd",
    d: "M16 10a2 2 0 100-4 2 2 0 000 4zm0 2a4 4 0 100-8 4 4 0 000 8zm5.677 6H10.394c.927-1.574 2.852-3 5.59-3 3.132 0 4.898 1.455 5.693 3zm-5.693-5c-4.107 0-6.91 2.497-7.89 5.046-.4 1.035.539 1.954 1.682 1.954h12.416c1.143 0 2.089-.915 1.731-1.964C23.056 15.49 20.495 13 15.983 13zm-9.94 3.017C4.435 16.147 3.22 16.98 2.5 18h3.544l-.004.029A3.173 3.173 0 006.36 20H1.775C.62 20-.328 19.077.108 18.055c.825-1.933 2.814-3.762 5.672-4.023a8.494 8.494 0 012.483.149 9.808 9.808 0 00-1.394 1.83 7.015 7.015 0 00-.66-.004c-.056.002-.11.006-.165.01zM8 9.5a1.5 1.5 0 11-3 0 1.5 1.5 0 013 0zm2 0a3.5 3.5 0 11-7 0 3.5 3.5 0 017 0z"
  }));
};

exports.IconClients = IconClients;