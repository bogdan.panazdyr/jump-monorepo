"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IconPencil = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var IconPencil = function IconPencil(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", _extends({}, props, {
    width: "24",
    height: "24",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    fill: "#333",
    fillRule: "evenodd",
    clipRule: "evenodd",
    d: "M16.728 6.272l-11.2 11.2-.82 2.235 2.234-.82 11.2-11.2-1.414-1.415zm.858-.858L19 4l1.414 1.414L19 6.828l-1.414-1.414z"
  }));
};

exports.IconPencil = IconPencil;