"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IconFlag = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var IconFlag = function IconFlag(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", _extends({}, props, {
    width: "24",
    height: "24",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    fill: "#333",
    d: "M5.086 21c.607 0 1.096-.473 1.096-1.06v-4.776c.284-.085.94-.274 1.87-.274 3.279 0 5.696 1.636 8.955 1.636 1.4 0 2.212-.208 2.946-.54.695-.311 1.047-.907 1.047-1.74V5.573c0-.729-.529-1.23-1.292-1.23-.626 0-1.419.303-2.76.303C13.778 4.646 11.262 3 7.993 3c-1.35 0-2.114.19-2.926.549C4.382 3.85 4 4.315 4 5.119V19.94C4 20.518 4.5 21 5.086 21zm11.843-6.65c-2.897 0-5.403-1.617-8.799-1.617-.734 0-1.4.076-1.85.218V5.63c.225-.218.803-.473 1.782-.473 3.014 0 5.53 1.627 8.798 1.627.734 0 1.36-.067 1.86-.19v7.312c-.206.208-.803.444-1.791.444z"
  }));
};

exports.IconFlag = IconFlag;