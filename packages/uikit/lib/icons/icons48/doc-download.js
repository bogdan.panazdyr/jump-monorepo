"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IconDocDownload = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var IconDocDownload = function IconDocDownload(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", _extends({}, props, {
    width: "48",
    height: "48",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    d: "M11.5 9A2.5 2.5 0 0114 6.5h13.343a2.5 2.5 0 011.768.732l6.657 6.657a2.5 2.5 0 01.732 1.768V39a2.5 2.5 0 01-2.5 2.5H14a2.5 2.5 0 01-2.5-2.5V9z",
    stroke: "#333",
    strokeWidth: "3"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    d: "M27.5 6.5v10h9",
    stroke: "#333",
    strokeWidth: "3"
  }), /*#__PURE__*/_react["default"].createElement("circle", {
    cx: "35",
    cy: "35",
    r: "11",
    fill: "#2691FF"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    fill: "#fff",
    fillRule: "evenodd",
    clipRule: "evenodd",
    d: "M35 29a1 1 0 011 1v7l2.4-1.8a1 1 0 011.2 1.6l-3.995 2.996a.996.996 0 01-1.215-.004L30.4 36.8a1 1 0 111.2-1.6L34 37v-7a1 1 0 011-1z"
  }));
};

exports.IconDocDownload = IconDocDownload;