"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IconCheckCircleFill = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var IconCheckCircleFill = function IconCheckCircleFill(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", _extends({}, props, {
    width: "16",
    height: "16",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    fill: "#333",
    fillRule: "evenodd",
    clipRule: "evenodd",
    d: "M0 8a8 8 0 1116 0A8 8 0 010 8zm12.809-2.412a1 1 0 10-1.618-1.176L6.754 9.513 4.67 7.633a1 1 0 10-1.34 1.484l2.91 2.625a1 1 0 001.478-.154l5.09-6z"
  }));
};

exports.IconCheckCircleFill = IconCheckCircleFill;