"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IconStar = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var IconStar = function IconStar(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", _extends({}, props, {
    width: "16",
    height: "16",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    fill: "#333",
    d: "M8 1l1.85 4.452 4.806.385-3.661 3.136 1.118 4.69L8 11.15l-4.115 2.513 1.12-4.69-3.662-3.136 4.805-.385L8 1z"
  }));
};

exports.IconStar = IconStar;