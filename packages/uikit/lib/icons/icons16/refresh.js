"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IconRefresh = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var IconRefresh = function IconRefresh(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", _extends({}, props, {
    width: "16",
    height: "16",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    fillRule: "evenodd",
    clipRule: "evenodd",
    fill: "#333",
    d: "M6 1a7 7 0 019 7 1 1 0 01-1 1l-3-2a1 1 0 110-1h2a5 5 0 00-9-1 1 1 0 11-2-1l4-3zM1 7h1l3 2a1 1 0 110 1H3a5 5 0 009 1 1 1 0 012 1A7 7 0 011 8V7z"
  }));
};

exports.IconRefresh = IconRefresh;