"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IconInfoFill = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var IconInfoFill = function IconInfoFill(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", _extends({}, props, {
    width: "16",
    height: "16",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    fill: "#333",
    fillRule: "evenodd",
    clipRule: "evenodd",
    d: "M8 16A8 8 0 108 0a8 8 0 000 16zM9 5a1 1 0 11-2 0 1 1 0 012 0zm0 3a1 1 0 10-2 0v3a1 1 0 102 0V8z"
  }));
};

exports.IconInfoFill = IconInfoFill;