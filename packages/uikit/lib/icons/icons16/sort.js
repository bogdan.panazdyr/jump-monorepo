"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IconSort = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var IconSort = function IconSort(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", _extends({}, props, {
    width: "16",
    height: "16",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    fill: "#333",
    fillRule: "evenodd",
    clipRule: "evenodd",
    d: "M1 4a1 1 0 011-1h12a1 1 0 110 2H2a1 1 0 01-1-1zm2 4a1 1 0 011-1h8a1 1 0 110 2H4a1 1 0 01-1-1zm4 3a1 1 0 100 2h2a1 1 0 100-2H7z"
  }));
};

exports.IconSort = IconSort;