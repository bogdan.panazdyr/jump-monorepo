"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IconBank = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var IconBank = function IconBank(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", _extends({}, props, {
    width: "16",
    height: "16",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    fill: "#333",
    d: "M2 4l6-3 6 3v2H2V4zM5 7H3v5h2V7zM9 7H7v5h2V7zM2 15v-2h12v2H2zM13 7h-2v5h2V7z"
  }));
};

exports.IconBank = IconBank;