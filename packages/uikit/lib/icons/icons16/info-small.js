"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IconInfoSmall = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var IconInfoSmall = function IconInfoSmall(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", _extends({}, props, {
    width: "16",
    height: "16",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    fill: "#333",
    d: "M7.5 11V7h1v4h-1zM8.5 6V5h-1v1h1z"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    fill: "#333",
    fillRule: "evenodd",
    clipRule: "evenodd",
    d: "M8 14A6 6 0 108 2a6 6 0 000 12zm0-1A5 5 0 108 3a5 5 0 000 10z"
  }));
};

exports.IconInfoSmall = IconInfoSmall;