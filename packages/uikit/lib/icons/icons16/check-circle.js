"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IconCheckCircle = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var IconCheckCircle = function IconCheckCircle(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", _extends({}, props, {
    width: "16",
    height: "16",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    fill: "#333",
    d: "M11.809 6.588a1 1 0 10-1.618-1.176L6.754 8.513l-1.084-.88a1 1 0 10-1.34 1.484l1.91 1.625a1 1 0 001.478-.154l4.09-4z"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    fill: "#333",
    fillRule: "evenodd",
    clipRule: "evenodd",
    d: "M0 8a8 8 0 1116 0A8 8 0 010 8zm8 6A6 6 0 118 2a6 6 0 010 12z"
  }));
};

exports.IconCheckCircle = IconCheckCircle;