"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _arrowDown = require("./arrow-down");

var _arrowDownSolid = require("./arrow-down-solid");

var _arrowLeft = require("./arrow-left");

var _arrowRight = require("./arrow-right");

var _arrowUp = require("./arrow-up");

var _banCircle = require("./ban-circle");

var _banCircleFill = require("./ban-circle-fill");

var _bank = require("./bank");

var _check = require("./check");

var _checkCircle = require("./check-circle");

var _checkCircleFill = require("./check-circle-fill");

var _checkDouble = require("./check-double");

var _close = require("./close");

var _crossCircle = require("./cross-circle");

var _crossCircleFill = require("./cross-circle-fill");

var _download = require("./download");

var _external = require("./external");

var _info = require("./info");

var _infoFill = require("./info-fill");

var _infoSmall = require("./info-small");

var _kebab = require("./kebab");

var _loader = require("./loader");

var _lock = require("./lock");

var _pencil = require("./pencil");

var _plus = require("./plus");

var _receipt = require("./receipt");

var _refresh = require("./refresh");

var _settings = require("./settings");

var _sort = require("./sort");

var _sortDown = require("./sort-down");

var _sortUp = require("./sort-up");

var _star = require("./star");

var _warning = require("./warning");

var _watch = require("./watch");

var _watchFill = require("./watch-fill");

var _watchRhombus = require("./watch-rhombus");

var Icons = {
  IconArrowDown: _arrowDown.IconArrowDown,
  IconArrowDownSolid: _arrowDownSolid.IconArrowDownSolid,
  IconArrowLeft: _arrowLeft.IconArrowLeft,
  IconArrowRight: _arrowRight.IconArrowRight,
  IconArrowUp: _arrowUp.IconArrowUp,
  IconBanCircle: _banCircle.IconBanCircle,
  IconBanCircleFill: _banCircleFill.IconBanCircleFill,
  IconBank: _bank.IconBank,
  IconCheck: _check.IconCheck,
  IconCheckCircle: _checkCircle.IconCheckCircle,
  IconCheckCircleFill: _checkCircleFill.IconCheckCircleFill,
  IconCheckDouble: _checkDouble.IconCheckDouble,
  IconClose: _close.IconClose,
  IconCrossCircle: _crossCircle.IconCrossCircle,
  IconCrossCircleFill: _crossCircleFill.IconCrossCircleFill,
  IconDownload: _download.IconDownload,
  IconExternal: _external.IconExternal,
  IconInfo: _info.IconInfo,
  IconInfoFill: _infoFill.IconInfoFill,
  IconInfoSmall: _infoSmall.IconInfoSmall,
  IconKebab: _kebab.IconKebab,
  IconLoader: _loader.IconLoader,
  IconLock: _lock.IconLock,
  IconPencil: _pencil.IconPencil,
  IconPlus: _plus.IconPlus,
  IconReceipt: _receipt.IconReceipt,
  IconRefresh: _refresh.IconRefresh,
  IconSettings: _settings.IconSettings,
  IconSort: _sort.IconSort,
  IconSortDown: _sortDown.IconSortDown,
  IconSortUp: _sortUp.IconSortUp,
  IconStar: _star.IconStar,
  IconWarning: _warning.IconWarning,
  IconWatch: _watch.IconWatch,
  IconWatchFill: _watchFill.IconWatchFill,
  IconWatchRhombus: _watchRhombus.IconWatchRhombus
};
var _default = Icons;
exports["default"] = _default;