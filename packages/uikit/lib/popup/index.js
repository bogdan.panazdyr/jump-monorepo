"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "Popup", {
  enumerable: true,
  get: function get() {
    return _popup.Popup;
  }
});

var _popup = require("./popup");