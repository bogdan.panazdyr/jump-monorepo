"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames4 = _interopRequireDefault(require("classnames"));

var _fontModule = _interopRequireDefault(require("./font.module.scss"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var Font = function Font(_ref) {
  var children = _ref.children,
      Component = _ref.as,
      _ref$size = _ref.size,
      size = _ref$size === void 0 ? 18 : _ref$size,
      _ref$muted = _ref.muted,
      muted = _ref$muted === void 0 ? false : _ref$muted,
      className = _ref.className;

  if (Component) {
    var _classnames;

    return /*#__PURE__*/_react["default"].createElement(Component, {
      className: (0, _classnames4["default"])(_fontModule["default"].base, (_classnames = {}, _defineProperty(_classnames, _fontModule["default"]["base".concat(size)], !!size), _defineProperty(_classnames, _fontModule["default"].muted, muted), _classnames), className)
    }, children);
  } // children - это React Component


  if (children !== null && children !== void 0 && children.props && children.type) {
    return _react.Children.map(children, function (child) {
      var _child$props;

      return /*#__PURE__*/(0, _react.cloneElement)(child, _objectSpread(_objectSpread({}, child.props), {}, {
        className: (0, _classnames4["default"])((_child$props = child.props) === null || _child$props === void 0 ? void 0 : _child$props.className, _fontModule["default"].base, _defineProperty({}, _fontModule["default"]["base".concat(size)], !!size), className)
      }));
    });
  }

  return /*#__PURE__*/_react["default"].createElement("span", {
    className: (0, _classnames4["default"])(_fontModule["default"].base, _defineProperty({}, _fontModule["default"]["base".concat(size)], !!size), className)
  }, children);
};

Font.propTypes = {
  className: _propTypes["default"].string,
  as: _propTypes["default"].string,
  children: _propTypes["default"].any,
  size: _propTypes["default"].oneOf([18, 24]),
  muted: _propTypes["default"].bool
};
var _default = Font;
exports["default"] = _default;