"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TablePopupSort = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _hooks = require("../hooks");

var _popup = require("../popup");

var _button = require("../button");

var _paragraph = require("../paragraph");

var _icons = require("../icons");

var _newTablePopupSortModule = _interopRequireDefault(require("./new-table-popup-sort.module.scss"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var TablePopupSort = function TablePopupSort(_ref) {
  var headerCell = _ref.headerCell,
      renderProps = _ref.renderProps,
      field = _ref.field,
      onClickSort = _ref.onClickSort;

  var _useModalState = (0, _hooks.useModalState)(false),
      isShow = _useModalState.isShow,
      onOpenForm = _useModalState.onOpenForm,
      onCloseForm = _useModalState.onCloseForm;

  var _useState = (0, _react.useState)(field),
      _useState2 = _slicedToArray(_useState, 2),
      selectedSort = _useState2[0],
      setSelectedSort = _useState2[1];

  var onSort = /*#__PURE__*/function () {
    var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return onClickSort(selectedSort);

            case 2:
              onCloseForm();

            case 3:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function onSort() {
      return _ref2.apply(this, arguments);
    };
  }();

  var onSelectSort = function onSelectSort(sort) {
    if (sort === selectedSort) {
      setSelectedSort(null);
    } else {
      setSelectedSort(sort);
    }
  };

  return /*#__PURE__*/_react["default"].createElement(_react.Fragment, null, renderProps(onOpenForm), isShow && /*#__PURE__*/_react["default"].createElement(_popup.Popup, {
    onDismiss: function onDismiss() {
      return onCloseForm();
    }
  }, /*#__PURE__*/_react["default"].createElement(_popup.Popup.Header, null, "\u0421\u043E\u0440\u0442\u0438\u0440\u043E\u0432\u043A\u0430"), /*#__PURE__*/_react["default"].createElement(_popup.Popup.Content, null, headerCell.map(function (item, index) {
    return /*#__PURE__*/_react["default"].createElement(_react.Fragment, {
      key: index
    }, item.sort && /*#__PURE__*/_react["default"].createElement("div", {
      className: _newTablePopupSortModule["default"].itemSortWrap,
      onClick: function onClick() {
        return onSelectSort(item.sort);
      }
    }, /*#__PURE__*/_react["default"].createElement(_paragraph.Paragraph, null, item.label), item.sort === selectedSort && /*#__PURE__*/_react["default"].createElement(_icons.Icons16.IconCheck, null)));
  })), /*#__PURE__*/_react["default"].createElement(_popup.Popup.Footer, null, /*#__PURE__*/_react["default"].createElement(_button.Button, {
    className: _newTablePopupSortModule["default"].button,
    onClick: function onClick() {
      return onSort();
    }
  }, "\u041F\u0440\u0438\u043C\u0435\u043D\u0438\u0442\u044C"))));
};

exports.TablePopupSort = TablePopupSort;
TablePopupSort.propTypes = {
  headerCell: _propTypes["default"].array,
  renderProps: _propTypes["default"].func,
  field: _propTypes["default"].any,
  onClickSort: _propTypes["default"].func
};