"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.NewTable = exports.TableV2 = void 0;

var _newTable = require("./new-table");

var _newTableBody = require("./new-table-body");

var _newTableCell = require("./new-table-cell");

var _newTableHeader = require("./new-table-header");

var _newTableRow = require("./new-table-row");

var TableV2 = _newTable.NewTable;
exports.TableV2 = TableV2;
var NewTable = {
  Table: _newTable.NewTable,
  TableBody: _newTableBody.TableBody,
  TableCell: _newTableCell.TableCell,
  TableHeader: _newTableHeader.TableHeader,
  TableRow: _newTableRow.TableRow
};
exports.NewTable = NewTable;