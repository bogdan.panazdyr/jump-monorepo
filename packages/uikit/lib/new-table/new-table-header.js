"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TableHeader = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _description = require("../description");

var _newTableCell = require("./new-table-cell");

var _icons = require("../icons");

var _header = require("../header");

var _button = require("../button");

var _hooks = require("../hooks");

var _newTablePopupSort = require("./new-table-popup-sort");

var _newTableHeaderModule = _interopRequireDefault(require("./new-table-header.module.scss"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

var TableHeader = function TableHeader(_ref) {
  var headerCell = _ref.headerCell,
      onSort = _ref.onSort,
      sortField = _ref.sortField,
      title = _ref.title,
      onAdd = _ref.onAdd;
  var sign = sortField.slice(0, 1);
  var direction = sign.search(/[+-]/) === 0 ? sign : '+';
  var field = sign.search(/[+-]/) === 0 ? sortField.slice(1) : sortField;

  var onClickSort = function onClickSort(selectedField) {
    if (!selectedField) {
      return;
    }

    var nextDirection = selectedField === field ? direction === '+' ? '-' : '+' : '+';
    onSort("".concat(nextDirection).concat(selectedField));
  };

  return /*#__PURE__*/_react["default"].createElement(_react.Fragment, null, /*#__PURE__*/_react["default"].createElement("div", {
    className: _newTableHeaderModule["default"].titleWrap
  }, title && /*#__PURE__*/_react["default"].createElement(_header.Header.H2, {
    className: _newTableHeaderModule["default"].title
  }, title), /*#__PURE__*/_react["default"].createElement("div", {
    className: _newTableHeaderModule["default"].buttons
  }, (0, _hooks.useIfMediaScreen)() && onSort && /*#__PURE__*/_react["default"].createElement(_newTablePopupSort.TablePopupSort, {
    renderProps: function renderProps(open) {
      return /*#__PURE__*/_react["default"].createElement(_button.Button, {
        styling: "hollow",
        icon: /*#__PURE__*/_react["default"].createElement(_icons.Icons24.IconSort, null),
        onClick: open
      });
    },
    headerCell: headerCell,
    sortField: sortField,
    field: field,
    onClickSort: onClickSort
  }), onAdd && /*#__PURE__*/_react["default"].createElement(_button.Button, {
    styling: "hollow",
    icon: /*#__PURE__*/_react["default"].createElement(_icons.Icons24.IconPlus, null),
    onClick: function onClick() {
      return onAdd();
    }
  }))), /*#__PURE__*/_react["default"].createElement("div", {
    className: _newTableHeaderModule["default"].base,
    style: {
      gridTemplateColumns: "repeat(".concat(headerCell.length, ", 1fr)")
    }
  }, headerCell.map(function (item, index) {
    return /*#__PURE__*/_react["default"].createElement(_newTableCell.TableCell, {
      key: index
    }, /*#__PURE__*/_react["default"].createElement(_description.Description.LH24, {
      element: "span",
      className: "\n                                ".concat(_newTableHeaderModule["default"].itemHeader, "\n                                ").concat((item === null || item === void 0 ? void 0 : item.sort) && _newTableHeaderModule["default"].hover, "\n                                ").concat((item === null || item === void 0 ? void 0 : item.sort) === field && _newTableHeaderModule["default"].active, "\n                            "),
      onClick: function onClick() {
        return onClickSort(item === null || item === void 0 ? void 0 : item.sort);
      }
    }, item.label, (item === null || item === void 0 ? void 0 : item.sort) === field ? direction === '+' ? /*#__PURE__*/_react["default"].createElement(_icons.Icons16.IconSortDown, null) : /*#__PURE__*/_react["default"].createElement(_icons.Icons16.IconSortUp, null) : null));
  })));
};

exports.TableHeader = TableHeader;
TableHeader.defaultProps = {
  headerCell: [],
  onSort: function onSort() {}
};
TableHeader.propTypes = {
  onSort: _propTypes["default"].any,
  sortField: _propTypes["default"].string,
  headerCell: _propTypes["default"].array,
  title: _propTypes["default"].string,
  onAdd: _propTypes["default"].oneOfType([_propTypes["default"].func, _propTypes["default"].bool])
};