"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TablePopup = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _button = require("../button");

var _icons = require("../icons");

var _header = require("../header");

var _paragraph = require("../paragraph");

var _hooks = require("../hooks");

var _newTablePopupModule = _interopRequireDefault(require("./new-table-popup.module.scss"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

var TablePopup = function TablePopup(_ref) {
  var data = _ref.data,
      accessor = _ref.accessor,
      headerCell = _ref.headerCell,
      renderCustomCells = _ref.renderCustomCells,
      renderProps = _ref.renderProps;

  var _useModalState = (0, _hooks.useModalState)(false),
      isShow = _useModalState.isShow,
      onOpenForm = _useModalState.onOpenForm,
      onCloseForm = _useModalState.onCloseForm;

  return /*#__PURE__*/_react["default"].createElement(_react.Fragment, null, renderProps(onOpenForm), isShow && /*#__PURE__*/_react["default"].createElement("div", {
    className: _newTablePopupModule["default"].base
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: _newTablePopupModule["default"].shadow
  }), /*#__PURE__*/_react["default"].createElement("div", {
    className: _newTablePopupModule["default"].content
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: _newTablePopupModule["default"].header
  }, /*#__PURE__*/_react["default"].createElement(_header.Header.H3, null, data[accessor[0]]), /*#__PURE__*/_react["default"].createElement(_button.Button, {
    styling: "hollow",
    icon: /*#__PURE__*/_react["default"].createElement(_icons.Icons24.IconClose, null),
    onClick: function onClick() {
      return onCloseForm();
    }
  })), headerCell.map(function (item, index) {
    return /*#__PURE__*/_react["default"].createElement(_paragraph.Paragraph, {
      key: index,
      className: _newTablePopupModule["default"].paragraph
    }, item.label, ": ", data[accessor[index]]);
  }), /*#__PURE__*/_react["default"].createElement("div", {
    className: _newTablePopupModule["default"].buttons
  }, renderCustomCells(data)))));
};

exports.TablePopup = TablePopup;
TablePopup.propTypes = {
  data: _propTypes["default"].object,
  accessor: _propTypes["default"].array,
  headerCell: _propTypes["default"].array,
  renderCustomCells: _propTypes["default"].func,
  renderProps: _propTypes["default"].func
};