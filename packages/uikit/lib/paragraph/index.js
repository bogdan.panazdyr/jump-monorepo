"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "Paragraph", {
  enumerable: true,
  get: function get() {
    return _paragraph.Paragraph;
  }
});

var _paragraph = require("./paragraph");