"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "InfoMessage", {
  enumerable: true,
  get: function get() {
    return _infoMessage.InfoMessage;
  }
});

var _infoMessage = require("./info-message");