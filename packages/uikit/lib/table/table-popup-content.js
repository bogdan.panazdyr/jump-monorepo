"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TablePopupContent = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _tablePopupContentModule = _interopRequireDefault(require("./table-popup-content.module.scss"));

var _renderHelper = require("./render-helper");

var _icons = require("../icons");

var _button = _interopRequireDefault(require("../button/button"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var TablePopupContent = function TablePopupContent(_ref) {
  var columns = _ref.columns,
      rowData = _ref.rowData,
      onRowSelection = _ref.onRowSelection;
  var mainColumnInfo = columns.find(function (col) {
    return col.main;
  });
  var restColumns = columns.filter(function (col) {
    return !col.main;
  });
  return /*#__PURE__*/_react["default"].createElement("div", {
    className: _tablePopupContentModule["default"].base
  }, mainColumnInfo && /*#__PURE__*/_react["default"].createElement("div", {
    className: _tablePopupContentModule["default"].main
  }, (0, _renderHelper.renderColumn)(mainColumnInfo, rowData)), restColumns.map(function (columnInfo, index) {
    return /*#__PURE__*/_react["default"].createElement("div", {
      className: _tablePopupContentModule["default"].normal,
      key: index
    }, columnInfo.Header, ":", ' ', (0, _renderHelper.renderColumn)(columnInfo, rowData));
  }), onRowSelection && /*#__PURE__*/_react["default"].createElement("div", {
    className: _tablePopupContentModule["default"].edit
  }, /*#__PURE__*/_react["default"].createElement(_button["default"], {
    icon: /*#__PURE__*/_react["default"].createElement(_icons.Icons24.IconPencil, null),
    element: "button",
    styling: "hollow-border",
    type: "button",
    onClick: onRowSelection,
    style: {
      width: '100%'
    }
  }, "\u0420\u0435\u0434\u0430\u043A\u0442\u0438\u0440\u043E\u0432\u0430\u0442\u044C")));
};

exports.TablePopupContent = TablePopupContent;
TablePopupContent.propTypes = {
  columns: _propTypes["default"].array.isRequired,
  rowData: _propTypes["default"].object.isRequired,
  onRowSelection: _propTypes["default"].func
};