"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TableHeader = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _tableHeaderModule = _interopRequireDefault(require("./table-header.module.scss"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var TableHeader = function TableHeader(_ref) {
  var header = _ref.header,
      children = _ref.children;
  return /*#__PURE__*/_react["default"].createElement("div", {
    className: _tableHeaderModule["default"].base
  }, header && /*#__PURE__*/_react["default"].createElement("div", {
    className: _tableHeaderModule["default"].title
  }, header), children && /*#__PURE__*/_react["default"].createElement("div", {
    className: _tableHeaderModule["default"].toolbar
  }, children));
};

exports.TableHeader = TableHeader;
TableHeader.defaultProps = {
  isMediaScreen: false
};
TableHeader.propTypes = {
  header: _propTypes["default"].string,
  children: _propTypes["default"].node
};