"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TableHeaderSort = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _icons = require("../icons");

var _overflowMenu = require("../overflow-menu");

var _order = require("./order");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

var TableHeaderSort = function TableHeaderSort(_ref) {
  var columns = _ref.columns,
      sort = _ref.sort,
      onSort = _ref.onSort;
  var sortableColumns = (0, _react.useMemo)(function () {
    return columns.filter(function (column) {
      return column.sortable;
    });
  }, [columns]);
  var items = (0, _react.useMemo)(function () {
    var arr = [];
    var index = 0;
    sortableColumns.forEach(function (column) {
      _order.orders.forEach(function (order) {
        arr.push( /*#__PURE__*/_react["default"].createElement(_overflowMenu.OverflowMenu.Item, {
          key: index,
          value: {
            by: column.accessor,
            desc: order.desc
          }
        }, column.Header, ' ', /*#__PURE__*/_react["default"].createElement(order.icon, {
          style: {
            verticalAlign: 'middle'
          }
        }), sort.by === column.accessor && sort.desc === order.desc && /*#__PURE__*/_react["default"].createElement(_react["default"].Fragment, null, ' ', /*#__PURE__*/_react["default"].createElement(_icons.Icons16.IconCheck, {
          style: {
            verticalAlign: 'middle'
          }
        }))));
        index += 1;
      });
    });
    return arr;
  }, [sort, sortableColumns]);
  return sortableColumns.length > 0 ? /*#__PURE__*/_react["default"].createElement(_overflowMenu.OverflowMenu, {
    icon: /*#__PURE__*/_react["default"].createElement(_icons.Icons24.IconSort, null),
    style: {
      padding: 0
    },
    onItemSelect: onSort
  }, items, /*#__PURE__*/_react["default"].createElement(_overflowMenu.OverflowMenu.Item, {
    value: {
      by: null
    }
  }, "\u043D\u0435 \u0441\u043E\u0440\u0442\u0438\u0440\u043E\u0432\u0430\u0442\u044C", ' ', (!sort.by || sort.by === '') && /*#__PURE__*/_react["default"].createElement(_icons.Icons16.IconCheck, {
    style: {
      verticalAlign: 'middle'
    }
  }))) : null;
};

exports.TableHeaderSort = TableHeaderSort;
TableHeaderSort.defaultProps = {
  columns: [],
  onSort: function onSort() {}
};
TableHeaderSort.propTypes = {
  columns: _propTypes["default"].arrayOf(_propTypes["default"].shape({
    accessor: _propTypes["default"].string,
    Header: _propTypes["default"].string,
    sortable: _propTypes["default"].bool
  })),
  sort: _propTypes["default"].shape({
    by: _propTypes["default"].string,
    desc: _propTypes["default"].bool
  }),
  onSort: _propTypes["default"].func
};