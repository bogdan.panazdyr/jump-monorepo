"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.isValidDate = isValidDate;
exports.addDays = addDays;
exports.declensionWords = declensionWords;
exports.formatDate = exports.idFromString = void 0;

var idFromString = function idFromString(str) {
  var id = 'id_';

  for (var i = 0, l = str.length; i < l; i++) {
    id += str.charCodeAt(i);
  }

  return id;
};

exports.idFromString = idFromString;

function isValidDate(d) {
  return d instanceof Date && !isNaN(d);
}

var formatDate = function formatDate(value) {
  var d = value.split('.');
  var getDate = d[0];
  var getMonth = d[1] - 1;
  var getYear = d[2];

  if (!~value.indexOf('_')) {
    var date = new Date(getYear, getMonth, getDate);
    return isValidDate(date) ? date : undefined;
  } else {
    return undefined;
  }
};

exports.formatDate = formatDate;

function addDays(date, days) {
  var result = new Date(date);
  result.setDate(result.getDate() + days);
  return result;
}

function declensionWords(num, labels) {
  num = Math.abs(num) % 100;
  var copy_num = num % 10;

  if (num > 10 && num < 20) {
    return labels[2];
  }

  if (copy_num > 1 && copy_num < 5) {
    return labels[1];
  }

  if (copy_num === 1) {
    return labels[0];
  }

  return labels[2];
}