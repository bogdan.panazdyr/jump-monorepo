"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SearchListItem = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _searchListItemModule = _interopRequireDefault(require("./search-list-item.module.scss"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var SearchListItem = function SearchListItem(_ref) {
  var main = _ref.main,
      description = _ref.description,
      onClick = _ref.onClick;
  return /*#__PURE__*/_react["default"].createElement("div", {
    className: _searchListItemModule["default"].base,
    onClick: onClick
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: _searchListItemModule["default"].main
  }, main), /*#__PURE__*/_react["default"].createElement("div", {
    className: _searchListItemModule["default"].desc
  }, description));
};

exports.SearchListItem = SearchListItem;
SearchListItem.defaultProps = {
  onClick: function onClick() {}
};
SearchListItem.propTypes = {
  main: _propTypes["default"].string.isRequired,
  description: _propTypes["default"].string.isRequired,
  onClick: _propTypes["default"].func
};