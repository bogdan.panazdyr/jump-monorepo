"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SelectAsync = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _async = _interopRequireDefault(require("react-select/async"));

var _useSelectStyle = require("./useSelectStyle");

var _selectModule = _interopRequireDefault(require("./select.module.scss"));

var _excluded = ["onChange", "placeholder", "error", "label", "disabled", "defaultValue", "defaultOptions", "loadOptions"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var SelectAsync = function SelectAsync(_ref) {
  var _onChange = _ref.onChange,
      placeholder = _ref.placeholder,
      error = _ref.error,
      label = _ref.label,
      disabled = _ref.disabled,
      defaultValue = _ref.defaultValue,
      defaultOptions = _ref.defaultOptions,
      loadOptions = _ref.loadOptions,
      rest = _objectWithoutProperties(_ref, _excluded);

  var customStyle = (0, _useSelectStyle.useCustomStyle)(error, disabled);
  return /*#__PURE__*/_react["default"].createElement("div", {
    className: _selectModule["default"].content
  }, label.length > 1 ? /*#__PURE__*/_react["default"].createElement("label", {
    htmlFor: "",
    className: _selectModule["default"].label
  }, label) : null, /*#__PURE__*/_react["default"].createElement(_async["default"], _extends({
    styles: customStyle,
    onChange: function onChange(selectedOption) {
      _onChange(selectedOption.value);
    },
    placeholder: placeholder,
    isDisabled: disabled,
    defaultValue: defaultValue,
    defaultOptions: defaultOptions,
    loadOptions: loadOptions,
    open: true
  }, rest)), error && /*#__PURE__*/_react["default"].createElement("span", {
    className: _selectModule["default"]['error-message']
  }, error));
};

exports.SelectAsync = SelectAsync;
SelectAsync.defaultProps = {
  onChange: function onChange() {},
  placeholder: '',
  label: '',
  error: '',
  disabled: false
};
SelectAsync.propTypes = {
  onChange: _propTypes["default"].func,
  placeholder: _propTypes["default"].string,
  label: _propTypes["default"].string,
  error: _propTypes["default"].any,
  disabled: _propTypes["default"].bool,
  defaultValue: _propTypes["default"].object,
  defaultOptions: _propTypes["default"].oneOfType([_propTypes["default"].bool, _propTypes["default"].array]),
  loadOptions: _propTypes["default"].func
};