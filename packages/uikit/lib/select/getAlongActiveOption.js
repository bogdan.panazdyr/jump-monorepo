"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getAlongActiveOption = void 0;

var getAlongActiveOption = function getAlongActiveOption() {
  var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
  var active = options.filter(function (option) {
    return option.isDisabled !== true;
  });
  return active.length === 1 ? active[0] : null;
};

exports.getAlongActiveOption = getAlongActiveOption;