"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SelectOption = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactSelect = _interopRequireDefault(require("react-select"));

var _classnames = _interopRequireDefault(require("classnames"));

var _getAlongActiveOption = require("./getAlongActiveOption");

var _useSelectStyle = require("./useSelectStyle");

var _selectModule = _interopRequireDefault(require("./select.module.scss"));

var _excluded = ["options", "onChange", "placeholder", "error", "label", "disabled", "compact", "isClearable", "prefix", "isSearchable", "defaultValue", "value"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var SelectOption = function SelectOption(_ref) {
  var options = _ref.options,
      onChange = _ref.onChange,
      placeholder = _ref.placeholder,
      error = _ref.error,
      label = _ref.label,
      disabled = _ref.disabled,
      compact = _ref.compact,
      isClearable = _ref.isClearable,
      prefix = _ref.prefix,
      isSearchable = _ref.isSearchable,
      defaultValue = _ref.defaultValue,
      value = _ref.value,
      rest = _objectWithoutProperties(_ref, _excluded);

  var option = (0, _getAlongActiveOption.getAlongActiveOption)(options); // select должен быть выключен, если:
  // - есть только одна активная опция (больше нельзя ничего выбрать)
  // - выключено программно через props - disabled

  var isDisabled = option || disabled;
  var customStyle = (0, _useSelectStyle.useCustomStyle)(error, option || disabled);
  var compactStyle = (0, _useSelectStyle.useCustomCompactStyle)();

  var CustomOption = function CustomOption(_ref2) {
    var _classes;

    var innerProps = _ref2.innerProps,
        isSelected = _ref2.isSelected,
        isDisabled = _ref2.isDisabled,
        label = _ref2.label,
        data = _ref2.data;
    var cls = (0, _classnames["default"])(_selectModule["default"].compactOption, (_classes = {}, _defineProperty(_classes, _selectModule["default"].active, isSelected), _defineProperty(_classes, _selectModule["default"].disabled, isDisabled), _classes));
    return /*#__PURE__*/_react["default"].createElement("div", _extends({}, innerProps, {
      className: cls
    }), /*#__PURE__*/_react["default"].createElement("div", null, label), Boolean(data.badge) && /*#__PURE__*/_react["default"].createElement("span", {
      className: _selectModule["default"].badge
    }, data.badge));
  };

  CustomOption.propTypes = {
    innerProps: _propTypes["default"].any,
    label: _propTypes["default"].string,
    isSelected: _propTypes["default"].bool,
    isDisabled: _propTypes["default"].bool,
    data: _propTypes["default"].any
  };

  var CustomSingleValue = function CustomSingleValue(_ref3) {
    var innerProps = _ref3.innerProps,
        data = _ref3.data;
    return /*#__PURE__*/_react["default"].createElement("div", _extends({}, innerProps, {
      className: _selectModule["default"].customSingleValue
    }), /*#__PURE__*/_react["default"].createElement("div", null, data.label), Boolean(data.badge) && /*#__PURE__*/_react["default"].createElement("span", {
      className: _selectModule["default"].badge
    }, data.badge));
  };

  CustomSingleValue.propTypes = {
    innerProps: _propTypes["default"].any,
    data: _propTypes["default"].any
  };

  var CustomControl = function CustomControl(_ref4) {
    var children = _ref4.children,
        innerProps = _ref4.innerProps;
    return /*#__PURE__*/_react["default"].createElement("div", _extends({
      className: _selectModule["default"].compactControl
    }, innerProps, {
      tabIndex: '0'
    }), /*#__PURE__*/_react["default"].createElement("div", {
      className: _selectModule["default"].compactLabel
    }, prefix), children);
  };

  CustomControl.propTypes = {
    children: _propTypes["default"].any,
    innerProps: _propTypes["default"].any
  };
  return /*#__PURE__*/_react["default"].createElement("div", {
    className: _selectModule["default"].content
  }, label.length > 1 ? /*#__PURE__*/_react["default"].createElement("label", {
    className: _selectModule["default"].label
  }, label) : null, /*#__PURE__*/_react["default"].createElement(_reactSelect["default"], _extends({
    styles: compact ? compactStyle : customStyle,
    value: option || value,
    options: options,
    onChange: onChange,
    placeholder: placeholder,
    isDisabled: isDisabled,
    isClearable: isClearable,
    isSearchable: isSearchable,
    components: compact ? {
      Option: CustomOption,
      Control: CustomControl
    } : {
      Option: CustomOption,
      SingleValue: CustomSingleValue
    },
    defaultValue: defaultValue
  }, rest)), error && /*#__PURE__*/_react["default"].createElement("span", {
    className: _selectModule["default"]['error-message']
  }, error));
};

exports.SelectOption = SelectOption;
SelectOption.defaultProps = {
  options: [],
  onChange: function onChange() {},
  placeholder: '',
  label: '',
  error: '',
  disabled: false,
  compact: false,
  isClearable: false,
  isSearchable: true,
  prefix: ''
};
SelectOption.propTypes = {
  options: _propTypes["default"].array,
  onChange: _propTypes["default"].func,
  placeholder: _propTypes["default"].string,
  label: _propTypes["default"].string,
  error: _propTypes["default"].any,
  disabled: _propTypes["default"].bool,
  compact: _propTypes["default"].bool,
  isClearable: _propTypes["default"].bool,
  isSearchable: _propTypes["default"].bool,
  prefix: _propTypes["default"].string,
  defaultValue: _propTypes["default"].object,
  value: _propTypes["default"].oneOfType([_propTypes["default"].string, _propTypes["default"].shape({
    value: _propTypes["default"].oneOfType([_propTypes["default"].string, _propTypes["default"].number]).isRequired,
    label: _propTypes["default"].oneOfType([_propTypes["default"].string, _propTypes["default"].number]).isRequired
  })])
};