"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.OverflowMenuItem = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _overflowMenuItemModule = _interopRequireDefault(require("./overflow-menu-item.module.scss"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var OverflowMenuItem = function OverflowMenuItem(_ref) {
  var onItemClick = _ref.onItemClick,
      value = _ref.value,
      index = _ref.index,
      children = _ref.children;

  var _onItemClick = function _onItemClick() {
    return onItemClick(value, index);
  };

  return /*#__PURE__*/_react["default"].createElement("div", {
    className: _overflowMenuItemModule["default"].base,
    onClick: _onItemClick
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: _overflowMenuItemModule["default"].content
  }, children));
};

exports.OverflowMenuItem = OverflowMenuItem;
OverflowMenuItem.defaultProps = {
  onItemClick: function onItemClick() {}
};
OverflowMenuItem.propTypes = {
  onItemClick: _propTypes["default"].func,
  value: _propTypes["default"].any,
  children: _propTypes["default"].any,
  index: _propTypes["default"].number
};