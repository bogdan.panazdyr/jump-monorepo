"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.RadioButton = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _helpers = require("../helpers");

var _hooks = require("../hooks");

var _radioButtonModule = _interopRequireDefault(require("./radio-button.module.scss"));

var _excluded = ["label", "id", "className", "onChange", "error"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var RadioButton = function RadioButton(props) {
  var _useTabListener = (0, _hooks.useTabListener)(),
      focusedByTab = _useTabListener.focusedByTab;

  var label = props.label,
      id = props.id,
      className = props.className,
      onChange = props.onChange,
      error = props.error,
      attr = _objectWithoutProperties(props, _excluded);

  var key = id || (0, _helpers.idFromString)(label);
  var clsName = (0, _classnames["default"])(_radioButtonModule["default"].base, className, _defineProperty({}, _radioButtonModule["default"].error, error));
  var clsNameInput = (0, _classnames["default"])(_radioButtonModule["default"].inputRadio, _defineProperty({}, _radioButtonModule["default"].focus, focusedByTab));

  var handleChange = function handleChange(e) {
    return onChange(props.value, props.name, e);
  };

  return /*#__PURE__*/_react["default"].createElement("div", {
    className: clsName
  }, /*#__PURE__*/_react["default"].createElement("input", _extends({
    type: "radio",
    className: clsNameInput,
    id: key,
    onChange: handleChange
  }, attr)), /*#__PURE__*/_react["default"].createElement("label", {
    htmlFor: key,
    className: _radioButtonModule["default"].labelRadio
  }, label));
};

exports.RadioButton = RadioButton;
RadioButton.defaultProps = {
  onChange: function onChange() {},
  value: '',
  id: null,
  className: '',
  error: false
};
RadioButton.propTypes = {
  value: _propTypes["default"].oneOfType([_propTypes["default"].string, _propTypes["default"].number]).isRequired,
  onChange: _propTypes["default"].func,
  name: _propTypes["default"].string,
  id: _propTypes["default"].string,
  className: _propTypes["default"].string,
  disabled: _propTypes["default"].bool,
  checked: _propTypes["default"].bool,
  defaultChecked: _propTypes["default"].bool,
  error: _propTypes["default"].bool,
  label: _propTypes["default"].string
};