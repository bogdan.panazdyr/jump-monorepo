import React from 'react';
import { storiesOf } from '@storybook/react';

import { SplashScreen } from './index';
import readme from './readme.md';

storiesOf('Экраны/Инициализация', module)
    .add(
        'SplashScreen',
        () => {
            return (
                <SplashScreen/>
            )
        }, {
            notes: { readme }
        }
    );