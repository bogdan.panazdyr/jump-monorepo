# Upload

## Импорт
```javascript
import { Upload } from '@justlook/uikit';
```

## Использование
```javascript
const el = () => {
    return (
        <Upload
            onChange={(value) => console.log(value)}
            error={''}
        />
    )
}
```