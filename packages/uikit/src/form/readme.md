# Форма

##Импорт

```javascript
import { Form } from '@justlook/uikit';
```

##Использование

```javascript
const el = () => {
    return (
        <Form onSubmit={(e) => e.preventDefault()}>
            <Form.Title>Форма</Form.Title>

            <Form.Field>
                <Input
                    name={'input'}
                    placeholder={'placeholder'}
                />
            </Form.Field>

            <Form.Field>
                <Select
                    name={'select'}
                    options={[
                            {value: 'option1', label: 'option1'},
                            {value: 'option2', label: 'option2'}
                        ]}
                />
            </Form.Field>

            <Form.Field>
                <Button type={'submit'} >Отправить</Button>
            </Form.Field>
        </Form>
    )
}
```