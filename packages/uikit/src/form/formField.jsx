import React from 'react';
import PropTypes from 'prop-types';

import cx from './formField.module.scss';

export const Field = ({children, ...attr}) => {
    return (
        <div className={cx.base} {...attr}>
            {children}
        </div>
    )
};

Field.displayName = 'Form.Field';

Field.propTypes = {
    children: PropTypes.any.isRequired,
};