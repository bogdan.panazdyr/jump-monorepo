import React from 'react';
import { storiesOf } from '@storybook/react';

import { Form } from './index';
import { Input } from '../input';
import { Select } from '../select';
import { Button } from '../button';

storiesOf('Компоненты/Форма', module)
    .add(
        'Form',
        () => {

            return (
                <Form onSubmit={(e) => e.preventDefault()}>
                    <Form.Title>Форма</Form.Title>

                    <Form.Field>
                        <Input
                            name={'input'}
                            placeholder={'placeholder'}
                        />
                    </Form.Field>

                    <Form.Field>
                        <Select
                            name={'select'}
                            options={[
                                    {value: 'option1', label: 'option1'},
                                    {value: 'option2', label: 'option2'},
                                ]}
                        />
                    </Form.Field>

                    <Form.Field>
                        <Button type={'submit'} >Отправить</Button>
                    </Form.Field>
                </Form>
            )
        }
    );