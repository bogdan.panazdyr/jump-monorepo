import React from 'react';
import PropTypes from 'prop-types';

import { Header } from '../header';
import cx from './formTitle.module.scss';

export const Title = ({children, ...attr}) => {
    return (
        <Header.H1 {...attr} className={cx.title}>{children}</Header.H1>
    )
};

Title.displayName = 'Form.Title';

Title.propTypes = {
    children: PropTypes.string.isRequired,
};