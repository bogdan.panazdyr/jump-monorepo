import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Link } from '../index';
import { ErrorScreen } from './error-screen';

export const ErrorScreen404 = ({ homeLink, footerContent }) => {
    return (
        <ErrorScreen
            error={{
                title: 'Страница, которую вы ищете, не существует',
                message: 'Код ошибки 404',
            }}
            footerContent={() => (
                <Fragment>
                    <Link to={homeLink}>Перейти на главную</Link>
                    { footerContent() }
                </Fragment>
            )}
        />
    );
};

ErrorScreen404.defaultProps = {
    homeLink: '/',
    footerContent: () => null,
};

ErrorScreen404.propTypes = {
    homeLink: PropTypes.string,
    footerContent: PropTypes.func,
};