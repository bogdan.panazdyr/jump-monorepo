# Загрузчик

## Импорт

```javascript
import { ErrorScreen, ErrorScreen404, ErrorScreen503 } from '@justlook/uikit';
```

## Использование

```javascript
const el = () => {
    return <ErrorScreen />
}
```