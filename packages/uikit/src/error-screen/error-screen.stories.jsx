import React from 'react';
import { storiesOf } from '@storybook/react';
import { Link } from '../link';

import { ErrorScreen, ErrorScreen404, ErrorScreen503 } from './index';
import readme from './readme.md';

storiesOf('Экраны/Ошибка', module)
    .add(
        'ErrorScreen',
        () => {
            return (
                <ErrorScreen
                    error={{
                        title: 'В приложении  возникла ошибка',
                        message: 'Что-то пошло не так',
                    }}
                    footerContent={() => (
                        <Link to='#'>Перейти на главную</Link>
                    )}
                />
            );
        }, {
            notes: { readme },
        },
    )
    .add(
        'ErrorScreen404',
        () => {
            return (
                <ErrorScreen404 homeLink={'/home'}/>
            );
        }, {
            notes: { readme },
        },
    )
    .add(
        'ErrorScreen503',
        () => {
            return (
                <ErrorScreen503/>
            );
        }, {
            notes: { readme },
        },
    );