import React      from 'react';
import PropTypes  from 'prop-types';
import classNames from 'classnames';

import cx from './description.module.scss';

const makeDescription = (LH) => {
    const component = ({ element: Tag, children, className, ...attr }) => {
        const clsName = classNames(
            cx.base,
            { [ cx[ 'base-24' ] ]: LH === '24', [ cx[ 'base-16' ] ]: LH === '16' },
            className,
        );
        return <Tag className={clsName} {...attr}>{children}</Tag>
    };

    component.displayName = 'Description.LH' + LH.toUpperCase();

    component.defaultProps = {
        element: 'p'
    }

    component.propTypes = {
        className: PropTypes.string,
        children: PropTypes.any,
        element: PropTypes.string,
    };
    return component;
};

export const Description = {
    LH24: makeDescription('24'),
    LH16: makeDescription('16'),
};

