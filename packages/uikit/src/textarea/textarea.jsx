import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Description } from '../description';

import { idFromString } from '../helpers';

import cx from './textarea.module.scss';

export const Textarea = ({ id, name, label, placeholder, error, className, onChange, ...rest }) => {

    const key = id || idFromString(name || label || placeholder);
    const clsName = classNames(cx.base, { [cx.error]: error });
    const clsNameTextArea = classNames(cx.textarea, className);

    return (
        <div className={clsName}>
            {
                label === ''
                    ? null
                    : <label htmlFor={key} className={cx.label}>{label}</label>
            }

            <textarea
                id={key}
                name={name}
                placeholder={placeholder}
                className={clsNameTextArea}
                onChange={(e) => onChange(e.target.value)}
                {...rest}
            />
            {error && <Description.LH16
                className={cx['error-message']}>{error}</Description.LH16>}
        </div>
    );
};

Textarea.defaultProps = {
    id: null,
    label: '',
    onChange: () => {},
};

Textarea.propTypes = {
    id: PropTypes.string,
    name: PropTypes.string,
    label: PropTypes.string,
    placeholder: PropTypes.string,
    className: PropTypes.string,
    error: PropTypes.any,
    onChange: PropTypes.func,
};