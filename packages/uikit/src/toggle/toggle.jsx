import React from 'react';
import PropTypes from 'prop-types';
import classes from 'classnames';

import { Icons16 } from '../icons';

import { idFromString } from '../helpers';

import cx from './toggle.module.scss';

export const Toggle = ({ id, name, label, onChange, positionToggle, isLoading, disabled, ...rest }) => {
    const key = id || idFromString(name || label || '');
    const cls = classes(cx.base, {
        [cx.before]: positionToggle === 'before',
        [cx.after]: positionToggle === 'after',
        [cx.disabled]: disabled
    });

    const toggleCls = classes(cx.toggle, {[cx.loading]: isLoading});

    const handlerOnChange = (e) => onChange(e.target.checked);

    return (
        <label htmlFor={key} className={cls}>
            {
                label &&
                <div className={cx.label}>
                    {label}
                </div>
            }

            <input
                id={key}
                type='checkbox'
                name={name}
                className={cx.input}
                onChange={handlerOnChange}
                disabled={disabled}
                hidden
                {...rest}
            />
            {
                <div className={toggleCls}/>
            }
        </label>
    );
};

Toggle.defaultProps = {
    positionToggle: 'before',
    isLoading: false,
    onChange: () => {},
};

Toggle.propTypes = {
    label: PropTypes.string,
    id: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]),
    name: PropTypes.string,
    onChange: PropTypes.func,
    isLoading: PropTypes.bool,
    positionToggle: PropTypes.oneOf([ 'before', 'after' ]),
    disabled: PropTypes.bool,
};
