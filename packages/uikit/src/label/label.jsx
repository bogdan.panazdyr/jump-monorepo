import React, { memo } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import cx from './label.module.scss';

export const LabelThemes = ['success', 'info', 'warning', 'danger'];

export const Label = (({text, theme, className}) => {
    const classes = classnames(cx.label, className, {
        [cx.success]: theme === 'success',
        [cx.info]: theme === 'info',
        [cx.warning]: theme === 'warning',
        [cx.danger]: theme === 'danger',
    })

    if(!text) return;
    return (
        <span className={classes}>
            {text}
        </span>
    )
});


Label.propTypes = {
    text: PropTypes.string,
    theme: PropTypes.oneOf(LabelThemes),
    className: PropTypes.string,
}

