# Checkbox 

## Импорт

```javascript
import { InfoMessage } from '@justlook/uikit';
```

## Использование

```javascript
<InfoMessage
    type='info'
    icon={<Icons24.IconInfo/>}
    fluid={false}
>
    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
</InfoMessage>
```
