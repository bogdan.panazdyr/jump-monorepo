import React     from 'react';
import PropTypes from 'prop-types';
import classes   from 'classnames'

import { Icons24 }            from '../icons';
import { Header }             from '../header';
import { Description }        from '../description';
import { Button }             from '../button';
import { MobileHeaderButton } from './mobile-header-button';

import cx from './mobile-header.module.scss';

export const MobileHeader = ({ header, className, description, onGoBack, children }) => {

    const cls = classes(cx.base, className, {
        [ cx.description ]: description
    })

    return (
        <div className={cls}>
            {onGoBack &&
            <div className={cx.backBtn}>
                <Button styling='hollow' icon={<Icons24.IconArrowLeftLine/>} onClick={onGoBack}/>
            </div>
            }

            <div className={cx.descArea}>
                <Header.H1 className={cx.title}>{header}</Header.H1>
                {description && <Description.LH24 className={cx[ 'description-text' ]}>{description}</Description.LH24>}
            </div>

            <div className={cx.toolbox}>
                {children}
            </div>

        </div>
    );
};

MobileHeader.Button = MobileHeaderButton;

MobileHeader.propTypes = {
    header: PropTypes.string.isRequired,
    description: PropTypes.string,
    onGoBack: PropTypes.func,
    children: PropTypes.node,
    className: PropTypes.string,
};
