import React, { useState } from 'react';
import { storiesOf }       from '@storybook/react';
import { text }            from '@storybook/addon-knobs';

import readme           from './readme.md';
import cx               from '../story.module.scss';
import { MobileHeader } from './mobile-header';
import { Icons24 }      from '../icons';

storiesOf('Компоненты/Заголовок', module)
    .add(
        'MobileHeader',
        () => {
            const [ state ] = useState('/cars')

            return (
                <div className={cx.position}>
                    <div style={{ display: 'flex' }}>
                        <div className={cx.device} style={{ marginRight: '2rem' }}>
                            <MobileHeader header={text('Header', 'Header')}>
                                <MobileHeader.Button icon={<Icons24.IconPlus/>} onClick={() => alert('PLUS')}/>
                                <MobileHeader.Button icon={<Icons24.IconSearch/>} onClick={() => alert('Search')}/>
                            </MobileHeader>
                        </div>

                        <div className={cx.device}>
                            <MobileHeader
                                header={text('Header', 'Header')}
                                description={text('Description', 'Description')}
                                onGoBack={state ? () => alert('goBack') : null}
                            >
                                <MobileHeader.Button icon={<Icons24.IconMenu/>} onClick={() => alert('Menu')}/>
                            </MobileHeader>
                        </div>
                    </div>
                </div>
            )
        }, {
            notes: { readme }
        }
    );
