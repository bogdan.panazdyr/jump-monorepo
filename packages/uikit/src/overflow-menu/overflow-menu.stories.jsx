import React from 'react';
import { storiesOf } from '@storybook/react';

import { OverflowMenu } from './overflow-menu';
import readme from './readme.md';
import { Icons24 } from '../icons';

storiesOf('Компоненты/Кнопка с меню', module)
    .add(
        'OverflowMenu',
        () => {

            return (
                <div style={{ width: '100%', display: 'flex', flexDirection: 'row-reverse', marginBottom: '100px'}}>
                    <OverflowMenu
                        icon={<Icons24.IconSort/>}
                        onItemSelect={value => alert('Выбранный элемент ' + value)}
                    >
                        <OverflowMenu.Item value='watermelon'>арбуз</OverflowMenu.Item>
                        <OverflowMenu.Item value='tomato'>помидор</OverflowMenu.Item>
                        <OverflowMenu.Item value='apple'>яблоко</OverflowMenu.Item>
                    </OverflowMenu>
                </div>
            )
        }, {
            notes: { readme }
        }
    );
