import React, { useState, useRef } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import cx from './overflow-menu.module.scss';
import { Button } from '../button';
import { OverflowMenuItem } from './overflow-menu-item';
import { useOnClickOutside } from '../hooks';

export const OverflowMenu = ({ icon, children, onItemSelect, preventClosing, ...attr }) => {

    const [open, setOpen] = useState(false);
    const menuRef = useRef(null);

    const onMenuClick = () => {
        setOpen(!open);
    };

    const outsideClicked = () => {
        if (open) {
            setOpen(false);
        }
    };

    const cls = classNames([cx.base], {
        [cx.active]: open
    });

    useOnClickOutside(menuRef, outsideClicked);

    const itemClicked = (value, index) => {
        onItemSelect && onItemSelect(value, index);
        if (!preventClosing) {
            outsideClicked();
        }
    };

    const renderMenuItems = () => {
        return React.Children.map(children, (item, index) => (
            <OverflowMenuItem key={index} onItemClick={itemClicked} index={index} {...item.props}/>
        ))
    };

    return (
        <div className={cls} ref={menuRef}>
            <Button
                element="button"
                icon={icon}
                onClick={onMenuClick}
                styling="hollow"
                type="button"
                {...attr}
            />
            { open &&
                <div className={cx.menu}>
                    {renderMenuItems()}
                </div>
            }
        </div>
    )
};

OverflowMenu.Item = OverflowMenuItem;

OverflowMenuItem.defaultProps = {
    onItemSelect: () => {},
    preventClosing: false
};

OverflowMenu.propTypes = {
    icon: PropTypes.element.isRequired,
    children: PropTypes.any,
    onItemSelect: PropTypes.func,
    preventClosing: PropTypes.bool
};
