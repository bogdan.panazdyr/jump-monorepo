import React, { useState, useRef } from 'react';
import PropTypes                   from 'prop-types';
import classNames                  from 'classnames';
import InputMask                   from 'react-input-mask';

import { idFromString } from '../helpers';
import { Icons24 }      from '../icons';

import cx from './input.module.scss';

const idType = PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
]);

export const Password = ({ id, name, label, error, className, onChange, ...rest }) => {
    const key = id || idFromString(name || label || rest.placeholder);
    const inputRef = useRef(null);

    const [ controlType, changeControlType ] = useState('password');

    const isPassword = controlType === 'password';

    const onInvert = () => {
        changeControlType(isPassword ? 'text' : 'password');
        inputRef.current.focus();
    };

    const handlerOnChange = (e) => onChange(e.target.value || '');

    const clsName = classNames(cx.base, className, { [ cx.error ]: error });

    return (
        <div className={clsName}>
            {label && <label htmlFor={key} className={cx.label}>{label}</label>}

            <div className={cx.inputWrapper}>
                <input
                    id={key}
                    name={name}
                    ref={inputRef}
                    className={cx.input}
                    autoComplete='on'
                    type={controlType}
                    onChange={handlerOnChange}
                    {...rest}
                />

                {
                    !rest.disabled && (
                        <div className={cx.control} onClick={onInvert}>
                            {
                                isPassword ? <Icons24.IconEyeClose/> : <Icons24.IconEyeOpen/>
                            }
                        </div>
                    )
                }
            </div>

            {error && <span className={cx[ 'error-message' ]}>{error}</span>}
        </div>
    );
}

Password.defaultProps = {
    label: '',
    className: '',
    onChange: () => {},
};

Password.propTypes = {
    id: idType,
    name: PropTypes.string,
    label: PropTypes.string,
    className: PropTypes.string,
    onChange: PropTypes.func,
    error: PropTypes.any
};

export const Input = ({ id, name, label, value, error, className, onChange, isClearable, disabled, mask, readOnly, ...rest }) => {
    const ref = useRef(null);

    const key = id || idFromString(name || label || rest.placeholder);
    const clsName = classNames(cx.base, className, { [ cx.error ]: error });

    const onClear = () => {
        onChange('');
        ref.current.focus()
    };

    const handlerOnChange = (e) => onChange(e.target.value || '');

    return (
        <div className={clsName}>
            {label && <label htmlFor={key} className={cx.label}>{label}</label>}

            <div className={cx.inputWrapper}>
                <InputMask
                    id={key}
                    name={name}
                    mask={mask}
                    className={cx.input}
                    autoComplete='off'
                    onChange={handlerOnChange}
                    value={value}
                    disabled={disabled}
                    readOnly={readOnly}
                    {...rest}
                >
                    {props => <input {...props} ref={ref} disabled={disabled} readOnly={readOnly}
                                     onChange={handlerOnChange} value={value}/>}
                </InputMask>

                {value && !disabled && isClearable &&
                <div className={cx.control} onClick={onClear}>
                    <Icons24.IconClose/>
                </div>
                }
            </div>

            {error && <span className={cx[ 'error-message' ]}>{error}</span>}
        </div>
    );
};

Input.defaultProps = {
    label: '',
    value: '',
    className: '',
    type: 'text',
    onChange: () => {},
    isClearable: true,
    readOnly: false,
};

Input.propTypes = {
    id: idType,
    name: PropTypes.string,
    label: PropTypes.string,
    value: PropTypes.any,
    className: PropTypes.string,
    onChange: PropTypes.func,
    error: PropTypes.any,
    type: PropTypes.string,
    isClearable: PropTypes.bool,
    disabled: PropTypes.bool,
    mask: PropTypes.string,
    readOnly: PropTypes.bool,
};
