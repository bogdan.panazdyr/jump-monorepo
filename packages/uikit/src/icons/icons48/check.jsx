import React from 'react';

export const IconCheck = (props) => {
    return (
        <svg {...props} width='48' height='48' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#26D962' d='M24 4C12.96 4 4 12.96 4 24s8.96 20 20 20 20-8.96 20-20S35.04 4 24 4zm-2.586 28.586a2 2 0 01-2.828 0L11.41 25.41a1.995 1.995 0 012.82-2.824L20 28.34l13.76-13.76a2.001 2.001 0 012.83 2.83L21.415 32.585z'/>
        </svg>
    );
};
