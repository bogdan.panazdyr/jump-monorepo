import React from 'react';

export const IconCross = (props) => {
    return (
        <svg {...props} width='48' height='48' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#EC393D' fillRule='evenodd' clipRule='evenodd' d='M24 44a20 20 0 100-40 20 20 0 000 40zm-5.6-28.4a2 2 0 10-2.8 2.8l5.6 5.6-5.6 5.6a2 2 0 102.8 2.8l5.6-5.6 5.6 5.6a2 2 0 102.8-2.8L26.8 24l5.6-5.6a2 2 0 10-2.8-2.8L24 21.2l-5.6-5.6z'/>
        </svg>
    );
};
