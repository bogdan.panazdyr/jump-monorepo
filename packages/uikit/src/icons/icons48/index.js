import { IconCheck } from './check';
import { IconCross } from './cross';
import { IconDocDownload } from './doc-download';
import { IconWarning } from './warning';

const Icons = {
    IconCheck,
    IconCross,
    IconDocDownload,
    IconWarning,
}

export default Icons;
