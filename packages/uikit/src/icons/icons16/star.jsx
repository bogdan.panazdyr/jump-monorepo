import React from 'react';

export const IconStar = (props) => {
    return (
        <svg {...props} width='16' height='16' fill='none'
             xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' d='M8 1l1.85 4.452 4.806.385-3.661 3.136 1.118 4.69L8 11.15l-4.115 2.513 1.12-4.69-3.662-3.136 4.805-.385L8 1z'/>
        </svg>
    );
};
