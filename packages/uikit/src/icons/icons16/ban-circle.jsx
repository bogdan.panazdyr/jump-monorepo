import React from 'react';

export const IconBanCircle = (props) => {
    return (
        <svg {...props} width='16' height='16' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' d='M5 7a1 1 0 000 2h6a1 1 0 100-2H5z'/>
            <path fill='#333' fillRule='evenodd' clipRule='evenodd' d='M8 0a8 8 0 100 16A8 8 0 008 0zM2 8a6 6 0 1012 0A6 6 0 002 8z'/>
        </svg>
    );
};
