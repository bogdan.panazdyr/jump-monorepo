import React from 'react';

export const IconArrowDown = (props) => {
    return (
        <svg {...props} width='16' height='16' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' fillRule='evenodd' clipRule='evenodd' d='M3.304 5.282a1 1 0 011.414.022L7.9 8.586l3.403-3.304a1 1 0 011.393 1.436l-4.12 4a1 1 0 01-1.415-.022l-3.879-4a1 1 0 01.022-1.414z'/>
        </svg>
    );
};