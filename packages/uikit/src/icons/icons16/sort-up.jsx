import React from 'react';

export const IconSortUp = (props) => {
    return (
        <svg {...props} width='16' height='16' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' fillRule='evenodd' clipRule='evenodd' d='M4.646 10.354a.5.5 0 00.708 0L8 7.707l2.646 2.647a.5.5 0 00.708-.708l-3-3a.5.5 0 00-.708 0l-3 3a.5.5 0 000 .708z'/>
        </svg>
    );
};
