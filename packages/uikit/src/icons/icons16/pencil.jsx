import React from 'react';

export const IconPencil = (props) => {
    return (
        <svg {...props} width='16' height='16' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' fillRule='evenodd' clipRule='evenodd' d='M10.902 3.934l-7.779 7.778-.353 1.768 1.768-.354 7.778-7.778-1.414-1.414zm.707-.707l1.414-1.414 1.414 1.414-1.414 1.414-1.414-1.414z'/>
        </svg>
    );
};