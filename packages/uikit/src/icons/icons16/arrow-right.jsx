import React from 'react';

export const IconArrowRight = (props) => {
    return (
        <svg {...props} width='16' height='16' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' fillRule='evenodd' clipRule='evenodd' d='M6.282 12.696a1 1 0 01.022-1.414L9.586 8.1 6.282 4.696a1 1 0 011.436-1.392l4 4.12a1 1 0 01-.022 1.415l-4 3.879a1 1 0 01-1.414-.022z'/>
        </svg>
    );
};