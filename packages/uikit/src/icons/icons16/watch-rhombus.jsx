import React from 'react';

export const IconWatchRhombus = (props) => {
    return (
        <svg {...props} width='16' height='16' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' fillRule='evenodd' clipRule='evenodd' d='M6.6.6a2 2 0 012.8 0l6 6c.8.8.8 2 0 2.8l-6 6a2 2 0 01-2.8 0l-6-6a2 2 0 010-2.8l6-6zM14 8L8 2 2 8l6 6 6-6z'/>
            <path fill='#333' fillRule='evenodd' clipRule='evenodd' d='M8 4c.6 0 1 .4 1 1v2h1.5a1 1 0 110 2H8a1 1 0 01-1-1V5c0-.6.4-1 1-1z'/>
        </svg>
    );
};
