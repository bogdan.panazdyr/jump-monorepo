import React from 'react';

export const IconReceipt = (props) => {
    return (
        <svg {...props} width='16' height='16' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' fillRule='evenodd' clipRule='evenodd' d='M2 15V2a1 1 0 011-1h10a1 1 0 011 1v13l-2-1-2 1-2-1-2 1-2-1-2 1zm2-9.5a.5.5 0 01.5-.5h7a.5.5 0 010 1h-7a.5.5 0 01-.5-.5zM4.5 7a.5.5 0 000 1h7a.5.5 0 000-1h-7z'/>
        </svg>
    );
};
