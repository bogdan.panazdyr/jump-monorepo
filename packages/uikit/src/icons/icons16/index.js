import { IconArrowDown } from './arrow-down';
import { IconArrowDownSolid } from './arrow-down-solid';
import { IconArrowLeft } from './arrow-left';
import { IconArrowRight } from './arrow-right';
import { IconArrowUp } from './arrow-up';
import { IconBanCircle } from './ban-circle';
import { IconBanCircleFill } from './ban-circle-fill';
import { IconBank } from './bank';
import { IconCheck } from './check';
import { IconCheckCircle } from './check-circle';
import { IconCheckCircleFill } from './check-circle-fill';
import { IconCheckDouble } from './check-double';
import { IconClose } from './close';
import { IconCrossCircle } from './cross-circle';
import { IconCrossCircleFill } from './cross-circle-fill';
import { IconDownload } from './download';
import { IconExternal } from './external';
import { IconInfo } from './info';
import { IconInfoFill } from './info-fill';
import { IconInfoSmall } from './info-small';
import { IconKebab } from './kebab';
import { IconLoader } from './loader';
import { IconLock } from './lock';
import { IconPencil } from './pencil';
import { IconPlus } from './plus';
import { IconReceipt } from './receipt';
import { IconRefresh } from './refresh';
import { IconSettings } from './settings';
import { IconSort } from './sort';
import { IconSortDown } from './sort-down';
import { IconSortUp } from './sort-up';
import { IconStar } from './star';
import { IconWarning } from './warning';
import { IconWatch } from './watch';
import { IconWatchFill } from './watch-fill';
import { IconWatchRhombus } from './watch-rhombus';

const Icons = {
    IconArrowDown,
    IconArrowDownSolid,
    IconArrowLeft,
    IconArrowRight,
    IconArrowUp,
    IconBanCircle,
    IconBanCircleFill,
    IconBank,
    IconCheck,
    IconCheckCircle,
    IconCheckCircleFill,
    IconCheckDouble,
    IconClose,
    IconCrossCircle,
    IconCrossCircleFill,
    IconDownload,
    IconExternal,
    IconInfo,
    IconInfoFill,
    IconInfoSmall,
    IconKebab,
    IconLoader,
    IconLock,
    IconPencil,
    IconPlus,
    IconReceipt,
    IconRefresh,
    IconSettings,
    IconSort,
    IconSortDown,
    IconSortUp,
    IconStar,
    IconWarning,
    IconWatch,
    IconWatchFill,
    IconWatchRhombus,
};

export default Icons;
