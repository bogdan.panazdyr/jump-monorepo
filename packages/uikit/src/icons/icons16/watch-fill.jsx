import React from 'react';

export const IconWatchFill = (props) => {
    return (
        <svg {...props} width='16' height='16' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' fillRule='evenodd' clipRule='evenodd' d='M0 8a8 8 0 1116 0A8 8 0 010 8zm9-4a1 1 0 10-2 0v4a1 1 0 001 1h3a1 1 0 100-2H9V4z'/>
        </svg>
    );
};
