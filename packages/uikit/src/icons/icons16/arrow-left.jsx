import React from 'react';

export const IconArrowLeft = (props) => {
    return (
        <svg {...props} width='16' height='16' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' fillRule='evenodd' clipRule='evenodd' d='M9.718 3.304a1 1 0 01-.022 1.414L6.414 7.9l3.304 3.403a1 1 0 01-1.436 1.393l-4-4.12a1 1 0 01.022-1.415l4-3.879a1 1 0 011.414.022z'/>
        </svg>
    );
};