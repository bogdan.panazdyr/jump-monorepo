import React from 'react';

export const IconBank = (props) => {
    return (
        <svg {...props} width='16' height='16' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' d='M2 4l6-3 6 3v2H2V4zM5 7H3v5h2V7zM9 7H7v5h2V7zM2 15v-2h12v2H2zM13 7h-2v5h2V7z'/>
        </svg>
    );
};
