import React from 'react';

export const IconRefresh = (props) => {
    return (
        <svg {...props} width='16' height='16' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fillRule='evenodd' clipRule='evenodd' fill='#333' d='M6 1a7 7 0 019 7 1 1 0 01-1 1l-3-2a1 1 0 110-1h2a5 5 0 00-9-1 1 1 0 11-2-1l4-3zM1 7h1l3 2a1 1 0 110 1H3a5 5 0 009 1 1 1 0 012 1A7 7 0 011 8V7z'/>
        </svg>
    );
};
