# Иконки 24*24

## Импорт
```javascript
import { Icons24 } from '@justlook/uikit';
```

## Использование
```javascript
const el = () => {
    return <Icons24.IconClose/>
}
```

## Дополнительные атрибуты
В качестве props, омпоненту иконки можно передать атрибуты, которые поддерживает тег ```<svg></svg>``` 

```javascript
const el = () => {
    return <Icons24.IconClose className={'class'}/>
}
```