import React from 'react';

export const IconDownload = (props) => {
    return (
        <svg {...props} width='24' height='24' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' fillRule='evenodd' clipRule='evenodd' d='M12 3a1 1 0 011 1v10.92l3.375-2.7a1 1 0 111.25 1.56l-4.997 3.998a.996.996 0 01-1.26-.004l-4.993-3.993a1 1 0 011.25-1.562L11 14.92V4a1 1 0 011-1zm5 18a1 1 0 100-2H7a1 1 0 100 2h10z'/>
        </svg>
    );
};