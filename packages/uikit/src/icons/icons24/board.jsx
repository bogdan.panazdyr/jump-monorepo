import React from 'react';

export const IconBoard = (props) => {
    return (
        <svg {...props} width='24' height='24' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' fillRule='evenodd' clipRule='evenodd' d='M10 17H5v-3a7 7 0 1114 0V17h-6.056l1.556-7h-1L10 17zm-1 2H4c-.552 0-1-.446-1-.998V14a9 9 0 0118 0v4a1 1 0 01-1 1H9z'/>
        </svg>
    );
};