import React from 'react';

export const IconReloadBack = (props) => {
    return (
        <svg {...props} width='24' height='24' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' d='M7.514 3.336a1 1 0 011.972.328l-.153.919c.915-.39 1.911-.619 2.981-.577A8 8 0 114 12a1 1 0 012 0 6 6 0 106.236-5.995 4.79 4.79 0 00-1.929.34l.564.227a1 1 0 11-.742 1.856l-2.484-.993a1 1 0 01-.63-1.114l.499-2.985z'/>
        </svg>
    );
};
