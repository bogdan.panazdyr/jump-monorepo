import React from 'react';

export const IconInfoFill = (props) => {
    return (
        <svg {...props} width='24' height='24' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' fillRule='evenodd' clipRule='evenodd' d='M12 3a9 9 0 110 18 9 9 0 010-18zm1 8a1 1 0 10-2 0v5a1 1 0 102 0v-5zm0-3a1 1 0 11-2 0 1 1 0 012 0z'/>
        </svg>
    );
};
