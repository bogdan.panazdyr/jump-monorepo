import React from 'react';

import cx from '../animate.module.scss';

export const IconProgress = (props) => {
    return (
        <svg {...props} width='24' height='24' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#DBDDE6' opacity='.3' fillRule='evenodd' clipRule='evenodd' d='M12 4a8 8 0 100 16 8 8 0 000-16zM2 12C2 6.477 6.477 2 12 2s10 4.477 10 10-4.477 10-10 10S2 17.523 2 12z'/>
            <path className={cx.base} fill='#333' fillRule='evenodd' clipRule='evenodd' d='M5.917 17.196A8 8 0 1012 4V2a10 10 0 11-7.604 16.494l1.52-1.298z'/>
        </svg>
    );
};