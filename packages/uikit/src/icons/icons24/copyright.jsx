import React from 'react';

export const IconCopyright = (props) => {
    return (
        <svg {...props} width='24' height='24' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' fillRule='evenodd' clipRule='evenodd' d='M20 12a8 8 0 11-16 0 8 8 0 0116 0zm2 0c0 5.523-4.477 10-10 10S2 17.523 2 12 6.477 2 12 2s10 4.477 10 10zM9 12a3 3 0 015.6-1.5 1 1 0 101.73-1 5 5 0 100 5.001 1 1 0 10-1.73-1.002A3 3 0 019 12z'/>
        </svg>
    );
};