import React from 'react';

export const IconCheckCircle = (props) => {
    return (
        <svg {...props} width='24' height='24' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' fillRule='evenodd' clipRule='evenodd' d='M5 12a7 7 0 1114 0 7 7 0 01-14 0zm7 9a9 9 0 110-18 9 9 0 010 18z'/>
            <path fill='#333' d='M16.768 9.64a1 1 0 10-1.536-1.28l-4.3 5.159-2.225-2.226a1 1 0 00-1.414 1.414l3 3a1 1 0 001.475-.067l5-6z'/>
        </svg>
    );
};
