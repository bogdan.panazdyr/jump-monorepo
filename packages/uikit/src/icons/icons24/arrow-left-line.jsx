import React from 'react';

export const IconArrowLeftLine = (props) => {
    return (
        <svg {...props} width='24' height='24' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' d='M20 12a1 1 0 00-1-1H7.83l4.88-4.88a1 1 0 00-1.415-1.415l-6.588 6.588a1 1 0 000 1.414l6.588 6.588a.997.997 0 001.41-1.41L7.83 13H19a1 1 0 001-1z'/>
        </svg>
    );
};
