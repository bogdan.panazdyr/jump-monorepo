import React from 'react';

export const IconImage = (props) => {
    return (
        <svg {...props} width='24' height='24' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' fillRule='evenodd' clipRule='evenodd' d='M3 5h18v14H3V5zM1 5a2 2 0 012-2h18a2 2 0 012 2v14a2 2 0 01-2 2H3a2 2 0 01-2-2V5zm3 12l4.571-6.429 2.286 2.572L14.286 8 20 17H4z'/>
        </svg>
    );
};