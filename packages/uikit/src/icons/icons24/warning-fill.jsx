import React from 'react';

export const IconWarningFill = (props) => {
    return (
        <svg {...props} width='24' height='24' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' fillRule='evenodd' clipRule='evenodd' d='M12 21a9 9 0 100-18 9 9 0 000 18zm1-8a1 1 0 11-2 0V8a1 1 0 112 0v5zm0 3a1 1 0 10-2 0 1 1 0 002 0z'/>
        </svg>
    );
};
