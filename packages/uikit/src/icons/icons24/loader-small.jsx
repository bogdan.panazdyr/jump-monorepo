import React from 'react';

import cx from '../animate.module.scss';

export const IconLoaderSmall = (props) => {
    return (
        <svg {...props} width='24' height='24' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path className={cx.base} fill='#333' fillRule='evenodd' clipRule='evenodd' d='M14.939 7.955A5 5 0 1017 12h2a7 7 0 11-2.886-5.663L14.94 7.955z'/>
        </svg>
    );
};
