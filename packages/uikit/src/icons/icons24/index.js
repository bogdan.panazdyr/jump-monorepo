import { IconArrowDown } from './arrow-down';
import { IconArrowLeft } from './arrow-left';
import { IconArrowLeftLine } from './arrow-left-line';
import { IconArrowReload } from './arrow-reload';
import { IconArrowRight } from './arrow-right';
import { IconArrowRightLine } from './arrow-right-line';
import { IconArrowTop } from './arrow-top';
import { IconAuto } from './auto';
import { IconBack } from './back';
import { IconBell } from './bell';
import { IconBoard } from './board';
import { IconBox } from './box';
import { IconCalendar } from './calendar';
import { IconChart } from './chart';
import { IconChat } from './chat';
import { IconCheck } from './check';
import { IconCheckCircle } from './check-circle';
import { IconCheckCircleFill } from './check-circle-fill';
import { IconClients } from './clients';
import { IconClose } from './close';
import { IconCopyright } from './copyright';
import { IconCross } from './cross';
import { IconDoc } from './doc';
import { IconDownload } from './download';
import { IconEyeClose } from './eye-close';
import { IconEyeOpen } from './eye-open';
import { IconFlag } from './flag';
import { IconFlasher } from './flasher';
import { IconFly } from './fly';
import { IconForward } from './forward';
import { IconImage } from './image';
import { IconInfo } from './info';
import { IconInfoFill } from './info-fill';
import { IconIntercom } from './intercom';
import { IconKebab } from './kebab';
import { IconKebabVertical } from './kebab-vertical';
import { IconLoader } from './loader';
import { IconLoaderSmall } from './loader-small';
import { IconMenu } from './menu';
import { IconMinus } from './minus';
import { IconNews } from './news';
import { IconPanel } from './panel';
import { IconPencil } from './pencil';
import { IconPlus } from './plus';
import { IconProgress } from './progress';
import { IconReceipt } from './receipt';
import { IconReloadBack } from './reload-back';
import { IconRouble } from './rouble';
import { IconSearch } from './search';
import { IconSetting } from './setting';
import { IconSignature } from './signature';
import { IconSort } from './sort';
import { IconStick } from './stick';
import { IconStore } from './store';
import { IconTrash } from './trash';
import { IconUser } from './user';
import { IconWallet } from './wallet';
import { IconWarning } from './warning';
import { IconWarningFill } from './warning-fill';
import { IconWatch } from './watch';
import { IconWatchRhombus } from './watch-rhombus';

export default {
    IconArrowDown,
    IconArrowLeft,
    IconArrowLeftLine,
    IconArrowReload,
    IconArrowRight,
    IconArrowRightLine,
    IconArrowTop,
    IconAuto,
    IconBack,
    IconBell,
    IconBoard,
    IconBox,
    IconCalendar,
    IconChart,
    IconChat,
    IconCheck,
    IconCheckCircle,
    IconCheckCircleFill,
    IconClients,
    IconClose,
    IconCopyright,
    IconCross,
    IconDoc,
    IconDownload,
    IconEyeClose,
    IconEyeOpen,
    IconFlag,
    IconFlasher,
    IconFly,
    IconForward,
    IconImage,
    IconInfo,
    IconInfoFill,
    IconIntercom,
    IconKebab,
    IconKebabVertical,
    IconLoader,
    IconLoaderSmall,
    IconMenu,
    IconMinus,
    IconNews,
    IconPanel,
    IconPencil,
    IconPlus,
    IconProgress,
    IconReceipt,
    IconReloadBack,
    IconRouble,
    IconSearch,
    IconSetting,
    IconSignature,
    IconSort,
    IconStick,
    IconStore,
    IconTrash,
    IconUser,
    IconWallet,
    IconWarning,
    IconWarningFill,
    IconWatch,
    IconWatchRhombus,
};
