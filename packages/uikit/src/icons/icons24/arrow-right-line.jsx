import React from 'react';

export const IconArrowRightLine = (props) => {
    return (
        <svg {...props} width='24' height='24' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' d='M4 12a1 1 0 001 1h11.17l-4.88 4.88a1 1 0 001.415 1.415l6.588-6.588a1 1 0 000-1.414l-6.588-6.588a.997.997 0 00-1.41 1.41L16.17 11H5a1 1 0 00-1 1z'/>
        </svg>
    );
};
