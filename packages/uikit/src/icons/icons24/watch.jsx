import React from 'react';

export const IconWatch = (props) => {
    return (
        <svg {...props} width='24' height='24' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' fillRule='evenodd' clipRule='evenodd' d='M12 18a6 6 0 110-12 6 6 0 010 12zm-8-6a8 8 0 1116 0 8 8 0 01-16 0zm9-3.5a1 1 0 10-2 0V12a1 1 0 001 1h3a1 1 0 100-2h-2V8.5z'/>
        </svg>
    );
};