import React from 'react';

export const IconKebabVertical = (props) => {
    return (
        <svg {...props} width='24' height='24' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fillRule='evenodd' clipRule='evenodd' fill='#333' d='M12 7a2 2 0 110-4 2 2 0 010 4zm0 7a2 2 0 110-4 2 2 0 010 4zm-2 5a2 2 0 104 0 2 2 0 00-4 0z'/>
        </svg>
    );
};
