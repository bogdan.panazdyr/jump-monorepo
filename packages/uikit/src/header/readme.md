# Заголовок

## Импорт
```javascript
import { Header } from '@justlook/uikit';
```

## Использование

```javascript
const H1 = () => {
    return <Header.H1>Заголовок первого уровня</Header.H1>
}

const H2 = () => {
    return <Header.H2>Заголовок второго уровня</Header.H2>
}

const H3 = () => {
    return <Header.H3>Заголовок третьего уровня</Header.H3>
}
```

## Классы

```javascript
const EL = () => {
    return <Header.H1 className={'class'}>Заголовок первого уровня</Header.H1>
}
```