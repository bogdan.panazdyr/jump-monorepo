import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import cx from './header.module.scss';

const makeHeader = (El) => {
    const component = ({children, className,...attr}) => {
        const clsName = classNames(cx.base, className);

        return <El className={clsName} {...attr}>
            {children}
        </El>
    };

    component.displayName = 'Header.' + El.toUpperCase();
    component.propTypes = {
        className: PropTypes.string,
        children: PropTypes.any
    };
    return component;
};

export const Header = {
    H1: makeHeader('h1'),
    H2: makeHeader('h2'),
    H3: makeHeader('h3'),
};