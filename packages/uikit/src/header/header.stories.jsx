import React from 'react';
import { storiesOf } from '@storybook/react'

import { Header } from './header';

import readme from './readme.md';

storiesOf('Типографика', module)
    .add(
        'Заголовки',
        () => {
            return (
                <div>
                    <Header.H1>Заголовок H1</Header.H1>
                    <Header.H2>Заголовок H2</Header.H2>
                    <Header.H3>Заголовок H3</Header.H3>
                </div>
            )
        },
        {
            info: {
                text: 'Info: Элементы заголовков H1, H2, H3',
            },
            notes: { readme },
        }
    );