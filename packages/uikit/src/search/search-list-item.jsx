import React from 'react';
import PropTypes from 'prop-types';

import cx from './search-list-item.module.scss';

export const SearchListItem = ({ main, description, onClick }) => {

    return (
        <div className={cx.base} onClick={onClick}>
            <div className={cx.main}>{main}</div>
            <div className={cx.desc}>{description}</div>
        </div>
    )
};

SearchListItem.defaultProps = {
    onClick: () => {}
};

SearchListItem.propTypes = {
    main: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    onClick: PropTypes.func
};
