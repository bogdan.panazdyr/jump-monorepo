# Картинка

## Импорт

```javascript
import { Image } from '@justlook/uikit';
```

## Использование

```javascript
const el = () => {
    return <Image 
                size={null}
                src={'https://cdn.iconscout.com/icon/free/png-256/react-2-458175.png'}
                avatar={false}
                round={false}
            />
}
```

В качестве дополнительных `props` можно передать атрибуты, которые поддерживает тег `<img />`
