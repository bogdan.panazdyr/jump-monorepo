import React from 'react';
import PropTypes from 'prop-types';
import classes from 'classnames';

import cx from './image.module.scss';

export const Image = ({avatar, size, round, ...attrs}) => {

    const cls = classes(
        {
            [cx.mini]: size === 'mini',
            [cx.middle]: size === 'middle',
            [cx.big]: size === 'big',
            [cx.avatar]: avatar,
            [cx.round]: round,
        }
    );

    return <img className={cls} alt="" {...attrs}/>
};

Image.defaultProps = {
    avatar: false,
    size: null,
    round: false,
};

Image.propTypes = {
    avatar: PropTypes.bool,
    size: PropTypes.string,
    round: PropTypes.bool
};