import React from 'react';
import PropTypes from 'prop-types';
import classes from 'classnames';

import cx from './itemNotification.module.scss';

export const Notification = ({alert, color, ...rest}) => {
    const cls = classes(cx.base, {
        [cx.alert]: alert,
    });

    return (
        <span
            className={cls}
            style={{background: `${color}`}}
            {...rest}
        />
    )
};

Notification.displayName = 'Item.Notification';

Notification.defaultProps = {
    alert: false,
};

Notification.propTypes = {
    alert: PropTypes.bool,
    color: PropTypes.string,
};