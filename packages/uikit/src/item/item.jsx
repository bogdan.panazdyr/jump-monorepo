import React from 'react';
import PropTypes from 'prop-types';
import classes from 'classnames';
import { Header } from './itemHeader';
import { Description } from './itemDescription';
import { Meta } from './itemMeta';
import { Notification } from './itemNotification';

import cx from './item.module.scss';

export const Item = ({noPadding, children, ...rest}) => {

    const cls = classes(cx.base, {
        [cx.padding]: !noPadding,
    });

    return (
        <div className={cls} {...rest}>
            {children}
        </div>
    )
};

Item.Header = Header;
Item.Description = Description;
Item.Meta= Meta;
Item.Notification = Notification;

Item.displayName = 'Item';

Item.defaultProps = {
    noPadding: false,
};

Item.propTypes = {
    children: PropTypes.any.isRequired,
    noPadding: PropTypes.bool,
};
