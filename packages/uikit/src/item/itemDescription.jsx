import React from 'react';
import PropTypes from 'prop-types';
import { Paragraph } from '../paragraph';

import cx from './itemDescription.module.scss';

export const Description = ({children, ...rest}) => {

    return (
        <div className={cx.base} {...rest}>
            <Paragraph className={cx.paragraph}>{children}</Paragraph>
        </div>
    )
};

Description.displayName = 'Item.Description';

Description.propTypes = {
    children: PropTypes.any.isRequired,
};