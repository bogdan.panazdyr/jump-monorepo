# Параграф

## Импорт

```javascript
import { ParagraphV2 } from '@justlook/uikit';
```

## Использование

```javascript
<ParagraphV2.LH24>Children</ParagraphV2.LH24>
<ParagraphV2.LH18>Children</ParagraphV2.LH18>
```