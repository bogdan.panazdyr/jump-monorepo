import React from 'react';
import { Paragraph } from '../paragraph';
import PropTypes from 'prop-types';

import cx from './link.module.scss';

export const Link = ({to, children, ...attrs}) => {
    return (
        <a href={to} className={cx.base} {...attrs}>
            <Paragraph>{children}</Paragraph>
        </a>
    )  
};

Link.defaultProps = {
    to: ''
};

Link.propTypes = {
    children: PropTypes.any.isRequired,
    to: PropTypes.string.isRequired
};