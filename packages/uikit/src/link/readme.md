# Ссылка

## Импорт
```javascript
import { Link } from '@justlook/uikit';
```

## Использование
```javascript
const el = () => {
    return <Link to={'#'}>Текст ссылки</Link>
}
```

В качестве дополнительных `props` можно передать атрибуты которые поддерживает тег `<a href=''></a>`