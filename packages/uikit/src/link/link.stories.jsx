import React from 'react';
import { storiesOf } from '@storybook/react';
import { text } from '@storybook/addon-knobs';

import { Link } from './index';
import readme from './readme.md';

storiesOf('Компоненты/Ссылка', module)
    .add(
        'Link',
        () => {
            return (
                <Link to={text('Path', '#')}>
                    {text('Children', 'Текст ссылки')}
                </Link>
            )
        }, {
            notes: { readme }
        }
    );