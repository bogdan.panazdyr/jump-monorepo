import React                           from 'react';
import PropTypes                       from 'prop-types';
import { default as ReactSelectAsync } from 'react-select/async';

import { useCustomStyle } from './useSelectStyle';

import cx                 from './select.module.scss';

export const SelectAsync = (
    {
        onChange,
        placeholder,
        error,
        label,
        disabled,
        defaultValue,
        defaultOptions,
        loadOptions,
        ...rest
    }) => {

    const customStyle = useCustomStyle(error, disabled);

    return (
        <div className={cx.content}>
            {label.length > 1 ? <label htmlFor="" className={cx.label}>{label}</label> : null}

            <ReactSelectAsync
                styles={customStyle}
                onChange={
                    (selectedOption) => {
                        onChange(selectedOption.value);
                    }
                }
                placeholder={placeholder}
                isDisabled={disabled}
                defaultValue={defaultValue}
                defaultOptions={defaultOptions}
                loadOptions={loadOptions}
                open
                {...rest}
            />

            {error && <span className={cx[ 'error-message' ]}>{error}</span>}
        </div>
    )
};

SelectAsync.defaultProps = {
    onChange: () => {},
    placeholder: '',
    label: '',
    error: '',
    disabled: false,
};

SelectAsync.propTypes = {
    onChange: PropTypes.func,
    placeholder: PropTypes.string,
    label: PropTypes.string,
    error: PropTypes.any,
    disabled: PropTypes.bool,
    defaultValue: PropTypes.object,
    defaultOptions: PropTypes.oneOfType([ PropTypes.bool, PropTypes.array ]),
    loadOptions: PropTypes.func
};

