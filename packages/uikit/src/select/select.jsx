import React from 'react';
import PropTypes from 'prop-types';
import Select from 'react-select';
import classes from 'classnames';

import { getAlongActiveOption } from './getAlongActiveOption';

import { useCustomStyle, useCustomCompactStyle } from './useSelectStyle';

import cx from './select.module.scss';

export const SelectOption = (
    {
        options,
        onChange,
        placeholder,
        error,
        label,
        disabled,
        compact,
        isClearable,
        prefix,
        isSearchable,
        defaultValue,
        value,
        ...rest
    }) => {

    const option = getAlongActiveOption(options);

    // select должен быть выключен, если:
    // - есть только одна активная опция (больше нельзя ничего выбрать)
    // - выключено программно через props - disabled

    const isDisabled = option || disabled;

    const customStyle = useCustomStyle(error, option || disabled);
    const compactStyle = useCustomCompactStyle();

    const CustomOption = ({ innerProps, isSelected, isDisabled, label, data }) => {
        const cls = classes(cx.compactOption, {
            [cx.active]: isSelected,
            [cx.disabled]: isDisabled,
        })

        return (
            <div {...innerProps} className={cls}>
                <div>{label}</div>
                {Boolean(data.badge) && <span className={cx.badge}>{data.badge}</span>}
            </div>
        )
    };

    CustomOption.propTypes = {
        innerProps: PropTypes.any,
        label: PropTypes.string,
        isSelected: PropTypes.bool,
        isDisabled: PropTypes.bool,
        data: PropTypes.any
    }

    const CustomSingleValue = ({ innerProps, data }) => {
        return (
            <div {...innerProps} className={cx.customSingleValue}>
                <div>{data.label}</div>
                {Boolean(data.badge) && <span className={cx.badge}>{data.badge}</span>}
            </div>
        )
    };

    CustomSingleValue.propTypes = {
        innerProps: PropTypes.any,
        data: PropTypes.any
    }

    const CustomControl = ({ children, innerProps }) => (
        <div className={cx.compactControl} {...innerProps} tabIndex={'0'}>
            <div className={cx.compactLabel}>{prefix}</div>
            {children}
        </div>
    );

    CustomControl.propTypes = {
        children: PropTypes.any,
        innerProps: PropTypes.any,
    }

    return (
        <div className={cx.content}>
            {label.length > 1 ? <label className={cx.label}>{label}</label> : null}

            <Select
                styles={compact ? compactStyle : customStyle}
                value={option || value}
                options={options}
                onChange={onChange}
                placeholder={placeholder}
                isDisabled={isDisabled}
                isClearable={isClearable}
                isSearchable={isSearchable}
                components={compact
                    ? { Option: CustomOption, Control: CustomControl }
                    : { Option: CustomOption, SingleValue: CustomSingleValue }}
                defaultValue={defaultValue}
                {...rest}
            />

            {error && <span className={cx['error-message']}>{error}</span>}
        </div>
    )
};

SelectOption.defaultProps = {
    options: [],
    onChange: () => {},
    placeholder: '',
    label: '',
    error: '',
    disabled: false,
    compact: false,
    isClearable: false,
    isSearchable: true,
    prefix: '',
};

SelectOption.propTypes = {
    options: PropTypes.array,
    onChange: PropTypes.func,
    placeholder: PropTypes.string,
    label: PropTypes.string,
    error: PropTypes.any,
    disabled: PropTypes.bool,
    compact: PropTypes.bool,
    isClearable: PropTypes.bool,
    isSearchable: PropTypes.bool,
    prefix: PropTypes.string,
    defaultValue: PropTypes.object,
    value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.shape({
            value: PropTypes.oneOfType([
                PropTypes.string,
                PropTypes.number,
            ]).isRequired,
            label: PropTypes.oneOfType([
                PropTypes.string,
                PropTypes.number,
            ]).isRequired
        })
    ])
};
