import React from 'react';
import PropTypes from 'prop-types';
import classes from 'classnames';

import { Paragraph } from '../paragraph';
import { Button } from '../button';
import { Icons16 } from '../icons';
import cx from './chips.module.scss';

export const Chips = ({element: Tag, label, onDelete, color, avatar, onClick, ...attrs}) => {
    const cls = classes(cx.base, {
        [cx.color]:color,
        [cx.alert]:color === 'alert',
        [cx.warning]:color === 'warning',
        [cx.success]:color === 'success',
        [cx.primary]:color === 'primary',
        [cx.onDelete]:onDelete,
        [cx.avatar]:avatar,
        [cx.onClick]:onClick,
    });
    return (
        <Tag className={cls} onClick={onClick} {...attrs}>
            {avatar &&
                <div className={cx.status}>
                    {avatar}
                </div>
            }
            <Paragraph>{label}</Paragraph>
            {
                onDelete &&
                <Button icon={<Icons16.IconClose />} styling={'hollow'} onClick={onDelete}/>
            }
        </Tag>
    )
};

Chips.defaultProps = {
    element: 'div',
    label: 'default',
    color: '',
    avatar: null,
    onClick: null,
};

Chips.propTypes = {
    label: PropTypes.string,
    onDelete: PropTypes.func,
    element: PropTypes.string,
    color: PropTypes.string,
    avatar: PropTypes.any,
    onClick: PropTypes.func
};
