import React, { useState, useMemo } from 'react';
import PropTypes from 'prop-types';

import cx from './mob-table.module.scss';
import { Icons16 } from '../icons';
import { TablePopup } from './table-popup';
import { TablePopupContent } from './table-popup-content';
import { renderColumn } from './render-helper';
import { TableHeader } from './table-header';
import { TableHeaderAdd } from './table-header-add';
import { TableHeaderSort } from './table-header-sort';

export const MobTable = ({ columns, data, onRowSelection, sort, onSort, ...rest }) => {

    const mainColumn = useMemo(() => columns.find(info => info.main), [columns]);
    const descColumn = useMemo(() => columns.find(info => info.description), [columns]);

    const [currentRow, setCurrentRow] = useState(null);
    const [popup, setPopup] = useState(false);
    const closePopup = () => {
        setPopup(false);
        setCurrentRow(null);
    };
    const openPopup = (row) => {
        setCurrentRow(row);
        setPopup(true);
    };
    const _onRowSelection = () => {
        onRowSelection(currentRow);
        closePopup();
    };

    const renderRow = (row, index) => {
        const onInfoClicked = () => openPopup(row);

        return (
            <>
                <div className={cx.row} key={index}>
                    <div className={cx.description}>
                        <div className={cx.mainText}>{renderColumn(mainColumn, row)}</div>
                        <div className={cx.descriptionText}>{renderColumn(descColumn, row)}</div>
                    </div>
                    <div className={cx.info} onClick={onInfoClicked}>
                        <Icons16.IconInfo/>
                    </div>
                </div>
            </>
        )
    };

    return (
        <>
            <TableHeader header={rest.header}>
                <TableHeaderSort columns={columns} sort={sort} onSort={onSort}/>
                <TableHeaderAdd onAdd={rest.onAdd}/>
            </TableHeader>
            <div className={cx.base}>
                {data.map(renderRow)} {/* TODO вынести в отдельный компонент: лишние перерисовки */}
                <TablePopup onClose={closePopup}
                            show={popup}
                >
                    { () => <TablePopupContent columns={columns} rowData={currentRow} onRowSelection={_onRowSelection} /> }
                </TablePopup>
            </div>
        </>
    )
};

MobTable.propTypes = {
    columns: PropTypes.array.isRequired,
    data: PropTypes.arrayOf(PropTypes.object).isRequired,
    onRowSelection: PropTypes.func,
    header: PropTypes.string,
    onAdd: PropTypes.func,
    sort: PropTypes.shape({
        by: PropTypes.string,
        desc: PropTypes.bool
    }),
    onSort: PropTypes.func
};
