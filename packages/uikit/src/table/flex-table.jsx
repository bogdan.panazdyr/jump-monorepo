import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { useTable, useResizeColumns, useFlexLayout, useRowSelect } from 'react-table';

import cx from './flex-table.module.scss';
import { Icons16, Icons24 } from '../icons';
import { TableHeader } from './table-header';
import { TableHeaderAdd } from './table-header-add';

export const FlexTable = ({ columns, data, onRowSelection, sort, onSort, ...rest }) => {
    const defaultColumn = {
            minWidth: 30,
            width: 150,
            maxWidth: 200
        };

    const headerProps = (props, { column }) => getStyles(props, column.align);

    const cellProps = (props, { cell }) => getStyles(props, cell.column.align);

    const getStyles = (props, align = 'left') => [
        props,
        {
            style: {
                justifyContent: align === 'right' ? 'flex-end' : 'flex-start',
                alignItems: 'flex-start',
                display: 'flex',
            },
        },
    ];

    const getSortProps = ({ id, sortable }) => () => {
        const by = (sort.by && sort.by !== '' && sort.by === id && sort.desc) ? null : id;
        const desc = by === id ? !sort.desc : null;
        if (sortable) {
            onSort({ by, desc });
        }
    };

    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        rows,
        prepareRow,
    } = useTable(
        {
            columns,
            data,
            defaultColumn,
        },
        useResizeColumns,
        useFlexLayout,
        useRowSelect
    );

    return (
        <>
            <TableHeader header={rest.header}>
                <TableHeaderAdd onAdd={rest.onAdd}/>
            </TableHeader>
            <div {...getTableProps()} className={cx.table}>
                <div className={cx.thead}>
                    {headerGroups.map((headerGroup, index) => (
                        <div
                            key={index}
                            {...headerGroup.getHeaderGroupProps({
                                style: { paddingRight: '0px' },
                            })}
                            className={cx.tr}
                        >
                            {headerGroup.headers.map((column, index) => {
                                return <div key={index}
                                            {...column.getHeaderProps(headerProps)}
                                            className={classNames(cx.th, { [cx.sortable]: column.sortable })}
                                            onClick={getSortProps(column)}>
                                    {column.render('Header')}
                                    {column.canResize && (
                                        <div
                                            {...column.getResizerProps()}
                                            className={classNames([cx.resizer], { [cx.isResizing]: column.isResizing })}
                                        />
                                    )}
                                    <span>
                                        {column.sortable && sort.by === column.id
                                            ? sort.desc
                                                ? <Icons16.IconSortDown/>
                                                : <Icons16.IconSortUp/>
                                            : ''}
                                    </span>
                                </div>
                            })}
                        </div>
                    ))}
                </div>
                <div {...getTableBodyProps()} className={cx.tbody}>
                    {rows.map((row, index) => {
                        prepareRow(row);
                        const _onRowSelection = () => onRowSelection(row.original, row.values);
                        const _onEditClick = e => {
                            e.stopPropagation();
                            onRowSelection(row.original, row.values);
                        };
                        return (
                            <div
                                key={index}
                                {...row.getRowProps()}
                                className={cx.tr}
                                onClick={_onRowSelection}>
                                {row.cells.map((cell, index) => {
                                    return (
                                        <div key={index} {...cell.getCellProps(cellProps)} className={cx.td}>
                                            {cell.render('Cell')}
                                        </div>
                                    )
                                })}
                                <div className={cx.editButton} onClick={_onEditClick}>
                                    <div className={cx.editButtonInner}>
                                        <Icons24.IconPencil/>
                                    </div>
                                </div>
                            </div>
                        )
                    })}
                </div>
            </div>
        </>
    )
};

FlexTable.propTypes = {
    columns: PropTypes.array.isRequired,
    data: PropTypes.arrayOf(PropTypes.object).isRequired,
    onRowSelection: PropTypes.func,
    header: PropTypes.string,
    onAdd: PropTypes.func,
    sort: PropTypes.shape({
        by: PropTypes.string,
        desc: PropTypes.bool
    }),
    onSort: PropTypes.func
};
