import React from 'react';
import PropTypes from 'prop-types';

import { Icons24 } from '../icons';
import { Button } from '../button';

export const TableHeaderAdd = ({ onAdd }) => {
    if(!onAdd) return null;

    return (
        <Button
            element='button'
            icon={<Icons24.IconPlus />}
            onClick={onAdd}
            styling='hollow'
            style={{ padding: 0 }}
        />
    )
};

TableHeaderAdd.propTypes = {
    onAdd: PropTypes.func
};
