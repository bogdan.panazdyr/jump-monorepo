import React from 'react';
import PropTypes from 'prop-types';

import cx from './table-popup-content.module.scss';
import { renderColumn } from './render-helper';
import { Icons24 } from '../icons';
import Button from '../button/button';

export const TablePopupContent = ({ columns, rowData, onRowSelection }) => {

    const mainColumnInfo = columns.find(col => col.main);
    const restColumns = columns.filter(col => !col.main);

    return (
        <div className={cx.base}>
            { mainColumnInfo && <div className={cx.main}>{renderColumn(mainColumnInfo, rowData)}</div> }
            { restColumns.map((columnInfo, index) => (
                <div className={cx.normal} key={index}>
                    {columnInfo.Header}:{' '}
                    {renderColumn(columnInfo, rowData)}
                </div>
            ))}
            { onRowSelection &&
                <div className={cx.edit}>
                    <Button
                        icon={<Icons24.IconPencil/>}
                        element='button'
                        styling='hollow-border'
                        type='button'
                        onClick={onRowSelection}
                        style={{ width: '100%'}}
                    >
                        Редактировать
                    </Button>
                </div>
            }
        </div>
    )
};

TablePopupContent.propTypes = {
    columns: PropTypes.array.isRequired,
    rowData: PropTypes.object.isRequired,
    onRowSelection: PropTypes.func
};
