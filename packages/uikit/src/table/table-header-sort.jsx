import React, { useMemo } from 'react';
import PropTypes from 'prop-types';

import { Icons16, Icons24 } from '../icons';
import { OverflowMenu } from '../overflow-menu';
import { orders } from './order';

export const TableHeaderSort = ({ columns, sort, onSort }) => {

    const sortableColumns = useMemo(() => columns.filter(column => column.sortable), [columns]);

    const items = useMemo(() => {
        const arr = [];
        let index = 0;

        sortableColumns.forEach(column => {
            orders.forEach(order => {
                arr.push(
                    <OverflowMenu.Item key={index} value={{ by: column.accessor, desc: order.desc }}>
                        {column.Header}
                        {' '}
                        <order.icon style={{ verticalAlign: 'middle' }} />
                        {(sort.by === column.accessor && sort.desc === order.desc) &&
                            <>
                                {' '}
                                <Icons16.IconCheck style={{ verticalAlign: 'middle' }}/>
                            </>
                        }
                    </OverflowMenu.Item>
                );
                index += 1;
            });
        });

        return arr;
    }, [sort, sortableColumns]);

    return sortableColumns.length > 0 ? (
        <OverflowMenu
            icon={<Icons24.IconSort/>}
            style={{ padding: 0 }}
            onItemSelect={onSort}
        >
            {items}
            <OverflowMenu.Item value={{ by: null }}>
                не сортировать{' '}
                {(!sort.by || sort.by === '') && <Icons16.IconCheck style={{ verticalAlign: 'middle' }}/>}
            </OverflowMenu.Item>
        </OverflowMenu>
    ) : null
};

TableHeaderSort.defaultProps = {
    columns: [],
    onSort: () => {}
};

TableHeaderSort.propTypes = {
    columns: PropTypes.arrayOf(PropTypes.shape({
        accessor: PropTypes.string,
        Header: PropTypes.string,
        sortable: PropTypes.bool
    })),
    sort: PropTypes.shape({
        by: PropTypes.string,
        desc: PropTypes.bool
    }),
    onSort: PropTypes.func
};
