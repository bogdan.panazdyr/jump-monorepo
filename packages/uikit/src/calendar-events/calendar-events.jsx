import React, { useState, useRef } from 'react';
import PropTypes from 'prop-types';
import { Calendar, dateFnsLocalizer } from 'react-big-calendar';
import { format, parse, startOfWeek, getDay } from 'date-fns';
import ru from 'date-fns/locale/ru';

import { Icons24 } from '../icons';
import { Button } from '../button';
import { declensionWords } from '../helpers';
import { useOnClickOutside, useIfMediaScreen } from '../hooks';

import './custom-style-calendar-events.scss';
import cx from './calendar-events.module.scss';

const Toolbar = ({props: {date, label, onNavigate}, count, declensionNameCount}) => {
    const goToBack = () => {
        let mDate = date;
        let newDate = new Date(
            mDate.getFullYear(),
            mDate.getMonth() - 1,
            1);
        onNavigate('prev', newDate);
    }

    const goToNext = () => {
        let mDate = date;
        let newDate = new Date(
            mDate.getFullYear(),
            mDate.getMonth() + 1,
            1);
        onNavigate('next', newDate);
    }

    return (
        <div className={cx.toolbar}>
            <Icons24.IconArrowLeft onClick={() => goToBack()} className={cx.control} />
            <div className={cx['toolbar-label']}>{label}</div>
            <Icons24.IconArrowRight onClick={() => goToNext()} className={cx.control} />
            {count > 0 && <div className={cx.count}>{count} {declensionWords(count, declensionNameCount)}</div>}
        </div>
    )
};

const eventStyleGetter = ({status}) => {
    const closeBackgroundColor = status.name === 'close' ? '#E7EAEE' : '#FFDD99';
    const closeBorderColor = status.name === 'close' ? '#E7EAEE' : '#FFCC66';
    const style = {
        backgroundColor: closeBackgroundColor,
        borderBottom: '0.0625rem solid ' + closeBorderColor
    }

    return { style }
};

export const CalendarEvents = ({
        events,
        renderPopup,
        declensionNameCount,
        onClickByEvent,
        createEventControl,
    }) => {

    const [selectedEvent, selectEvent] = useState(null);
    const isMedia = useIfMediaScreen();
    const ref = useRef(null);

    const locales = { 'ru-RU': ru }
    const localizer = dateFnsLocalizer({
        format,
        parse,
        startOfWeek,
        getDay,
        locales,
    })

    const formatWeekDay = {
        weekdayFormat: (date, culture, localizer) =>
            localizer.format(
                date,
                isMedia ? 'EEEEEE' : 'EEEE',
                culture
            )
    }

    const onSelectEvent = (e) => {
        selectEvent(e);
        onClickByEvent(e);
    }

    const onClickOutside = () => { selectEvent(null) }
    useOnClickOutside(ref, onClickOutside)

    return (
        <div className={cx.base} ref={ref}>
            {createEventControl &&
                <div className={cx['event-control']}>
                    {createEventControl}
                </div>
            }

            <Calendar
                localizer={localizer}
                culture='ru-RU'
                formats={formatWeekDay}
                eventPropGetter={(eventStyleGetter)}
                events={events}
                onNavigate={onClickOutside}
                onSelectEvent={onSelectEvent}
                onSelectSlot={onClickOutside}
                selectable={true}
                components={{
                    toolbar: (props) =>
                        <Toolbar
                            props={props}
                            /* eslint-disable-next-line react/prop-types */
                            count={events.length}
                            declensionNameCount={declensionNameCount}
                        />
                }}
            />
            { selectedEvent && (
                <div className={cx.popup}>
                    {isMedia && <div className={cx.shadow}/>}
                    <div className={cx['popup-content']} >
                        {isMedia &&
                            <Button
                                icon={<Icons24.IconClose/>}
                                styling='hollow'
                                className={cx['popup-close']}
                                onClick={() => selectEvent(null)}
                            />
                        }
                        { renderPopup(selectedEvent) }
                    </div>
                </div>
            )}
        </div>
    )
}

CalendarEvents.defaultProps = {
    onClickByEvent: () => {}
}

CalendarEvents.propTypes = {
    events: PropTypes.array.isRequired,
    renderPopup: PropTypes.func,
    createEventControl: PropTypes.node,
    onClickByEvent: PropTypes.func,
    declensionNameCount: PropTypes.array,
}

Toolbar.propTypes = {
    props: PropTypes.object,
    count: PropTypes.number,
    declensionNameCount: PropTypes.array,
}
