# Календарь событий

## Импорт
```javascript
import { CalendarEvents } from '@justlook/uikit';
```

## Использование

```javascript
<CalendarEvents
    events={shifts}
    createEventControl={
        <ExampleRenderControl
            renderControl={(open) => (
                isMedia
                    ? <Button onClick={open} icon={<Icons24.IconPlus/>}/>
                    : <Button onClick={open}>Открыть смену</Button>
            )}
        />
    }
    declensionNameCount={['смена', 'смены', 'смен']}
    renderPopup={(shift) => {}}
/>
```

##Props
### ***events*** 
Массив объектов с данными. 
<br/>
<br/>
type: `array` <br/>
*required*

### ***createEventControl*** 
Контрол в правой части `Toolbar` 
<br/>
<br/>
type: `node` <br/>

### ***declensionNameCount*** 
Массив, содержащий название событий для отображения их количества в верхнем `Toolbar`.
`[ 'ед.число', 'мн.число', 'род.падеж мн.число' ]` 
<br/>
<br/>
type: `array` <br/>

### ***renderPopup*** 
Обратный вызов срабатывает при клике на событие.
```javascript
(shift: object) => any
```

type: `func` <br/>

