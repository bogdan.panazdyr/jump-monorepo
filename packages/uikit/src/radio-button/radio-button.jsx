import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import { idFromString } from '../helpers';
import { useTabListener } from '../hooks';

import cx from './radio-button.module.scss';

export const RadioButton = (props) => {
    const { focusedByTab } = useTabListener();

    const {label, id, className, onChange, error, ...attr} = props;
    const key = id || idFromString(label);
    const clsName = classNames(cx.base, className, { [cx.error]: error });
    const clsNameInput = classNames(cx.inputRadio, {
        [cx.focus]: focusedByTab,
    })
    const handleChange = (e) => onChange(props.value, props.name, e);

    return (
        <div className={clsName}>
            <input type="radio"
                   className={clsNameInput}
                   id={key}
                   onChange={handleChange}
                   {...attr}
            />

            <label htmlFor={key} className={cx.labelRadio}>
                {label}
            </label>
        </div>
    )
};

RadioButton.defaultProps = {
    onChange: () => {},
    value: '',
    id: null,
    className: '',
    error: false
};

RadioButton.propTypes = {
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    onChange: PropTypes.func,
    name: PropTypes.string,
    id: PropTypes.string,
    className: PropTypes.string,
    disabled: PropTypes.bool,
    checked: PropTypes.bool,
    defaultChecked: PropTypes.bool,
    error: PropTypes.bool,
    label: PropTypes.string,
};
