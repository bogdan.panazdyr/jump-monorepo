import React from 'react';
import PropTypes from 'prop-types';
import {Header as BaseHeader} from '../header';

import cx from './cardHeader.module.scss';

export const Header = ({children, ...attrs}) => {
    return (
        <div className={cx.base}>
            <BaseHeader.H3 {...attrs}>{children}</BaseHeader.H3>
        </div>
    )
};

Header.displayName = 'Card.Header';

Header.propTypes = {
    children: PropTypes.any.isRequired,
};