import React from 'react';
import { storiesOf } from '@storybook/react';
import { boolean, text } from '@storybook/addon-knobs';

import { Card } from './index';
import { BigHeader } from '../big-header';
import { Image } from '../image';
import readme from './readme.md';

storiesOf('Компоненты/Карточка', module)
    .add(
        'Card',
        () => {
            return (
                <Card noPadding={boolean('Выключить отступы', false)}>
                    <Card.Header>{text('Header', 'Lorem ipsum dolor sit amet.')}</Card.Header>
                    <Card.Body style={{display: 'flex', justifyContent: 'space-between'}} >
                        <BigHeader>{text('Count', '320')}</BigHeader>
                        <Image
                            size={'big'}
                            src={'https://static.tildacdn.com/tild3163-3531-4134-a535-326236333262/logo_450.png'}
                            round
                        />
                    </Card.Body>
                </Card>
            )
        }, {
            notes: { readme }
        }
    );