import React from 'react';
import { storiesOf } from '@storybook/react';
import { text } from '@storybook/addon-knobs';

import { Paragraph } from './index';
import readme from './readme.md';

storiesOf('Типографика', module)
    .add(
        'Параграф',
        () => {
            return (
                <Paragraph>{text('Children', 'Параграф')}</Paragraph>
            )
        }, {
            notes: { readme }
        }
    );