import React, { Component } from 'react';
import PropTypes from 'prop-types';
import DayPicker from 'react-day-picker';
import classes from 'classnames';
import enhanceWithClickOutside from 'react-click-outside';
import 'react-day-picker/lib/style.css';

import { Input } from '../input';
import {Button} from '../button';
import {Icons24} from '../icons';
import { idFromString, formatDate } from '../helpers';
import './custom-style--calendar.scss';
import cx from './calendar.module.scss';

const WEEKDAYS_SHORT = ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'];

const MONTHS = [
    'Январь',
    'Февраль',
    'Март',
    'Апрель',
    'Май',
    'Июнь',
    'Июль',
    'Август',
    'Сентябрь',
    'Октябрь',
    'Ноябрь',
    'Декабрь',
];

const WEEKDAYS_LONG = [
    'Воскресенье',
    'Понедельник',
    'Вторник',
    'Среда',
    'Четверг',
    'Пятница',
    'Суббота',
];

const Navbar = ({onPreviousClick, onNextClick, className}) => {
    return (
        <div className={className}>
            <Button
                styling={'hollow'}
                icon={<Icons24.IconArrowLeft />}
                onClick={() => onPreviousClick()}
                className={cx.prevBtn}
            />
            <Button
                styling={'hollow'}
                icon={<Icons24.IconArrowRight />}
                onClick={() => onNextClick()}
                className={cx.nextBtn}
            />
        </div>
    )
};

class Calendar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedDay: formatDate(this.props.selectedDay),
            isVisible: false,
            focus: false,
        }
    }

    handleClickOutside = () => {
        const { selectedDay } = this.state;
        const { onChange } = this.props;
        this.setState({
            focus: false,
        });
        onChange(selectedDay);
    };

    handleDayClick = (day, { selected }) => {
        const { onChange } = this.props;
        this.setState({
            selectedDay: selected ? undefined : day,
            focus: false,
        });
        onChange(day)
    };

    handleSelectedDay = (value) => {
        this.setState({
            selectedDay: formatDate(value)
        })
    };

    render() {
        const { selectedDay, focus } = this.state;
        const { label, placeholder, id, labelPosition } = this.props;
        const key = id || idFromString(label) || idFromString(placeholder);
        const cls = classes(cx.input, {
            [cx.focus]:focus
        });
        const clsWrapper = classes(cx.wrapper, {
            [cx.grid]:labelPosition === 'left'
        });
        const positionBase = classes(cx.overlay, {
            [cx.normal]:!label,
            [cx.left]:labelPosition === 'left',
        });

        return (
            <div className={clsWrapper}>
                {label.length > 1 ? <label htmlFor={key} className={cx.label}>{label}</label> : '' }
                <div className={cls}>
                    <Input
                        id={key}
                        value={selectedDay && selectedDay.toLocaleDateString('ru-RU')}
                        className={cx.field}
                        placeholder={placeholder}
                        onFocus={() => this.setState({focus: true})}
                        onChange={this.handleSelectedDay}
                    />
                    <Icons24.IconCalendar />
                </div>
                {
                    focus &&
                        <div className={positionBase}>
                            <DayPicker
                                className={'input'}
                                selectedDays={selectedDay}
                                onDayClick={this.handleDayClick}
                                month={selectedDay}
                                months={MONTHS}
                                weekdaysLong={WEEKDAYS_LONG}
                                weekdaysShort={WEEKDAYS_SHORT}
                                firstDayOfWeek={1}
                                navbarElement={<Navbar />}
                            />
                        </div>
                }
            </div>
        )
    }
}

Calendar.defaultProps = {
    label: '',
    placeholder: '',
    onChange: () => {},
    labelPosition: 'top',
    id: null,
    selectedDay: '',
};

Calendar.propTypes = {
    id: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]),
    label: PropTypes.string,
    placeholder: PropTypes.string,
    onChange: PropTypes.func.isRequired,
    labelPosition: PropTypes.string,
    selectedDay: PropTypes.string,
};

Navbar.propTypes = {
    onPreviousClick: PropTypes.func,
    onNextClick: PropTypes.func,
    className: PropTypes.string,
}

export default enhanceWithClickOutside(Calendar);
