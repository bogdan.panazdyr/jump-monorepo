# Календарь

## Импорт
````javascript
import { Calendar } from '@justlook/uikit';
````

## Использование
```javascript
const el = () => {  
    return (
        <Calendar
            label={'Дата'}
            placeholder={'Выберите дату'}
            onChange={(value) => action(value)}
            labelPosition={'left'}
            selectedDay={'01.03.2013'}
        />
    )   
}
```

## Props

- **label**: текст тега label
- **placeholder**: текст placeholder 
- **onChange**: функция возвращает объект `{startDate:'', endDate:''}`
- **labelPosition**: позиция label, есть 2 значения `top` и `left`
- **selectedDay**: активная дата в формате `dd.mm.yyyy`