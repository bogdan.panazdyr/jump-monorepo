# Календарь

## Импорт
````javascript
import { NewCalendar } from '@justlook/uikit';
````

## Использование
```javascript
<NewCalendar
    label='Календарь'
    placeholder='Выберите дату'
    onChange={action()}
/>
```

## Props

- **label**: текст тега label
- **id**: id поля  
- **placeholder**: текст placeholder 
- **onChange**: функция возвращает выбранную дату в виде `Fri Apr 03 2020 00:00:00 GMT+0700 (Новосибирск, стандартное время)`
- **labelPosition**: позиция label
- **selectDay**: активная дата в формате `dd.mm.yyyy`
- **error**: ошибка поля