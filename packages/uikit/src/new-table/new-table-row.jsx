import React from 'react';
import PropTypes from 'prop-types';

import cx from './new-table-row.module.scss';

export const TableRow = ({children, ...rest}) => {
    return (
        <div className={cx.base} {...rest}>
            {children}
        </div>
    )
}

TableRow.defaultProps = {
    children: null,
}

TableRow.propTypes = {
    children: PropTypes.any,
}