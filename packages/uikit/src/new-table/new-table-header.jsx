import React, { Fragment }  from 'react';
import PropTypes            from 'prop-types';
import { Description }      from '../description';
import { TableCell }        from './new-table-cell';
import { Icons16, Icons24 } from '../icons';
import { Header }           from '../header';
import { Button }           from '../button';
import { useIfMediaScreen } from '../hooks';

import { TablePopupSort } from './new-table-popup-sort';

import cx from './new-table-header.module.scss';

export const TableHeader = ({ headerCell, onSort, sortField, title, onAdd }) => {
    const sign = sortField.slice(0, 1);
    const direction = sign.search(/[+-]/) === 0 ? sign : '+';
    const field = sign.search(/[+-]/) === 0 ? sortField.slice(1) : sortField;

    const onClickSort = (selectedField) => {
        if (!selectedField) {
            return;
        }

        const nextDirection = selectedField === field
            ? (direction === '+' ? '-' : '+')
            : '+';

        onSort(`${nextDirection}${selectedField}`)
    }

    return (
        <Fragment>
            <div className={cx.titleWrap}>
                {title && <Header.H2 className={cx.title}>{title}</Header.H2>}

                <div className={cx.buttons}>
                    {useIfMediaScreen() && onSort &&
                    <TablePopupSort
                        renderProps={(open) => (
                            <Button styling='hollow' icon={<Icons24.IconSort/>} onClick={open}/>
                        )}
                        headerCell={headerCell}
                        sortField={sortField}
                        field={field}
                        onClickSort={onClickSort}
                    />
                    }
                    {onAdd &&
                    <Button styling='hollow' icon={<Icons24.IconPlus/>} onClick={() => onAdd()}/>
                    }
                </div>
            </div>

            <div
                className={cx.base}
                style={{ gridTemplateColumns: `repeat(${headerCell.length}, 1fr)` }}
            >
                {headerCell.map((item, index) => (
                    <TableCell key={index}>
                        <Description.LH24
                            element='span'
                            className={`
                                ${cx.itemHeader}
                                ${item?.sort && cx.hover}
                                ${item?.sort === field && cx.active}
                            `}
                            onClick={() => onClickSort(item?.sort)}
                        >
                            {item.label}

                            {item?.sort === field
                                ? direction === '+'
                                    ? <Icons16.IconSortDown/>
                                    : <Icons16.IconSortUp/>
                                : null
                            }
                        </Description.LH24>
                    </TableCell>
                ))}
            </div>
        </Fragment>
    )
}

TableHeader.defaultProps = {
    headerCell: [],
    onSort: () => {},
}

TableHeader.propTypes = {
    onSort: PropTypes.any,
    sortField: PropTypes.string,
    headerCell: PropTypes.array,
    title: PropTypes.string,
    onAdd: PropTypes.oneOfType([
        PropTypes.func,
        PropTypes.bool,
    ]),
}
