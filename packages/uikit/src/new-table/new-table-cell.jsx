import React from 'react';
import PropTypes from 'prop-types';

import { Paragraph } from '../paragraph';

import cx from './new-table-cell.module.scss';

export const TableCell = ({children, ...rest}) => {
    return (
        <div className={cx.base} {...rest}>
            <Paragraph className={cx.paragraph}>{children}</Paragraph>
        </div>
    )
}

TableCell.defaultProps = {
    children: null,
}

TableCell.propTypes = {
    children: PropTypes.any,
}