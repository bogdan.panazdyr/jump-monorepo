import { NewTable as Table } from './new-table';
import { TableBody } from './new-table-body';
import { TableCell } from './new-table-cell';
import { TableHeader } from './new-table-header';
import { TableRow } from './new-table-row';

export const TableV2 = Table;
export const NewTable = {
    Table,
    TableBody,
    TableCell,
    TableHeader,
    TableRow
}