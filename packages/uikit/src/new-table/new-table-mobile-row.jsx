import React from 'react';
import PropTypes from 'prop-types';

import cx from './new-table-mobile-row.module.scss';

export const TableMobileRow = ({children, ...rest}) => {
    return (
        <div className={cx.base} {...rest}>
            {children}
        </div>
    )
}

TableMobileRow.propTypes = {
    children: PropTypes.any
}