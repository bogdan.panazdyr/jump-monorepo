import React from 'react';
import PropTypes from 'prop-types';
import classes from 'classnames';

import cx from './popupContent.module.scss';

export const Content = React.forwardRef(({ children, className, ...attrs }, ref) => {
    const cls = classes(cx.base, className);

    return(
        <div data-module='content'>
            <div className={cls} ref={ref} {...attrs}>
                {children}
            </div>
        </div>
    )
});

Content.displayName = 'Popup.Content';

Content.propTypes = {
    children: PropTypes.any.isRequired,
    className: PropTypes.string,
};
