# Модально окно

## Импорт
```javascript
import { Popup } from '@justlook/uikit';
```

## Использование
```javascript
    const [showPopup, setShowPopup] = useState(false);
    return(
        <div>
            <Button onClick={() => setShowPopup(!showPopup)}>Открыть Popup</Button>
            {
                showPopup && 
                    <Popup onDismiss={() => setShowPopup(!showPopup)}>
                        <Popup.Header description='Description'>
                            Header
                        </Popup.Header>
            
                        <Popup.Content>
                            Content
                        </Popup.Content>
            
                        <Popup.Footer>
                            <Button styling='primary' type='submit'>Создать</Button>
                            <Button styling='hollow-border' onClick={() => setShowPopup(!showPopup)}>Отменить</Button>
                        </Popup.Footer>
                    </Popup>
            }
        </div>
    )
```

```javascript
    const [showPopup, setShowPopup] = useState(false);
    return (
        <div>
            <Button onClick={() => setShowPopup(!showPopup)}>Открыть Popup</Button>
            {
                showPopup &&
                <Popup
                    onDismiss={() => setShowPopup(!showPopup)}
                >
                    <Popup.Header description={text('Description', 'Description')}>
                        {text('Header', 'Header')}
                    </Popup.Header>

                    <Popup.Content>
                        Content
                    </Popup.Content>

                    <Popup.Footer
                        onDismissButton={<Button styling='hollow-border' onClick={() => setShowPopup(!showPopup)}>Отменить</Button>}
                        majorButton={<Button styling='primary'>Создать</Button>}
                        minorButton={<Button styling='hollow-border' disabled>Удалить</Button>}
                    />
                </Popup>
            }
        </div>
    )
```
 