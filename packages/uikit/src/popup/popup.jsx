import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import classes from 'classnames';

import { Button } from '../button';
import { Icons24 } from '../icons';

import { Header } from './popupHeader';
import { Content } from './popupContent';
import { Footer } from './popupFooter';

import { useResizeObserver, useWindowSize } from '../hooks';

import cx from './popup.module.scss';

export const Popup = ({children, onDismiss, className, ...attrs}) => {
    const contentRef = useRef(null);

    const { height: windowHeight } = useWindowSize();

    const { height } = useResizeObserver(contentRef);

    const cls = classes(cx.window, className, {
        [cx.fullHeight]: height + 146 > windowHeight
    });

    const cloneContent = (child) => {
        if(child.type.displayName === 'Popup.Content') {
            return React.cloneElement(child, { ref: node => contentRef.current = node })
        }

        return child;
    }

    return(
        <div className={cx.base}>
            <div className={cx.shadow} onClick={onDismiss}/>
            <div className={cls} {...attrs}>
                {onDismiss &&
                    <Button
                        styling='hollow'
                        icon={<Icons24.IconClose />}
                        className={cx.close}
                        onClick={() => onDismiss()}
                    />
                }
                {React.Children.map(children, cloneContent)}
            </div>
        </div>
    )
};

Popup.Header = Header;
Popup.Content = Content;
Popup.Footer = Footer;

Popup.propTypes = {
    children: PropTypes.any.isRequired,
    onDismiss: PropTypes.func,
    className: PropTypes.string,
};
