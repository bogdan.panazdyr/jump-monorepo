import React from 'react';
import PropTypes from 'prop-types';
import classes from 'classnames';

import { Description } from '../description';
import { Header as BaseHeader } from '../header';
import cx from './popupHeader.module.scss';

export const Header = ({children, label, description, className, ...attrs}) => {

    const style = {
        gridRow: description ? '1/2' : '1/3'
    };

    const cls = classes(cx.base, {
        [className]:className,
    });

    return(
        <div className={cls} {...attrs}>
            <BaseHeader.H1 style={style} className={cx['header']}>
                {label || children}
            </BaseHeader.H1>
            {description &&
                <Description.LH24 className={cx['description']}>{description}</Description.LH24>
            }
        </div>
    )
};

Header.displayName = 'Popup.Header';

Header.propTypes = {
    children: PropTypes.any,
    label: PropTypes.string,
    description: PropTypes.string,
    className: PropTypes.string,
};
