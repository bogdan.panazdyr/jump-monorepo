import React, { useState } from 'react';
import { storiesOf } from '@storybook/react';
import { text } from '@storybook/addon-knobs';

import { Popup } from './index';
import { Button } from '../button';
import cx from '../story.module.scss';
import readme from './readme.md';

storiesOf('Компоненты/Модальное окно', module)
    .add(
        'Popup',
        () => {
            const [showPopup, setShowPopup] = useState(false);

            return (
                <div className={cx.position}>
                    <Button onClick={() => setShowPopup(!showPopup)}>Открыть Popup</Button>
                    {
                        showPopup &&
                            <Popup
                                onDismiss={() => setShowPopup(!showPopup)}
                            >
                                <Popup.Header description={text('Description', 'Description')}>
                                    {text('Header', 'Header')}
                                </Popup.Header>

                                <Popup.Content>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                    Deleniti dolorum et officiis perspiciatis qui rem tempora unde.
                                    Dignissimos doloremque, nulla. Ducimus explicabo facilis ipsum nihil nulla,
                                    obcaecati omnis optio quos repellendus sed? Adipisci dignissimos, error est eveniet
                                    fugiat impedit in iste neque, quaerat recusandae repellendus sed? Accusamus
                                    accusantium ad debitis dolorum hic ipsa ipsum magnam nihil quibusdam quos, tempore
                                    temporibus, veniam voluptate? Accusantium aperiam culpa cumque deserunt dolore
                                    dolorum eos est expedita explicabo labore laboriosam maxime molestias nihil non
                                    pariatur quaerat ratione reiciendis, rem, sit veniam veritatis vitae voluptatibus?
                                    At, autem esse excepturi exercitationem optio quisquam rem suscipit
                                    tenetur voluptatem. Accusamus, harum maiores minima nam neque perferendis quas
                                    quidem voluptate! Dignissimos dolorum perspiciatis quia quis.
                                    Animi doloribus eos maiores nam officia officiis porro sapiente similique voluptate,
                                    voluptatem! Beatae debitis distinctio dolore facere ipsa magni maiores nihil,
                                    nisi praesentium repudiandae saepe voluptatum. Consequatur, cum, exercitationem.
                                    Corporis cum dignissimos maiores reprehenderit tempore.
                                </Popup.Content>

                                <Popup.Footer>
                                    <Button styling='hollow-border' onClick={() => setShowPopup(!showPopup)}>Отмена</Button>
                                    <Button styling='primary'>Создать</Button>
                                </Popup.Footer>
                            </Popup>
                    }
                </div>
            )
        }, {
            notes: { readme }
        }
    ).add(
        'Popup - Minor Control Button',
        () => {
            const [showPopup, setShowPopup] = useState(false);

            return (
                <div className={cx.position}>
                    <Button onClick={() => setShowPopup(!showPopup)}>Открыть Popup</Button>
                    {
                        showPopup &&
                        <Popup
                            onDismiss={() => setShowPopup(!showPopup)}
                        >
                            <Popup.Header description={text('Description', 'Description')}>
                                {text('Header', 'Header')}
                            </Popup.Header>

                            <Popup.Content>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                Deleniti dolorum et officiis perspiciatis qui rem tempora unde.
                                Dignissimos doloremque, nulla. Ducimus explicabo facilis ipsum nihil nulla,
                                obcaecati omnis optio quos repellendus sed? Adipisci dignissimos, error est eveniet
                                fugiat impedit in iste neque, quaerat recusandae repellendus sed? Accusamus
                                accusantium ad debitis dolorum hic ipsa ipsum magnam nihil quibusdam quos, tempore
                                temporibus, veniam voluptate? Accusantium aperiam culpa cumque deserunt dolore
                                dolorum eos est expedita explicabo labore laboriosam maxime molestias nihil non
                                pariatur quaerat ratione reiciendis, rem, sit veniam veritatis vitae voluptatibus?
                                At, autem esse excepturi exercitationem optio quisquam rem suscipit
                                tenetur voluptatem. Accusamus, harum maiores minima nam neque perferendis quas
                                quidem voluptate! Dignissimos dolorum perspiciatis quia quis.
                                Animi doloribus eos maiores nam officia officiis porro sapiente similique voluptate,
                                voluptatem! Beatae debitis distinctio dolore facere ipsa magni maiores nihil,
                                nisi praesentium repudiandae saepe voluptatum. Consequatur, cum, exercitationem.
                                Corporis cum dignissimos maiores reprehenderit tempore.
                            </Popup.Content>

                            <Popup.Footer
                                onDismissButton={<Button styling='hollow-border' onClick={() => setShowPopup(!showPopup)}>Отменить</Button>}
                                majorButton={<Button styling='primary'>Создать</Button>}
                                minorButton={<Button styling='hollow-border' disabled>Удалить</Button>}
                            />
                        </Popup>
                    }
                </div>
            )
        }, {
            notes: { readme }
        }
    );
