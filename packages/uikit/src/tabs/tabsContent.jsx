import PropTypes from 'prop-types';

export const Content = ({children}) => {
    return children
};

Content.displayName = 'Tab.Content';

Content.defaultProps = {
    activeTab: '',
    disabled: false,
};

Content.propTypes = {
    label: PropTypes.string.isRequired,
    value: PropTypes.any,
    children: PropTypes.any.isRequired,
    activeTab: PropTypes.string,
    disabled: PropTypes.bool,
    notification: PropTypes.any,
};
