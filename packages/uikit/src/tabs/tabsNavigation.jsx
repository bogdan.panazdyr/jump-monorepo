import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Button } from '../button';

import cx from './tabsNavigation.module.scss';

export const Navigation = ({navLabel, value, className, onChangeActiveTab, disabled, notification, isActive}) => {

    const classes = classNames(
        cx.base,
        {[cx.active]: isActive},
        className
    );

    return (
        <div className={cx.wrapper}>
            <Button
                styling={'hollow'}
                className={classes}
                onClick={() => {onChangeActiveTab(navLabel, value)}}
                disabled={disabled}
            >
                {navLabel}
                {notification}
            </Button>
            <div className={cx.focus} />
        </div>

    )
};

Navigation.defaultProps = {
    navLabel: 'Tab',
    className: '',
    onChangeActiveTab: () => {}
};

Navigation.propTypes = {
    navLabel: PropTypes.string,
    value: PropTypes.any,
    className: PropTypes.string,
    onChangeActiveTab: PropTypes.func,
    disabled: PropTypes.bool,
    notification: PropTypes.any,
    isActive: PropTypes.bool,
};

